package com.lynx.evoucher.models;

/**
 * Created by Administrator on 8/29/2016.
 */

import com.orm.SugarRecord;
import com.orm.dsl.Table;

@Table(name = "colors")
public class Colors extends SugarRecord {

    private Long id;
    private String colorcode;
    private String imageid;
    private String colorno;
    private String name;



    public Colors() {

    }

    public Colors(Long id, String colorcode, String name) {
        this.id = id;

        this.name = name;
        this.colorcode = colorcode;
    }

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    public String getImageid() {
        return imageid;
    }

    public void setImageid(String imageid) {
        this.imageid = imageid;
    }

    public String getColorcode() {
        return colorcode;
    }

    public String getColorno() {
        return colorno;
    }

    public void setColorno(String colorno) {
        this.colorno = colorno;
    }

    public void setColorcode(String colorcode) {
        this.colorcode = colorcode;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
