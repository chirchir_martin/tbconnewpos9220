package com.lynx.evoucher.models;

import com.orm.SugarRecord;

/**
 * Created by Administrator on 8/29/2016.
 */
public class Users extends SugarRecord {
    private Long id;
    private String username;
    private String password;
    private String usersid;
    private String level;
    private String locationid;
    private String isloggedin;


    public Users() {

    }

    public Users(Long id, String username, String password, String usersid,String lev) {
        this.level=lev;
        this.id = id;
        this.username = username;
        this.password = password;
        this.usersid = usersid;
    }

    public String getUsersid() {
        return usersid;
    }

    public void setUsersid(String usersid) {
        this.usersid = usersid;
    }

    public String getUsername() {
        return this.username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getIsloggedin() {
        return isloggedin;
    }

    public void setIsloggedin(String isloggedin) {
        this.isloggedin = isloggedin;
    }

    public String getPassword() {
        return password;
    }

    public String getLocationid() {
        return locationid;
    }

    public void setLocationid(String locationid) {
        this.locationid = locationid;
    }

    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }

    public void setPassword(String password) {
        this.password = password;
    }

}
