package com.lynx.evoucher.models;

import com.orm.SugarRecord;
import com.orm.dsl.Table;

/**
 * Created by Kibet on 9/6/2016.
 */

@Table(name = "bio_images")
public class FingerPrintVO extends SugarRecord {

    private Long id;
    private String beneficiary_id;
    private String image;


    public FingerPrintVO() {
    }

    public FingerPrintVO(Long id, String beneficiary_id, String image) {
        this.id = id;
        this.beneficiary_id = beneficiary_id;
        this.image = image;
    }

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    public String getBeneficiary_id() {
        return beneficiary_id;
    }

    public void setBeneficiary_id(String beneficiary_id) {
        this.beneficiary_id = beneficiary_id;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
}
