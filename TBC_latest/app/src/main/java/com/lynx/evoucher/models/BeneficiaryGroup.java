package com.lynx.evoucher.models;

import com.orm.SugarRecord;
import com.orm.dsl.Table;

/**
 * Created by Kibet on 9/6/2016.
 */
@Table(name = "beneficiary_groups")
public class BeneficiaryGroup extends SugarRecord {

    private Long id;
    private String bnfGrpId;
    private String bnfGrpName;

    public BeneficiaryGroup() {
    }

    public BeneficiaryGroup(Long id, String bnfGrpId, String bnfGrpName) {
        this.id = id;
        this.bnfGrpId = bnfGrpId;
        this.bnfGrpName = bnfGrpName;
    }

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    public String getBnfGrpId() {
        return bnfGrpId;
    }

    public void setBnfGrpId(String bnfGrpId) {
        this.bnfGrpId = bnfGrpId;
    }

    public String getBnfGrpName() {
        return bnfGrpName;
    }

    public void setBnfGrpName(String bnfGrpName) {
        this.bnfGrpName = bnfGrpName;
    }
}
