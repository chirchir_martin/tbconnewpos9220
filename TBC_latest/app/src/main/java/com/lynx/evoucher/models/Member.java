package com.lynx.evoucher.models;

/**
 * Created by Kibet on 9/2/2016.
 */

public class Member {
    public static Long id;
    public static String firstname;
    public static String middlename;
    public static String lastname;
    public static String user_image;
    public static String signature;
    public static String national_id;
    public static String front_national_id;
    public static String back_national_id;
    public static String right_thumb;
    public static String right_index;
    public static String left_thumb;
    public static String left_index;
    public static String title;
    public static String gender;
    public static String date_of_birth;
    public static String familysize;
    public static String nationality;
    public static String programmeid;
    public static String beneficiary_group_id;
    public static String card_number;
    public static String card_serial_number;
    public static String mobile;

    public Member(){
        super();
    }



}
