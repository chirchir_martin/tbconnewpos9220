package com.lynx.evoucher.models;

import com.orm.SugarRecord;
import com.orm.dsl.Table;

/**
 * Created by Administrator on 8/30/2016.
 */
@Table(name = "vouchers")
public class Vouchers extends SugarRecord {
    private Long id;

    private String voucherName;
    private String startdate;
    private String enddate;
    private String vouchersid;
    private String programmeid;
    private String bendroup;

    public Vouchers() {
    }

    public Vouchers(Long id, String voucherName, String startdate, String enddate, String vouchersid, String programmeid,String bengrp) {
        this.id = id;
        this.voucherName = voucherName;
        this.startdate = startdate;
        this.enddate = enddate;
        this.vouchersid = vouchersid;
        this.programmeid = programmeid;
        this.bendroup=bengrp;
    }

    public String getVouchersid() {
        return vouchersid;
    }

    public void setVouchersid(String vouchersid) {
        this.vouchersid = vouchersid;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getBendroup() {
        return bendroup;
    }

    public void setBendroup(String bendroup) {
        this.bendroup = bendroup;
    }

    public String getVoucherName(){
        return this.voucherName;
    }
    public void setVoucherName(String voucherName){
        this.voucherName=voucherName;
    }
    public String getStartdate(){
        return this.startdate;
    }
    public void setStartdate(String startdate){
        this.startdate=startdate;
    }
    public String getEnddate(){
        return this.enddate;
    }
    public void setEnddate(String enddate){
        this.enddate=enddate;
    }
    public String getProgrammeid(){
        return this.programmeid;
    }
    public void setProgrammeid(String programmeid){
        this.programmeid=programmeid;
    }
}
