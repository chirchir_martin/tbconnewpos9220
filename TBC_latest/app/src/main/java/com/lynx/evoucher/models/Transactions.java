package com.lynx.evoucher.models;

import com.orm.SugarRecord;

/**
 * Created by Administrator on 9/2/2016.
 */
public class Transactions extends SugarRecord {
    private Long id;
    private String voucherid;
    private String transactiontype;
    private String cancelledtransaction;
    private String date;
    private String uom;
    private String authenticationtype;
    private String voucheridno;
    private String cardno;
    private String receiptno;
    private String isuploaded;
    private String locationid;
    private String rationno;
    private String deviceid;
    private String user;
    public Transactions(){

    }
    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }
    private String timestamp;
    private String totalamountchargedbyretail;
    private String totalvaluereamining;
    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    public String getCardno() {
        return cardno;
    }

    public void setCardno(String cardno) {
        this.cardno = cardno;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getVoucheridno() {
        return voucheridno;
    }

    public String getRationno() {
        return rationno;
    }

    public void setRationno(String rationno) {
        this.rationno = rationno;
    }

    public String getUom() {
        return uom;
    }

    public String getLocationid() {
        return locationid;
    }

    public void setLocationid(String locationid) {
        this.locationid = locationid;
    }

    public String getIsuploaded() {
        return isuploaded;
    }

    public void setIsuploaded(String isuploaded) {
        this.isuploaded = isuploaded;
    }

    public void setUom(String uom) {
        this.uom = uom;
    }

    public String getDeviceid() {
        return deviceid;
    }

    public void setDeviceid(String deviceid) {
        this.deviceid = deviceid;
    }

    public void setVoucheridno(String voucheridno) {
        this.voucheridno = voucheridno;
    }

    public String getTransactiontype() {
        return transactiontype;
    }
    public String getVoucherid() {
        return voucherid;
    }



    public void setVoucherid(String voucherid) {
        this.voucherid = voucherid;
    }
    public void setTransactiontype(String transactiontype) {
        this.transactiontype = transactiontype;
    }
    public String getTotalvaluereamining() {
        return totalvaluereamining;
    }

    public void setTotalvaluereamining(String totalvaluereamining) {
        this.totalvaluereamining = totalvaluereamining;
    }

    public String getCancelledtransaction() {
        return cancelledtransaction;
    }

    public void setCancelledtransaction(String cancelledtransaction) {
        this.cancelledtransaction = cancelledtransaction;
    }

    public String getReceiptno() {
        return receiptno;
    }

    public void setReceiptno(String receiptno) {
        this.receiptno = receiptno;
    }

    public String getTotalamountchargedbyretail() {
        return totalamountchargedbyretail;
    }

    public void setTotalamountchargedbyretail(String totalamountchargedbyretail) {
        this.totalamountchargedbyretail = totalamountchargedbyretail;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getAuthenticationtype() {
        return authenticationtype;
    }

    public void setAuthenticationtype(String authenticationtype) {
        this.authenticationtype = authenticationtype;
    }





}
