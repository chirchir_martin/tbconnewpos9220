package com.lynx.evoucher.models;

/**
 * Created by Administrator on 8/29/2016.
 */

import com.orm.SugarRecord;
import com.orm.dsl.Table;

import java.util.Date;

@Table(name = "serviceprices")
public class ServicePrices extends SugarRecord {
    private Long id;
    private String serviceid;
    private String uom;
    private String maxprice;
    private String quantity;
    private Date createdOn = new Date();

    public ServicePrices() {

    }

    public ServicePrices(Long id, String serviceid, String price, Date createdOn) {
        this.id = id;
        this.serviceid = serviceid;

        this.createdOn = createdOn;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getServiceid() {
        return serviceid;
    }

    public void setServiceid(String serviceid) {
        this.serviceid = serviceid;
    }

    public String getUom() {
        return uom;
    }

    public void setUom(String uom) {
        this.uom = uom;
    }

    public String getMaxprice() {
        return maxprice;
    }

    public void setMaxprice(String maxprice) {
        this.maxprice = maxprice;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }
}
