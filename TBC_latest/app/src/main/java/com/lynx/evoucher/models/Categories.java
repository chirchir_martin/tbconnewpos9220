package com.lynx.evoucher.models;

/**
 * Created by Administrator on 8/29/2016.
 */

import com.orm.SugarRecord;
import com.orm.dsl.Table;
import java.util.Date;
@Table(name = "categories")
public class Categories extends SugarRecord {

    private Long id;


    private String code;

    private String name;

    private Date createdOn = new Date();

    public Categories() {

    }

    public Categories(Long id, String code, String name, Date createdOn) {
        this.id = id;
        this.code = code;
        this.name = name;
        this.createdOn = createdOn;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getname() {
        return name;
    }

    public void setname(String name) {
        this.name = name;
    }


    public void setCode(String code) {
        this.code = code;
    }
    public String getCode() {
        return code;
    }


}
