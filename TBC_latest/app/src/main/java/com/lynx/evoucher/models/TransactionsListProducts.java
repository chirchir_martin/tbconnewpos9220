package com.lynx.evoucher.models;

import com.orm.SugarRecord;

import java.sql.Date;

/**
 * Created by USER on 7/4/2017.
 */

public class TransactionsListProducts extends SugarRecord {
    public  String productid;
    public String productname;
    public String quantity;
    public String val;
    public  String unitofmeasure;
    public String  transactiondate;
    public String  deviceid;


    public  TransactionsListProducts(){


    }

    public String getTransactiondate() {
        return transactiondate;
    }

    public void setTransactiondate(String transactiondate) {
        this.transactiondate = transactiondate;
    }

    public  TransactionsListProducts(String productname, String quantity, String val, String unitofmeasure, String transactiondate){
        this.productname=productname;
        this.quantity=quantity;
        this.val=val;
        this.unitofmeasure=unitofmeasure;
        this.transactiondate=transactiondate;
    }

    @Override
    public String toString() {
        return   productname;
    }

    public String getProductid() {
        return productid;
    }

    public void setProductid(String productid) {
        this.productid = productid;
    }

    public String getDeviceid() {
        return deviceid;
    }

    public void setDeviceid(String deviceid) {
        this.deviceid = deviceid;
    }

    public String getProductname() {
        return productname;
    }

    public void setProductname(String productname) {
        this.productname = productname;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    public String getVal() {
        return val;
    }

    public void setVal(String val) {
        this.val = val;
    }

    public String getUnitofmeasure() {
        return unitofmeasure;
    }

    public void setUnitofmeasure(String unitofmeasure) {
        this.unitofmeasure = unitofmeasure;
    }
}
