package com.lynx.evoucher.models;
import com.orm.SugarRecord;
import com.orm.dsl.Table;

/**
 * Created by Administrator on 9/2/2016.
 */
@Table(name="configuration")
public class Configuration extends SugarRecord {
    private Long id;
    private String url;
    private String port;
    private String authenticationtype;
    private String transactionmode;
   public Configuration(){

   }

    public Configuration(Long id, String url, String port) {
        this.id = id;
        this.url = url;
        this.port = port;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getPort() {
        return port;
    }

    public void setPort(String port) {
        this.port = port;
    }
    public String getAuthenticationtype() {
        return authenticationtype;
    }

    public String getTransactionmode() {
        return transactionmode;
    }

    public void setTransactionmode(String transactionmode) {
        this.transactionmode = transactionmode;
    }

    public void setAuthenticationtype(String authenticationtype) {
        this.authenticationtype = authenticationtype;

    }
}
