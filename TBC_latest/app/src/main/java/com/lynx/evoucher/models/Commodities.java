package com.lynx.evoucher.models;

import com.orm.SugarRecord;

/**
 * Created by Administrator on 9/3/2016.
 */
public class Commodities extends SugarRecord {
    private Long id;


    private String productid;

    private String quantitydeducted;

    private String transactionno;
    private String uom;

    private String quantityremaining;



    private String totalamountcahgerdbyretailer;


        public Commodities(){

          }
    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    public String getProductid() {
        return productid;
    }

    public void setProductid(String productid) {
        this.productid = productid;
    }

    public String getQuantitydeducted() {
        return quantitydeducted;
    }

    public void setQuantitydeducted(String quantitydeducted) {
        this.quantitydeducted = quantitydeducted;
    }

    public String getUom() {
        return uom;
    }

    public void setUom(String uom) {
        this.uom = uom;
    }

    public String getTransactionno() {
        return transactionno;
    }

    public void setTransactionno(String transactionno) {
        this.transactionno = transactionno;
    }

    public String getQuantityremaining() {
        return quantityremaining;
    }

    public void setQuantityremaining(String quantityremaining) {
        this.quantityremaining = quantityremaining;
    }
    public String getTotalamountcahgerdbyretailer() {
        return totalamountcahgerdbyretailer;
    }

    public void setTotalamountcahgerdbyretailer(String totalamountcahgerdbyretailer) {
        this.totalamountcahgerdbyretailer = totalamountcahgerdbyretailer;
    }

}
