package com.lynx.evoucher.models;

import com.orm.SugarRecord;
import com.orm.dsl.Table;

/**
 * Created by JOHNIE on 9/8/2017.
 */

@Table(name = "cardblock")
public class CardBlock extends SugarRecord {

    public String cardno;
    public String rationno;

    public String getCardno() {
        return cardno;
    }

    public void setCardno(String cardno) {
        this.cardno = cardno;
    }

    public String getRationno() {
        return rationno;
    }

    public void setRationno(String rationno) {
        this.rationno = rationno;
    }
}
