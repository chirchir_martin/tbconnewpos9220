package com.lynx.evoucher.models;

import com.orm.SugarRecord;

/**
 * Created by Administrator on 10/29/2016.
 */
public class Reports extends SugarRecord {
    public String lawyercode;
    public String lawyername;
    public String programmeid;
    public String date;
    public String verifiedby;
    public Reports(String lawyercode, String lawyername, String programmeid, String date) {
        this.lawyercode = lawyercode;
        this.lawyername = lawyername;
        this.programmeid = programmeid;
        this.date = date;
    }
    public Reports() {

    }

    public String getLawyercode() {
        return lawyercode;
    }

    public void setLawyercode(String lawyercode) {
        this.lawyercode = lawyercode;
    }

    public String getLawyername() {
        return lawyername;
    }

    public void setLawyername(String lawyername) {
        this.lawyername = lawyername;
    }

    public String getProgrammeid() {
        return programmeid;
    }

    public void setProgrammeid(String programmeid) {
        this.programmeid = programmeid;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getVerifiedby() {
        return verifiedby;
    }

    public void setVerifiedby(String verifiedby) {
        this.verifiedby = verifiedby;
    }
}
