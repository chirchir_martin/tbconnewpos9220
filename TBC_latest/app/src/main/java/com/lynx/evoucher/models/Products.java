package com.lynx.evoucher.models;

/**
 * Created by Administrator on 8/29/2016.
 */

import com.orm.SugarRecord;
import com.orm.dsl.Table;
@Table(name = "products")
public class Products extends SugarRecord{
    private Long id;
    private String productcode;
    private String description;
    private String voucherid;
    private String programmeid;
    private String name;
    private String productid;
    private String categoryid;
    private String bengroup;
    private String image;
    private String maxprice;
    private String quantity;
    private String uom;
    public Products() {
    }

    public Products(Long id, String productcode, String description, String voucherid, String name, String productid, String categoryid, String image, String uom,String bgrp) {

        this.id = id;
        this.productcode = productcode;
        this.description = description;
        this.voucherid = voucherid;
        this.name = name;
        this.productid = productid;
        this.categoryid = categoryid;
        this.image = image;
        this.uom = uom;
        this.bengroup=bgrp;
    }

    public String getDesc() {
        return description;
    }
    public String getProductid() {
        return productid;
    }
    public String getProgrammeid() {
        return programmeid;
    }

    public void setProgrammeid(String programmeid) {
        this.programmeid = programmeid;
    }
    public void setProductid(String productid) {
        this.productid = productid;
    }
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUom() {
        return uom;
    }

    public String getproductcode() {
        return productcode;
    }

    public String getMaxprice() {
        return maxprice;
    }

    public void setMaxprice(String maxprice) {
        this.maxprice = maxprice;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    public String getBengroup() {
        return bengroup;
    }

    public void setBengroup(String bengroup) {
        this.bengroup = bengroup;
    }

    public String getVouchid() {
        return voucherid;
    }

    public void setVouchid(String vouchid) {
        this.voucherid = vouchid;
    }
    public String getproductname() {
        return name;
    }

    public void setproductname(String name) {
        this.name = name;
    }
    public void setproductcode(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCategory_id() {
        return categoryid;
    }

    public void setCategory_id(String category_id) {
        this.categoryid = category_id;
    }

    public String getimage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
    public void setUom(String uom) {
        this.uom = uom;
    }

}

