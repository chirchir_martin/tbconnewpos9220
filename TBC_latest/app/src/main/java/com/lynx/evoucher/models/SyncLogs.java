package com.lynx.evoucher.models;

import com.orm.SugarRecord;
import com.orm.dsl.Table;

/**
 * Created by JOHNIE on 8/18/2017.
 */
@Table(name="synclogs")
public class SyncLogs extends SugarRecord {
    private Long id;
    private String deviceid;
    private String nooftxns;
    private String totalamt;
    private String date;


    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    public String getDeviceid() {
        return deviceid;
    }

    public void setDeviceid(String deviceid) {
        this.deviceid = deviceid;
    }

    public String getTotalamt() {
        return totalamt;
    }

    public void setTotalamt(String totalamt) {
        this.totalamt = totalamt;
    }

    public String getNooftxns() {
        return nooftxns;
    }

    public void setNooftxns(String nooftxns) {
        this.nooftxns = nooftxns;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
