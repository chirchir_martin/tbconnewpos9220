package com.lynx.evoucher.models;

import com.orm.SugarRecord;
import com.orm.dsl.Table;

/**
 * Created by Administrator on 8/30/2016.
 */
@Table(name="topups")
public class Topups extends SugarRecord {

    private Long id;
    private String beneficiaryId;
    private String cardnumber;
    private String productquantity;
    private String productid;

    private String vouchervalue;
    private String programmeid;
    private String vocheridno;
    private String voucherid;
    private String vouchertype;
    private String bengroup;

    public Topups() {
    }


    public Topups(Long id, String beneficiaryId, String cardnumber, String productquantity, String productid, String productPrice, String programmeid, String voucherid, String vouchertype,String brngrp) {
        this.id = id;
        this.beneficiaryId = beneficiaryId;
        this.cardnumber = cardnumber;
        this.productquantity = productquantity;
        this.productid = productid;

        this.programmeid = programmeid;
        this.voucherid = voucherid;
        this.bengroup =brngrp;
        this.vouchertype = vouchertype;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
    public String getBeneficiaryId() {
        return beneficiaryId;
    }

    public void setBeneficiaryId(String beneficiaryId) {
        this.beneficiaryId = beneficiaryId;
    }
    public String getCardNumber() {
        return cardnumber;
    }

    public String getVocheridno() {
        return vocheridno;
    }

    public void setVocheridno(String vocheridno) {
        this.vocheridno = vocheridno;
    }

    public void setCardNumber(String cardNumber) {
        this.cardnumber = cardNumber;
    }
    public String getProductQuantity() {
        return productquantity;
    }

    public void setProductQuantity(String productQuantity) {
        this.productquantity = productQuantity;
    }

    public String getProductId() {
        return productid;
    }
    public void setProductId(String productId) {
        this.productid =productId;
    }


    public String getBengroup() {
        return bengroup;
    }

    public String getVouchervalue() {
        return vouchervalue;
    }

    public void setVouchervalue(String vouchervalue) {
        this.vouchervalue = vouchervalue;
    }

    public void setBengroup(String bengroup) {
        this.bengroup = bengroup;
    }

    public void setProgrammeId(String programmeid) {
        this.programmeid = programmeid;
    }
    public String getProgrammeId() {
        return  this.programmeid;
    }

    public void setVoucherId(String voucherId) {
        this.voucherid = voucherId;
    }
    public String getVoucherId() {
       return voucherid;
    }
    public void setvoucherType(String voucherType) {
       this.vouchertype=voucherType;
    }
    public String getvocherType() {
       return vouchertype;
    }
}
