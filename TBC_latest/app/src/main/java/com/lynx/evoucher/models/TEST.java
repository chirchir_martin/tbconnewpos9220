package com.lynx.evoucher.models;

import com.orm.SugarRecord;
import com.orm.dsl.Table;

/**
 * Created by JOHNIE on 8/20/2017.
 */
@Table(name="TEST")
public class TEST extends SugarRecord {
    private Long id;
    private String deviceid;
    private String nooftxns;
    private String totalamt;


    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    public String getDeviceid() {
        return deviceid;
    }

    public void setDeviceid(String deviceid) {
        this.deviceid = deviceid;
    }

    public String getNooftxns() {
        return nooftxns;
    }

    public void setNooftxns(String nooftxns) {
        this.nooftxns = nooftxns;
    }

    public String getTotalamt() {
        return totalamt;
    }

    public void setTotalamt(String totalamt) {
        this.totalamt = totalamt;
    }
}
