package com.lynx.evoucher.models;

import com.orm.SugarRecord;

/**
 * Created by Chirchir on 3/16/2019.
 */

public class Reprints extends SugarRecord {

    public Reprints(){}
    String username;
    String macid;
    String voucherid;
    String time;
    String receiptno;
    String rationno;
    String items;
    String openbal;
    String charges;
    String closebal;
    String date;
    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getMacid() {
        return macid;
    }

    public void setMacid(String macid) {
        this.macid = macid;
    }

    public String getVoucherid() {
        return voucherid;
    }

    public void setVoucherid(String voucherid) {
        this.voucherid = voucherid;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getReceiptno() {
        return receiptno;
    }

    public void setReceiptno(String receiptno) {
        this.receiptno = receiptno;
    }

    public String getRationno() {
        return rationno;
    }

    public void setRationno(String rationno) {
        this.rationno = rationno;
    }

    public String getItems() {
        return items;
    }

    public void setItems(String items) {
        this.items = items;
    }

    public String getOpenbal() {
        return openbal;
    }

    public void setOpenbal(String openbal) {
        this.openbal = openbal;
    }

    public String getCharges() {
        return charges;
    }

    public void setCharges(String charges) {
        this.charges = charges;
    }

    public String getClosebal() {
        return closebal;
    }

    public void setClosebal(String closebal) {
        this.closebal = closebal;
    }



}
