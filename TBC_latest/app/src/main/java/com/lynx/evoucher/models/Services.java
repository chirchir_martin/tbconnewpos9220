package com.lynx.evoucher.models;

import com.google.common.util.concurrent.Service;
import com.orm.SugarRecord;
import com.orm.dsl.Table;

/**
 * Created by Administrator on 1/13/2017.
 */
@Table(name="services")
public class Services extends SugarRecord {
    public  String serviceId;
    public  String voucherId;
    public  String ProgrammeId;
    public  String benGroupId;

    public Services(){

    }
    public Services(String serviceId,String voucherId,String programmeId,String benGroupId){
        this.voucherId=voucherId;
        this.serviceId=serviceId;
        this.ProgrammeId=programmeId;
        this.benGroupId=benGroupId;


    }
    public String getServiceId() {
        return serviceId;
    }

    public void setServiceId(String serviceId) {
        this.serviceId = serviceId;
    }

    public String getVoucherId() {
        return voucherId;
    }

    public void setVoucherId(String voucherId) {
        this.voucherId = voucherId;
    }

    public String getProgrammeId() {
        return ProgrammeId;
    }

    public void setProgrammeId(String programmeId) {
        ProgrammeId = programmeId;
    }

    public String getBenGroupId() {
        return benGroupId;
    }

    public void setBenGroupId(String benGroupId) {
        this.benGroupId = benGroupId;
    }


}
