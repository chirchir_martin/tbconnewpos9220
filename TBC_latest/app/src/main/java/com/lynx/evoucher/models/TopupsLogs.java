package com.lynx.evoucher.models;

import com.orm.SugarRecord;
import com.orm.dsl.Table;

/**
 * Created by JOHNIE on 8/16/2017.
 */
@Table(name="topupslogs")
public class TopupsLogs extends SugarRecord {
    private Long id;
    private String ntopupvalue;
    private String nvoucheridno;
    private String ocardbal;
    private String ncardbal;
    private String ovoucheridno;
    private String topuptime;
    private String cardno;
    private String isuploaded;
    private String deviceidno;
    private String username;
    private String refno;


    public String getDeviceidno() {
        return deviceidno;
    }

    public void setDeviceidno(String deviceidno) {
        this.deviceidno = deviceidno;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getRefno() {
        return refno;
    }

    public void setRefno(String refno) {
        this.refno = refno;
    }

    public String getTopuptime() {
        return topuptime;
    }

    public String getIsuploaded() {
        return isuploaded;
    }

    public void setIsuploaded(String isuploaded) {
        this.isuploaded = isuploaded;
    }

    public void setTopuptime(String topuptime) {
        this.topuptime = topuptime;
    }

    public String getCardno() {
        return cardno;
    }

    public void setCardno(String cardno) {
        this.cardno = cardno;
    }

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    public String getNtopupvalue() {
        return ntopupvalue;
    }

    public void setNtopupvalue(String ntopupvalue) {
        this.ntopupvalue = ntopupvalue;
    }

    public String getNvoucheridno() {
        return nvoucheridno;
    }

    public String getOcardbal() {
        return ocardbal;
    }

    public void setOcardbal(String ocardbal) {
        this.ocardbal = ocardbal;
    }

    public String getNcardbal() {
        return ncardbal;
    }

    public void setNcardbal(String ncardbal) {
        this.ncardbal = ncardbal;
    }

    public void setNvoucheridno(String nvoucheridno) {
        this.nvoucheridno = nvoucheridno;
    }



    public String getOvoucheridno() {
        return ovoucheridno;
    }

    public void setOvoucheridno(String ovoucheridno) {
        this.ovoucheridno = ovoucheridno;
    }
}
