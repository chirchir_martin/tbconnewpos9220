package com.lynx.evoucher.models;

import com.orm.SugarRecord;
import com.orm.dsl.Table;

/**
 * Created by Administrator on 8/30/2016.
 */
@Table(name = "programmes")
public class Programmes extends SugarRecord  {
    private Long id;
    private String programmename;
    private String programid;
    private String startdate;
    private String progtype;
    private String bengroup;

    private String enddate;

    public Programmes() {

    }

    public Programmes(Long id, String programmename, String programid, String startdate, String enddate,String progtype,String bgrop) {
        this.id = id;
        this.programmename = programmename;
        this.programid = programid;
        this.startdate = startdate;
        this.enddate = enddate;
        this.progtype=progtype;
        this.bengroup=bgrop;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getProgramid() {
        return programid;
    }

    public void setProgramid(String programid) {
        this.programid = programid;
    }

    public String getProgType() {
        return progtype;
    }

    public void setProgType(String progtype) {
        this.progtype = progtype;
    }

    public String getBengroup() {
        return bengroup;
    }

    public void setBengroup(String bengroup) {
        this.bengroup = bengroup;
    }

    public String getProgrammename(){
        return this.programmename;
    }
    public void setProgrammename(String programmename){
       this.programmename=programmename;
    }
    public String getStartdate(){
        return this.startdate;
    }
    public void setStartdate(String startdate){
        this.startdate=startdate;
    }
    public String getEnddate(){
        return this.enddate;
    }
    public void setEnddate(String enddate){
        this.enddate=enddate;
    }
}
