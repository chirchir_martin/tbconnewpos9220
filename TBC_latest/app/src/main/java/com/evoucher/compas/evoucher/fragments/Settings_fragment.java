package com.evoucher.compas.evoucher.fragments;

import android.graphics.Typeface;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.evoucher.compas.evoucher.Details;
import com.evoucher.compas.evoucher.R;
import com.lynx.evoucher.models.Configuration;

import java.util.List;

/**
 * Created by Administrator on 10/25/2016.
 */
public class Settings_fragment extends Fragment {
    EditText input;
    Button btn,update,settings;
    List<Configuration> config;
    Configuration config_table=new Configuration();
    Typeface typeface1;
    Details dt;
    TextView about,configurationtitle,iptitle,ip,porttitle,port,authentication,authenticationtitle,authenticationtype,deviceid;
    MaterialDialog dialog;
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        return inflater.inflate(R.layout.settings_fragment, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        dt=new Details(getActivity());
        typeface1 = Typeface.createFromAsset(getActivity().getAssets(), "Roboto-Regular.ttf");
        about=(TextView)getView().findViewById(R.id.about);
       deviceid=(TextView)getView().findViewById(R.id.deviceid);
        configurationtitle=(TextView)getView().findViewById(R.id.configuration_title);
        iptitle=(TextView)getView().findViewById(R.id.ip_title);
        ip=(TextView)getView().findViewById(R.id.ip);
        deviceid.setTypeface(typeface1);
        String android_id = Settings.Secure.getString(getActivity().getApplicationContext().getContentResolver(),
                Settings.Secure.ANDROID_ID);
        deviceid.setText(dt.getMac());
        config=Configuration.listAll(Configuration.class);
        ip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showInputDialog(getString(R.string.IpAddress),0);
            }
        });

        porttitle=(TextView)getView().findViewById(R.id.port_title);
        port=(TextView)getView().findViewById(R.id.port);
        authentication=(TextView)getView().findViewById(R.id.authentication);
        //authenticationtitle=(TextView)getView().findViewById(R.id.authentication_title);
        authenticationtype=(TextView)getView().findViewById(R.id.authentication_type);
        about.setTypeface(typeface1);
        configurationtitle.setTypeface(typeface1);
        iptitle.setTypeface(typeface1);
        ip.setText(config.get(0).getUrl());
        port.setText(config.get(0).getPort());
        authenticationtype.setText(config.get(0).getAuthenticationtype());
        porttitle.setTypeface(typeface1);
        port.setTypeface(typeface1);
        authentication.setTypeface(typeface1);
       // authenticationtitle.setTypeface(typeface1);
        authenticationtype.setTypeface(typeface1);
        authenticationtype.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ChooseAuthenticationtype();
            }
        });
        port.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showInputDialog(getString(R.string.PortNumber),1);
            }
        });
        ip.setTypeface(typeface1);
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onStop() {
        super.onStop();
    }
    public void showInputDialog(String title,final int pos) {
final String hint="";

        dialog = new MaterialDialog.Builder(getActivity())
                .title(title)
                .customView(R.layout.dialog_settings, true)
                .positiveText(R.string.Ok)
                .negativeText(android.R.string.cancel)
                .autoDismiss(false)
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {

                        if(input.getText().toString().equalsIgnoreCase("")){
                            Toast.makeText(getActivity(),"empty",Toast.LENGTH_LONG).show();
                        }else {
                            if (pos == 0) {
                                if (config.size() == 0) {
                                    config_table.setUrl(input.getText().toString());
                                    config_table.save();

                                } else {
                                    ip.setText(input.getText().toString());
                                    config.get(0).setUrl(input.getText().toString());
                                    config.get(0).save();

                                }
                                ip.setText(input.getText().toString());
                            } else {
                                if (config.size() == 0) {
                                    config_table.setPort(input.getText().toString());
                                    config_table.save();

                                } else {

                                    config.get(0).setPort(input.getText().toString());
                                    config.get(0).save();
                                }

                                port.setText(input.getText().toString());
                            }
                        }
                        dialog.dismiss();


                    }
                })
                .onNegative(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {

                        dialog.dismiss();

                    }
                })
                .build();
        input = (EditText)dialog.getCustomView().findViewById(R.id.input);
        Typeface typeface = Typeface.createFromAsset(getActivity().getAssets(), "Roboto-Thin.ttf");
        dialog.show();



    }
    public void ChooseAuthenticationtype() {
        new MaterialDialog.Builder(getActivity())
                .title(getString(R.string.Authenticationtype))
                .items(R.array.type)
                .itemsCallbackSingleChoice(1, new MaterialDialog.ListCallbackSingleChoice() {
                    @Override
                    public boolean onSelection(MaterialDialog dialog, View view, int which, CharSequence text) {
                        authenticationtype.setText(text);


                            if (config.size() == 0) {
                                config_table.setAuthenticationtype(text.toString());
                                config_table.save();

                            } else {
                                config.get(0).setAuthenticationtype(text.toString());
                                config.get(0).save();
                            }

                        return true;
                    }
                })
                .positiveText(getString(R.string.Ok))
                .negativeText(R.string.Cancel)
                .show();
    }
}
