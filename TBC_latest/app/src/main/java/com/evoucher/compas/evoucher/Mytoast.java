package com.evoucher.compas.evoucher;

import android.content.Context;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.*;

/**
 * Created by Administrator on 10/27/2016.
 */
public class Mytoast {
    public static void show(Context context, String text, boolean isLong) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View layout = inflater.inflate(R.layout.toast_layout, null);

        ImageView image = (ImageView) layout.findViewById(R.id.toast_image);
        image.setImageResource(R.drawable.ic_launcher);

        TextView textV = (TextView) layout.findViewById(R.id.toast_text);
        textV.setText(text);

        android.widget.Toast toast = new android.widget.Toast(context);
        toast.setGravity(Gravity.CENTER, 0, 0);
        toast.setDuration((isLong) ? android.widget.Toast.LENGTH_LONG : android.widget.Toast.LENGTH_SHORT);
        toast.setView(layout);
        toast.show();
    }
}
