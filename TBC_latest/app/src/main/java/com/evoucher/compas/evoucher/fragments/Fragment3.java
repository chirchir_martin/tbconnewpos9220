package com.evoucher.compas.evoucher.fragments;

import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;
import com.evoucher.compas.evoucher.Adapters.ScrollerViewPager;
import com.evoucher.compas.evoucher.R;
import com.lynx.evoucher.models.Member;

import java.io.ByteArrayOutputStream;

import cn.pedant.SweetAlert.SweetAlertDialog;


/**
 * Created by JOHNIE on 9/3/2015.
 */
public class Fragment3 extends Fragment implements OnClickListener {
    final int CAMERA_CAPTURE = 1;
    //keep track of cropping intent
    final int PIC_CROP = 2;
    //captured picture uri
    ImageView picViewa;
    private String encoded_unserimage;
    ImageView error1,next,prev;
    ScrollerViewPager viewPager;
    private Uri picUri; Bitmap thePic;
    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        if (savedInstanceState != null) {
            thePic =(Bitmap)savedInstanceState.getParcelable("BitmapImage");

        }
    }
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment3, container, false);

    }

    @Override
    public void onClick(View v) {
        Member dt=new Member();
        if (v.getId() == R.id.textt) {
            try {
                //use standard intent to capture an image
                Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                startActivityForResult(intent, CAMERA_CAPTURE);
            } catch (Exception e) {
                Log.d("FDFDFDFDFDFDF",e.toString());
                //display an error message
                String errorMessage = "Whoops - your device doesn't support capturing images!";
                Toast toast = Toast.makeText(getActivity(), errorMessage, Toast.LENGTH_SHORT);
                toast.show();
            }
        }else if(v.getId() == R.id.next){
//            if(picViewa.getDrawable()!=null){
               // error1.setVisibility(View.GONE);
                Member.user_image=encoded_unserimage;
                viewPager.setCurrentItem(3, true);
//            }else{
//                showAlert("ERROR","Beneficiary Image is Required!!");
//            }
        }else if(v.getId() == R.id.prev) {
            viewPager.setCurrentItem(1, true);

        }

    }
        @Override
        public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
            super.onViewCreated(view, savedInstanceState);
            //retrieve a reference to the UI button
            Button captureBtn = (Button) getView().findViewById(R.id.textt);
            //handle button clicks
            captureBtn.setOnClickListener(this);
            picViewa = (ImageView)getView().findViewById(R.id.picturew);
            error1 = (ImageView)getView().findViewById(R.id.error1);
            next = (ImageView)getView().findViewById(R.id.next);
            prev = (ImageView)getView().findViewById(R.id.prev);
            next.setOnClickListener(this);
            prev.setOnClickListener(this);
            captureBtn.setOnClickListener(this);
            viewPager = (ScrollerViewPager) getActivity().findViewById(R.id.view_pager);
        }


    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.d("###USERIMAGEERROR",String.valueOf( getActivity().RESULT_OK));
        if (resultCode == getActivity().RESULT_OK) {
            Log.d("###USERIMAGEERROR","DeONE");
            //user is returning from capturing an image using the camera
            if(requestCode == CAMERA_CAPTURE){
                Log.d("###USERIMAGEERROR","DONE");
                try {
                    //get the Uri for the captured image
                    picUri = data.getData();
                    //carry out the crop operation -- not today
                    Bitmap photo = (Bitmap) data.getExtras().get("data");
                    picViewa.setImageBitmap(photo);
                    ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                    photo.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream);
                    byte[] byteArray = byteArrayOutputStream.toByteArray();
                    encoded_unserimage = Base64.encodeToString(byteArray, Base64.DEFAULT);
                }catch (Exception e){
                    Log.d("###USERIMAGEERROR",e.toString());
                }

            }else{
                Log.d("###USERIMAGEERROR","RESULTERROR");
            }
        }
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (savedInstanceState != null) {
            thePic = (Bitmap) savedInstanceState.getParcelable("BitmapImage");
            if(thePic!=null) {
                picViewa.setImageBitmap(thePic);
            }
        }
    }
    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if (outState != null) {
            outState.putParcelable("BitmapImage", thePic);
        }
    }

    public void showAlert(String title,String message){
        Log.d("ALERT","ALERT");
        new SweetAlertDialog(getActivity(), SweetAlertDialog.WARNING_TYPE)
                .setTitleText("Empty Image")
                .setContentText("Please ensure that the client's image is captured.")
                .setConfirmText("Ok")
                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sDialog) {
                        sDialog.dismissWithAnimation();

                    }
                })
                .show();
    }
}
