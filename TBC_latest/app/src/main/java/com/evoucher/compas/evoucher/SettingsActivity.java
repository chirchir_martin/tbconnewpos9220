package com.evoucher.compas.evoucher;

import android.content.Intent;
import android.graphics.Typeface;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.InputType;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.lynx.evoucher.models.Configuration;
import com.rey.material.app.Dialog;
import com.rey.material.widget.RadioButton;
import java.util.List;

import cn.pedant.SweetAlert.SweetAlertDialog;

public class SettingsActivity extends AppCompatActivity {
   EditText input;
    Button btn,update,settings;
    List<Configuration> config;
    Typeface typeface1;
    TextView about,configurationtitle,iptitle,ip,porttitle,port,authentication,deviceid,authenticationtype;
      MaterialDialog dialog;
    ImageView next;
    Details dt;
    RadioButton rb1,rb2,rb3,rb4;

    Configuration config_table=new Configuration();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
        overridePendingTransition(0, 0);
        dt=new Details(SettingsActivity.this);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(R.string.Settings);

        typeface1 = Typeface.createFromAsset(getAssets(), "Roboto-Regular.ttf");
        about=(TextView)findViewById(R.id.about);
        next=(ImageView)findViewById(R.id.next);
        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(ip.getText().toString().equalsIgnoreCase("")==false&&port.getText().toString().equalsIgnoreCase("")==false) {
                    startActivity(new Intent(SettingsActivity.this, LoadingActivity.class));
                    finish();
                } else{
                    new SweetAlertDialog(SettingsActivity.this, SweetAlertDialog.WARNING_TYPE)
                            .setTitleText("Empty Fields")
                            .setContentText("")
                            .setConfirmText("Ok")
                            .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sDialog) {
                                    sDialog.dismissWithAnimation();

                                }
                            })
                            .show();
                }

            }
        });
       configurationtitle=(TextView)findViewById(R.id.configuration_title);
       iptitle=(TextView)findViewById(R.id.ip_title);

        ip=(TextView)findViewById(R.id.ip);

        ip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showInputDialog("Whats your IP address?",0);
            }
        });

      porttitle=(TextView)findViewById(R.id.port_title);
       port=(TextView)findViewById(R.id.port);
       authentication=(TextView)findViewById(R.id.authentication);
      deviceid=(TextView)findViewById(R.id.deviceid);
       authenticationtype=(TextView)findViewById(R.id.authentication_type);
        about.setTypeface(typeface1);
       configurationtitle.setTypeface(typeface1);
       iptitle.setTypeface(typeface1);
        porttitle.setTypeface(typeface1);
       port.setTypeface(typeface1);
        config=Configuration.listAll(Configuration.class);
        if(config.size()!=0){
            ip.setText(config.get(0).getUrl());
            port.setText(config.get(0).getPort());

        }

        authentication.setTypeface(typeface1);
        deviceid.setTypeface(typeface1);
        String android_id = Settings.Secure.getString(getApplicationContext().getContentResolver(),
                Settings.Secure.ANDROID_ID);
        deviceid.setText(dt.getMac());
       authenticationtype.setTypeface(typeface1);
        authenticationtype.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ChooseAuthenticationtype();
            }
        });
        port.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showInputDialog("Whats your Port number?",1);
            }
        });
       ip.setTypeface(typeface1);
      /*  settings=(Button)findViewById(R.id.settingsbtn) ;
      //  candno=(EditText)findViewById(R.id.inputcard);
        url=(EditText)findViewById(R.id.inputurl);


        port=(EditText)findViewById(R.id.inputport);*/
      //  config= Configuration.listAll(Configuration.class);
       // Configuration.deleteAll(Configuration.class);
      /*  settings.setOnClickListener(new View.OnClickListener() {
                                           @Override
                                           public void onClick(View v) {

                                               if (port.getText().toString().isEmpty() == true) {
                                                   new SweetAlertDialog(SettingsActivity.this, SweetAlertDialog.WARNING_TYPE)
                                                           .setTitleText("")
                                                           .setContentText("Empty fields")
                                                           .setConfirmText("Ok")
                                                           .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                                               @Override
                                                               public void onClick(SweetAlertDialog sDialog) {
                                                                   sDialog.dismissWithAnimation();

                                                               }
                                                           })
                                                           .show();
                                               } else {
                                                   Configuration configuration = new Configuration();

                                                   configuration.setPort(port.getText().toString());
                                                   configuration.setUrl(url.getText().toString());
                                                   configuration.setPort(port.getText().toString());
                                                   configuration.setUrl(url.getText().toString());

//                                                   configuration.setPort(port.getText().toString());
//                                                   configuration.setUrl(url.getText().toString());



//                                                   if (rb1.isChecked()) {
//                                                       configuration.setAuthenticationtype("fingerprint");
//
//                                                   }
//                                                   if (rb2.isChecked()) {
//                                                       configuration.setAuthenticationtype("pin");
//
//                                                   }
//                                                   if (rb3.isChecked()) {
//                                                       configuration.setTransactionmode("phoneno");
//                                                   }
//                                                   if (rb4.isChecked()) {

                                                       configuration.setPort(port.getText().toString());
                                                       configuration.setUrl(url.getText().toString());

                                                       if (rb1.isChecked()) {
                                                           configuration.setAuthenticationtype("fingerprint");
                                                       }
                                                       if (rb2.isChecked()) {
                                                           configuration.setAuthenticationtype("pin");
                                                       }
                                                       if (rb3.isChecked()) {
                                                           configuration.setTransactionmode("phoneno");
                                                       }
                                                       if (rb4.isChecked()) {

                                                           configuration.setTransactionmode("card");
                                                       }
                                                       configuration.save();
                                                  //----------------- Log.d("SDEWEEERRR","DDFD=D=D");
                                                       startActivity(new Intent(SettingsActivity.this, LoadingActivity.class));
                                                       finish();




//                                                   }

                                               }
                                           }
                                       });

      *//*  rb1 = (RadioButton)findViewById(R.id.switches_rb1);
        rb2 = (RadioButton)findViewById(R.id.switches_rb2);
        rb3 = (RadioButton)findViewById(R.id.switches_rb11);
        rb4 = (RadioButton)findViewById(R.id.switches_rb22);*//*
        CompoundButton.OnCheckedChangeListener listener = new CompoundButton.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked){
                    rb1.setChecked(rb1 == buttonView);
                    rb2.setChecked(rb2 == buttonView);


                }

            }

        };
        CompoundButton.OnCheckedChangeListener listenerm = new CompoundButton.OnCheckedChangeListener() {


            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked){

                    rb3.setChecked(rb3 == buttonView);
                    rb4.setChecked(rb4 == buttonView);

                }

            }

        };

        rb1.setOnCheckedChangeListener(listener);
        rb2.setOnCheckedChangeListener(listener);
        rb3.setOnCheckedChangeListener(listenerm);
        rb4.setOnCheckedChangeListener(listenerm);

*/
    }
    public void showInputDialog(String title,final int pos) {


        dialog = new MaterialDialog.Builder(this)
                .title(title)
                .customView(R.layout.dialog_settings, true)
                .positiveText("Ok")
                .negativeText(android.R.string.cancel)
                .autoDismiss(false)
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                                @Override
                                public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                    if(input.getText().toString().equalsIgnoreCase("")){
                                        Toast.makeText(SettingsActivity.this,"empty",Toast.LENGTH_LONG).show();
                                    }else{
                                        if(pos==0){

                                            ip.setText(input.getText().toString());
                                            config.get(0).setUrl(input.getText().toString());
                                            config.get(0).save();
                                        }else{
                                            port.setText(input.getText().toString());
                                            config.get(0).setPort(input.getText().toString());
                                            config.get(0).save();
                                        }

                                    }

                                        dialog.dismiss();
                                        invalidateOptionsMenu();

                                }
                            })
                .onNegative(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {

                        dialog.dismiss();

                    }
                })
                .build();
      input = (EditText) dialog.getCustomView().findViewById(R.id.input);


        Typeface typeface = Typeface.createFromAsset(getAssets(), "Roboto-Thin.ttf");




        dialog.show();



    }
    public void ChooseAuthenticationtype( ) {
        new MaterialDialog.Builder(this)
                .title("Authentication type")
                .items(R.array.type)
                .itemsCallbackSingleChoice(1, new MaterialDialog.ListCallbackSingleChoice() {
                    @Override
                    public boolean onSelection(MaterialDialog dialog, View view, int which, CharSequence text) {
                        if(input.getText().toString().equalsIgnoreCase("")){
                            Toast.makeText(SettingsActivity.this,"empty",Toast.LENGTH_LONG).show();
                        }else {

                            authenticationtype.setText(text);
                            config.get(0).setAuthenticationtype(text.toString());
                            config.get(0).save();
                        }
                        return true; // allow selection
                    }
                })
                .positiveText("Ok")
                .negativeText("Cancel")
                .show();
    }
    @Override
    public void onBackPressed() {
        // TODO Auto-generated method stub
        Intent intent1 = new Intent(this, PinActivity.class);
        startActivity(intent1);
        finish();
    }
}
