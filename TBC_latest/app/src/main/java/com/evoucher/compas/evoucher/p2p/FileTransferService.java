// Copyright 2011 Google Inc. All Rights Reserved.

package com.evoucher.compas.evoucher.p2p;

import android.app.IntentService;
import android.app.ProgressDialog;
import android.content.ContentResolver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.util.Log;
import android.widget.Toast;


import org.json.JSONArray;
import org.json.JSONObject;

import com.evoucher.compas.evoucher.Details;
import com.lynx.evoucher.models.Commodities;
import com.lynx.evoucher.models.Topups;
import com.lynx.evoucher.models.Transactions;

import org.json.JSONObject;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.util.List;

/**
 * A service that process each file transfer request i.e Intent by opening a
 * socket connection with the WiFi Direct Group Owner and writing the file
 */
public class FileTransferService extends IntentService {
    ProgressDialog progressDialog;
    JSONArray commodities,cs;
    JSONObject transaction,jsonObject;
    JSONObject topup,data;
     Context context;
    private static final int SOCKET_TIMEOUT = 5000;
    public static final String ACTION_SEND_FILE = "com.example.android.wifidirect.SEND_FILE";
    public static final String EXTRAS_FILE_PATH = "file_url";
    public static final String EXTRAS_GROUP_OWNER_ADDRESS = "go_host";
    public static final String EXTRAS_GROUP_OWNER_PORT = "go_port";
    public static final String SYNC_TOPUPS = "sync_tps";
    public static final String SYNC_TXNS= "sync_txns";
    public static final String SYNC_STATUS= "sync_status";
    List<Topups> topups=null;
    List<Transactions> txns ;
    Details dt;
    public FileTransferService(String name) {
        super(name);
    }

    public FileTransferService() {
        super("FileTransferService");
    }

    /*
     * (non-Javadoc)
     * @see android.app.IntentService#onHandleIntent(android.content.Intent)
     */
    @Override
    protected void onHandleIntent(Intent intent) {

         context = getApplicationContext();
        dt=new Details(context);
        if (intent.getAction().equals(ACTION_SEND_FILE)) {
            final String status = intent.getExtras().getString(SYNC_STATUS);
            String host = intent.getExtras().getString(EXTRAS_GROUP_OWNER_ADDRESS);
            Socket socket = new Socket();
            int port = intent.getExtras().getInt(EXTRAS_GROUP_OWNER_PORT);

            try {
                Log.d(SyncActivity.TAG, "Opening client socket - ");
                socket.bind(null);
                socket.connect((new InetSocketAddress(host, port)), SOCKET_TIMEOUT);

                Log.d(SyncActivity.TAG, "Client socket - " + socket.isConnected());
              final   OutputStream stream = socket.getOutputStream();
                progressDialog = ProgressDialog.show(context, "Fetching the data", "Please wait", true,
                        true, new DialogInterface.OnCancelListener() {

                            @Override
                            public void onCancel(DialogInterface dialog) {

                            }
                        });
                progressDialog.setCancelable(false);

                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            data =new JSONObject();
                            JSONArray topUps = new JSONArray();
                            if(status.equalsIgnoreCase("0")) {
                                data.put("status","0");
                                topups = Topups.listAll(Topups.class);
                                if(topups.size()>0) {
                                    for (int i = 0; i < topups.size(); i++) {
                                        topup = new JSONObject();
                                        topup.put("beneficiaryId", topups.get(i).getBeneficiaryId());
                                        topup.put("cardNumber", topups.get(i).getCardNumber());
                                        topup.put("voucherValue", topups.get(i).getVouchervalue());
                                        topup.put("programmeId", topups.get(i).getProgrammeId());
                                        topup.put("voucherIdNo", topups.get(i).getVocheridno());
                                        topup.put("voucherId", topups.get(i).getVoucherId());
                                        topup.put("beneficiaryGroup", topups.get(i).getBengroup());
                                        topUps.put(topup);
                                    }
                                    data.put("content", topUps);
                                }else{
                                    Toast.makeText(context,"No Topups found",Toast.LENGTH_LONG).show();
                                }

                            }else if(status.equalsIgnoreCase("1")) {
                                data.put("status","1");
                                cs=new JSONArray();
                                transaction=new JSONObject();
                                List<Transactions> transactions = Transactions.listAll(Transactions.class);
                                Log.d("###TRANS", String.valueOf(transactions.size()));
                                if (transactions.size() != 0) {
                                    Log.d("dpsdpspdpsWE", String.valueOf(transactions.size()) + "ppppp");
                                    for (int i = 0; i < transactions.size(); i++) {
                                        Log.d("dpsdpspdps", transactions.get(i).getTotalvaluereamining());
                                        transaction = new JSONObject();
                                        commodities = new JSONArray();
                                        transaction.put("voucher", transactions.get(i).getVoucheridno());
                                        transaction.put("transaction_type", 0);
                                        transaction.put("cancelled_transaction", 0);
                                        transaction.put("receipt_number", transactions.get(i).getReceiptno());
                                        transaction.put("value_remaining", transactions.get(i).getTotalvaluereamining());
                                        transaction.put("total_amount_charged_by_retailer", transactions.get(i).getTotalamountchargedbyretail());
                                        transaction.put("user", transactions.get(i).getUser());
                                        transaction.put("cardNumber", transactions.get(i).getCardno());
                                        transaction.put("timestamp_transaction_created", transactions.get(i).getTimestamp());
                                        Log.d("###transactioncreated", transactions.get(i).getTimestamp());
                                        transaction.put("authentication_type", 0);
                                        transaction.put("pos_terminal", dt.getMac());
                                        List<Commodities> tops = Commodities.find(Commodities.class, "transactionno =?", transactions.get(i).getId().toString());
                                        Log.d("dpsdpspdps", String.valueOf(tops.size()));
                                        Log.d("##ERROR", "DONE" + tops.size());
                                        Log.d("#QRRRRRR", transactions.get(i).getId().toString());
                                        for (int j = 0; j < tops.size(); j++) {
                                            jsonObject = new JSONObject();
                                            jsonObject.put("pos_commodity", tops.get(j).getProductid());
                                            jsonObject.put("quantity_remaining", tops.get(j).getQuantityremaining());
                                            jsonObject.put("uom", tops.get(j).getUom());
                                            jsonObject.put("amount_charged_by_retailer", tops.get(j).getTotalamountcahgerdbyretailer());
                                            jsonObject.put("deducted_quantity", tops.get(j).getQuantitydeducted());
                                            commodities.put(jsonObject);
                                        }

                                        transaction.put("commodities", commodities);
                                        cs.put(transaction);

                                    }
                                    data.put("content",cs);
                                }else{
                                    Toast.makeText(context,"No trnasactions found",Toast.LENGTH_LONG).show();
                                }
                            }
                         //   DeviceDetailFragment.copyFile(data, stream);
                            progressDialog.dismiss();
                        }catch (Exception e){

                        }

                    }
                }).start();

                Log.d(SyncActivity.TAG, "Client: Data written");
            } catch (IOException e) {
                Log.e(SyncActivity.TAG, e.getMessage());
            } finally {
                if (socket != null) {
                    if (socket.isConnected()) {
                        try {
                            socket.close();
                        } catch (IOException e) {
                            // Give up
                            e.printStackTrace();
                        }
                    }
                }
            }

        }
    }
}
