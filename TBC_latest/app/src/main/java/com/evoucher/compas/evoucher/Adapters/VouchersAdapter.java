package com.evoucher.compas.evoucher.Adapters;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.amulyakhare.textdrawable.TextDrawable;
import com.amulyakhare.textdrawable.util.ColorGenerator;
import com.evoucher.compas.evoucher.ItemClickListener;
import com.lynx.evoucher.models.Vouchers;
import com.evoucher.compas.evoucher.R;

import java.util.List;

/**
 * Created by Administrator on 8/17/2016.
 */
public class VouchersAdapter extends RecyclerView.Adapter<VouchersAdapter.MyViewHolder> {
    private ItemClickListener clickListener;
    private List<Vouchers> vouchers;
    Context context;
    public TextView  d;

    String tag;

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        public TextView title, price, d;
        ImageView im;
        CardView card;
        public MyViewHolder(View view) {
            super(view);
            card=(CardView)view.findViewById(R.id.cardview);
            card.setOnClickListener(this);
            im=(ImageView)view.findViewById(R.id.main);
            title=(TextView)view.findViewById(R.id.title);
        }

        @Override
        public void onClick(View view) {
            if (clickListener != null)
                tag=view.getTag(R.id.holder).toString();
            clickListener.onClick(view,Integer.parseInt(tag));
        }
    }



    public VouchersAdapter(Context context, List<Vouchers> vouchers) {

      this.vouchers = vouchers;
        this.context=context;


        }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.vouchers_row, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {

        Typeface typeface = Typeface.createFromAsset(context.getAssets(), "Roboto-Thin.ttf");
      //  holder.title.setTypeface(typeface);
        ColorGenerator generator = ColorGenerator.DEFAULT;
        holder.card.setTag(R.id.holder,vouchers.get(position).getVouchersid());
        int color = generator.getColor(position);
      holder.title.setText( vouchers.get(position).getVoucherName());
        if( vouchers.get(position).getVoucherName().length()>3){
            String lettersForName =vouchers.get(position).getVoucherName().substring(0, 2);
            TextDrawable letterDrawable = TextDrawable.builder()
                    .buildRound(lettersForName, color);
            holder.im.setImageDrawable(letterDrawable);
        }else{
            TextDrawable letterDrawable = TextDrawable.builder()
                    .buildRound(vouchers.get(position).getVoucherName(), color);
            holder.im.setImageDrawable(letterDrawable);
        }





    }

 @Override
    public int getItemCount() {
        return  vouchers.size();
    }
      public void setClickListener(ItemClickListener itemClickListener) {

          this.clickListener = itemClickListener;
    }

}