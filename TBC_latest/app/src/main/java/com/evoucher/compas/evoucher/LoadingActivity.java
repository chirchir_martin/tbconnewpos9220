package com.evoucher.compas.evoucher;

import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.lynx.evoucher.models.Categories;
import com.lynx.evoucher.models.Products;
import com.lynx.evoucher.models.Programmes;
import com.lynx.evoucher.models.Topups;
import com.lynx.evoucher.models.Users;
import com.lynx.evoucher.models.Vouchers;
import com.wang.avi.AVLoadingIndicatorView;

import org.json.JSONArray;
import org.json.JSONObject;

public class LoadingActivity extends AppCompatActivity{
    private AVLoadingIndicatorView avi;
    ImageView logo;
    TextView tv0, tv1;
    Typeface typeface;
Network net;
    public static Activity fa;
    StringBuilder sb1 = null;
    SharedPreferences settings;
    private final Handler mHandler = new Handler();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_loading);
        overridePendingTransition(0, 0);
           fa=this;
        final String PREFS_NAME = "MyPrefsFile";
        settings =getSharedPreferences(PREFS_NAME, 0);
        avi = (AVLoadingIndicatorView) findViewById(R.id.avi);
        logo = (ImageView) findViewById(R.id.im);
        tv0 = (TextView) findViewById(R.id.text0);
        typeface = Typeface.createFromAsset(getAssets(), "Roboto-Regular.ttf");
        logo.setAlpha(0f);
        tv0.setTypeface(typeface);
        avi = (AVLoadingIndicatorView) findViewById(R.id.avi);

        final Thread s = new Thread() {
            @Override
            public void run() {
                try {
                    avi.setIndicator("BallSpinFadeLoaderIndicator");

                   /* ObjectAnimator alpha = ObjectAnimator.ofFloat(logo, "alpha", 0, 1);
                    ObjectAnimator anim = ObjectAnimator.ofFloat(logo, "translationY", -30, 0);
                    AnimatorSet animSetXY = new AnimatorSet();
                    animSetXY.setDuration(1500);
                    animSetXY.playTogether(alpha, anim);
                    animSetXY.setStartDelay(500);

                    animSetXY.start();*/
                } catch (Exception e) {

                }
            }
        };
       s.start();

        if (settings.getBoolean("fistTime", true)) {
           // Toast.makeText(LoadingActivity.this,"calling net",Toast.LENGTH_LONG).show();
            net=new Network(LoadingActivity.this,"Master");
            final Thread t = new Thread() {
                @Override
                public void run() {
                    try {
                        net.fetchData();
                    } catch (Exception e) {

                    }
                }
            };
            t.start();
        }else{
            startActivity(new Intent(LoadingActivity.this, ProgrammeActivity.class));
            finish();
            }

    }





    @Override
    public void onBackPressed() {

        Intent intent1 = new Intent(this, PinActivity.class);
        startActivity(intent1);
        finish();


    }

}