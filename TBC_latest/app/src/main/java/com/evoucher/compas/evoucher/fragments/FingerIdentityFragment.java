
package com.evoucher.compas.evoucher.fragments;
import android.app.AlertDialog;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.ColorMatrix;
import android.graphics.ColorMatrixColorFilter;
import android.graphics.Paint;
import android.hardware.usb.UsbDevice;
import android.hardware.usb.UsbManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.LinearInterpolator;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.evoucher.compas.evoucher.R;
import com.evoucher.compas.evoucher.ui.CustomImageView;
import com.lynx.evoucher.models.Beneficiary;





import java.nio.ByteBuffer;
import java.util.List;

import SecuGen.FDxSDKPro.JSGFPLib;
import SecuGen.FDxSDKPro.SGAutoOnEventNotifier;
import SecuGen.FDxSDKPro.SGFDxDeviceName;
import SecuGen.FDxSDKPro.SGFDxErrorCode;
import SecuGen.FDxSDKPro.SGFDxSecurityLevel;
import SecuGen.FDxSDKPro.SGFDxTemplateFormat;
import SecuGen.FDxSDKPro.SGFingerInfo;
import SecuGen.FDxSDKPro.SGFingerPresentEvent;


/**
 * Created by JOHNIE on 9/3/2015.
 */
public class FingerIdentityFragment extends Fragment implements View.OnClickListener, Runnable, SGFingerPresentEvent {
    TextView b1;
    ImageView imageView,sl0,sl1,sr0,sr1,r0,r1,l0,l1;
    CustomImageView img0,img1,img2,img3;
    Button mCapture,saveButton;
    private static final String ACTION_USB_PERMISSION = "com.android.example.USB_PERMISSION";
    ImageView error1,next,prev;
    private EditText mEditLog;
    private boolean mAutoOnEnabled;
    private String state;
    private int mImageWidth;
    private String firstName = "";
    private String lastName = "";
    private String mobile = "";
    private String encoded_lthumb,encoded_lindex,encoded_rthumb,encoded_rindex;
    private int LTHUMB=0,LINDEX=1,RTHUMB=2,RINDEX=3;
    private int mImageHeight;
    boolean feed;
 TextView scannerbar;
    private int[] mMaxTemplateSize;
    private int[] grayBuffer;
    private PendingIntent mPermissionIntent;
    private Bitmap grayBitmap;
    private IntentFilter filter; //2014-04-11
    private SGAutoOnEventNotifier autoOn;
    private boolean mLed;
    private byte[] mRegisterTemplate0,mRegisterTemplate1,mRegisterTemplate2,mRegisterTemplate3;

    private JSGFPLib sgfplib;
    Animation animation;

    private void debugMessage(String message) {
        this.mEditLog.append(message);
        this.mEditLog.invalidate(); //TODO trying to get Edit log to update after each line written
    }

    private final BroadcastReceiver mUsbReceiver = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            //DEBUG Log.d(TAG,"Enter mUsbReceiver.onReceive()");
            if (ACTION_USB_PERMISSION.equals(action)) {
                synchronized (this) {
                    UsbDevice device = (UsbDevice) intent.getParcelableExtra(UsbManager.EXTRA_DEVICE);
                    if (intent.getBooleanExtra(UsbManager.EXTRA_PERMISSION_GRANTED, false)) {
                        if (device != null) {
                            //DEBUG Log.d(TAG, "Vendor ID : " + device.getVendorId() + "\n");
                            //DEBUG Log.d(TAG, "Product ID: " + device.getProductId() + "\n");

                        } else {

                        }


                    }
                }
            }
        }
    };

    public Handler fingerDetectedHandler = new Handler() {
        // @Override
        public void handleMessage(Message msg) {
            //Handle the message
        }
    };

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mPermissionIntent = PendingIntent.getBroadcast(getActivity(), 0, new Intent(ACTION_USB_PERMISSION), 0);
        mLed = false;
        filter = new IntentFilter(ACTION_USB_PERMISSION);
        getActivity().registerReceiver(mUsbReceiver, filter);
        sgfplib = new JSGFPLib((UsbManager)getActivity().getSystemService(Context.USB_SERVICE));
        mMaxTemplateSize = new int[1];
        autoOn = new SGAutoOnEventNotifier(sgfplib, this);
        return inflater.inflate(R.layout.finger_identity_enquiry, container, false);

    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        animation = new AlphaAnimation(1, 0);
        animation.setDuration(200);
        animation.setInterpolator(new LinearInterpolator());
        animation.setRepeatCount(Animation.INFINITE);
        animation.setRepeatMode(Animation.REVERSE);


        mPermissionIntent = PendingIntent.getBroadcast(getActivity(), 0, new Intent(ACTION_USB_PERMISSION), 0);
        mLed = false;
        filter = new IntentFilter(ACTION_USB_PERMISSION);
        getActivity().registerReceiver(mUsbReceiver, filter);
        sgfplib = new JSGFPLib((UsbManager)getActivity().getSystemService(Context.USB_SERVICE));


        mMaxTemplateSize = new int[1];
        autoOn = new SGAutoOnEventNotifier(sgfplib,this);
        mCapture = (Button)getView().findViewById(R.id. button1);
        scannerbar=(TextView) getView().findViewById(R.id.scannerbar);
        //mCapture.setOnClickListener(this);
        mCapture.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                img0.setImageBitmap(null);
            }
        });


        img0 = (CustomImageView)getView().findViewById(R.id.img0);

        img0.setOnClickListener(this);

    }
    public void onClick(View v) {


        if(v.getId()==R.id.img0){

            CaptureFingerPrint();
            if(state=="low") {
                Toast.makeText(getActivity(), "Please recapture again ", Toast.LENGTH_LONG).show();

            }else {

            }
        }
    }
    @Override
    public void onPause() {

        autoOn.stop();

        sgfplib.CloseDevice();
        getActivity(). unregisterReceiver(mUsbReceiver);

        super.onPause();
    }

    @Override
    public void onResume() {
        super.onResume();
        getActivity().registerReceiver(mUsbReceiver, filter);
        long error = sgfplib.Init(SGFDxDeviceName.SG_DEV_AUTO);
        if (error != SGFDxErrorCode.SGFDX_ERROR_NONE) {
            AlertDialog.Builder dlgAlert = new AlertDialog.Builder(getActivity());
            if (error == SGFDxErrorCode.SGFDX_ERROR_DEVICE_NOT_FOUND)
                dlgAlert.setMessage("Please attach the secugen device to continue");
            else
                dlgAlert.setMessage("Fingerprint device initialization failed!");
            dlgAlert.setTitle("SecuGen Fingerprint SDK");
            dlgAlert.setPositiveButton("OK",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int whichButton) {
                            getActivity().finish();
                            return;
                        }
                    }
            );
            dlgAlert.setCancelable(false );
           // dlgAlert.create().show();

        } else {
            UsbDevice usbDevice = sgfplib.GetUsbDevice();
            if (usbDevice == null) {
                AlertDialog.Builder dlgAlert = new AlertDialog.Builder(getActivity());
                dlgAlert.setMessage("SDU04P or SDU03P fingerprint sensor not found!");
                dlgAlert.setTitle("SecuGen Fingerprint SDK");
                dlgAlert.setPositiveButton("OK",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int whichButton) {
                                getActivity().finish();
                                return;
                            }
                        }
                );
                dlgAlert.setCancelable(true);
                dlgAlert.create().show();
            } else {
                sgfplib.GetUsbManager().requestPermission(usbDevice, mPermissionIntent);
                error = sgfplib.OpenDevice(0);
                SecuGen.FDxSDKPro.SGDeviceInfoParam deviceInfo = new SecuGen.FDxSDKPro.SGDeviceInfoParam();
                error = sgfplib.GetDeviceInfo(deviceInfo);
                mImageWidth = deviceInfo.imageWidth;
                mImageHeight = deviceInfo.imageHeight;
                sgfplib.SetTemplateFormat(SGFDxTemplateFormat.TEMPLATE_FORMAT_SG400);
                sgfplib.GetMaxTemplateSize(mMaxTemplateSize);
                mRegisterTemplate0 = new byte[mMaxTemplateSize[0]];
                mRegisterTemplate1 = new byte[mMaxTemplateSize[0]];
                mRegisterTemplate2 = new byte[mMaxTemplateSize[0]];
                mRegisterTemplate3 = new byte[mMaxTemplateSize[0]];
                autoOn.start();



            }
        }
    }
    @Override
    public void onDestroy() {

        sgfplib.CloseDevice();
        sgfplib.Close();
        super.onDestroy();
    }

    //Converts image to grayscale (NEW)
    public Bitmap toGrayscale(byte[] mImageBuffer)
    {
        byte[] Bits = new byte[mImageBuffer.length * 4];
        for (int i = 0; i < mImageBuffer.length; i++) {
            Bits[i * 4] = Bits[i * 4 + 1] = Bits[i * 4 + 2] = mImageBuffer[i]; // Invert the source bits
            Bits[i * 4 + 3] = -1;// 0xff, that's the alpha.
        }

        Bitmap bmpGrayscale = Bitmap.createBitmap(mImageWidth, mImageHeight, Bitmap.Config.ARGB_8888);
        //Bitmap bm contains the fingerprint img
        bmpGrayscale.copyPixelsFromBuffer(ByteBuffer.wrap(Bits));
        return bmpGrayscale;
    }


    //Converts image to grayscale (NEW)
    public Bitmap toGrayscale(Bitmap bmpOriginal)
    {
        int width, height;
        height = bmpOriginal.getHeight();
        width = bmpOriginal.getWidth();
        Bitmap bmpGrayscale = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
        for (int y=0; y< height; ++y) {
            for (int x=0; x< width; ++x){
                int color = bmpOriginal.getPixel(x, y);
                int r = (color >> 16) & 0xFF;
                int g = (color >> 8) & 0xFF;
                int b = color & 0xFF;
                int gray = (r+g+b)/3;
                color = Color.rgb(gray, gray, gray);
                //color = Color.rgb(r/3, g/3, b/3);
                bmpGrayscale.setPixel(x, y, color);
            }
        }
        return bmpGrayscale;
    }

    //Converts image to binary (OLD)
    public Bitmap toBinary(Bitmap bmpOriginal)
    {
        int width, height;
        height = bmpOriginal.getHeight();
        width = bmpOriginal.getWidth();
        Bitmap bmpGrayscale = Bitmap.createBitmap(width, height, Bitmap.Config.RGB_565);
        Canvas c = new Canvas(bmpGrayscale);
        Paint paint = new Paint();

        ColorMatrix cm = new ColorMatrix();
        cm.setSaturation(0);
        ColorMatrixColorFilter f = new ColorMatrixColorFilter(cm);
        paint.setColorFilter(f);
        c.drawBitmap(bmpOriginal, 0, 0, paint);
        return bmpGrayscale;
    }


    public void DumpFile(String fileName, byte[] buffer)
    {
        //Uncomment section below to dump images and templates to SD card
    	/*
        try {
            File myFile = new File("/sdcard/Download/" + fileName);
            myFile.createNewFile();
            FileOutputStream fOut = new FileOutputStream(myFile);
            fOut.write(buffer,0,buffer.length);
            fOut.close();
        } catch (Exception e) {
            debugMessage("Exception when writing file" + fileName);
        }
       */
    }

    public void SGFingerPresentCallback (){
        autoOn.stop();
        fingerDetectedHandler.sendMessage(new Message());
    }

    public void CaptureFingerPrint(){

//        new Handler().post(new Runnable() {
//            @Override
//            public void run() {
//               triggerScannerAnimation();
//            }
//        });

        //CustomerVO customer=null;
        long dwTimeStart = 0, dwTimeEnd = 0, dwTimeElapsed = 0;
        SGFingerInfo fpInfo = new SGFingerInfo();
        byte[] buffer = new byte[mImageWidth*mImageHeight];
        dwTimeStart = System.currentTimeMillis();
        long result = sgfplib.GetImage(buffer);
        DumpFile("capture.raw", buffer);
        boolean[] matched = new boolean[1];
        boolean[] matched1 = new boolean[1];
        boolean[] matched2 = new boolean[1];
        dwTimeElapsed = dwTimeEnd-dwTimeStart;
        int[] qual = new int[1];
       // Details dt=new Details(getActivity());
        sgfplib.GetImageQuality(mImageWidth, mImageHeight,buffer, qual);
        if (qual[0] > 80) {

            result = sgfplib.SetTemplateFormat(SecuGen.FDxSDKPro.SGFDxTemplateFormat.TEMPLATE_FORMAT_SG400);
            result = sgfplib.CreateTemplate(fpInfo, buffer, mRegisterTemplate0);
           state = "okay";
            Bitmap b0=this.toGrayscale(buffer);


            List<Beneficiary> fingerprints = Beneficiary.listAll(Beneficiary.class);
       // Log.d("ERROR",fingerprints.get(0).getCustomerVO()+"yyyy");
       /*     //Toast.makeText(getActivity(),String.valueOf(fingerprints.size()),Toast.LENGTH_LONG).show();
        try {

            for (Beneficiary fp: fingerprints) {
                //Log.d("ERROR",fingerprints.get(0).getPosition()+"yyyy");
                byte[] imageAsBytes = Base64.decode(fp.getfingerprint().getBytes(), Base64.DEFAULT);
                  result= sgfplib.MatchTemplate(mRegisterTemplate0, imageAsBytes, SGFDxSecurityLevel.SL_NORMAL, matched);
                if(matched[0]) {
                    //customer = fp.getCustomerVO();
                    break;
                }
            }
        }catch(Exception e){

            Log.d("ERROR",e.toString());

        }*/
           /* if(customer!=null){
                showDialog("Identification", "Welcome '" +"'!\n"+"\nNAME: "+
                        customer.getFirstName()+"\nMOBILE: "+customer.getMobileNumber()+"\n\nYour identity has been identified successfully.");

            } else{
                showDialog("Identification", "You are not enrolled");
            }
*/
//                ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
//                b0.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream);
//                byte[] byteArray = byteArrayOutputStream .toByteArray();
//                encoded_lthumb = Base64.encodeToString(byteArray, Base64.DEFAULT);
               // dt.setLthumb( encoded_lthumb);
            img0.setImageBitmap(b0);
            //scannerbar.setVisibility(View.INVISIBLE);


        }else{
           state="low";
        }
    }


    void showDialog(String title, String message)
    {
        AlertDialog alertDialog = new AlertDialog.Builder(getActivity()).create();
        alertDialog.setTitle(title);
        alertDialog.setMessage(message);
        //alertDialog.setIcon(R.drawable.ic_menu_notifications);
        alertDialog.setButton("OK", new DialogInterface.OnClickListener()
        {
            public void onClick(DialogInterface dialog, int which)
            {
                //Do nothing
            }
        });
        alertDialog.show();
    }


    @Override
    public void run() {


        //ByteBuffer buffer = ByteBuffer.allocate(1);
        //UsbRequest request = new UsbRequest();
        //request.initialize(mSGUsbInterface.getConnection(), mEndpointBulk);
        //byte status = -1;
        while (true) {


            // queue a request on the interrupt endpoint
            //request.queue(buffer, 1);
            // send poll status command
            //  sendCommand(COMMAND_STATUS);
            // wait for status event
            /*
            if (mSGUsbInterface.getConnection().requestWait() == request) {
                byte newStatus = buffer.get(0);
                if (newStatus != status) {
                    Log.d(TAG, "got status " + newStatus);
                    status = newStatus;
                    if ((status & COMMAND_FIRE) != 0) {
                        // stop firing
                        sendCommand(COMMAND_STOP);
                    }
                }
                try {
                    Thread.sleep(100);
                } catch (InterruptedException e) {
                }
            } else {
                Log.e(TAG, "requestWait failed, exiting");
                break;
            }
            */
        }
    }
}
