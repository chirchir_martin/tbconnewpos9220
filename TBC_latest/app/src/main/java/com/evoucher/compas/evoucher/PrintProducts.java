package com.evoucher.compas.evoucher;



/**
 * Created by Administrator on 8/31/2016.
 */
public class PrintProducts{
    private Long id;
    private String productid;
    private String quantity;
    private String totalprice;
    private String programmeid;
    private String quantitydeducted;
    private String bendrp;
    private String uom;
    private String cardnumber;

    private String voucherid;

    public PrintProducts(){

    }
    public void setProductId(String productid){
        this.productid=productid;
    }
    public String getProductId(){
        return this.productid;
    }
    public void setQuSantity(String quantity){
       this.quantity=quantity;
    }
    public String getQuantity(){
        return this.quantity;
    }
    public void setTotalprice(String totalprice){
        this.totalprice=totalprice;
    }
    public String getTotalprice(){
        return this.totalprice;
    }
    public String getVoucherid() {
        return voucherid;
    }

    public String getQuantitydeducted() {
        return quantitydeducted;
    }

    public String getBendrp() {
        return bendrp;
    }

    public String getUom() {
        return uom;
    }

    public void setUom(String uom) {
        this.uom = uom;
    }

    public void setBendrp(String bendrp) {
        this.bendrp = bendrp;
    }

    public void setQuantitydeducted(String quantitydeducted) {
        this.quantitydeducted = quantitydeducted;
    }

    public String getProgrammeid() {
        return programmeid;
    }

    public void setProgrammeid(String programmeid) {
        this.programmeid = programmeid;
    }

    public String getCardnumber() {
        return cardnumber;
    }

    public void setCardnumber(String cardnumber) {
        this.cardnumber = cardnumber;
    }

    public void setVoucherid(String voucherid) {
        this.voucherid = voucherid;
    }
}
