package com.evoucher.compas.evoucher;

import android.annotation.TargetApi;
import android.app.PendingIntent;
import android.content.Intent;
import android.content.IntentFilter;
import android.nfc.NdefMessage;
import android.nfc.NdefRecord;
import android.nfc.NfcAdapter;
import android.nfc.Tag;
import android.nfc.tech.Ndef;
import android.nfc.tech.NdefFormatable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Parcelable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.afollestad.materialdialogs.MaterialDialog;
import com.evoucher.compas.evoucher.card.KeyInfoProvider;
import com.evoucher.compas.evoucher.card.SampleAppKeys;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.lynx.evoucher.models.Beneficiary;
import com.lynx.evoucher.models.Member;
import com.lynx.evoucher.models.Programmes;
import com.lynx.evoucher.models.Topups;
import com.newpos.libpay.device.card.CardInfo;
import com.newpos.libpay.device.card.CardListener;
import com.newpos.libpay.device.card.CardManager;
import com.nxp.nfclib.CardType;
import com.nxp.nfclib.KeyType;
import com.nxp.nfclib.NxpNfcLib;
import com.nxp.nfclib.desfire.DESFireFactory;
import com.nxp.nfclib.desfire.DESFireFile;
import com.nxp.nfclib.desfire.EV1ApplicationKeySettings;
import com.nxp.nfclib.desfire.IDESFireEV1;
import com.nxp.nfclib.exceptions.NxpNfcLibException;
import com.nxp.nfclib.interfaces.IKeyData;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Type;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;

import javax.crypto.Cipher;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.IvParameterSpec;

import cn.pedant.SweetAlert.SweetAlertDialog;

public class Beneficairy_Activity extends AppCompatActivity {

    private IKeyData objKEY_2KTDES_ULC = null;
    private IKeyData objKEY_2KTDES = null;
    private IKeyData objKEY_AES128 = null;
    private byte[] default_ff_key = null;
    private IKeyData default_zeroes_key = null;
    private NxpNfcLib libInstance = null;
    private IDESFireEV1 desFireEV1;
    private CardType mCardType = CardType.UnknownCard;
    private Cipher cipher = null;
    private byte[] bytesKey = null;
    private IvParameterSpec iv = null;
    MaterialDialog pDialogg;
    private static final String KEY_APP_MASTER = "This is my key  ";
    private static final String ALIAS_KEY_AES128 = "key_aes_128";
    private static final String ALIAS_KEY_2KTDES = "key_2ktdes";
    private static final String ALIAS_KEY_2KTDES_ULC = "key_2ktdes_ulc";
    private static final String ALIAS_DEFAULT_FF = "alias_default_ff";
    private static final String ALIAS_KEY_AES128_ZEROES = "alias_default_00";
    private static final String EXTRA_KEYS_STORED_FLAG = "keys_stored_flag";
    static String packageKey = "b927aab8842ab54cbaf2ea1df9917159";
    byte[] appId = new byte[]{0x1, 0x00, 0x00};
    int fileSize = 100;
    int timeOut = 4000;
    int fileNo = 0;

    boolean aflag = false;
    TextView name, gender, dob, idno, programme, cardno;
    NfcAdapter nfcAdapter;
    Button activate;
    List<Beneficiary> beneficiary = new ArrayList<>();
    Beneficiary ben;
    StringBuilder sb;
    // List<Topups> topups;
    public static String status = "", benname = "";
    MaterialDialog dialog;
    ImageView act;
    Details dt;
    String rationNo = "";
    JSONArray topUps, aa;
    JSONObject personalData, cardpin;
    String data="";
    ToggleButton tglReadWrite;
    EditText txtTagContent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_beneficairy_);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(getString(R.string.CardHolderDetails));

        initializeLibrary();
        initializeKeys();
        initializeCipherinitVector();

        try {

            nfcAdapter = NfcAdapter.getDefaultAdapter(this);
            dt = new Details(this);
            ben = dt.getBeneficiary();
            activate = (Button) findViewById(R.id.activate_card);
            act = (ImageView) findViewById(R.id.state);
            name = (TextView) findViewById(R.id.name);
            gender = (TextView) findViewById(R.id.gender);
            dob = (TextView) findViewById(R.id.dob);
            idno = (TextView) findViewById(R.id.idno);
            programme = (TextView) findViewById(R.id.programme);
            cardno = (TextView) findViewById(R.id.cardno);
            name.setText(getString(R.string.Name) + ": " + ben.getFirstname().toUpperCase());
            gender.setText(getString(R.string.Gender) + ben.getGender());
            dob.setText(getString(R.string.DOB) + ben.getDate_of_birth());
            idno.setText(getString(R.string.RatioNo) + ben.getNational_id());
            cardno.setText(getString(R.string.CardNo) + ": " + ben.getCard_number());
            programme.setText("Programme: " + Programmes.find(Programmes.class, "programid =?", ben.getProgrammeid()).get(0).getProgrammename());

        } catch (Exception e) {

            Log.i("###EEEEE", e.toString());

        }
        // activate =(Button) findViewById(R.i)

        activate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                aflag = true;
                dialog = new MaterialDialog.Builder(Beneficairy_Activity.this)
                        .title(R.string.TapCard)
                        .content("")
                        .progress(true, 0)
                        .progressIndeterminateStyle(true)
                        .cancelable(false)
                        .show();
                final Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        dialog.dismiss();
                        aflag = false;

                    }

                }, 8000);

                personalise_card_balance(com.newpos.libpay.device.card.CardType.INMODE_NFC.getVal(),10000);

            }
        });

    }

    @Override
    protected void onPause() {
        libInstance.stopForeGroundDispatch();
        super.onPause();
    }


    @Override
    protected void onResume() {
        libInstance.startForeGroundDispatch();
        super.onResume();
    }


    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        setIntent(intent);
        if (aflag) {
            CardType type = CardType.UnknownCard;
            try {
                type = libInstance.getCardType(intent);
            } catch (NxpNfcLibException ex) {
                // Toast.makeText(Beneficairy_Activity.this, "Error", Toast.LENGTH_SHORT).show();
                pDialogg.dismiss();
                new SweetAlertDialog(Beneficairy_Activity.this, SweetAlertDialog.WARNING_TYPE)
                        .setTitleText(getString(R.string.nfc_error))
                        .setContentText(getString(R.string.nfc_error_totle))
                        .setConfirmText(getString(R.string.Ok))
                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sDialog) {
                                sDialog.dismissWithAnimation();

                            }
                        })
                        .show();
            }

            switch (type) {

                case DESFireEV1: {
                    mCardType = CardType.DESFireEV1;
                    desFireEV1 = DESFireFactory.getInstance().getDESFire(libInstance.getCustomModules());
                    try {
                        try {
                            dialog.dismiss();
                        } catch (Exception e) {

                        }
                        desFireEV1.getReader().connect();
                        desFireEV1.getReader().setTimeout(2000);
                        desfireEV1CardLogic();


                    } catch (Exception t) {
                        t.printStackTrace();
                        Toast.makeText(Beneficairy_Activity.this, "Error", Toast.LENGTH_SHORT).show();
                        Log.d("##ERRORRR", t.toString());

                    }
                    break;
                }


            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        // getMenuInflater().inflate(nfctutorials.tutorial04.R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == android.R.id.home) {

            Intent intent1 = new Intent(this, ProgrammeActivity.class);
            startActivity(intent1);
            finish();

        }

        //noinspection SimplifiableIfStatement
       /* if (id == nfctutorials.tutorial04.R.id.action_settings) {
            return true;
        }*/

        return super.onOptionsItemSelected(item);
    }


    private void enableForegroundDispatchSystem() {

        Intent intent = new Intent(this, Beneficairy_Activity.class).addFlags(Intent.FLAG_RECEIVER_REPLACE_PENDING);

        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, 0);

        IntentFilter[] intentFilters = new IntentFilter[]{};

        nfcAdapter.enableForegroundDispatch(this, pendingIntent, intentFilters, null);
    }

    public void cardLogic(final Intent intent) {
        CardType type = CardType.UnknownCard;
        try {
            type = libInstance.getCardType(intent);
        } catch (NxpNfcLibException ex) {
            //  Toast.makeText(getActivity(), ex.getMessage(), Toast.LENGTH_SHORT).show();
        }

        switch (type) {

            case DESFireEV1: {
                mCardType = CardType.DESFireEV1;
                desFireEV1 = DESFireFactory.getInstance().getDESFire(libInstance.getCustomModules());
                try {

                    desFireEV1.getReader().connect();
                    desFireEV1.getReader().setTimeout(2000);

                    desfireEV1CardLogic();

                } catch (Exception t) {
                    t.printStackTrace();
                    Log.d("##ERRORRR", t.toString());

                }
                break;
            }


        }
    }

    @TargetApi(19)
    private void initializeLibrary() {
        libInstance = NxpNfcLib.getInstance();
        try {
            libInstance.registerActivity(Beneficairy_Activity.this, packageKey);
        } catch (NxpNfcLibException ex) {
            // Toast.makeText(this, ex.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    /**
     * Initialize the Cipher and init vector of 16 bytes with 0xCD.
     */


    private void desfireEV1CardLogic() {
        pDialogg = new MaterialDialog.Builder(Beneficairy_Activity.this)
                .title("Card Activation")
                .content("Please dont remove the card...")
                .progress(true, 0)
                .progressIndeterminateStyle(false)
                .show();
        pDialogg.setCancelable(false);
        new ActivateCard().execute();


    }

    private void initializeKeys() {
        KeyInfoProvider infoProvider = KeyInfoProvider.getInstance(Beneficairy_Activity.this.getApplicationContext());
        infoProvider.setKey(ALIAS_KEY_2KTDES, SampleAppKeys.EnumKeyType.EnumDESKey, SampleAppKeys.KEY_2KTDES);
        infoProvider.setKey(ALIAS_KEY_AES128, SampleAppKeys.EnumKeyType.EnumAESKey, SampleAppKeys.KEY_AES128);
        infoProvider.setKey(ALIAS_KEY_AES128_ZEROES, SampleAppKeys.EnumKeyType.EnumAESKey, SampleAppKeys.KEY_AES128_ZEROS);
        infoProvider.setKey(ALIAS_DEFAULT_FF, SampleAppKeys.EnumKeyType.EnumMifareKey, SampleAppKeys.KEY_DEFAULT_FF);
        objKEY_2KTDES_ULC = infoProvider.getKey(ALIAS_KEY_2KTDES_ULC, SampleAppKeys.EnumKeyType.EnumDESKey);
        objKEY_2KTDES = infoProvider.getKey(ALIAS_KEY_2KTDES, SampleAppKeys.EnumKeyType.EnumDESKey);
        objKEY_AES128 = infoProvider.getKey(ALIAS_KEY_AES128, SampleAppKeys.EnumKeyType.EnumAESKey);
        default_zeroes_key = infoProvider.getKey(ALIAS_KEY_AES128_ZEROES, SampleAppKeys.EnumKeyType.EnumAESKey);
        default_ff_key = infoProvider.getMifareKey(ALIAS_DEFAULT_FF);
    }


    private void initializeCipherinitVector() {

		/* Initialize the Cipher */
        try {
            cipher = Cipher.getInstance("AES/CBC/NoPadding");
        } catch (NoSuchAlgorithmException e) {

            e.printStackTrace();
        } catch (NoSuchPaddingException e) {
            e.printStackTrace();
        }

		/* set Application Master Key */
        bytesKey = KEY_APP_MASTER.getBytes();

		/* Initialize init vector of 16 bytes with 0xCD. It could be anything */
        byte[] ivSpec = new byte[16];
        Arrays.fill(ivSpec, (byte) 0xCD);
        iv = new IvParameterSpec(ivSpec);

    }


    private class ActivateCard extends AsyncTask<String, String, String> {

        @Override
        protected String doInBackground(String... params) {
            try {

                String uidhex = new String();
                byte[] uid = desFireEV1.getUID();
                for (int i = 0; i < uid.length; i++) {
                    String x = Integer.toHexString(((int) uid[i] & 0xff));
                    if (x.length() == 1) {
                        x = '0' + x;
                    }
                    uidhex += x;
                }
                Log.i("######UID", String.valueOf(ben.getNational_id()));

                personalData = new JSONObject();
                personalData.put("name", ben.getFirstname());
                personalData.put("rationalNo",Integer.parseInt(ben.getNational_id().trim()));
                personalData.put("groupId", Integer.parseInt(ben.getBeneficiary_group_id().trim()));
                Log.i("######UrrrID", personalData.toString() + "    kkffkgfgfgf");

                cardpin = new JSONObject();
                cardpin.put("pin", ben.getCard_pin());
                cardpin.put("cardNo", ben.getCard_number());
                Log.i("####SASASASa", cardpin.toString());
/*
                        topups = new JSONObject();
                        topups.put("vochervalue", ts.get(0).getVouchervalue());
                        topups.put("voucherno",ts.get(0).getVocheridno());
                        topups.put("programmeid", ts.get(0).getProgrammeId());
                        topups.put("voucherid", ts.get(0).getVoucherId());
                        topups.put("benid",  ts.get(0).getBeneficiaryId());
                        topups.put("cardno", ts.get(0).getCardNumber());
                        topups.put("bengrpId", ts.get(0).getBengroup());*/


            } catch (Exception ex) {

                Log.i("dsssssssssdsdsd", ex.toString());
                return "0";
            }

            return "1";
        }


        protected void onPostExecute(final String result) {
            Log.d("#####RESPONSE", result);
            try {
                  String name = personalData.getString("name");
                  personalData.put("name",name.replaceAll(" ","   ").toLowerCase());
                   Log.d("##name", personalData.toString());
            }catch (Exception e){
                Log.d("##name", e.toString());
            }
            if (result.equalsIgnoreCase("1")) {
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            desFireEV1.getReader().setTimeout(timeOut);
                            desFireEV1.selectApplication(0);
                            try {
                                desFireEV1.selectApplication(appId);
                                desFireEV1.authenticate(0, IDESFireEV1.AuthType.Native, KeyType.TWO_KEY_THREEDES, objKEY_2KTDES);
                                rationNo = new JSONObject(new String(desFireEV1.readData(Card_Details.personalDetails, 0, 0))).getString("rationalNo");
                                benname = new JSONObject(new String(desFireEV1.readData(Card_Details.personalDetails, 0, 0))).getString("name");
                            } catch (Exception e) {
                                Log.i("####xxdffdfdfd", e.toString());
                                rationNo = "";
                            }

                            if (!(rationNo.length() > 0)) {
                                desFireEV1.selectApplication(0);
                                desFireEV1.authenticate(0, IDESFireEV1.AuthType.Native, KeyType.THREEDES, objKEY_2KTDES);
                                desFireEV1.format();
                                EV1ApplicationKeySettings.Builder appsetbuilder = new EV1ApplicationKeySettings.Builder();
                                EV1ApplicationKeySettings appsettings = appsetbuilder.setAppKeySettingsChangeable(true)
                                        .setAppMasterKeyChangeable(true)
                                        .setAuthenticationRequiredForApplicationManagement(false)
                                        .setAuthenticationRequiredForDirectoryConfigurationData(false)
                                        .setKeyTypeOfApplicationKeys(KeyType.TWO_KEY_THREEDES).build();

                                desFireEV1.createApplication(appId, appsettings);
                                desFireEV1.selectApplication(appId);
                                //Create files
                                desFireEV1.createFile(Card_Details.personalDetails, new DESFireFile.StdDataFileSettings(
                                        IDESFireEV1.CommunicationType.Plain, (byte) 0, (byte) 0, (byte) 0, (byte) 0, 1000));
                                desFireEV1.createFile(Card_Details.cardDetails, new DESFireFile.StdDataFileSettings(
                                        IDESFireEV1.CommunicationType.Plain, (byte) 0, (byte) 0, (byte) 0, (byte) 0, 1000));
                                desFireEV1.createFile(Card_Details.cpin, new DESFireFile.StdDataFileSettings(
                                        IDESFireEV1.CommunicationType.Plain, (byte) 0, (byte) 0, (byte) 0, (byte) 0, 100));
                                desFireEV1.createFile(Card_Details.carddata, new DESFireFile.StdDataFileSettings(
                                        IDESFireEV1.CommunicationType.Plain, (byte) 0, (byte) 0, (byte) 0, (byte) 0, 3000));
                                desFireEV1.createFile(Card_Details.transactionHistory, new DESFireFile.StdDataFileSettings(
                                        IDESFireEV1.CommunicationType.Plain, (byte) 0, (byte) 0, (byte) 0, (byte) 0, 200));
                                //write Data
                                desFireEV1.authenticate(0, IDESFireEV1.AuthType.Native, KeyType.TWO_KEY_THREEDES, objKEY_2KTDES);
                                desFireEV1.writeData(Card_Details.personalDetails, 0, personalData.toString().getBytes());
                                desFireEV1.writeData(Card_Details.cpin, 0,  cardpin.toString().toString().trim().getBytes());
                                desFireEV1.getReader().close();
                                Log.d("####EERRRRRr", String.valueOf(desFireEV1.getFreeMemory()));
                                Beneficairy_Activity.this.runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                aflag = false;
                                pDialogg.dismiss();
                                alertSuccess();
                            }
                                });
                            } else {
                                Beneficairy_Activity.this.runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        pDialogg.dismiss();
                                        new SweetAlertDialog(Beneficairy_Activity.this, SweetAlertDialog.WARNING_TYPE)
                                                .setTitleText("Sorry")
                                                .setContentText("This card is already belongs to " + benname.replace("   "," ")+ " Ration No: " + rationNo)
                                                .setConfirmText("Ok")
                                                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                                    @Override
                                                    public void onClick(SweetAlertDialog sDialog) {
                                                        sDialog.dismissWithAnimation();

                                                    }
                                                })
                                                .show();
                                    }
                                });
                            }

                        } catch (final Exception err) {
                            Beneficairy_Activity.this.runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    pDialogg.dismiss();
                                    aflag = false;

                                    //Toast.makeText(getActivity(), err.getMessage(), Toast.LENGTH_LONG).show();

                                    Log.i("####EERRRRRr", err.toString());
                                    err.printStackTrace();
                                    new SweetAlertDialog(Beneficairy_Activity.this, SweetAlertDialog.WARNING_TYPE)
                                            .setTitleText("Error")
                                            .setContentText("Card not successfully activated.Please tap card and wait")
                                            .setConfirmText("Ok")
                                            .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                                @Override
                                                public void onClick(SweetAlertDialog sDialog) {
                                                    sDialog.dismissWithAnimation();

                                                }
                                            })
                                            .show();
                                }
                            });
                        }
                    }
                }).start();


            } else if (result.equalsIgnoreCase("0")) {

                aflag = false;
                pDialogg.dismiss();
                new SweetAlertDialog(Beneficairy_Activity.this, SweetAlertDialog.WARNING_TYPE)
                        .setTitleText(getString(R.string.Errorr))
                        .setContentText(getString(R.string.ErrorFetchingData))
                        .setConfirmText(getString(R.string.Ok))
                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sDialog) {
                                sDialog.dismissWithAnimation();

                            }
                        })
                        .show();

            }

        }
    }
    public void alertSuccess() {
        aflag = false;
        final SweetAlertDialog s = new SweetAlertDialog(Beneficairy_Activity.this, SweetAlertDialog.SUCCESS_TYPE);
        s.setTitleText(getString(R.string.Great))
                .setContentText(getString(R.string.CardActivated))
                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sDialog) {

                        sDialog.dismissWithAnimation();
                        startActivity(new Intent(Beneficairy_Activity.this, ProgrammeActivity.class));
                        finish();

                    }
                }).show();
        s.setCancelable(false);
    }
    public CardManager personalise_card_balance(int mode, int timeout) {
        JSONObject carddata= new JSONObject();
        try {

            personalData = new JSONObject();
            personalData.put("name", ben.getFirstname());
            personalData.put("rationalNo", Integer.parseInt(ben.getNational_id().trim()));
            personalData.put("groupId", Integer.parseInt(ben.getBeneficiary_group_id().trim()));
            Log.i("######UrrrID", personalData.toString() + "    kkffkgfgfgf");
            carddata.put("personaldetails",personalData);
            cardpin = new JSONObject();
            cardpin.put("pin", ben.getCard_pin());
            cardpin.put("cardNo", ben.getCard_number());
            carddata.put("cardpin",cardpin);
            data=carddata.toString();

        }catch (Exception e){

            data=null;

        }
        final CardInfo cInfo = new CardInfo();
        CardManager cardManager = CardManager.getInstance(this, mode,"personalise",data.getBytes());
        cardManager.getCard(timeout, new CardListener() {
            @Override
            public void callback(CardInfo cardInfo) {
                dialog.dismiss();
                status ="";
                if(cardInfo.isResultFalg()){
                    Beneficairy_Activity.this.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            aflag = false;
                            pDialogg.dismiss();
                            alertSuccess();
                        }
                    });

                }else{


                    Beneficairy_Activity.this.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            pDialogg.dismiss();
                            aflag = false;
                            new SweetAlertDialog(Beneficairy_Activity.this, SweetAlertDialog.WARNING_TYPE)
                                    .setTitleText("Error")
                                    .setContentText("Card not successfully activated.Please tap card and wait")
                                    .setConfirmText("Ok")
                                    .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                        @Override
                                        public void onClick(SweetAlertDialog sDialog) {
                                            sDialog.dismissWithAnimation();

                                        }
                                    })
                                    .show();
                        }
                    });
                }

            }
        });

        return cardManager;
    }


}
