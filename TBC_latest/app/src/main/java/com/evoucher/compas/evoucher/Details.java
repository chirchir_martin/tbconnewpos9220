package com.evoucher.compas.evoucher;

import android.content.Context;

import com.lynx.evoucher.models.Beneficiary;

import org.json.JSONArray;
import org.json.JSONObject;

/**
 * Created by Administrator on 8/31/2016.
 */
public class Details {
    public  static  String voucherid="";
    public  static  String programmeid="";
    public  static  String cardnumber="";
    public  static  String status="";
    public  static  String error="";
    public  static  String level="";
    static Beneficiary beneficiary =null;
    public  static  String valuevoucher="";
    public  static JSONObject topups=null;
    public  static  String uid="";
    public  static  String downloadstatus="";
    public  static  String cycle="";
    public  static  String voucherno="";
    public  static  String benGroup="";
    public  static  String mac="";
    public  static  String rationNo="";
    public  static  String tcount="";
    public  static  String netsales="";
    public  static  String date="";
    public  static  String vcount="";
    public  static  String vAmount="";
    public  static  String sales="";

    public static String getTime() {
        return time;
    }

    public static void setTime(String time) {
        Details.time = time;
    }

    public  static  String time="";
    public Details(Context context){

    }

    public static String getUser() {
        return user;
    }

    public static void setUser(String user) {
        Details.user = user;
    }

    public static String getTcount() {
        return tcount;
    }

    public static void setTcount(String tcount) {
        Details.tcount = tcount;
    }

    public static String getNetsales() {
        return netsales;
    }

    public static void setNetsales(String netsales) {
        Details.netsales = netsales;
    }

    public static String getDate() {
        return date;
    }

    public static void setDate(String date) {
        Details.date = date;
    }

    public static String getVcount() {
        return vcount;
    }

    public static void setVcount(String vcount) {
        Details.vcount = vcount;
    }

    public static String getvAmount() {
        return vAmount;
    }

    public static void setvAmount(String vAmount) {
        Details.vAmount = vAmount;
    }

    public static String getSales() {
        return sales;
    }

    public static void setSales(String sales) {
        Details.sales = sales;
    }

    public  static  String user="";

    public static String getValuevoucher() {
        return valuevoucher;
    }

    public Beneficiary getBeneficiary() {
        return beneficiary;
    }

    public static JSONObject getTopups() {
        return topups;
    }

    public static void setTopups(JSONObject topups) {
        Details.topups = topups;
    }

    public void setBeneficiary(Beneficiary beneficiary) {
        this.beneficiary = beneficiary;
    }

    public static void setValuevoucher(String valuevoucher) {
        Details.valuevoucher = valuevoucher;
    }

    public static String getRationNo() {
        return rationNo;
    }

    public static void setRationNo(String rationNo) {
        Details.rationNo = rationNo;
    }

    public static String getUid() {
        return uid;
    }

    public static String getDownloadstatus() {
        return downloadstatus;
    }

    public static void setDownloadstatus(String downloadstatus) {
        Details.downloadstatus = downloadstatus;
    }

    public static String getCycle() {
        return cycle;
    }

    public static void setCycle(String cycle) {
        Details.cycle = cycle;
    }

    public static void setUid(String uid) {
        Details.uid = uid;
    }

    public static String getBenGroup() {
        return benGroup;
    }

    public static String getVoucherno() {
        return voucherno;
    }

    public static String getError() {
        return error;
    }

    public static void setError(String error) {
        Details.error = error;
    }

    public static void setVoucherno(String voucherno) {
        Details.voucherno = voucherno;
    }



    public static void setBenGroup(String benGroup) {
        Details.benGroup = benGroup;
    }

    public static String getStatus() {
        return status;
    }

    public static void setStatus(String status) {
        Details.status = status;
    }

    public static String getCardnumber() {
        return cardnumber;
    }

    public static void setCardnumber(String cardnumber) {
        Details.cardnumber = cardnumber;
    }

    public static String getMac() {
        return mac;
    }

    public static String getLevel() {
        return level;
    }

    public static void setLevel(String level) {
        Details.level = level;
    }

    public static void setMac(String mac) {
        Details.mac = mac;
    }

    public String getVoucherid(){
        return voucherid;
    }
    public void setVoucherid(String voucherid){
        this.voucherid=voucherid;
    }
    public String getProgrammeid(){
        return programmeid;
    }
    public void setProgrammeid(String programmeid){
        this.programmeid=programmeid;
    }
}
