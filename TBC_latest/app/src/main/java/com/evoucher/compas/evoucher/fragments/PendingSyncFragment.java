package com.evoucher.compas.evoucher.fragments;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.evoucher.compas.evoucher.Details;
import com.evoucher.compas.evoucher.ItemClickListener;
import com.evoucher.compas.evoucher.R;
import com.evoucher.compas.evoucher.p2p.DeviceListFragment;
import com.lynx.evoucher.models.Beneficiary;
import com.lynx.evoucher.models.Commodities;
import com.lynx.evoucher.models.Products;
import com.lynx.evoucher.models.Topups;
import com.lynx.evoucher.models.TopupsLogs;
import com.lynx.evoucher.models.Transactions;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import cn.pedant.SweetAlert.SweetAlertDialog;

/**
 * Created by Administrator on 10/25/2016.
 */
public class PendingSyncFragment extends Fragment implements ItemClickListener {
    RecyclerView recyclerView;

    ProgressDialog  progressDialog;
    TextView txns, tpslogs;
    Details dt;
    ViewGroup x;
    List<Transactions> transaction_table;
    List<TopupsLogs> topups_table;
    Button reset_txns, reset_tps, clear_txns, clear_tps;
    ArrayList<String> programIDs = new ArrayList<String>();
    private Button reset_txns1;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        return inflater.inflate(R.layout.pendingsync_fragment, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        dt = new Details(getActivity());
        txns = (TextView) getView().findViewById(R.id.txns);
        reset_tps = (Button) getView().findViewById(R.id.reset_tps);
        reset_tps.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new MaterialDialog.Builder(getActivity())
                        .iconRes(R.drawable.ecompass)
                        .limitIconToDefaultSize()
                        .title("RESET TOPUPS LOGS")
                        .content("Are you Sure you want to reset these topups")
                        .positiveText("Yes")
                        .negativeText("No")
                        .callback(new MaterialDialog.ButtonCallback() {
                            @Override
                            public void onPositive(MaterialDialog dialog) {
                                progressDialog = ProgressDialog.show(getActivity(), "Resetting", "Please wait", true,
                                        true, new DialogInterface.OnCancelListener() {
                                            @Override
                                            public void onCancel(DialogInterface dialog) {

                                            }
                                        });
                                progressDialog.setCancelable(false);

                                new Thread(new Runnable() {
                                    @Override
                                    public void run() {
                                  topups_table =TopupsLogs.find(TopupsLogs.class,"isuploaded =?","1");
                                        for(TopupsLogs tlogs:topups_table){
                                            tlogs.setIsuploaded("0");
                                            tlogs.save();
                                        }

                                        getActivity().runOnUiThread(new Runnable() {
                                            @Override
                                            public void run() {
                                                renderUi();
                                                progressDialog.dismiss();
                                                new SweetAlertDialog(getActivity(), SweetAlertDialog.SUCCESS_TYPE)
                                                        .setTitleText("Great")
                                                        .setContentText("Topup logs reset successfully")
                                                        .setConfirmText("Ok")
                                                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                                            @Override
                                                            public void onClick(SweetAlertDialog sDialog) {
                                                                sDialog.dismissWithAnimation();

                                                            }
                                                        })
                                                        .show();
                                            }
                                        });

                                    }
                                }).start();


                            }

                            @Override
                            public void onNegative(MaterialDialog dialog) {

                            }
                        })
                        .show();


            }
        });
        clear_tps = (Button) getView().findViewById(R.id.clear_tps);
        clear_tps.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new MaterialDialog.Builder(getActivity())
                        .iconRes(R.drawable.ecompass)
                        .limitIconToDefaultSize()
                        .title("CLEAR TOPUPS LOGS")
                        .content("Please ensure the topups logs has been synchronized before deleting")
                        .positiveText("Yes")
                        .negativeText("No")
                        .callback(new MaterialDialog.ButtonCallback() {
                            @Override
                            public void onPositive(MaterialDialog dialog) {
                                progressDialog = ProgressDialog.show(getActivity(), "Clearing", "Please wait", true,
                                        true, new DialogInterface.OnCancelListener() {
                                            @Override
                                            public void onCancel(DialogInterface dialog) {

                                            }
                                        });
                                progressDialog.setCancelable(false);


                                new Thread(new Runnable() {
                                    @Override
                                    public void run() {

                                        topups_table =TopupsLogs.find(TopupsLogs.class,"isuploaded =?","1");

                                        JSONObject topupLogs =null;
                                        JSONArray topUpsLogs = new JSONArray();



                                        if (topups_table.size() > 0) {
                                            for (int i = 0; i <   topups_table.size(); i++) {
                                                try {
                                                    topupLogs = new JSONObject();
                                                    topupLogs.put("cardNumber",   topups_table.get(i).getCardno());
                                                    topupLogs.put("newTopupValue",   topups_table.get(i).getNtopupvalue());
                                                    topupLogs.put("newVoucherNo",   topups_table.get(i).getNvoucheridno());
                                                    topupLogs.put("newCardBal",   topups_table.get(i).getNcardbal());
                                                    topupLogs.put("oldVoucherNo",   topups_table.get(i).getOvoucheridno());
                                                    topupLogs.put("oldCardBal",   topups_table.get(i).getOcardbal());
                                                    topupLogs.put("topupTime",   topups_table.get(i).getTopuptime());
                                                    topupLogs.put("deviceId",   topups_table.get(i).getDeviceidno());
                                                    topupLogs.put("username",   topups_table.get(i).getUsername());
                                                    topupLogs.put("refNo",   topups_table.get(i).getRefno());

                                                    topUpsLogs.put(topupLogs);
                                                } catch (Exception e) {

                                                }
                                            }

                                            //create folder
                                            File folder = new File(Environment.getExternalStorageDirectory() +
                                                    File.separator + "Tbc_Topups_Logs");
                                            boolean success = true;
                                            if (!folder.exists()) {
                                                success = folder.mkdirs();

                                            }else {

                                            }

                                            if (success) {
                                                // Do something on success
                                            } else {
                                                // Do something else on failure
                                            }

                                            writeToFile(topUpsLogs.toString(),folder);

                                        }

                                        for(TopupsLogs tlogs:topups_table){
                                            tlogs.delete();
                                        }


                                        getActivity().runOnUiThread(new Runnable() {

                                            @Override
                                            public void run() {
                                                renderUi();
                                                progressDialog.dismiss();
                                                new SweetAlertDialog(getActivity(), SweetAlertDialog.SUCCESS_TYPE)
                                                        .setTitleText("Great")
                                                        .setContentText("Topups Logs cleared successfully")
                                                        .setConfirmText("Ok")
                                                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                                            @Override
                                                            public void onClick(SweetAlertDialog sDialog) {
                                                                sDialog.dismissWithAnimation();

                                                            }
                                                        })
                                                        .show();
                                            }
                                        });

                                    }
                                }).start();


                            }

                            @Override
                            public void onNegative(MaterialDialog dialog) {

                            }
                        })
                        .show();


            }
        });
        reset_txns = (Button) getView().findViewById(R.id.reset_txns);
        reset_txns.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new MaterialDialog.Builder(getActivity())
                        .iconRes(R.drawable.ecompass)
                        .limitIconToDefaultSize()
                        .title("RESET PENDING TRANSACTIONS")
                        .content("Are you Sure you want to reset these transactions?")
                        .positiveText("Yes")
                        .negativeText("No")
                        .callback(new MaterialDialog.ButtonCallback() {
                            @Override
                            public void onPositive(MaterialDialog dialog) {
                                Log.i("##DSDSDSD","lldlsdllsdlkkk");
                                progressDialog = ProgressDialog.show(getActivity(), "Resetting", "Please wait", true,
                                        true, new DialogInterface.OnCancelListener() {
                                            @Override
                                            public void onCancel(DialogInterface dialog) {

                                            }
                                        });
                                progressDialog.setCancelable(false);
                                new Thread(new Runnable() {
                                    @Override
                                    public void run() {
                                        Log.i("##DSDSDSD","lldlsdllsdl");
                                        transaction_table =Transactions.find(Transactions.class,"isuploaded =?","1");
                                        for(Transactions txns:transaction_table){
                                           txns.setIsuploaded("0");
                                           txns.save();
                                        }



                                        getActivity().runOnUiThread(new Runnable() {
                                            @Override
                                            public void run() {
                                                renderUi();
                                                progressDialog.dismiss();
                                                new SweetAlertDialog(getActivity(), SweetAlertDialog.SUCCESS_TYPE)
                                                        .setTitleText("Great")
                                                        .setContentText("Transactions reset successfully")
                                                        .setConfirmText("Ok")
                                                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                                            @Override
                                                            public void onClick(SweetAlertDialog sDialog) {
                                                                sDialog.dismissWithAnimation();

                                                            }
                                                        })
                                                        .show();
                                            }
                                        });

                                    }
                                }).start();


                            }

                            @Override
                            public void onNegative(MaterialDialog dialog) {

                            }
                        })
                        .show();


            }
        });
        clear_txns = (Button) getView().findViewById(R.id.clear_txns);
        clear_txns.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new MaterialDialog.Builder(getActivity())
                        .iconRes(R.drawable.ecompass)
                        .limitIconToDefaultSize()
                        .title("CLEAR PENDING TRANSACTIONS")
                        .content("Please ensure the transactions has been synchronized before deleting")
                        .positiveText("Yes")
                        .negativeText("No")
                        .callback(new MaterialDialog.ButtonCallback() {
                            @Override
                            public void onPositive(MaterialDialog dialog) {
                                progressDialog = ProgressDialog.show(getActivity(), "Clearing", "Please wait", true,
                                        true, new DialogInterface.OnCancelListener() {
                                            @Override
                                            public void onCancel(DialogInterface dialog) {

                                            }
                                        });
                                progressDialog.setCancelable(false);

                                new Thread(new Runnable() {
                                    @Override
                                    public void run() {
                                        transaction_table =Transactions.find(Transactions.class,"isuploaded =?","1");
                                       try {
                                           JSONArray cs = new JSONArray();
                                           JSONArray commodities;
                                           JSONObject transaction,jsonObject;


                                           // Log.d("###TRANS", String.valueOf(transactions.size()));
                                           if (transaction_table.size() != 0){
                                               Log.d("dpsdpspdpsWE", String.valueOf(transaction_table.size()) + "ppppp");
                                               for (int i = 0; i < transaction_table.size(); i++) {
                                                   Log.d("dpsdpspdps", transaction_table.get(i).getTotalvaluereamining());
                                                   transaction = new JSONObject();
                                                   commodities = new JSONArray();
                                                   transaction.put("voucher", transaction_table.get(i).getVoucheridno());
                                                   transaction.put("transaction_type", transaction_table.get(i).getTransactiontype());
                                                   transaction.put("voucherIdNo", transaction_table.get(i).getVoucheridno());
                                                   transaction.put("cancelled_transaction", 0);
                                                   transaction.put("receipt_number",transaction_table.get(i).getReceiptno());
                                                   transaction.put("value_remaining",transaction_table.get(i).getTotalvaluereamining());
                                                   transaction.put("total_amount_charged_by_retailer", transaction_table.get(i).getTotalamountchargedbyretail());
                                                   transaction.put("user", transaction_table.get(i).getUser());
                                                   transaction.put("rationNo", transaction_table.get(i).getRationno());
                                                   transaction.put("cardNumber", transaction_table.get(i).getCardno());
                                                   transaction.put("locationID", transaction_table.get(i).getLocationid());
                                                   transaction.put("timestamp_transaction_created", transaction_table.get(i).getTimestamp());
                                                   Log.d("###transactioncreated", transaction_table.get(i).getTimestamp());
                                                   transaction.put("authentication_type", 0);
                                                   transaction.put("pos_terminal", transaction_table.get(0).getDeviceid());
                                                   List<Commodities> tops = Commodities.find(Commodities.class, "transactionno =?",transaction_table.get(i).getId().toString());
                                                   Log.d("dpsdpspdps", String.valueOf(tops.size()));
                                                   Log.d("##ERROR", "DONE" + tops.size());

                                                   for (int j = 0; j < tops.size(); j++) {
                                                       jsonObject = new JSONObject();
                                                       jsonObject.put("pos_commodity", tops.get(j).getProductid());
                                                       jsonObject.put("uom", tops.get(j).getUom());
                                                       jsonObject.put("quantity_remaining", tops.get(j).getQuantityremaining());
                                                       jsonObject.put("amount_charged_by_retailer", tops.get(j).getTotalamountcahgerdbyretailer());
                                                       jsonObject.put("deducted_quantity", tops.get(j).getQuantitydeducted());
                                                       commodities.put(jsonObject);
                                                   }
                                                   transaction.put("commodities", commodities);
                                                   cs.put(transaction);

                                               }


                                               //create folder
                                               File folder = new File(Environment.getExternalStorageDirectory() +
                                                       File.separator + "Tbc_Transactions");
                                               boolean success = true;
                                               if (!folder.exists()) {
                                                   success = folder.mkdirs();

                                               }else {

                                               }

                                               if (success) {
                                                   // Do something on success
                                               } else {
                                                   // Do something else on failure
                                               }

                                               writeToFile(cs.toString(),folder);

                                           }

                                       }catch (Exception e){

                                       }

                                        for(Transactions txns:transaction_table){
                                            List<Commodities> cm = Commodities.find(Commodities.class, "transactionno =?", txns.getId().toString());
                                            if(cm.size()>0){
                                                for(Commodities cd:cm){
                                                    cd.delete();
                                                }
                                            }
                                            txns.delete();
                                        }

                                        getActivity().runOnUiThread(new Runnable() {
                                            @Override
                                            public void run() {
                                                renderUi();
                                                progressDialog.dismiss();
                                                new SweetAlertDialog(getActivity(), SweetAlertDialog.SUCCESS_TYPE)
                                                        .setTitleText("Great")
                                                        .setContentText("Transaction cleared successfully")
                                                        .setConfirmText("Ok")
                                                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                                            @Override
                                                            public void onClick(SweetAlertDialog sDialog) {
                                                                sDialog.dismissWithAnimation();

                                                            }
                                                        })
                                                        .show();
                                            }
                                        });

                                    }
                                }).start();


                            }

                            @Override
                            public void onNegative(MaterialDialog dialog) {

                            }
                        })
                        .show();


            }
        });
        tpslogs = (TextView) getView().findViewById(R.id.topups_logs);
        renderUi();


        super.onViewCreated(view, savedInstanceState);
    }

    void renderUi(){
        try {
            String[] params = {"1"};
            long ptxns = Transactions.count(Transactions.class, "isuploaded =?", params);
            if (!(ptxns > 0)) {
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                    }
                });
                clear_txns.setEnabled(false);
                clear_txns.setAlpha(0.4f);
                reset_txns.setEnabled(false);
                reset_txns.setAlpha(0.4f);

            }

            long ptps = TopupsLogs.count(TopupsLogs.class, "isuploaded =?", params);
            if (!(ptps > 0)) {
                clear_tps.setEnabled(false);
                clear_tps.setAlpha(0.4f);
                reset_tps.setEnabled(false);
                reset_tps.setAlpha(0.4f);

            }
            Log.i("####count", String.valueOf(ptxns) + "  fgfgf");

            txns.setText("Txns (" + String.valueOf(ptxns) + " )");
            tpslogs.setText("TopupsL (" + String.valueOf(ptps > 0 ? ptps : 0) + " )");



        } catch (Exception e) {
            Log.i("##WEWRERRR",e.toString());

        }
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    public void onClick(View view, int pos) {



    }



    private void writeToFile(String data,File folder) {

       //create a  directory



        SimpleDateFormat date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        //create a file and write tha data
        final File file = new File(folder, date.format(new Date())+".txt");
        try {
            file.createNewFile();
            FileOutputStream fOut = new FileOutputStream(file);
            OutputStreamWriter myOutWriter = new OutputStreamWriter(fOut);
            myOutWriter.append(data);
            myOutWriter.close();
            fOut.flush();
            fOut.close();
        } catch (IOException e) {
            Log.e("Exception", "File write failed: " + e.toString());
        }
    }
}
