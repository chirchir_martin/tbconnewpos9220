package com.evoucher.compas.evoucher.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.evoucher.compas.evoucher.Adapters.ProgrammesAdapter;
import com.evoucher.compas.evoucher.Adapters.ReportsAdapter;
import com.evoucher.compas.evoucher.BeneficiaryList;
import com.evoucher.compas.evoucher.CarryingBillPrint;
import com.evoucher.compas.evoucher.Details;
import com.evoucher.compas.evoucher.ItemClickListener;
import com.evoucher.compas.evoucher.ProgrammeActivity;
import com.evoucher.compas.evoucher.R;
import com.evoucher.compas.evoucher.ReportsActivity;
import com.evoucher.compas.evoucher.Utils;
import com.lynx.evoucher.models.Programmes;
import com.lynx.evoucher.models.Reports;
import com.lynx.evoucher.models.Transactions;
import com.lynx.evoucher.models.Users;
import com.newpos.libpay.PaySdk;
import com.newpos.libpay.device.printer.Xreport;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import jp.wasabeef.recyclerview.adapters.AlphaInAnimationAdapter;

/**
 * Created by Administrator on 10/25/2016.
 */
public class XreportFragment extends Fragment  implements ItemClickListener,DatePickerDialog.OnDateSetListener,View.OnClickListener {
    RecyclerView recyclerView;
    List<Transactions> trans,void_trans;
    String selecteddate;
    TextView count,vcount,samt,vamt,nsales,no,dview;
    Details dt;
    public MaterialDialog xReportDialogg;
    ViewGroup x;
    SimpleDateFormat date1,date;
    ArrayList<String> programIDs=new ArrayList<String>();
    Button printButton;
    int tCount,vCount;
    float sales,netSales,vAmount;
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

         return inflater.inflate(R.layout.xreport_fragment, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
       dt= new Details(getActivity());

        x=(ViewGroup)getView().findViewById(R.id.x) ;
        no = (TextView) getView().findViewById(R.id.no);
         dview = (TextView)getView().findViewById(R.id.date);
        printButton=(Button) getView().findViewById(R.id.printxreport);
        printButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar cal = Calendar.getInstance();
                SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
                String time= sdf.format(new Date());
                CarryingBillPrint carryingBillPrint =new CarryingBillPrint();
                Details dt =new Details(getContext());
                dt.setTcount(String.valueOf(tCount));
                dt.setSales(String.valueOf(sales));
                dt.setvAmount(String.valueOf(vAmount));
                dt.setVcount(String.valueOf(vCount));
                dt.setNetsales(String.valueOf(netSales));
                dt.setDate(selecteddate);
                dt.setTime(time);
                final Xreport xreport= new Xreport();
                xreport.setDate(selecteddate);
                xreport.setTime(time);
                xreport.setDevicemac(dt.getMac());
                xreport.setSales(""+sales);
                xreport.setVoidamount(""+vAmount);
                xreport.setNetsales(""+netSales);
                xreport.setVoidcount(""+vcount);
                 printButton.setText("Printed");
               try{
                   //carryingBillPrint.printXreport(getContext(),dt);
                   new Thread(new Runnable() {
                       @Override
                       public void run() {
                           ProgrammeActivity.sdkInstance.printReceipt(xreport,PaySdk.ReceiptType.tbc_xreport,"","");
                       }
                   });
               }catch (Exception e){
                 Log.d("##log","  "+e.toString());
               }

                printButton.setEnabled(false);

            }
        });
        dview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar now = Calendar.getInstance();
                DatePickerDialog dpd = DatePickerDialog.newInstance(
                      XreportFragment.this,
                        now.get(Calendar.YEAR),
                        now.get(Calendar.MONTH),
                        now.get(Calendar.DAY_OF_MONTH)
                );

                dpd.show(getActivity().getFragmentManager(), "Datepickerdialog");
            }
        });
         date =new SimpleDateFormat("yyyy-MM-dd");
        selecteddate = date.format(new Date());
        dview.setText(selecteddate);
        setViews();




        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    public void onClick(View view, int pos) {


    }
    @Override
    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
         date = new SimpleDateFormat("yyyy-MM-dd");
         date1 = new SimpleDateFormat("yyyy-M-dd");


        //selecteddate = (++monthOfYear)+ "/" +dayOfMonth   + "/" + year;
        final  String datee = ( year)+ "-" + (++monthOfYear)   + "-" +dayOfMonth;
        try {
            selecteddate =date.format(date1.parse(datee));

        }catch (Exception e){
            Log.i("#####selecteddateerror", e.toString());

        }
         dview.setText(selecteddate);
          setViews();
        //insert code for searching the reports from the db and refreshing the views


    }
    void setViews(){
        String username ="";
        try{
            username= Users.find(Users.class,"isloggedin =?","1").get(0).getUsername();
        }catch (Exception e){
            username ="";
        }
        dt.setUser(username);

        String android_id = Settings.Secure.getString(getActivity().getApplicationContext().getContentResolver(),
                Settings.Secure.ANDROID_ID);
        dt.setMac(android_id);

        try {
            trans = Transactions.find(Transactions.class, "date = ? and transactiontype =? and isuploaded =?", selecteddate,"0","0");
            void_trans = Transactions.find(Transactions.class, "date = ? and transactiontype =? and isuploaded =?", selecteddate,"-1","0");
            if (trans.size() > 0 ||void_trans.size()>0) {
                double totalprice =0.0,void_amt = 0.0;


                for (int i = 0; i < trans.size(); i++) {
                    totalprice = totalprice + Double.parseDouble(trans.get(i).getTotalamountchargedbyretail());
                }
                for (int i = 0; i < void_trans.size(); i++) {
                    void_amt = void_amt + Double.parseDouble(void_trans.get(i).getTotalamountchargedbyretail());
                }
                count = (TextView) getView().findViewById(R.id.count);
                vcount = (TextView) getView().findViewById(R.id.vcount);
                samt = (TextView) getView().findViewById(R.id.samt);
                vamt = (TextView) getView().findViewById(R.id.vamt);
                nsales = (TextView) getView().findViewById(R.id.nsales);

                samt.setText("Sales Amount:              " + String.valueOf(totalprice+void_amt));
                sales=(float)(totalprice+void_amt);
                vamt.setText("Void Amount                 "+String.valueOf(void_amt));
                vAmount=(float)void_amt;
                vcount.setText("Void Count                    "+String.valueOf(void_trans.size()));
                vCount=void_trans.size();
                count.setText("Transaction Count:       " + trans.size());
                tCount=trans.size();
                nsales.setText("Net Sales:                      " +String.valueOf(totalprice));
                netSales=(float)(totalprice);
                no.setVisibility(View.GONE);
                x.setVisibility(View.VISIBLE);
                printButton.setVisibility(View.VISIBLE);

            } else {
                no.setVisibility(View.VISIBLE);
                x.setVisibility(View.GONE);
                printButton.setVisibility(View.GONE);
            }
        }catch (Exception e){
            Log.i("###DSDSDS","Sffdlfldlfd");

        }
    }

    @Override
    public void onClick(View v) {

    }
}
