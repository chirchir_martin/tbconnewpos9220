package com.evoucher.compas.evoucher.fragments;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.hardware.usb.UsbManager;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.evoucher.compas.evoucher.Beneficairy_Activity;
import com.evoucher.compas.evoucher.Details;
import com.evoucher.compas.evoucher.ItemClickListener;
import com.evoucher.compas.evoucher.R;
import com.evoucher.compas.evoucher.card.KeyInfoProvider;
import com.evoucher.compas.evoucher.card.SampleAppKeys;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;
import com.lynx.evoucher.models.Beneficiary;
import com.lynx.evoucher.models.Member;
import com.lynx.evoucher.models.Topups;
import com.nxp.nfclib.CardType;
import com.nxp.nfclib.KeyType;
import com.nxp.nfclib.NxpNfcLib;
import com.nxp.nfclib.desfire.DESFireFactory;
import com.nxp.nfclib.desfire.IDESFireEV1;
import com.nxp.nfclib.exceptions.NxpNfcLibException;
import com.nxp.nfclib.interfaces.IKeyData;

import org.json.JSONArray;
import org.json.JSONObject;

import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ExecutionException;

import javax.crypto.Cipher;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.IvParameterSpec;

import SecuGen.FDxSDKPro.JSGFPLib;
import cn.pedant.SweetAlert.SweetAlertDialog;

/**
 * Created by Administrator on 12/9/2016.
 */
public class CardActivation extends Fragment implements ItemClickListener {


    Details dt=new Details(getActivity());
    Button search;
    EditText cardno;
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        return inflater.inflate(R.layout.cardactivation_fragment, container, false);
    }



    public void validateCode(String qrcode){
        try{
        List<Beneficiary> benlist = Beneficiary.find(Beneficiary.class, "nationalid =?", qrcode);
        Log.i("#####QRCODE",qrcode);
        if(benlist.size()>0) {
            //List<Topups> ts = Topups.find(Topups.class, "cardnumber = ?",benlist.get(0).getCard_number());
          //  if(ts.size()>0) {
                dt.setBeneficiary(benlist.get(0));
                startActivity(new Intent(getActivity(), Beneficairy_Activity.class));
                getActivity().finish();
          /*  }else{
                new SweetAlertDialog(getActivity(), SweetAlertDialog.WARNING_TYPE)
                        .setTitleText(getString(R.string.Errorr))
                        .setContentText("No topups found for this card holder")
                        .setConfirmText(getString(R.string.Ok))
                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sDialog) {
                                sDialog.dismissWithAnimation();

                            }
                        })
                        .show();
            }*/

        }else{
            Log.d("FDFDFDFD","FFSFFS");
            new SweetAlertDialog(getActivity(), SweetAlertDialog.WARNING_TYPE)
                    .setTitleText(getString(R.string.Errorr))
                    .setContentText(getString(R.string.InvalidRation))
                    .setConfirmText(getString(R.string.Ok))
                    .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                        @Override
                        public void onClick(SweetAlertDialog sDialog) {
                            sDialog.dismissWithAnimation();

                        }
                    })
                    .show();
        }
    }catch (Exception e){
        Log.i("###iuytt",e.toString());

    }

    }
    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
       // cardno=(EditText)getView().findViewById(R.id.cardcode);
        search=(Button)getView().findViewById(R.id.submitcode);
        search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                IntentIntegrator integrator = new IntentIntegrator(getActivity());
                integrator.setPrompt("Scan  QRcode");
                integrator.setOrientationLocked(false);
                integrator.initiateScan();


            }
        });
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    public void onClick(View view, int pos) {


    }

}
