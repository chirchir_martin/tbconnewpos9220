package com.evoucher.compas.evoucher.fragments;

import android.app.AlertDialog;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Typeface;
import android.hardware.usb.UsbDevice;
import android.hardware.usb.UsbManager;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.evoucher.compas.evoucher.Adapters.ProgrammesAdapter;
import com.evoucher.compas.evoucher.Details;
import com.evoucher.compas.evoucher.ItemClickListener;
import com.evoucher.compas.evoucher.LawyerDetails;
import com.evoucher.compas.evoucher.Mytoast;
import com.evoucher.compas.evoucher.Network;
import com.evoucher.compas.evoucher.R;
import com.evoucher.compas.evoucher.VouchersActivity;
import com.evoucher.compas.evoucher.ui.CustomImageView;
import com.lynx.evoucher.models.Beneficiary;
import com.lynx.evoucher.models.Configuration;
import com.lynx.evoucher.models.FingerPrintVO;
import com.lynx.evoucher.models.Programmes;
import com.lynx.evoucher.models.Reports;
import com.lynx.evoucher.models.Topups;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import SecuGen.FDxSDKPro.JSGFPLib;
import SecuGen.FDxSDKPro.SGAutoOnEventNotifier;
import SecuGen.FDxSDKPro.SGFDxDeviceName;
import SecuGen.FDxSDKPro.SGFDxErrorCode;
import SecuGen.FDxSDKPro.SGFDxSecurityLevel;
import SecuGen.FDxSDKPro.SGFDxTemplateFormat;
import SecuGen.FDxSDKPro.SGFingerInfo;
import cn.com.aratek.fp.FingerprintScanner;
import cn.pedant.SweetAlert.SweetAlertDialog;
import jp.wasabeef.recyclerview.adapters.AlphaInAnimationAdapter;

/**
 * Created by Administrator on 10/24/2016.
 */
public class Verification_Fragment extends Fragment  implements ItemClickListener {

    Button submitcode,verify;
    List<Topups> topups;
    MaterialDialog dialog;
    List<Programmes> programs=new ArrayList<Programmes>();
    EditText candno,url,port,pin,lawyer_code;
      Typeface typeface1;
    private static final String ACTION_USB_PERMISSION = "com.android.example.USB_PERMISSION";
    ViewGroup view0,view1,fingerprint;
    private static RecyclerView recyclerView;
    ArrayList<String> programIDs=new ArrayList<String>();
    Details dt=new Details(getActivity());
    Network net=new Network(getActivity(),"");
    private FingerprintScanner scanner = FingerprintScanner.getInstance();
    CustomImageView img0;
    Thread t;
   boolean captured;
    EditText input;
    public boolean isDone;
    Button mCapture;
    private byte[] mRegisterTemplate0;
    Animation animation;
    private String state;
    private int mImageWidth;
    private int[] mMaxTemplateSize;
    private int[] grayBuffer;
    String benid,iden="";
    List<Beneficiary> beneficiary;
    Beneficiary ben;
    List<Programmes> programmes;
    private PendingIntent mPermissionIntent;
    private Bitmap grayBitmap;
    private IntentFilter filter; //2014-04-11
    private SGAutoOnEventNotifier autoOn;
    private JSGFPLib sgfplib;
    private static final String TAG = "SecuGenUSB";
    private boolean mLed,idn;
    Reports reports_table;
    private int mImageHeight;
    private final BroadcastReceiver mUsbReceiver = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            Log.d("","Enter mUsbReceiver.onReceive()");
            if (ACTION_USB_PERMISSION.equals(action)) {
                synchronized (this) {
                    UsbDevice device = (UsbDevice)intent.getParcelableExtra(UsbManager.EXTRA_DEVICE);
                    if (intent.getBooleanExtra(UsbManager.EXTRA_PERMISSION_GRANTED, false)) {
                        if(device != null){
                            Log.d(TAG, "Vendor ID : " + device.getVendorId() + "\n");
                            Log.d(TAG, "Product ID: " + device.getProductId() + "\n");
                            debugMessage("Vendor ID : " + device.getVendorId() + "\n");
                            debugMessage("Product ID: " + device.getProductId() + "\n");
                        }
                        else
                            Log.e(TAG, "mUsbReceiver.onReceive() Device is null");
                    }
                    else
                        Log.e(TAG, "mUsbReceiver.onReceive() permission denied for device " + device);
                }
            }
        }
    };
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);



    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        filter = new IntentFilter(ACTION_USB_PERMISSION);
        // getActivity().registerReceiver(mUsbReceiver, filter);
        mPermissionIntent = PendingIntent.getBroadcast(getActivity(), 0, new Intent(ACTION_USB_PERMISSION), 0);

        return inflater.inflate(R.layout.verification_fragment, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
       // scancard=(Button)getView().findViewById(R.id.cardbtn);
        recyclerView = (RecyclerView)getView().findViewById(R.id.recycler_vieww);
        submitcode = (Button)getView().findViewById(R.id.submitcode);
        verify = (Button)getView().findViewById(R.id.verify);
        captured=false;
        img0=(CustomImageView)getView().findViewById(R.id.img0);
        lawyer_code = (EditText) getView().findViewById(R.id.lawyer_code);
        view1=(ViewGroup)getView().findViewById(R.id.f);
       fingerprint=(ViewGroup)getView().findViewById(R.id.fingerprint);
        fingerprint.setVisibility(View.GONE);
        view1.setVisibility(View.GONE);
        benid="";
        recyclerView.setVisibility(View.VISIBLE);
        typeface1 = Typeface.createFromAsset(getActivity().getAssets(), "Roboto-Regular.ttf");
        programmes=Programmes.find(Programmes.class, "progtype =?","IDN");
        lawyer_code.setTypeface(typeface1);
        recyclerView.setLayoutManager(new StaggeredGridLayoutManager(3, StaggeredGridLayoutManager.VERTICAL));
        ProgrammesAdapter adapter=new ProgrammesAdapter(getActivity(),programmes);
        adapter.setClickListener(this);
        AlphaInAnimationAdapter alphaAdapter = new AlphaInAnimationAdapter(adapter);
        alphaAdapter.setDuration(1500);
        recyclerView.setAdapter(alphaAdapter);
        submitcode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(lawyer_code.getText().toString().equalsIgnoreCase("")) {
                    showAlert("Error","Please input the lawyer code to proceed");
                }else {

                    beneficiary = Beneficiary.find(Beneficiary.class, "familysize = ? and programmeid=?", lawyer_code.getText().toString(),dt.getProgrammeid());

                    if (beneficiary.size() == 0) {

                        showAlert("Error","No lawyer registered with this code");

                    } else {

                        final MaterialDialog pDialog = new MaterialDialog.Builder(getActivity())
                                .title("Initializing fingerprint Scanner")
                                .content("Please wait...")
                                .progress(true, 0)
                                .progressIndeterminateStyle(false)
                                .show();
                        Thread t= new Thread(new Runnable() {
                            @Override
                            public void run() {
                                sgfplib = new JSGFPLib((UsbManager)getActivity().getSystemService(Context.USB_SERVICE));
                                mMaxTemplateSize = new int[1];
                                scanner.powerOn();
                                initLib();
                                getActivity().runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        pDialog.dismiss();
                                        view1.setVisibility(View.GONE);
                                        fingerprint.setVisibility(View.VISIBLE);


                                    }
                                });

                            }
                        });
                        t.start();

                    }
                }

            }
        });
        verify.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View v) {
               state="";
               benid="";
               CaptureFingerPrint();
               if(state=="okay") {
                           reports_table = new Reports();
                           reports_table.setLawyername(beneficiary.get(0).getFirstname() + " " + beneficiary.get(0).getMiddlename() + " " + beneficiary.get(0).getLastname());
                           reports_table.setLawyercode(beneficiary.get(0).getFamily_size());
                          SimpleDateFormat ft = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
                           reports_table.setDate(ft.format(new Date()));
                           reports_table.setVerifiedby(dt.getUser());
                           reports_table.setProgrammeid(beneficiary.get(0).getProgrammeid());
                           reports_table.save();
                           startActivity(new Intent(getActivity(), LawyerDetails.class).putExtra("id", benid));
               } else if(state=="nomatch"){
                   showAlert("ERROR","No match Found, Please try again!!");
               }else if(state=="low"){
                   showAlert("ERROR","Low quality, Please try again!!");

               }



           }
       });
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void onClick(View view, int id) {
       recyclerView.setVisibility(View.GONE);
        dt.setProgrammeid(String.valueOf(id));
        view1.setVisibility(View.VISIBLE);
    }
    public void showAlert(String title, String message){
        Log.d("ALERT","ALERT");
        new SweetAlertDialog(getActivity(), SweetAlertDialog.WARNING_TYPE)
                .setTitleText(title)
                .setContentText(message)
                .setConfirmText("Ok")
                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sDialog) {
                        sDialog.dismissWithAnimation();

                    }
                })
                .show();
    }
    public void CaptureFingerPrint(){
        Log.d("CAPTURE","CAPTURE");
        try{
            if(sgfplib!=null){
                long dwTimeStart = 0, dwTimeEnd = 0, dwTimeElapsed = 0;
                byte[] buffer = new byte[mImageWidth*mImageHeight];
                dwTimeStart = System.currentTimeMillis();
                long result = sgfplib.GetImage(buffer);
                dwTimeEnd = System.currentTimeMillis();
                dwTimeElapsed = dwTimeEnd-dwTimeStart;
                debugMessage("getImage() ret:" + result + " [" + dwTimeElapsed + "ms]\n");
                Bitmap b = Bitmap.createBitmap(mImageWidth,mImageHeight, Bitmap.Config.ARGB_8888);
                b.setHasAlpha(false);
                int[] intbuffer = new int[mImageWidth*mImageHeight];
                for (int i=0; i<intbuffer.length; ++i)
                    intbuffer[i] = (int) buffer[i];
                b.setPixels(intbuffer, 0, mImageWidth, 0, 0, mImageWidth, mImageHeight);

                result = sgfplib.SetTemplateFormat(SecuGen.FDxSDKPro.SGFDxTemplateFormat.TEMPLATE_FORMAT_SG400);
                dwTimeEnd = System.currentTimeMillis();
                dwTimeElapsed = dwTimeEnd-dwTimeStart;
                debugMessage("SetTemplateFormat(SG400) ret:" +  result + " [" + dwTimeElapsed + "ms]\n");
                SGFingerInfo fpInfo = new SGFingerInfo();

                boolean[] matched = new boolean[1];
                int[] quality = new int[1];

                sgfplib.GetImageQuality(mImageWidth, mImageHeight, buffer, quality);
                if (quality[0] > 60) {

                    img0.setImageBitmap(toGrayscale(b));
                    captured=true;
                    for (int i=0; i< mRegisterTemplate0.length; ++i)
                        mRegisterTemplate0[i] = 0;
                    dwTimeStart = System.currentTimeMillis();
                    result = sgfplib.CreateTemplate(fpInfo, buffer, mRegisterTemplate0);
                    dwTimeEnd = System.currentTimeMillis();
                    dwTimeElapsed = dwTimeEnd-dwTimeStart;
                    debugMessage("CreateTemplate() ret:" + result + " [" + dwTimeElapsed + "ms]\n");
                    List<FingerPrintVO> fingerprints = FingerPrintVO.listAll(FingerPrintVO.class);
                    Log.d("IMAGE$$",fingerprints.size()+"");

                    if (fingerprints.size() > 0){
                        for (FingerPrintVO finger: fingerprints) {

                            Log.d("IMAGE$$",finger.getImage()+"");
                            if (finger.getImage()!=null){
                                byte[] imageAsBytes = Base64.decode(finger.getImage().getBytes(), Base64.DEFAULT);
                                result= sgfplib.MatchTemplate(mRegisterTemplate0, imageAsBytes, SGFDxSecurityLevel.SL_NORMAL, matched);
                            }
                            if(matched[0]) {
                                Log.d("ENROLLED_BEN_ID##",finger.getBeneficiary_id()+"hhh");
                                Log.d("ENROLLED_BEN_ID##",   beneficiary.get(0).getId()+"hhh1");
                                Log.d("ENROLLED_BEN_ID##", String.valueOf( Beneficiary.listAll(Beneficiary.class).get(0).getId()+"hhh1"));
                                benid=beneficiary.get(0).getId().toString();
                               /* if(finger.getBeneficiary_id().equalsIgnoreCase(String.valueOf(beneficiary.get(0).getId()))){

                                    idn=true;
                                }else{
                                    benid=finger.getBeneficiary_id();
                                    idn=false;
                                }
*/

                              //  ben=Beneficiary.findById(Beneficiary.class,Long.parseLong(finger.getBeneficiary_id()));
                                state = "okay";
                                break;
                            }else{
                                state = "nomatch";
                            }
                        }
                    }else{
                        state = "nomatch";
                    }

                } else{
                    state = "low";
                    initLib();

                }
            } else {
                initLib();

            }

        }catch(Exception ex){
            ex.printStackTrace();
            initLib();
            //restartCapture();
            Log.d("ERROR",ex.toString());
            fingershowAlert("CAPTURE ERROR","Please Recapture FingerPrint");
            // state = "low";
//            Toast.makeText(getApplicationContext(),"E",Toast.LENGTH_SHORT).show();
        }

    }
    public Bitmap toGrayscale(Bitmap bmpOriginal)
    {
        int width, height;
        height = bmpOriginal.getHeight();
        width = bmpOriginal.getWidth();
        Bitmap bmpGrayscale = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
        for (int y=0; y< height; ++y) {
            for (int x=0; x< width; ++x){
                int color = bmpOriginal.getPixel(x, y);
                int r = (color >> 16) & 0xFF;
                int g = (color >> 8) & 0xFF;
                int b = color & 0xFF;
                int gray = (r+g+b)/3;
                color = Color.rgb(gray, gray, gray);
                //color = Color.rgb(r/3, g/3, b/3);
                bmpGrayscale.setPixel(x, y, color);
            }
        }
        return bmpGrayscale;
    }
    private void initLib() {
        try {
            long error = sgfplib.Init(SGFDxDeviceName.SG_DEV_AUTO);
            if (error != SGFDxErrorCode.SGFDX_ERROR_NONE) {
                final AlertDialog.Builder dlgAlert = new AlertDialog.Builder(getActivity());
                if (error == SGFDxErrorCode.SGFDX_ERROR_DEVICE_NOT_FOUND)

                    dlgAlert.setMessage("The attached fingerprint device is not supported on Android");
                else
                    dlgAlert.setMessage("Fingerprint device initialization failed!");
                dlgAlert.setTitle("SecuGen Fingerprint SDK");
                dlgAlert.setPositiveButton("OK",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int whichButton) {
                                getActivity().finish();
                                return;
                            }
                        }
                );

                dlgAlert.setCancelable(true);
                dlgAlert.create().show();


            } else {
                UsbDevice usbDevice = sgfplib.GetUsbDevice();
                if (usbDevice == null) {
                    final AlertDialog.Builder dlgAlert = new AlertDialog.Builder(getActivity());
                    dlgAlert.setMessage("SDU04P or SDU03P fingerprint sensor not found!");
                    dlgAlert.setTitle("SecuGen Fingerprint SDK");
                    dlgAlert.setPositiveButton("OK",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int whichButton) {
                                    getActivity().finish();
                                    return;
                                }
                            }
                    );

                    dlgAlert.setCancelable(true);
                    dlgAlert.create().show();


                } else {
                    sgfplib.GetUsbManager().requestPermission(usbDevice,
                            PendingIntent.getBroadcast(getActivity(), 0, new Intent(ACTION_USB_PERMISSION), 0));
                    try {
                        //sgfplib = new JSGFPLib((UsbManager)getActivity().getSystemService(Context.USB_SERVICE));
                        error = sgfplib.OpenDevice(0);
                    } catch (Exception e) {
                        Log.d("####SGFPLIBERROR", "SGFPLIBERROR");


                    }
                    Log.d(TAG, "OpenDevice=" + error);
                    debugMessage("OpenDevice() ret: " + error + "\n");
                    SecuGen.FDxSDKPro.SGDeviceInfoParam deviceInfo = new SecuGen.FDxSDKPro.SGDeviceInfoParam();
                    error = sgfplib.GetDeviceInfo(deviceInfo);
                    Log.d(TAG, "GetDeviceInfo=" + error);
                    debugMessage("GetDeviceInfo() ret: " + error + "\n");
                    mImageWidth = deviceInfo.imageWidth;
                    mImageHeight = deviceInfo.imageHeight;
                    sgfplib.SetTemplateFormat(SGFDxTemplateFormat.TEMPLATE_FORMAT_SG400);
                    sgfplib.GetMaxTemplateSize(mMaxTemplateSize);
                    debugMessage("TEMPLATE_FORMAT_SG400 SIZE: " + mMaxTemplateSize[0] + "\n");
                    Log.d(TAG, "TEMPLATE_FORMAT_SG400 SIZE: " + mMaxTemplateSize[0]);
                    mRegisterTemplate0 = new byte[mMaxTemplateSize[0]];

                    Log.d(TAG, "Init finish");
                }
            }
        }catch (Exception e){
            Log.d("#####ERROR",e.toString());

        }
    }
    private void debugMessage(String message) {
        Log.d(TAG,message);
    }


    @Override
    public void onPause() {
//        autoOn.stop();
        //t.interrupt();
        //   sgfplib.CloseDevice();
        // getActivity().unregisterReceiver(mUsbReceiver);
        super.onPause();
    }
    @Override
    public void onResume() {

      /*  sgfplib = new JSGFPLib((UsbManager)getActivity().getSystemService(Context.USB_SERVICE));
        scanner.powerOn();
        initLib();
        mMaxTemplateSize = new int[1];*/

       /* t= new Thread(new Runnable() {
            @Override
            public void run() {
                scanner.powerOn();
                initLib();}
        });
        t.start()*/;
        super.onResume();
    }
    @Override
    public void onDestroy(){
      /*  t.interrupt();
        isDone=false;*/
        //sgfplib.CloseDevice();
      Log.d("###Destroyed","DESTROYED");
        //  getActivity().unregisterReceiver(mUsbReceiver);
        super.onDestroy();
    }

    public void fingershowAlert(String title, String message){
        Log.d("ALERT","ALERT");
        new SweetAlertDialog(getActivity(), SweetAlertDialog.WARNING_TYPE)
                .setTitleText(title)
                .setContentText(message)
                .setConfirmText("Ok")
                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sDialog) {
                        sDialog.dismissWithAnimation();

                     //   showCustomViewFingerprint();

                    }
                })
                .show();
    }
    public void showInputDialog() {


        dialog = new MaterialDialog.Builder(getActivity())
                .title("Enter the Lawyer`s code.")
                .customView(R.layout.dialog_verification, true)
                .positiveText("Ok")
                .typeface(typeface1,typeface1)
                .negativeText(android.R.string.cancel)
                .autoDismiss(false)
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {

                        dialog.dismiss();
                        if(input.getText().toString().equalsIgnoreCase("")) {
                            showAlert("Error","Please input the lawyer code to proceed");
                        }else {
                            //Mytoast.show(getActivity(), "Please input the lawyer code to proceed", true);

                            List<Beneficiary> beneficiary = Beneficiary.find(Beneficiary.class, "familysize = ?", input.getText().toString());
                            if (beneficiary.size() == 0) {
                                // Mytoast.show(getActivity(), "No lawyer registered with this code", true);
                                showAlert("Error","No lawyer registered with this code");

                            } else {
                                //showCustomViewFingerprint();
                            }
                        }
/*
                        if(input.getText().toString().equalsIgnoreCase("")){
                           // Mytoast.show(getActivity(), "This filled must be filled", true);
                        }else {
                            dialog.dismiss();
                        }*/



                    }
                })
                .onNegative(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {

                        dialog.dismiss();

                    }
                })
                .build();
        input = (EditText) dialog.getCustomView().findViewById(R.id.input);
        input.setTypeface(typeface1);


        Typeface typeface = Typeface.createFromAsset(getActivity().getAssets(), "Roboto-Thin.ttf");




        dialog.show();



    }
}
