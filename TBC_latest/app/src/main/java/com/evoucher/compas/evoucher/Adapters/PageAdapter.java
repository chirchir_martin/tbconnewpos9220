package com.evoucher.compas.evoucher.Adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.evoucher.compas.evoucher.fragments.BeneficiaryGroupFragment;
import com.evoucher.compas.evoucher.fragments.Fragment1;
import com.evoucher.compas.evoucher.fragments.Fragment3;
import com.evoucher.compas.evoucher.fragments.Fragment4;
import com.evoucher.compas.evoucher.fragments.Fragment5;
import com.evoucher.compas.evoucher.fragments.Fragment6;

import java.util.ArrayList;

/**
 * Created by JOHNIE on 9/3/2015.
 */
public class PageAdapter extends FragmentStatePagerAdapter {
    ArrayList<CharSequence> title;
    public PageAdapter(FragmentManager fm) {
        super(fm);
      title=new ArrayList<>();
        title.add("1");
        title.add("2");
        title.add("3");
        title.add("4");
        title.add("5");
        title.add("6");



    }

    @Override
    public Fragment getItem(int position) {
      switch(position) {
          case 0:
              Fragment1 f1 = new Fragment1();
              return f1;

          case 1:
              BeneficiaryGroupFragment beneficiaryGroupFragment = new BeneficiaryGroupFragment();
              return beneficiaryGroupFragment;

          case 2:
              Fragment3 f3 = new Fragment3();
              return f3;

          case 3:

          Fragment4 f4 = new Fragment4();
          return f4;


          case 4:


              Fragment5 f5 = new Fragment5();
              return f5;

          case 5:


              Fragment6 f6 = new Fragment6();
              return f6;
      }
        return null;
    }

    @Override
    public int getCount() {
        return 6;
    }
    @Override
    public CharSequence getPageTitle(int position) {

        return title.get(position);
    }
}
