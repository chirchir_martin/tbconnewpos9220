package com.evoucher.compas.evoucher.fragments;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.nfc.NfcAdapter;
import android.os.Bundle;
import android.os.Handler;
import android.os.Vibrator;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.evoucher.compas.evoucher.Adapters.CartAdapter;
import com.evoucher.compas.evoucher.Adapters.ProgrammesAdapter;
import com.evoucher.compas.evoucher.Adapters.VoidAdapter;
import com.evoucher.compas.evoucher.Card_Details;
import com.evoucher.compas.evoucher.CartActivity;
import com.evoucher.compas.evoucher.Details;
import com.evoucher.compas.evoucher.ItemClickListener;
import com.evoucher.compas.evoucher.Network;
import com.evoucher.compas.evoucher.ProgrammeActivity;
import com.evoucher.compas.evoucher.R;
import com.evoucher.compas.evoucher.Utils;
import com.evoucher.compas.evoucher.VouchersActivity;
import com.evoucher.compas.evoucher.card.KeyInfoProvider;
import com.evoucher.compas.evoucher.card.SampleAppKeys;
import com.lynx.evoucher.models.Beneficiary;
import com.lynx.evoucher.models.Colors;
import com.lynx.evoucher.models.Commodities;
import com.lynx.evoucher.models.Products;
import com.lynx.evoucher.models.Programmes;
import com.lynx.evoucher.models.PurchasedProducts;
import com.lynx.evoucher.models.Topups;
import com.lynx.evoucher.models.Transactions;
import com.newpos.libpay.device.card.CardInfo;
import com.newpos.libpay.device.card.CardListener;
import com.newpos.libpay.device.card.CardManager;
import com.nxp.nfclib.CardType;
import com.nxp.nfclib.KeyType;
import com.nxp.nfclib.NxpNfcLib;
import com.nxp.nfclib.desfire.DESFireFactory;
import com.nxp.nfclib.desfire.IDESFireEV1;
import com.nxp.nfclib.exceptions.NxpNfcLibException;
import com.nxp.nfclib.interfaces.IKeyData;

import org.json.JSONArray;
import org.json.JSONObject;

import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import javax.crypto.Cipher;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.IvParameterSpec;

import cn.pedant.SweetAlert.SweetAlertDialog;
import jp.wasabeef.recyclerview.adapters.AlphaInAnimationAdapter;

/**
 * Created by Administrator on 10/24/2016.
 */
public class VoidFragment extends Fragment {
    String UID="";
    List<PurchasedProducts> products;
    List<Products> pds,pddd;
    String msg ="";
    List<Transactions> txns;
    ArrayList<Products> pdd=new ArrayList<Products>();
    TextView tl,tq,tn,ur;
    NfcAdapter nfcAdapter;
    // List<Topups> tops;

    StringBuilder sb;
    JSONArray dd,aa;
    String state="";

    Button btnsubmit;
    Button btnprint;
    float total,totalq;



    private IKeyData objKEY_2KTDES_ULC = null;
    private IKeyData objKEY_2KTDES = null;
    private IKeyData objKEY_AES128 = null;
    private byte[] default_ff_key = null;
    private IKeyData default_zeroes_key = null;
    private NxpNfcLib libInstance = null;
    private IDESFireEV1 desFireEV1;
    private CardType mCardType = CardType.UnknownCard;
    private Cipher cipher = null;
    private byte[] bytesKey = null;
    private IvParameterSpec iv = null;
    private static final String KEY_APP_MASTER = "This is my key  ";
    private static final String ALIAS_KEY_AES128 = "key_aes_128";
    private static final String ALIAS_KEY_2KTDES = "key_2ktdes";
    private static final String ALIAS_KEY_2KTDES_ULC = "key_2ktdes_ulc";
    private static final String ALIAS_DEFAULT_FF = "alias_default_ff";
    private static final String ALIAS_KEY_AES128_ZEROES = "alias_default_00";
    private static final String EXTRA_KEYS_STORED_FLAG = "keys_stored_flag";
    static String packageKey = "b927aab8842ab54cbaf2ea1df9917159";
    MaterialDialog pDialogg;
    Button void_button;
    byte[] appId = new byte[]{0x1, 0x00, 0x00};
    int timeOut = 2000;
    List<Topups> topups;
    MaterialDialog dialog;
    List<Programmes> pgs;
    JSONObject data,pdetails,card_pin,topup;
    ViewGroup tapview,views;
    TextView tap,receiptNo,date,rationNo,value;
    boolean visible =false;
    String  vouchervalue ="0";
    String status="";
    Details dt=new Details(getActivity());
    Network net=new Network(getActivity(),"");
    Thread t;
    String bgip;
    Button receiptno_submit_et;
    EditText receiptno_et;
    JSONArray tups;
    String  receiptNumber="null";
    TextView  tap_card_tv;
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);



    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        return inflater.inflate(R.layout.void_fragment, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        void_button=(Button)getView().findViewById(R.id.void_button);
        tapview =(ViewGroup)getView().findViewById(R.id.tapview) ;
        views =(ViewGroup)getView().findViewById(R.id.view) ;
        tapview.setVisibility(View.VISIBLE);
        views.setVisibility(View.GONE);
        tap_card_tv=(TextView) getView().findViewById(R.id.tap);
        receiptNo=(TextView)getView().findViewById(R.id.receiptNo);
        date=(TextView)getView().findViewById(R.id.date);
        rationNo=(TextView)getView().findViewById(R.id.rationNo);
        value=(TextView)getView().findViewById(R.id.value);
         receiptno_et=(EditText) getView().findViewById(R.id.receipt_no_et);
        receiptno_submit_et=(Button) getView().findViewById(R.id.receipt_submit_btn);
        receiptno_submit_et.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               receiptNumber= receiptno_et.getText().toString();
                if(receiptNumber.length()>0){
                    receiptno_et.setVisibility(View.GONE);
                    receiptno_submit_et.setVisibility(View.GONE);
                    tap_card_tv.setVisibility(View.VISIBLE);
                    status="fetch";
                    pDialogg = new MaterialDialog.Builder(getActivity())
                            .title(getString(R.string.searchcard))
                            .content(getString(R.string.tapcard))
                            .progress(true,0)
                            .progressIndeterminateStyle(false)
                            .show();
                    new Thread(new Runnable() {
                        @Override
                        public void run() {

                            read_nfc_card(com.newpos.libpay.device.card.CardType.INMODE_NFC.getVal(), 10000);

                        }
                    }).start();
                }else{
                    Toast.makeText(getContext(),"Please input receipt number",Toast.LENGTH_LONG).show();
                }
            }
        });
       /* tl=(TextView)getView().findViewById(R.id.total);
        tq=(TextView)getView().findViewById(R.id.t0);*/

        initializeLibrary();
        initializeKeys();
        initializeCipherinitVector();

        void_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                status="update";
                dialog=new MaterialDialog.Builder(getActivity())
                        .title(getString(R.string.TapToUpdate))
                        .content("")
                        .progress(true, 0)
                        .progressIndeterminateStyle(true)
                        .cancelable(false)
                        .show();
                desfireEV1CardLogic(null);
                final Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        status ="";
                        dialog.dismiss();
                    }

                }, 10000);
            }
        });
        super.onViewCreated(view, savedInstanceState);

    }

    public void showAlert(String title, String message){
        Log.d("ALERT","ALERT");
        new SweetAlertDialog(getActivity(), SweetAlertDialog.WARNING_TYPE)
                .setTitleText(title)
                .setContentText(message)
                .setConfirmText(getString(R.string.Ok))
                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sDialog) {
                        sDialog.dismissWithAnimation();

                    }
                })
                .show();
    }


    @Override
    public void onPause() {
//
        super.onPause();
    }

    @Override
    public void onDestroy(){
        Log.d("###Destroyed","DESTROYED");
        super.onDestroy();
    }


    public void cardLogic(final Intent intent) {

        CardType type = CardType.UnknownCard;
        try {
            type = libInstance.getCardType(intent);
        } catch (NxpNfcLibException ex) {
            pDialogg.dismiss();
            new SweetAlertDialog(getActivity(), SweetAlertDialog.WARNING_TYPE)
                    .setTitleText(getString(R.string.nfc_error))
                    .setContentText(getString(R.string.nfc_error_totle))
                    .setConfirmText(getString(R.string.Ok))
                    .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                        @Override
                        public void onClick(SweetAlertDialog sDialog) {
                            sDialog.dismissWithAnimation();

                        }
                    })
                    .show();
        }

        switch (type) {

            case DESFireEV1: {
                mCardType = CardType.DESFireEV1;
                desFireEV1 = DESFireFactory.getInstance().getDESFire(libInstance.getCustomModules());
                try {
                    if(status.equalsIgnoreCase("update")){
                        dialog.dismiss();
                        desFireEV1.getReader().connect();
                        desFireEV1.getReader().setTimeout(2000);
                        pDialogg = new MaterialDialog.Builder(getActivity())
                                .title(R.string.void_title)
                                .content(getString(R.string.DontRemoveCard))
                                .progress(true, 0)
                                .progressIndeterminateStyle(false)
                                .show();
                        new Thread(new Runnable() {
                            @Override
                            public void run() {
                                desfireEV1CardLogic();
                            }
                        }).start();
                    }else if(status.equalsIgnoreCase("fetch")){
                        desFireEV1.getReader().connect();
                        desFireEV1.getReader().setTimeout(2000);
                        pDialogg = new MaterialDialog.Builder(getActivity())
                                .title(R.string.fetchin_txn)
                                .content(getString(R.string.DontRemoveCard))
                                .progress(true, 0)
                                .progressIndeterminateStyle(false)
                                .show();
                        new Thread(new Runnable() {
                            @Override
                            public void run() {
                                desfireEV1CardLogic();
                            }
                        }).start();
                    }


                } catch (Exception t) {
                    t.printStackTrace();
                    Log.d("##ERRORRR", t.toString());

                }
                break;
            }


        }


    }

    @TargetApi(19)
    private void initializeLibrary() {
        libInstance = NxpNfcLib.getInstance();
        try {
            libInstance.registerActivity(getActivity(), packageKey);
        } catch (NxpNfcLibException ex) {
            // Toast.makeText(this, ex.getMessage(), Toast.LENGTH_SHORT).show();
            pDialogg.dismiss();
            new SweetAlertDialog(getActivity(), SweetAlertDialog.WARNING_TYPE)
                    .setTitleText(getString(R.string.nfc_error))
                    .setContentText(getString(R.string.nfc_error_totle))
                    .setConfirmText(getString(R.string.Ok))
                    .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                        @Override
                        public void onClick(SweetAlertDialog sDialog) {
                            sDialog.dismissWithAnimation();

                        }
                    })
                    .show();
        }
    }

    /**
     * Initialize the Cipher and init vector of 16 bytes with 0xCD.
     */


    private void desfireEV1CardLogic() {
        try {
            if(status.equalsIgnoreCase("update")){

                String uidhex = new String();
                byte[] uid = desFireEV1.getUID();
                for (int i = 0; i < uid.length; i++) {


                    String x = Integer.toHexString(((int) uid[i] & 0xff));
                    if (x.length() == 1) {
                        x = '0' + x;
                    }
                    uidhex += x;
                }

                if(UID.equalsIgnoreCase(String.valueOf(uidhex))) {
                    desFireEV1.getReader().setTimeout(timeOut);
                    desFireEV1.authenticate(0, IDESFireEV1.AuthType.Native, KeyType.TWO_KEY_THREEDES, objKEY_2KTDES);
                    desFireEV1.selectApplication(0);
                    desFireEV1.selectApplication(appId);
                    desFireEV1.authenticate(0, IDESFireEV1.AuthType.Native, KeyType.TWO_KEY_THREEDES, objKEY_2KTDES);

                    desFireEV1.writeData(Card_Details.carddata, 0, data.toString().getBytes());
                    desFireEV1.getReader().close();

                    txns.get(txns.size() - 1).setTransactiontype("-1");
                    txns.get(txns.size() - 1).save();
                    UID ="";
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            pDialogg.dismiss();
                            alertSuccess();

                        }
                    });
                }else{
                    getActivity().runOnUiThread(new Runnable(){
                        @Override
                        public void run() {
                            pDialogg.dismiss();
                            new SweetAlertDialog(getActivity(), SweetAlertDialog.WARNING_TYPE)
                                    .setTitleText(getString(R.string.Errorr))
                                    .setContentText("Please tap the correct card")
                                    .setConfirmText(getString(R.string.Ok))
                                    .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                        @Override
                                        public void onClick(SweetAlertDialog sDialog) {
                                            sDialog.dismissWithAnimation();

                                        }
                                    })
                                    .show();

                        }
                    });
                }

            }else{
                UID ="";
                String uidhex = new String();
                byte[] uid = desFireEV1.getUID();
                for (int i = 0; i < uid.length; i++) {


                    String x = Integer.toHexString(((int) uid[i] & 0xff));
                    if (x.length() == 1) {
                        x = '0' + x;
                    }
                    uidhex += x;
                }
                UID=String.valueOf(uidhex);


                desFireEV1.getReader().setTimeout(timeOut);
                desFireEV1.authenticate(0, IDESFireEV1.AuthType.Native, KeyType.TWO_KEY_THREEDES, objKEY_2KTDES);
                desFireEV1.selectApplication(0);
                desFireEV1.selectApplication(appId);
                desFireEV1.authenticate(0, IDESFireEV1.AuthType.Native, KeyType.TWO_KEY_THREEDES, objKEY_2KTDES);
                //  pdetails = new JSONObject(new String(desFireEV1.readData(Card_Details.personalDetails, 0, 0)));
                card_pin = new JSONObject(new String(desFireEV1.readData(Card_Details.cpin, 0, 0)));
                data = new JSONObject(new String(desFireEV1.readData(Card_Details.carddata, 0, 0)));
                desFireEV1.getReader().close();
                vouchervalue =data.getString("vochervalue");
                SimpleDateFormat datee = new SimpleDateFormat("yyyy-MM-dd");
                String dateStr=datee.format(new Date());
                txns =Transactions.find(Transactions.class," cardno =? and transactiontype = ? and receiptno=? and  isuploaded=? ",card_pin.getString("cardNo"),"0",receiptNumber,"0");
                // txns =Transactions.find(Transactions.class,"cardno =?",card_pin.getString("cardNo"));
                Log.i("##ERERTRTT",String.valueOf(txns.size()+"   erere"));
                if(txns.size()>0){

                    if(txns.get(txns.size() -1).getDeviceid().equalsIgnoreCase(dt.getMac())) {
                        Log.i("###VOUCHER_VALUE", vouchervalue);
                        Log.i("###VOUCHER_VALUE", txns.get(txns.size() - 1).getTotalamountchargedbyretail());
                        Log.i("###VOUCHER_VALUE", "-------------------------------------------");
                        vouchervalue = String.valueOf(Float.parseFloat(vouchervalue) + Float.parseFloat(txns.get(txns.size() - 1).getTotalamountchargedbyretail()));

                        Log.i("###VOUCHER_VALUE", vouchervalue);
                        data.put("vochervalue", vouchervalue);
                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                receiptNo.setText("Receipt No:        "+txns.get(txns.size() -1).getReceiptno());
                                date.setText("Date :                   "+txns.get(txns.size() -1).getTimestamp());
                                rationNo.setText("Ration No :          "+txns.get(txns.size() -1).getRationno());
                                value.setText("Total amount:      THB "+txns.get(txns.size() -1).getTotalamountchargedbyretail());
                                tapview.setVisibility(View.GONE);
                                views.setVisibility(View.VISIBLE);
                                pDialogg.dismiss();

                            }
                        });


                    }
                      else{
                        pDialogg.dismiss();
                        new SweetAlertDialog(getActivity(), SweetAlertDialog.WARNING_TYPE)
                                .setTitleText(getString(R.string.Errorr))
                                .setContentText("You are not allowed to void the transaction from this device")
                                .setConfirmText(getString(R.string.Ok))
                                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                    @Override
                                    public void onClick(SweetAlertDialog sDialog) {
                                        sDialog.dismissWithAnimation();

                                    }
                                })
                                .show();
                    }

                } else {
                    List<Transactions> transList=Transactions.find(Transactions.class," cardno =? and transactiontype = ? and receiptno=? and  isuploaded=? ",card_pin.getString("cardNo"),"-1",receiptNumber,"0");
                    List<Transactions> transList1=Transactions.find(Transactions.class," cardno =? and transactiontype = ? and receiptno=? and  isuploaded=? ",card_pin.getString("cardNo"),"-1",receiptNumber,"1");
                    List<Transactions> transList2=Transactions.find(Transactions.class," cardno =? and transactiontype = ? and receiptno=? and  isuploaded=? ",card_pin.getString("cardNo"),"0",receiptNumber,"1");
                    if(transList.size()>0)
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            pDialogg.dismiss();
                            new SweetAlertDialog(getActivity(), SweetAlertDialog.WARNING_TYPE)
                                    .setTitleText("Sorry")
                                    .setContentText("The transaction has already been voided")
                                    .setConfirmText(getString(R.string.Ok))
                                    .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                        @Override
                                        public void onClick(SweetAlertDialog sDialog) {
                                            sDialog.dismissWithAnimation();

                                        }
                                    })
                                    .show();

                        }
                    });
                   else if(transList1.size()>0)
                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                pDialogg.dismiss();
                                new SweetAlertDialog(getActivity(), SweetAlertDialog.WARNING_TYPE)
                                        .setTitleText("Sorry")
                                        .setContentText("The transaction has been voided and uploaded")
                                        .setConfirmText(getString(R.string.Ok))
                                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                            @Override
                                            public void onClick(SweetAlertDialog sDialog) {
                                                sDialog.dismissWithAnimation();

                                            }
                                        })
                                        .show();

                            }
                        });
                  else  if(transList2.size()>0)
                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                pDialogg.dismiss();
                                new SweetAlertDialog(getActivity(), SweetAlertDialog.WARNING_TYPE)
                                        .setTitleText("Sorry")
                                        .setContentText("The transaction is not available for voiding")
                                        .setConfirmText(getString(R.string.Ok))
                                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                            @Override
                                            public void onClick(SweetAlertDialog sDialog) {
                                                sDialog.dismissWithAnimation();

                                            }
                                        })
                                        .show();

                            }
                        });
                    else
                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                pDialogg.dismiss();
                                new SweetAlertDialog(getActivity(), SweetAlertDialog.WARNING_TYPE)
                                        .setTitleText(getString(R.string.Errorr))
                                        .setContentText(getString(R.string.no_txns))
                                        .setConfirmText(getString(R.string.Ok))
                                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                            @Override
                                            public void onClick(SweetAlertDialog sDialog) {
                                                sDialog.dismissWithAnimation();

                                            }
                                        })
                                        .show();

                            }
                        });
                }

            }



        }catch (Exception e){
            Log.i("####WEWEWEWEW",e.toString());
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    pDialogg.dismiss();
                    new SweetAlertDialog(getActivity(), SweetAlertDialog.WARNING_TYPE)
                            .setTitleText(getString(R.string.Errorr))
                            .setContentText(getString(R.string.UnAbletoReadCard))
                            .setConfirmText(getString(R.string.Ok))
                            .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sDialog) {
                                    sDialog.dismissWithAnimation();

                                }
                            })
                            .show();

                }
            });

        }


    }


    private void initializeKeys() {
        KeyInfoProvider infoProvider = KeyInfoProvider.getInstance(getActivity().getApplicationContext());
        infoProvider.setKey(ALIAS_KEY_2KTDES, SampleAppKeys.EnumKeyType.EnumDESKey, SampleAppKeys.KEY_2KTDES);
        infoProvider.setKey(ALIAS_KEY_AES128, SampleAppKeys.EnumKeyType.EnumAESKey, SampleAppKeys.KEY_AES128);
        infoProvider.setKey(ALIAS_KEY_AES128_ZEROES, SampleAppKeys.EnumKeyType.EnumAESKey, SampleAppKeys.KEY_AES128_ZEROS);
        infoProvider.setKey(ALIAS_DEFAULT_FF, SampleAppKeys.EnumKeyType.EnumMifareKey, SampleAppKeys.KEY_DEFAULT_FF);
        objKEY_2KTDES_ULC = infoProvider.getKey(ALIAS_KEY_2KTDES_ULC, SampleAppKeys.EnumKeyType.EnumDESKey);
        objKEY_2KTDES = infoProvider.getKey(ALIAS_KEY_2KTDES, SampleAppKeys.EnumKeyType.EnumDESKey);
        objKEY_AES128 = infoProvider.getKey(ALIAS_KEY_AES128, SampleAppKeys.EnumKeyType.EnumAESKey);
        default_zeroes_key = infoProvider.getKey(ALIAS_KEY_AES128_ZEROES, SampleAppKeys.EnumKeyType.EnumAESKey);
        default_ff_key = infoProvider.getMifareKey(ALIAS_DEFAULT_FF);
    }


    private void initializeCipherinitVector() {

		/* Initialize the Cipher */
        try {
            cipher = Cipher.getInstance("AES/CBC/NoPadding");
        } catch (NoSuchAlgorithmException e) {

            e.printStackTrace();
        } catch (NoSuchPaddingException e) {
            e.printStackTrace();
        }

		/* set Application Master Key */
        bytesKey = KEY_APP_MASTER.getBytes();

		/* Initialize init vector of 16 bytes with 0xCD. It could be anything */
        byte[] ivSpec = new byte[16];
        Arrays.fill(ivSpec, (byte) 0xCD);
        iv = new IvParameterSpec(ivSpec);

    }

    public void alertSuccess(){
        status ="";
        SweetAlertDialog success =new SweetAlertDialog(getActivity(), SweetAlertDialog.SUCCESS_TYPE)
                .setTitleText(getString(R.string.CardUpdated))
                .setContentText(getString(R.string.successfully))
                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sDialog) {
                        sDialog.dismissWithAnimation();
                        tapview.setVisibility(View.VISIBLE);
                        views.setVisibility(View.GONE);


                    }
                });

        success.show();
        success.setCancelable(false);
    }
    private void desfireEV1CardLogic(String cardData) {
        try {
            if(status.equalsIgnoreCase("update")){

                update_card_balance(com.newpos.libpay.device.card.CardType.INMODE_NFC.getVal(),10000);

            }else{
                JSONObject carddata= new JSONObject(cardData);
                dt.setValuevoucher("");
                pdetails = carddata.getJSONObject("personaldetails");
                card_pin = carddata.getJSONObject("cardpin");
                data = carddata.getJSONObject("topups");
                UID =carddata.getString("uid");
                vouchervalue =data.getString("vochervalue");
                SimpleDateFormat datee = new SimpleDateFormat("yyyy-MM-dd");
                String dateStr=datee.format(new Date());
                txns =Transactions.find(Transactions.class," cardno =? and transactiontype = ? and receiptno=? and  isuploaded=? ",card_pin.getString("cardNo"),"0",receiptNumber,"0");
                // txns =Transactions.find(Transactions.class,"cardno =?",card_pin.getString("cardNo"));
                Log.i("##ERERTRTT",String.valueOf(txns.size()+"   erere"));
                if(txns.size()>0){

                    if(txns.get(txns.size() -1).getDeviceid().equalsIgnoreCase(dt.getMac())) {
                        Log.i("###VOUCHER_VALUE", vouchervalue);
                        Log.i("###VOUCHER_VALUE", txns.get(txns.size() - 1).getTotalamountchargedbyretail());
                        Log.i("###VOUCHER_VALUE", "-------------------------------------------");
                        vouchervalue = String.valueOf(Float.parseFloat(vouchervalue) + Float.parseFloat(txns.get(txns.size() - 1).getTotalamountchargedbyretail()));

                        Log.i("###VOUCHER_VALUE", vouchervalue);
                        data.put("vochervalue", vouchervalue);
                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                receiptNo.setText("Receipt No:        "+txns.get(txns.size() -1).getReceiptno());
                                date.setText("Date :                   "+txns.get(txns.size() -1).getTimestamp());
                                rationNo.setText("Ration No :          "+txns.get(txns.size() -1).getRationno());
                                value.setText("Total amount:      THB "+txns.get(txns.size() -1).getTotalamountchargedbyretail());
                                tapview.setVisibility(View.GONE);
                                views.setVisibility(View.VISIBLE);
                                pDialogg.dismiss();

                            }
                        });


                    }
                    else{
                        pDialogg.dismiss();
                        new SweetAlertDialog(getActivity(), SweetAlertDialog.WARNING_TYPE)
                                .setTitleText(getString(R.string.Errorr))
                                .setContentText("You are not allowed to void the transaction from this device")
                                .setConfirmText(getString(R.string.Ok))
                                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                    @Override
                                    public void onClick(SweetAlertDialog sDialog) {
                                        sDialog.dismissWithAnimation();

                                    }
                                })
                                .show();
                    }

                } else {
                    List<Transactions> transList=Transactions.find(Transactions.class," cardno =? and transactiontype = ? and receiptno=? and  isuploaded=? ",card_pin.getString("cardNo"),"-1",receiptNumber,"0");
                    List<Transactions> transList1=Transactions.find(Transactions.class," cardno =? and transactiontype = ? and receiptno=? and  isuploaded=? ",card_pin.getString("cardNo"),"-1",receiptNumber,"1");
                    List<Transactions> transList2=Transactions.find(Transactions.class," cardno =? and transactiontype = ? and receiptno=? and  isuploaded=? ",card_pin.getString("cardNo"),"0",receiptNumber,"1");
                    if(transList.size()>0)
                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                pDialogg.dismiss();
                                new SweetAlertDialog(getActivity(), SweetAlertDialog.WARNING_TYPE)
                                        .setTitleText("Sorry")
                                        .setContentText("The transaction has already been voided")
                                        .setConfirmText(getString(R.string.Ok))
                                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                            @Override
                                            public void onClick(SweetAlertDialog sDialog) {
                                                sDialog.dismissWithAnimation();

                                            }
                                        })
                                        .show();

                            }
                        });
                    else if(transList1.size()>0)
                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                pDialogg.dismiss();
                                new SweetAlertDialog(getActivity(), SweetAlertDialog.WARNING_TYPE)
                                        .setTitleText("Sorry")
                                        .setContentText("The transaction has been voided and uploaded")
                                        .setConfirmText(getString(R.string.Ok))
                                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                            @Override
                                            public void onClick(SweetAlertDialog sDialog) {
                                                sDialog.dismissWithAnimation();

                                            }
                                        })
                                        .show();

                            }
                        });
                    else  if(transList2.size()>0)
                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                pDialogg.dismiss();
                                new SweetAlertDialog(getActivity(), SweetAlertDialog.WARNING_TYPE)
                                        .setTitleText("Sorry")
                                        .setContentText("The transaction is not available for voiding")
                                        .setConfirmText(getString(R.string.Ok))
                                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                            @Override
                                            public void onClick(SweetAlertDialog sDialog) {
                                                sDialog.dismissWithAnimation();

                                            }
                                        })
                                        .show();

                            }
                        });
                    else
                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                pDialogg.dismiss();
                                new SweetAlertDialog(getActivity(), SweetAlertDialog.WARNING_TYPE)
                                        .setTitleText(getString(R.string.Errorr))
                                        .setContentText(getString(R.string.no_txns))
                                        .setConfirmText(getString(R.string.Ok))
                                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                            @Override
                                            public void onClick(SweetAlertDialog sDialog) {
                                                sDialog.dismissWithAnimation();

                                            }
                                        })
                                        .show();

                            }
                        });
                }

            }



        }catch (Exception e){
            Log.i("####WEWEWEWEW",e.toString());
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    pDialogg.dismiss();
                    new SweetAlertDialog(getActivity(), SweetAlertDialog.WARNING_TYPE)
                            .setTitleText(getString(R.string.Errorr))
                            .setContentText(getString(R.string.UnAbletoReadCard))
                            .setConfirmText(getString(R.string.Ok))
                            .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sDialog) {
                                    sDialog.dismissWithAnimation();

                                }
                            })
                            .show();

                }
            });

        }


    }
    public CardManager update_card_balance(int mode, int timeout) {

        final CardInfo cInfo = new CardInfo();
        CardManager cardManager = CardManager.getInstance(getContext(), mode,"write",data.toString().getBytes());
        cardManager.getCard(timeout, new CardListener() {
            @Override
            public void callback(CardInfo cardInfo) {
                dialog.dismiss();
                status ="";
                if(cardInfo.isResultFalg()){
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            pDialogg.dismiss();
                            alertSuccess();

                        }
                    });
                }else{

                    getActivity().runOnUiThread(new Runnable(){
                        @Override
                        public void run() {
                            pDialogg.dismiss();
                            new SweetAlertDialog(getActivity(), SweetAlertDialog.WARNING_TYPE)
                                    .setTitleText(getString(R.string.Errorr))
                                    .setContentText("Card update not successful!")
                                    .setConfirmText(getString(R.string.Ok))
                                    .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                        @Override
                                        public void onClick(SweetAlertDialog sDialog) {
                                            sDialog.dismissWithAnimation();

                                        }
                                    })
                                    .show();

                        }
                    });


                }

            }
        });

        return cardManager;
    }
    public CardManager read_nfc_card(int mode, int timeout) {
        final CardInfo cInfo = new CardInfo();
        CardManager cardManager = CardManager.getInstance(getActivity(), mode,"read",null);
        cardManager.getCard(timeout, new CardListener() {
            @Override
            public void callback(CardInfo cardInfo) {

                Log.d("##callback"," card info "+ cardInfo.getCarddata());
                try{

                    desfireEV1CardLogic(cardInfo.getCarddata());

                }catch(Exception e){

                    Log.d("##callback"," card info "+e.toString());

                }

            }
        });

        Log.d("##callback"," About to return the instance of the card manager ");
        return cardManager;
    }

}
