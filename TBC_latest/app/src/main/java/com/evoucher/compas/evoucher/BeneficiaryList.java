package com.evoucher.compas.evoucher;

/**
 * Created by Kibet on 9/2/2016.
 */

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.evoucher.compas.evoucher.Adapters.BeneficiaryAdapter;
import com.evoucher.compas.evoucher.network.BeneficiaryHttp;
import com.lynx.evoucher.models.Beneficiary;
import com.lynx.evoucher.models.FingerPrintVO;
import com.lynx.evoucher.models.Member;

import java.util.ArrayList;
import java.util.List;

import cn.pedant.SweetAlert.SweetAlertDialog;

public class BeneficiaryList extends AppCompatActivity {
    FloatingActionButton add;
    List<Beneficiary> beneficiaryList;
    BeneficiaryAdapter adapter;
    Details dt;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.beneficiary_list);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        //toolbar.setNavigationIcon(R.drawable.ic_launcher);
        setSupportActionBar(toolbar);
        dt=new Details(BeneficiaryList.this);
        TextView empty=(TextView)findViewById(R.id.empty);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        overridePendingTransition(0, 0);
        beneficiaryList = Beneficiary.listAll(Beneficiary.class);
        System.out.println("$$BEN_LIST$$ "+beneficiaryList.size());
        if(beneficiaryList.size() == 0){
            empty.setText("No Beneficiaries currently created");
        }else{
            adapter=new BeneficiaryAdapter(BeneficiaryList.this,new ArrayList<Beneficiary>(beneficiaryList));
            ListView list=(ListView)findViewById(R.id.profiles);
            list.setAdapter(adapter);
            list.setOnItemClickListener(listPairedClickItem);
            adapter.notifyDataSetChanged();
        }



        add=(FloatingActionButton)findViewById(R.id.floating_action_button) ;
        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(BeneficiaryList.this,BeneficiaryCreation.class).putExtra("w","YES"));
                BeneficiaryList.this.finish();
            }
        });
    }


    @Override
    public void onBackPressed() {
        startActivity(new Intent(new Intent(this, ProgrammeActivity.class)));
        BeneficiaryList.this.finish();
    }


    private AdapterView.OnItemClickListener listPairedClickItem = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
            final Beneficiary beneficiary=beneficiaryList.get(arg2);
            new MaterialDialog.Builder(BeneficiaryList.this)
                    .iconRes(R.drawable.ecompass)
                    .limitIconToDefaultSize() // limits the displayed icon size to 48dp
                    .title("REMOVE BENEFICIARY "+beneficiary.getFirstname().toUpperCase())
                    .content("Are you Sure??")
                    .positiveText("Yes")
                    .negativeText("No")
                    .callback(new MaterialDialog.ButtonCallback() {
                        @Override
                        public void onPositive(MaterialDialog dialog) {
                          // FingerPrintVO.deleteAll(FingerPrintVO.class);
                            beneficiary.delete();
                            List<FingerPrintVO> fingers=FingerPrintVO.find(FingerPrintVO.class,"beneficiaryid = ?",String.valueOf(beneficiary.getId()));
                            Log.d("#######Finger",String.valueOf(fingers.size()));
                            for(FingerPrintVO fing:fingers){
                                fing.delete();
                            }

                            alertSuccess();
                        }

                        @Override
                        public void onNegative(MaterialDialog dialog) {

                        }
                    })
                    .show();
        }
    };


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.beneficiary_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if(id == R.id.sync_beneficiary){
            performDataSysnc();
            return true;
        }

        if(id == android.R.id.home){
            startActivity(new Intent(BeneficiaryList.this,ProgrammeActivity.class));
            BeneficiaryList.this.finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    /**
     * uploads the beneficiary data to server
     */
    private void performDataSysnc() {
        new MaterialDialog.Builder(BeneficiaryList.this)
                .iconRes(R.drawable.ecompass)
                .limitIconToDefaultSize() // limits the displayed icon size to 48dp
                .title("DATA UPLOAD")
                .content("Proceed to upload details to server??")
                .positiveText("Yes")
                .negativeText("No")
                .callback(new MaterialDialog.ButtonCallback() {
                    @Override
                    public void onPositive(MaterialDialog dialog) {
                        BeneficiaryHttp beneficiaryHttp=new BeneficiaryHttp(BeneficiaryList.this);
                        beneficiaryHttp.UploadBeneficiary();
                    }

                    @Override
                    public void onNegative(MaterialDialog dialog) {

                    }
                })
                .show();
    }


    public void alertSuccess(){
        new SweetAlertDialog(BeneficiaryList.this, SweetAlertDialog.SUCCESS_TYPE)
                .setTitleText("DETAILS DELETED")
                .setContentText("SUCCESSFULLY")
                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sDialog) {
                        sDialog.dismissWithAnimation();
                        Intent intent=new Intent(BeneficiaryList.this,BeneficiaryList.class);
                        startActivity(intent);


                    }
                })

                .show();
    }
}
