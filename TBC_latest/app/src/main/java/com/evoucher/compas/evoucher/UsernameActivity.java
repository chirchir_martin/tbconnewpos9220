package com.evoucher.compas.evoucher;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Typeface;
import android.media.Image;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
//import android.print.PrintManager;
import android.provider.Settings;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.ThemedSpinnerAdapter;
import android.support.v7.widget.Toolbar;
import android.telephony.TelephonyManager;
import android.text.InputType;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.Toast;

import com.evoucher.compas.evoucher.network.BeneficiaryHttp;
import com.evoucher.compas.evoucher.ui.PrintingHandler;
import com.lynx.evoucher.models.Beneficiary;
import com.lynx.evoucher.models.CardBlock;
import com.lynx.evoucher.models.Configuration;
import com.lynx.evoucher.models.FingerPrintVO;
import com.lynx.evoucher.models.PurchasedProducts;
import com.lynx.evoucher.models.Reports;
import com.lynx.evoucher.models.SyncLogs;
import com.lynx.evoucher.models.TEST;
import com.lynx.evoucher.models.TopupsLogs;
import com.lynx.evoucher.models.Transactions;
import com.lynx.evoucher.models.TransactionsListProducts;
import com.lynx.evoucher.models.Users;
import com.newpos.libpay.device.printer.PrintManager;
import com.wang.avi.AVLoadingIndicatorView;

import junit.framework.Test;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import cn.pedant.SweetAlert.SweetAlertDialog;

public class UsernameActivity extends AppCompatActivity {
    private AVLoadingIndicatorView avi;
    EditText username;
    Button login;
    String result;
    com.evoucher.compas.evoucher.Details dt;
    Typeface typeface1;
    Configuration config = new Configuration();
    List<Configuration> configuration;
    List<Users> user;
    Spinner spinnerctrl;
    Button btn;
    Locale myLocale;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //requestWindowFeature(Window.FEATURE_NO_TITLE);
        //getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_username);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        dt = new com.evoucher.compas.evoucher.Details(UsernameActivity.this);
        typeface1 = Typeface.createFromAsset(getAssets(), "font0.otf");
        username = (EditText) findViewById(R.id.inputusername);

        PurchasedProducts.deleteAll(PurchasedProducts.class);
        List<CardBlock> vv=CardBlock.listAll(CardBlock.class);
         /*for(CardBlock c:vv){
             Log.i("####RATIONNO",c.getRationno()+"   CARDNO: "+c.getCardno());
         }*/
       /* List<Beneficiary> bb =Beneficiary.listAll(Beneficiary.class);
        for(Beneficiary b: bb){
            Log.i(" ####RATIONNO  ",b.getNational_id()+"   CARDNO  : "+b.getCard_number());
        }
*/
        login = (Button) findViewById(R.id.login);
        avi = (AVLoadingIndicatorView) findViewById(R.id.avi);
        user = Users.listAll(Users.class);
        // Log.i("##EFEFDFDFDF",user.get(0).getLocationid()+"    "+user.get(0).getUsername());
        for (Users u : user) {
            u.setIsloggedin("0");
            u.save();
        }
        configuration = Configuration.listAll(Configuration.class);
        if (configuration.size() == 0) {
            config.setUrl("tbc.compasglobal.com");
            config.setPort("80");
            config.save();

        }
        PurchasedProducts.deleteAll(PurchasedProducts.class);
        //Transactions.deleteAll(Transactions.class);
        Log.i("##SDSDSDs", String.valueOf(TopupsLogs.listAll(TopupsLogs.class).size() + "   flflfl"));

        avi.setIndicator("LineScaleIndicator");
        // TelephonyManager tManager = (TelephonyManager)UsernameActivity.this.getSystemService(Context.TELEPHONY_SERVICE);
        // String uid = tManager.getDeviceId();
        String android_id = Settings.Secure.getString(getApplicationContext().getContentResolver(),
                Settings.Secure.ANDROID_ID);


        Log.i("###dfpdfpdf", android_id);
      /*  WifiManager wifiManager = (WifiManager)getApplicationContext(). getSystemService(Context.WIFI_SERVICE);
        WifiInfo wInfo = wifiManager.getConnectionInfo();
        String macAddress = wInfo.getMacAddress();*/
        dt.setMac(android_id);
        // Log.d("dpskddpkadppa", macAddress);
        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    Verify(username.getText().toString());
                } catch (Exception e) {

                }

            }
        });

          Log.i("##DFFDFDFDF",String.valueOf(TEST.listAll(Test.class)));

      /*  spinnerctrl = (Spinner) findViewById(R.id.spinner1);
        spinnerctrl.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            public void onItemSelected(AdapterView<?> parent, View view,
                                       int pos, long id) {

                if (pos == 1) {

                    Intent intent = new Intent(Intent.ACTION_MAIN);
                    intent.setClassName("com.android.settings", "com.android.settings.LanguageSettings");
                    startActivity(intent);
                    setLocale("th");
                    finish();
                } else if (pos == 2) {

                    setLocale("bm");
                    finish();
                } else if (pos == 3) {

                    setLocale("en");
                    finish();

                }

            }

            public void onNothingSelected(AdapterView<?> arg0) {
                // TODO Auto-generated method stub
            }

        });*/
        new Thread(new Runnable() {

            @Override
            public void run() {

               // PrintManager.getmInstance(UsernameActivity.this, new PrintingHandler(UsernameActivity.this)).testPrinter();

            }

        }).start();
    }

    public void setLocale(String lang) {

        myLocale = new Locale(lang);
        Resources res = getResources();
        DisplayMetrics dm = res.getDisplayMetrics();
        android.content.res.Configuration conf = res.getConfiguration();
        conf.locale = myLocale;
        res.updateConfiguration(conf, dm);
        Intent refresh = new Intent(this, UsernameActivity.class);
        startActivity(refresh);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
     /*   if(dt.getLevel().equalsIgnoreCase("0"))
            getMenuInflater().inflate(R.menu.admin_menu, menu);
        else if (dt.getLevel().equalsIgnoreCase("1"))
            getMenuInflater().inflate(R.menu.programmes_menu, menu);*/
        getMenuInflater().inflate(R.menu.languages, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.english) {
            setLocale("en");
            finish();
        }
        if (id == R.id.karen) {
            setLocale("th");
            finish();
        }


        return super.onOptionsItemSelected(item);
    }

    public void Verify(String username) throws Exception {
        final String PREFS_NAME = "MyPrefsFile";
        SharedPreferences settings = getSharedPreferences(PREFS_NAME, 0);
        if (settings.getBoolean("fistTime", true)) {
            if (username.equalsIgnoreCase("Admin")) {
                startActivity(new Intent(UsernameActivity.this, PinActivity.class));
                finish();
            } else {
                //Toast.makeText(UsernameActivity.this, "INVALID", Toast.LENGTH_LONG).show();
                showAlert(getString(R.string.Errorr), getString(R.string.InvalidUserName));
            }
            //settings.edit().putBoolean("fistTime", false).apply();


        } else {
            List<Users> user = Users.listAll(Users.class);
            if (user.size() != 0) {

                for (Users ur : user) {
                    Log.d("##username", ur.getUsername());
                    Log.d("##password", ur.getPassword());
                    if (username.equalsIgnoreCase(ur.getUsername())) {
                        ur.setIsloggedin("1");
                        ur.save();

                        dt.setUser(username);
                        dt.setStatus("");

                        Log.d("##E#EEWEEWEWe", ur.getLevel() + "p;pp");
                        dt.setLevel(ur.getLevel());
                        startActivity(new Intent(UsernameActivity.this, PinActivity.class));
                        finish();
                        result = "";
                        break;
                    } else {
                        result = "0";
                        // Toast.makeText(UsernameActivity.this, "INVALID", Toast.LENGTH_LONG).show();
                        //
                    }

                }
                if (result.equalsIgnoreCase("0")) {

                    showAlert(getString(R.string.Errorr), getString(R.string.InvalidUserName));

                }



            } else {

                // Toast.makeText(UsernameActivity.this, "NO USERS FOUND", Toast.LENGTH_LONG).show();
                showAlert(getString(R.string.Errorr), getString(R.string.NoUserFound));

            }
        }
    }

    public void showAlert(String title, String message) {
        Log.d("ALERT", "ALERT");
        new SweetAlertDialog(UsernameActivity.this, SweetAlertDialog.WARNING_TYPE)
                .setTitleText(title)
                .setContentText(message)
                .setConfirmText("Ok")
                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sDialog) {
                        sDialog.dismissWithAnimation();

                    }
                })
                .show();
    }





}