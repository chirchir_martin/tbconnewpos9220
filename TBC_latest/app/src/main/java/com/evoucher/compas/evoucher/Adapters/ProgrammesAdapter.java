package com.evoucher.compas.evoucher.Adapters;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.amulyakhare.textdrawable.TextDrawable;
import com.amulyakhare.textdrawable.util.ColorGenerator;
import com.evoucher.compas.evoucher.ItemClickListener;
import com.lynx.evoucher.models.Programmes;
import com.evoucher.compas.evoucher.R;

import java.util.List;

/**
 * Created by Administrator on 8/17/2016.
 */
public class ProgrammesAdapter extends RecyclerView.Adapter<ProgrammesAdapter.MyViewHolder> {
    private ItemClickListener clickListener;
    private List<Programmes> programmes;
    Context context;
    String tag;
    public TextView  d;



    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        public TextView title, price, d;
        ImageView im;
        CardView card;

        public MyViewHolder(View view) {
            super(view);

            im=(ImageView)view.findViewById(R.id.main);
            title=(TextView)view.findViewById(R.id.title);
            card=(CardView)view.findViewById(R.id.cardview);


            card.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if (clickListener != null)
                tag=view.getTag(R.id.holder).toString();
                clickListener.onClick(view,Integer.parseInt(tag));
            Log.d("#####CLICK1",view.getTag(R.id.holder).toString());

        }
    }


    public ProgrammesAdapter(Context context, List<Programmes> programmes) {

      this.programmes = programmes;
        this.context=context;

        }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.programmes_row, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {

        Typeface typeface = Typeface.createFromAsset(context.getAssets(), "Roboto-Thin.ttf");
      //  holder.title.setTypeface(typeface);
       // holder.price.setTypeface(typeface);
        ColorGenerator generator = ColorGenerator.DEFAULT;
       holder.card.setTag(R.id.holder,programmes.get(position).getProgramid());
        System.out.println("##PROGRAM.GET");
        int color = generator.getColor(position);
        if(programmes.get(position).getProgrammename().length()>3){
            String lettersForName =programmes.get(position).getProgrammename().substring(0, 2);


            TextDrawable letterDrawable = TextDrawable.builder()
                     .buildRound(lettersForName, color);

            holder.im.setImageDrawable(letterDrawable);
        }else{
            TextDrawable letterDrawable = TextDrawable.builder()
                    .buildRound(programmes.get(position).getProgrammename(), color);

            holder.im.setImageDrawable(letterDrawable);
        }
        holder.title.setText(programmes.get(position).getProgrammename());
       /* Log.d("sdsd0dsdsd",programmes.get(position).getimage()+position);
        Bitmap decodedByte=null;
        byte[] decodedString = Base64.decode(programmes.get(position).getimage(), Base64.DEFAULT);
        decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
        holder.im.setImageBitmap(decodedByte);*/

     /*   holder.im.setOnClickListener(new View.OnClickListener() {
            @Override public void onClick(View v) {
               showCustomView1();
            }
        });*/

    }

 @Override
    public int getItemCount() {
        return programmes.size();
    }
      public void setClickListener(ItemClickListener itemClickListener) {

          this.clickListener = itemClickListener;
    }

}