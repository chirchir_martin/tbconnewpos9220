package com.evoucher.compas.evoucher.Adapters;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Typeface;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.GravityEnum;
import com.afollestad.materialdialogs.MaterialDialog;
import com.afollestad.materialdialogs.Theme;
import com.evoucher.compas.evoucher.CartActivity;
import com.evoucher.compas.evoucher.Details;
import com.evoucher.compas.evoucher.ItemClickListener;
import com.evoucher.compas.evoucher.R;
import com.lynx.evoucher.models.Products;
import com.lynx.evoucher.models.PurchasedProducts;
import com.lynx.evoucher.models.SyncLogs;
import com.lynx.evoucher.models.Topups;

import org.json.JSONArray;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Administrator on 8/17/2016.
 */


public class SyncAdapter extends RecyclerView.Adapter<SyncAdapter.MyViewHolder> {
    private ItemClickListener clickListener;
    private List<Products> products;
    Context context;
    List<SyncLogs> slogs;

    CardView card;
    List<Topups> tps;
    JSONArray tops;
    public TextView d;
    String tag;
    Details dt;

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public TextView deviveid, nooftxn, amount, date;
        ImageView im;
        CardView card;

        public MyViewHolder(View view) {
            super(view);


            deviveid = (TextView) view.findViewById(R.id.deviceid);
            amount = (TextView) view.findViewById(R.id.amount);
            nooftxn = (TextView) view.findViewById(R.id.no_of_txns);
            date = (TextView) view.findViewById(R.id.date);




        }

        @Override
        public void onClick(View view) {
            if (clickListener != null)
                tag = view.getTag(R.id.holder).toString();
            clickListener.onClick(view, Integer.parseInt(tag));
        }
    }


    public SyncAdapter(Context context, List<SyncLogs> logs) {

        slogs = logs;
        this.context = context;

        dt = new Details(context);
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.sync_row, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {

        Typeface typeface = Typeface.createFromAsset(context.getAssets(), "Roboto-Thin.ttf");
        holder.deviveid.setText("Device id:  " + slogs.get(position).getDeviceid());
        holder.nooftxn.setText("No of txns:  " + slogs.get(position).getNooftxns());
        holder.amount.setText("Amount:  " + slogs.get(position).getTotalamt());
        holder.date.setText("Date:  " + slogs.get(position).getDate());



    }

    @Override
    public int getItemCount() {
        return slogs.size();
    }

    public void setClickListener(ItemClickListener itemClickListener) {

        this.clickListener = itemClickListener;
    }
    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }
}

