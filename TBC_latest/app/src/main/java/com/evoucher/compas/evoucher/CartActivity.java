package com.evoucher.compas.evoucher;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.nfc.NdefMessage;
import android.nfc.NdefRecord;
import android.nfc.NfcAdapter;
import android.nfc.Tag;
import android.nfc.tech.Ndef;
import android.nfc.tech.NdefFormatable;
import android.os.Handler;
import android.os.Parcelable;
import android.provider.Settings;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.FloatProperty;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.evoucher.compas.evoucher.Adapters.CartAdapter;
import com.evoucher.compas.evoucher.card.KeyInfoProvider;
import com.evoucher.compas.evoucher.card.SampleAppKeys;
import com.evoucher.compas.evoucher.p2p.DeviceListFragment;
import com.lynx.evoucher.models.BeneficiaryGroup;
import com.lynx.evoucher.models.Products;
import com.lynx.evoucher.models.Programmes;
import com.lynx.evoucher.models.PurchasedProducts;
import com.lynx.evoucher.models.Reprints;
import com.lynx.evoucher.models.Topups;
import com.lynx.evoucher.models.Commodities;
import com.lynx.evoucher.models.Transactions;
import com.lynx.evoucher.models.TransactionsListProducts;
import com.lynx.evoucher.models.Users;
import com.newpos.libpay.PaySdk;
import com.newpos.libpay.device.card.CardInfo;
import com.newpos.libpay.device.card.CardListener;
import com.newpos.libpay.device.card.CardManager;
import com.newpos.libpay.device.printer.Quantitydetails;
import com.newpos.libpay.device.printer.TBCTransactionReceipt;
import com.nxp.nfclib.CardType;
import com.nxp.nfclib.KeyType;
import com.nxp.nfclib.NxpNfcLib;
import com.nxp.nfclib.desfire.DESFireFactory;
import com.nxp.nfclib.desfire.IDESFireEV1;
import com.nxp.nfclib.exceptions.NxpNfcLibException;
import com.nxp.nfclib.interfaces.IKeyData;


import org.json.JSONArray;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import javax.crypto.Cipher;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.IvParameterSpec;

import cn.pedant.SweetAlert.SweetAlertDialog;

public class CartActivity extends AppCompatActivity {
    private String  openingbal="";
    private String  vid="";
    private String receiptno="";
    private IKeyData objKEY_2KTDES_ULC = null;
    private IKeyData objKEY_2KTDES = null;
    private IKeyData objKEY_AES128 = null;
    private byte[] default_ff_key = null;
    private IKeyData default_zeroes_key = null;
    private NxpNfcLib libInstance = null;
    private IDESFireEV1 desFireEV1;
    private CardType mCardType = CardType.UnknownCard;
    private Cipher cipher = null;
    private byte[] bytesKey = null;
    private IvParameterSpec iv = null;
    private static final String KEY_APP_MASTER = "This is my key  ";
    private static final String ALIAS_KEY_AES128 = "key_aes_128";
    private static final String ALIAS_KEY_2KTDES = "key_2ktdes";
    private static final String ALIAS_KEY_2KTDES_ULC = "key_2ktdes_ulc";
    private static final String ALIAS_DEFAULT_FF = "alias_default_ff";
    private static final String ALIAS_KEY_AES128_ZEROES = "alias_default_00";
    private static final String EXTRA_KEYS_STORED_FLAG = "keys_stored_flag";
    static String packageKey = "b927aab8842ab54cbaf2ea1df9917159";

    Activity instance;
    public static PaySdk sdkInstance;
    MaterialDialog pDialogg;
    List<PurchasedProducts> products;
    List<Products> pds,pddd;
    JSONObject topups;
    String receiptNo ="";
    ArrayList<Products> pdd=new ArrayList<Products>();
    TextView tl,tq,tn,ur;
    NfcAdapter nfcAdapter;
    // List<Topups> tops;
    MaterialDialog dialog;
    StringBuilder sb;
    JSONArray dd,aa;
    String state="";
    String msg ="";
    ViewGroup card_views;
    String vvalue ="0";
    Button btnsubmit;
    Button btnprint;
    float total =0.00f,totalq;
    Details dt;
    public static String status="";
    List<Topups> ts;
    Network net;
    TBCTransactionReceipt receipt;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cart);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        net=new Network(CartActivity.this,"");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        dt=new Details(CartActivity.this);
        vvalue =getIntent().getStringExtra("VOUCHERVALUE");
        dt.setValuevoucher(vvalue);
        nfcAdapter = NfcAdapter.getDefaultAdapter(this);
        ur=(TextView)findViewById(R.id.ur) ;
        ur.setVisibility(View.GONE);
        card_views =(ViewGroup)findViewById(R.id.card_views);
        ur.setText("Logged in as "+dt.getUser());
        setData();
        btnsubmit=(Button)findViewById(R.id.submit);
        initializeLibrary();
        initializeKeys();
        initializeCipherinitVector();
        instance=this;
        sdkInstance=PaySdk.getInstance();
        try {
            sdkInstance.init(instance);
        }catch (Exception e){

        }
        btnsubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                try {
                    Thread.sleep(500);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                if(pdd.size()==0){
                    if(state.equalsIgnoreCase("retry")){
                        status="update";
                        dialog=new MaterialDialog.Builder(CartActivity.this)
                                .title(getString(R.string.TapToUpdate))
                                .content("")
                                .progress(true, 0)
                                .progressIndeterminateStyle(true)
                                .cancelable(false)
                                .show();
                        final Handler handler = new Handler();
                        handler.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                status ="";
                                dialog.dismiss();
                            }

                        }, 10000);
                    }else {
                        new SweetAlertDialog(CartActivity.this, SweetAlertDialog.WARNING_TYPE)
                                .setTitleText("")
                                .setContentText(getString(R.string.NoItems))
                                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                    @Override
                                    public void onClick(SweetAlertDialog sDialog) {
                                        sDialog.dismissWithAnimation();
                                        Intent intent = new Intent(CartActivity.this, MainScreenActivity.class).putExtra("VOUCHERVALUE",vvalue);
                                        startActivity(intent);
                                        finish();

                                    }
                                })

                                .show();

                    }
                }else{
                    status="update";
                    dialog=new MaterialDialog.Builder(CartActivity.this)
                            .title(R.string.TapToUpdate)
                            .content("")
                            .progress(true, 0)
                            .progressIndeterminateStyle(true)
                            .cancelable(false)
                            .show();
                    final Handler handler = new Handler();
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            status ="";
                            dialog.dismiss();
                        }

                    }, 10000);
                    topups =dt.getTopups();
                    float amount=0;
                    try {
                        Log.d("##log"," balance "+total);
                        openingbal=topups.getString("vochervalue");
                        amount=Float.parseFloat(topups.getString("vochervalue")) - total;
                        topups.put("vochervalue",(String.format("%.2f",Float.parseFloat(""+amount))));
                        String voucherno=topups.getString("voucherno");
                        saveData(""+amount,voucherno);
                        update_card_balance(com.newpos.libpay.device.card.CardType.INMODE_NFC.getVal(),10000);
                    }catch (Exception e){

                    }

                }

            }
        });
    }


    public void printData(boolean newpos){

        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
        String time= sdf.format(new Date());
        dt.setTime(time);
        List<Transactions> txns= Transactions.find(Transactions.class, null, null, null, "id DESC", "1");
        List<Commodities> items = Commodities.find(Commodities.class, "transactionno =?", txns.get(0).getId().toString());
        Backup.outPutLogs("##Transasction => ReceiptNo "+txns.get(0).getReceiptno()+"  RationNo "+txns.get(0).getRationno()+"  Amount "+txns.get(0).getTotalamountchargedbyretail()+"   Card Balance "+txns.get(0).getTotalvaluereamining()+"   Transaction date "+txns.get(0).getDate()+"  Time "+txns.get(0).getTimestamp(),"",getApplicationContext());
        Log.d("##log", "##Transasction => ReceiptNo "+txns.get(0).getReceiptno()+"  RationNo "+txns.get(0).getRationno()+"  Amount "+txns.get(0).getTotalamountchargedbyretail()+"   Card Balance "+txns.get(0).getTotalvaluereamining()+"   Transaction date "+txns.get(0).getDate()+"  Time "+txns.get(0).getTimestamp());
        receiptno=txns.get(0).getReceiptno();
        List<Receipt> receiptList = new ArrayList<Receipt>();
        double totalprice = 0.0;
        double valueremaining = 0.0;
        Commodities commodity_table;
        SimpleDateFormat datee = new SimpleDateFormat("yyyy-MM-dd");
        SimpleDateFormat date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String pitems="";
        List lst= new ArrayList<Quantitydetails>();
        for (int i = 0; i < items.size(); i++) {
            String comm="";
            Receipt receipt = new Receipt();
            receipt.name = split(Products.find(Products.class, "productid =?", items.get(i).getProductid()).get(0).getproductname());
            receipt.qty = items.get(i).getQuantitydeducted();
            receipt.val = items.get(i).getTotalamountcahgerdbyretailer();
            receipt.uom = "(" + items.get(i).getUom() + ")";
            receiptList.add(receipt);
            comm=receipt.name+"##"+receipt.qty+"##"+receipt.val+"##"+receipt.uom;
            Quantitydetails det= new Quantitydetails();
            det.setName(receipt.name);
            det.setQuantity(receipt.qty);
            det.setValue(receipt.val);
            lst.add(det);
            pitems=pitems+"###"+comm;
        }
        dt.setDate(txns.get(0).getDate());
        receipt= new TBCTransactionReceipt();
        receipt.setDate(txns.get(0).getDate());
        receipt.setCloseval(txns.get(0).getTotalvaluereamining());
        receipt.setOpenbal(openingbal);
        receipt.setTransval(txns.get(0).getTotalamountchargedbyretail());
        receipt.setDetails(lst);
        receipt.setVoucherid(vid);
        receipt.setTime(dt.getTime());
        receipt.setReceiptNo(receiptno);
        receipt.setMacid(dt.getMac());
        receipt.setUsername(dt.getUser());
        receipt.setRationNo(txns.get(0).getRationno());
        saveReceipt(pitems,txns.get(0).getTotalamountchargedbyretail(),openingbal,txns.get(0).getTotalvaluereamining(),receiptno,txns.get(0).getRationno(),vid);
        if(newpos){
            new Thread(new Runnable() {
                @Override
                public void run() {
                    receipt.setOwner("Card Holder Copy");
                    sdkInstance.printReceipt(receipt, PaySdk.ReceiptType.tbc_txn,"Card Holder Copy"," ");
                    receipt.setOwner("Merchant Copy");
                    sdkInstance.printReceipt(receipt, PaySdk.ReceiptType.tbc_txn,"Merchant Copy"," ");
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {

                            alertSuccess();

                        }
                    });
                }

            }).start();
            return;
        }
        try {
            String devAddress = BlueTooth.getAddress("BT-SPP");
            if (devAddress == null) {
                SweetAlertDialog dd = new SweetAlertDialog(CartActivity.this, SweetAlertDialog.ERROR_TYPE)
                        .setTitleText(CartActivity.this.getString(R.string.Sorry))
                        .setContentText(CartActivity.this.getString(R.string.printernotpaired))
                        .setConfirmText(CartActivity.this.getString(R.string.Ok))
                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sDialog) {
                                sDialog.dismissWithAnimation();
                                Intent intent = new Intent(CartActivity.this, ProgrammeActivity.class);
                                startActivity(intent);
                                finish();

                            }
                        });
                dd.show();
                dd.setCancelable(false);

            }else {

                CarryingBillPrint carryingBillPrint = new CarryingBillPrint();
                dt.setDate(txns.get(0).getDate());
                carryingBillPrint.printReceipt(dt,receiptList, txns.get(0).getTotalamountchargedbyretail(), txns.get(0).getTotalvaluereamining(), txns.get(0).getRationno(), CartActivity.this,openingbal,vid,receiptno);
                carryingBillPrint.printMerReceipt(dt,receiptList, txns.get(0).getTotalamountchargedbyretail(), txns.get(0).getTotalvaluereamining(), txns.get(0).getRationno(), CartActivity.this,openingbal,vid,receiptno);
                alertSuccess();

            }

        }catch (Exception e){

        }

}

    public void update(String tag){
        Log.d("######CART",tag);
        PurchasedProducts pd=PurchasedProducts.findById(PurchasedProducts.class,Long.parseLong(tag));
        String qty;
        Log.d("######CARTwww",String.valueOf(pd.getQuantitydeducted()));
        // tops=Topups.find(Topups.class,"productid = ?",pd.getProductId());
        //  tops= Topups.find(Topups.class, "voucherid=? and cardnumber =? and bengroup=?",dt.getVoucherid(),dt.getCardnumber(),dt.getBenGroup());
        float totalp = Float.parseFloat(vvalue);
        Log.d("######CART",String.valueOf(totalp));
        Log.d("######CART",String.valueOf(pd.getQuantitydeducted()));
        vvalue =String.valueOf(totalp+Float.parseFloat(pd.getTotalprice()));
        dt.setValuevoucher(vvalue);
        //top
        // s.get(0).save();
        pd.delete();
        setData();
       /* if(tops.get(0).getvocherType().equalsIgnoreCase("CM")) {
            tops= Topups.find(Topups.class, "productid =? and voucherid=? and cardnumber = ? and bengroup=?",pd.getProductId(),dt.getVoucherid(),dt.getCardnumber(),dt.getBenGroup());

            int totalqty = Math.round(Float.parseFloat(tops.get(0).getProductQuantity()));
            Log.d("######CART",String.valueOf(totalqty));
            Log.d("######CART",String.valueOf(pd.getQuantitydeducted()));
            tops.get(0).setProductQuantity(String.valueOf(totalqty+Integer.parseInt(pd.getQuantitydeducted())));
            tops.get(0).save();
            pd.delete();

            setData();
        }else{
            tops= Topups.find(Topups.class, "voucherid=? and cardnumber =? and bengroup=?",dt.getVoucherid(),dt.getCardnumber(),dt.getBenGroup());
            int totalqty = Math.round(Float.parseFloat(dt.getValuevoucher()));
            Log.d("######CART",String.valueOf(totalqty));
            Log.d("######CART",String.valueOf(pd.getQuantitydeducted()));
           dt.setValuevoucher(String.valueOf(totalqty+Integer.parseInt(pd.getTotalprice())));
            //tops.get(0).save();
            pd.delete();
            setData();
        }*/
    }
    public void alertSuccess(){
        new SweetAlertDialog(this, SweetAlertDialog.SUCCESS_TYPE)
                .setTitleText("Printed")
                .setContentText(getString(R.string.successfully))
                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sDialog) {
                        sDialog.dismissWithAnimation();

                       // printData();
                        Intent intent=new Intent(CartActivity.this,ProgrammeActivity.class);
                        startActivity(intent);
                        finish();

                    }
                })

                .show();
    }

    public void setData(){
        total=0;
        totalq=0;
        pdd.clear();
        dt=new Details(CartActivity.this);
        // pds=Products.find(Products.class,"productid = ? and voucherid = ? and programmeid = ?",products.get(i).getProductId(),dt.getVoucherid(),dt.getProgrammeid());
        products=PurchasedProducts.listAll(PurchasedProducts.class);
        tl=(TextView)findViewById(R.id.total);
        tq=(TextView)findViewById(R.id.t0);
        tn=(TextView)findViewById(R.id.no);
        Log.d("######CART",String.valueOf(products.size())+"SIZE");
        //ts=Topups.find(Topups.class, "voucherid=? and cardnumber =? and bengroup=?",dt.getVoucherid(),dt.getCardnumber(),dt.getBenGroup());
        //  String vtp=ts.get(0).getvocherType();

        //  Log.d("VOUCHERTYPE",vtp);
        /*if(vtp.equalsIgnoreCase("CA")){
            tl.setVisibility(View.GONE);
            tq.setVisibility(View.GONE);
            tn.setVisibility(View.GONE);
        }*/
        for (int i = 0; i < products.size(); i++) {
            pds=Products.find(Products.class,"productid = ?",products.get(i).getProductId());
            pdd.addAll(pds);
            total+=Float.parseFloat(products.get(i).getTotalprice());
            // if(!vtp.equalsIgnoreCase("CA")) {
            totalq += Float.parseFloat(products.get(i).getQuantitydeducted());
            // }
            //totalq+=Float.parseFloat(products.get(i).getQuantity());
        }
        Log.d("######CART",String.valueOf(  pdd.size())+"PDD");
        Log.d("######CART",String.valueOf(products.size())+"PP");
        tl.setText(String.valueOf("Total amount: THB "+total));
        //tq.setText(Strtotalq);
        tq.setText(String.format("%.2f", totalq));
        CartAdapter ad=new CartAdapter(CartActivity.this,pdd);
        ListView tv=(ListView)findViewById(R.id.items);
        tv.setAdapter(ad);
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == android.R.id.home) {

            Intent intent1 = new Intent(this, MainScreenActivity.class).putExtra("VOUCHERVALUE",vvalue);
            startActivity(intent1);
            finish();

        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        Intent intent1 = new Intent(this, MainScreenActivity.class).putExtra("VOUCHERVALUE",vvalue);
        startActivity(intent1);
        finish();
    }
    private void enableForegroundDispatchSystem() {

        Intent intent = new Intent(this, CartActivity.class).addFlags(Intent.FLAG_RECEIVER_REPLACE_PENDING);

        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, 0);

        IntentFilter[] intentFilters = new IntentFilter[]{};

        nfcAdapter.enableForegroundDispatch(this, pendingIntent, intentFilters, null);
    }

    private void disableForegroundDispatchSystem() {
        nfcAdapter.disableForegroundDispatch(this);
    }

    public void cardLogic(final Intent intent) {
        CardType type = CardType.UnknownCard;
        try {
            type = libInstance.getCardType(intent);
        } catch (NxpNfcLibException ex) {
            Toast.makeText(CartActivity.this, ex.getMessage(), Toast.LENGTH_SHORT).show();
        }

        switch (type) {

            case DESFireEV1: {
                mCardType = CardType.DESFireEV1;
                desFireEV1 = DESFireFactory.getInstance().getDESFire(libInstance.getCustomModules());
                try {
                    desFireEV1.getReader().connect();
                    desFireEV1.getReader().setTimeout(2000);
                    dialog.dismiss();
                    pDialogg = new MaterialDialog.Builder(CartActivity.this)
                            .title(getString(R.string.Updatingcard))
                            .content(getString(R.string.DontRemoveCard))
                            .progress(true, 0)
                            .progressIndeterminateStyle(false)
                            .show();
                    pDialogg.setCancelable(false);
                    new Thread(new Runnable() {
                        @Override
                        public void run() {
                            desfireEV1CardLogic();
                        }
                    }).start();

                } catch (Exception t) {
                    t.printStackTrace();
                    Log.d("##ERRORRR", t.toString());

                }
                break;
            }


        }
    }

    @TargetApi(19)
    private void initializeLibrary() {
        libInstance = NxpNfcLib.getInstance();
        try {
            libInstance.registerActivity(CartActivity.this, packageKey);
        } catch (NxpNfcLibException ex) {
            Toast.makeText(CartActivity.this, ex.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }


    public void saveData(String amt,String vno){

        //  String time=String.valueOf(System.currentTimeMillis());
        double totalprice=0.0;
        double valueremaining=0.0;
        Commodities commodity_table;
        SimpleDateFormat datee = new SimpleDateFormat("yyyy-MM-dd");
        SimpleDateFormat date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        //products.size();
        List<PurchasedProducts> products=PurchasedProducts.listAll(PurchasedProducts.class);
        for(int i=0;i<products.size();i++) {
            totalprice += Float.parseFloat(products.get(i).getTotalprice());
        }

        valueremaining = Float.parseFloat(amt);
        Transactions transactionstable=new Transactions();
        transactionstable.setVoucherid(dt.getVoucherid());
        String username ="";
        String locationid ="";
        try{
            username=Users.find(Users.class,"isloggedin =?","1").get(0).getUsername();
            locationid =Users.find(Users.class,"isloggedin =?","1").get(0).getLocationid();

        }catch (Exception e){
            username ="";
            locationid ="";
        }
        dt.setUser(username);
        transactionstable.setUser(username);
        String android_id = Settings.Secure.getString(getApplicationContext().getContentResolver(),
                Settings.Secure.ANDROID_ID);
        transactionstable.setTransactiontype("0");

        transactionstable.setVoucheridno(vno);

        vid=dt.getVoucherno();
        transactionstable.setDeviceid(android_id);
        dt.setMac(android_id);
        transactionstable.setRationno(dt.getRationNo());
        transactionstable.setCardno(dt.getCardnumber());
        transactionstable.setDate(datee.format(new Date()));
        transactionstable.setLocationid(locationid);
        transactionstable.setIsuploaded("0");
        transactionstable.setTotalamountchargedbyretail(String.format("%.02f", totalprice));
        transactionstable.setTotalvaluereamining(String.valueOf(valueremaining));
        transactionstable.setTimestamp(date.format(new Date()));
       // receiptNo =String.valueOf(android_id+System.currentTimeMillis());
        receiptNo =String.valueOf(System.currentTimeMillis());
        transactionstable.setReceiptno(receiptNo);
        transactionstable.save();
        List<Transactions> transaction= Transactions.find(Transactions.class, null, null, null, "id DESC", "1");
        for(int i=0;i<products.size();i++) {

            commodity_table=new Commodities();
            commodity_table.setProductid(products.get(i).getProductId());
            commodity_table.setQuantityremaining("0");
            commodity_table.setUom(products.get(i).getUom());
            commodity_table.setQuantitydeducted(products.get(i).getQuantitydeducted());
            commodity_table.setTotalamountcahgerdbyretailer(String.format("%.02f", Float.parseFloat(String.valueOf(products.get(i).getTotalprice()))));
            commodity_table.setTransactionno(transaction.get(0).getId().toString());
            commodity_table.save();

            List<Products> pr=Products.find(Products.class, "productid = ?", products.get(i).getProductId());

            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            String strDate = sdf.format(new Date());

            List<TransactionsListProducts> tr=TransactionsListProducts.find(TransactionsListProducts.class, "productid = ? and unitofmeasure =? and transactiondate =?", products.get(i).getProductId(),products.get(i).getUom(), strDate);

            if (tr.size()>0){
                tr.get(0).setVal(String.valueOf(Float.parseFloat(tr.get(0).getVal())+Float.parseFloat(products.get(i).getTotalprice())));
                tr.get(0).setQuantity(String.valueOf(Float.parseFloat(tr.get(0).getQuantity())+Float.parseFloat(products.get(i).getQuantitydeducted())));
                tr.get(0).save();
            }else {
                TransactionsListProducts transactionsListProducts=new TransactionsListProducts();
                transactionsListProducts.setProductid(pr.get(0).getProductid());
                transactionsListProducts.setProductname(pr.get(0).getproductname());
                transactionsListProducts.setQuantity(products.get(i).getQuantitydeducted());
                transactionsListProducts.setUnitofmeasure(products.get(i).getUom());
                transactionsListProducts.setVal(String.valueOf(products.get(i).getTotalprice()));
                transactionsListProducts.setTransactiondate(strDate);
                transactionsListProducts.save();

            }


        }

        PurchasedProducts.deleteAll(PurchasedProducts.class);


    }

    /**
     * Initialize the Cipher and init vector of 16 bytes with 0xCD.
     */


    private void desfireEV1CardLogic() {
        byte[] appId = new byte[]{0x1, 0x00, 0x00};
        JSONObject data;
        String amount ="";
        int timeOut = 2000;
        try {
            String uidhex = new String();
            byte[] uid = desFireEV1.getUID();
            for (int i = 0; i < uid.length; i++) {


                String x = Integer.toHexString(((int) uid[i] & 0xff));
                if (x.length() == 1) {
                    x = '0' + x;
                }
                uidhex += x;
            }
            if(String.valueOf(uidhex).equalsIgnoreCase(dt.getUid())){

                desFireEV1.getReader().setTimeout(timeOut);
                desFireEV1.authenticate(0, IDESFireEV1.AuthType.Native, KeyType.TWO_KEY_THREEDES, objKEY_2KTDES);
                desFireEV1.selectApplication(0);
                desFireEV1.selectApplication(appId);
                desFireEV1.authenticate(0, IDESFireEV1.AuthType.Native, KeyType.TWO_KEY_THREEDES, objKEY_2KTDES);
                data = new JSONObject(new String(desFireEV1.readData(Card_Details.carddata, 0, 0)));
                // desFireEV1.getReader().close();
                if(Float.parseFloat(data.getString("vochervalue"))>=total){
                    amount = String.valueOf(Float.parseFloat(data.getString("vochervalue"))-total);
                    openingbal=data.getString("vochervalue");
                    String voucherno= data.getString("voucherno");
                    saveData(amount,voucherno);
                    topups =dt.getTopups();
                    topups.put("vochervalue",(String.format("%.2f",Float.parseFloat(amount))));
                    desFireEV1.authenticate(0, IDESFireEV1.AuthType.Native, KeyType.TWO_KEY_THREEDES, objKEY_2KTDES);
                    desFireEV1.writeData(Card_Details.carddata, 0, topups.toString().getBytes());
                    CartActivity.this.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            pDialogg.dismiss();
                            status ="";

                           MaterialDialog md = new MaterialDialog.Builder(CartActivity.this)
                                    .iconRes(R.drawable.ecompass)
                                    .limitIconToDefaultSize()
                                    .title("Card Updated Successfully")
                                    .content("Proceed to print?")
                                    .positiveText("Print")
                                    .negativeText("Cancel")
                                    .callback(new MaterialDialog.ButtonCallback() {
                                        @Override
                                        public void onPositive(MaterialDialog dialog){
                                            card_views.setVisibility(View.GONE);
                                            final MaterialDialog  ddialog=new MaterialDialog.Builder(CartActivity.this)
                                                    .title("Printing")
                                                    .content("Please wait")
                                                    .progress(true, 0)
                                                    .progressIndeterminateStyle(true)
                                                    .cancelable(false)
                                                    .show();
                                            ddialog.setCancelable(false);
                                            new Handler().postDelayed(new Runnable() {
                                                @Override
                                                public void run() {

                                                    printData(true);
                                                    //setData();
                                                    ddialog.dismiss();

                                                }
                                            },2000);


                                        }
                                        @Override
                                        public void onNegative(MaterialDialog dialog) {
                                            Intent intent=new Intent(CartActivity.this,ProgrammeActivity.class);
                                            startActivity(intent);
                                            finish();
                                        }
                                    })
                                    .show();

                            md.setCancelable(false);



                        }
                    });
                }else{
                    CartActivity.this.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            pDialogg.dismiss();
                            new SweetAlertDialog(CartActivity.this, SweetAlertDialog.WARNING_TYPE)
                                    .setTitleText("Sorry.")
                                    .setContentText("The total price exceeds the balance from the card")
                                    .setConfirmText("Ok")
                                    .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                        @Override
                                        public void onClick(SweetAlertDialog sDialog) {
                                            sDialog.dismissWithAnimation();

                                        }
                                    })
                                    .show();

                        }
                    });
                }

            }else{
                CartActivity.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        pDialogg.dismiss();
                        new SweetAlertDialog(CartActivity.this, SweetAlertDialog.WARNING_TYPE)
                                .setTitleText(getString(R.string.Errorr))
                                .setContentText("Please tap the right card")
                                .setConfirmText(getString(R.string.Ok))
                                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                    @Override
                                    public void onClick(SweetAlertDialog sDialog) {
                                        sDialog.dismissWithAnimation();

                                    }
                                })
                                .show();

                    }
                });


            }
        }catch (Exception e){

            if (e.getMessage().trim().contains("Tag was lost"))
                msg = "Card was removed before the process was complete.Please tap card again and wait";
            else msg = getString(R.string.UnableToWriteData);
          CartActivity.this.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    pDialogg.dismiss();
                    new SweetAlertDialog(CartActivity.this, SweetAlertDialog.WARNING_TYPE)
                            .setTitleText(getString(R.string.Errorr))
                            .setContentText(msg)
                            .setConfirmText(getString(R.string.Ok))
                            .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sDialog) {
                                    sDialog.dismissWithAnimation();

                                }
                            })
                            .show();

                }
            });



            Log.i("#####ERRORRERe",e.toString());


        }
        //fetch the data

    }
    private void initializeKeys() {
        KeyInfoProvider infoProvider = KeyInfoProvider.getInstance(CartActivity.this.getApplicationContext());
        infoProvider.setKey(ALIAS_KEY_2KTDES, SampleAppKeys.EnumKeyType.EnumDESKey, SampleAppKeys.KEY_2KTDES);
        infoProvider.setKey(ALIAS_KEY_AES128, SampleAppKeys.EnumKeyType.EnumAESKey, SampleAppKeys.KEY_AES128);
        infoProvider.setKey(ALIAS_KEY_AES128_ZEROES, SampleAppKeys.EnumKeyType.EnumAESKey, SampleAppKeys.KEY_AES128_ZEROS);
        infoProvider.setKey(ALIAS_DEFAULT_FF, SampleAppKeys.EnumKeyType.EnumMifareKey, SampleAppKeys.KEY_DEFAULT_FF);
        objKEY_2KTDES_ULC = infoProvider.getKey(ALIAS_KEY_2KTDES_ULC, SampleAppKeys.EnumKeyType.EnumDESKey);
        objKEY_2KTDES = infoProvider.getKey(ALIAS_KEY_2KTDES, SampleAppKeys.EnumKeyType.EnumDESKey);
        objKEY_AES128 = infoProvider.getKey(ALIAS_KEY_AES128, SampleAppKeys.EnumKeyType.EnumAESKey);
        default_zeroes_key = infoProvider.getKey(ALIAS_KEY_AES128_ZEROES, SampleAppKeys.EnumKeyType.EnumAESKey);
        default_ff_key = infoProvider.getMifareKey(ALIAS_DEFAULT_FF);
    }


    private void initializeCipherinitVector() {
        /* Initialize the Cipher */
        try {
            cipher = Cipher.getInstance("AES/CBC/NoPadding");
        } catch (NoSuchAlgorithmException e) {

            e.printStackTrace();
        } catch (NoSuchPaddingException e) {
            e.printStackTrace();
        }

		/* set Application Master Key */
        bytesKey = KEY_APP_MASTER.getBytes();

		/* Initialize init vector of 16 bytes with 0xCD. It could be anything */
        byte[] ivSpec = new byte[16];
        Arrays.fill(ivSpec, (byte) 0xCD);
        iv = new IvParameterSpec(ivSpec);

    }
    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        setIntent(intent);
        if(status.equalsIgnoreCase("update"))
            cardLogic(intent);
    }
    @Override
    protected void onResume() {
        super.onResume();
        enableForegroundDispatchSystem();
    }

    @Override
    protected void onPause() {
        super.onPause();

        disableForegroundDispatchSystem();
    }
    public void showAlert(String title, String message){
        new SweetAlertDialog(CartActivity.this, SweetAlertDialog.WARNING_TYPE)
                .setTitleText(title)
                .setContentText(message)
                .setConfirmText(getString(R.string.Ok))
                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sDialog) {
                        sDialog.dismissWithAnimation();

                    }
                })
                .show();
    }
    public String split(String str){

        int l=str.lastIndexOf("(");
        int  r=str.lastIndexOf(")");
        return  str.substring(l+1,r);

    }
    private void saveReceipt(String items, String charges,String openbal,String closebal,String receiptno,String rationno,String voucherid){
         Reprints reprints= new Reprints();
         reprints.setItems(items);
         reprints.setCharges(charges);
         reprints.setOpenbal(openbal);
         reprints.setClosebal(closebal);
         reprints.setReceiptno(receiptno);
         reprints.setRationno(rationno);
         reprints.setVoucherid(voucherid);
         reprints.setTime(dt.getTime());
         reprints.setUsername(dt.getUser());
         reprints.setMacid(dt.getMac());
         reprints.setDate(dt.getDate());
         Reprints.deleteAll(Reprints.class);
         reprints.save();

    }
    public CardManager update_card_balance(int mode, int timeout) {
        final CardInfo cInfo = new CardInfo();
        CardManager cardManager = CardManager.getInstance(this, mode,"write",topups.toString().getBytes());
        cardManager.getCard(timeout, new CardListener() {
            @Override
            public void callback(CardInfo cardInfo) {
                dialog.dismiss();
                status ="";
                if(cardInfo.isResultFalg()){
                    MaterialDialog md = new MaterialDialog.Builder(CartActivity.this)
                            .iconRes(R.drawable.ecompass)
                            .limitIconToDefaultSize()
                            .title("Card Updated Successfully")
                            .content("Proceed to print?")
                            .positiveText("Print")
                            .negativeText("Cancel")
                            .callback(new MaterialDialog.ButtonCallback() {
                                @Override
                                public void onPositive(MaterialDialog dialog){
                                    card_views.setVisibility(View.GONE);
                                    final MaterialDialog  ddialog=new MaterialDialog.Builder(CartActivity.this)
                                            .title("Printing")
                                            .content("Please wait")
                                            .progress(true, 0)
                                            .progressIndeterminateStyle(true)
                                            .cancelable(false)
                                            .show();
                                    ddialog.setCancelable(false);
                                    new Handler().postDelayed(new Runnable() {
                                        @Override
                                        public void run() {

                                            printData(true);
                                            ddialog.dismiss();


                                        }
                                    },2000);


                                }
                                @Override
                                public void onNegative(MaterialDialog dialog) {
                                    Intent intent=new Intent(CartActivity.this,ProgrammeActivity.class);
                                    startActivity(intent);
                                    finish();
                                }
                            })
                            .show();
                }else{

                    Log.d("##callback"," card not updated successfully ");


                }

            }
        });

        return cardManager;
    }

}