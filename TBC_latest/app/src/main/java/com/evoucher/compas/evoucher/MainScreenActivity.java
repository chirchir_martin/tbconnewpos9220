package com.evoucher.compas.evoucher;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.evoucher.compas.evoucher.Adapters.ItemsAdapter;
import com.google.common.util.concurrent.Service;
import com.lynx.evoucher.models.Categories;
import com.lynx.evoucher.models.Products;
import com.lynx.evoucher.models.Programmes;
import com.lynx.evoucher.models.PurchasedProducts;
import com.lynx.evoucher.models.ServicePrices;
import com.lynx.evoucher.models.Services;
import com.lynx.evoucher.models.Topups;
import com.lynx.evoucher.models.Users;
import com.lynx.evoucher.models.Vouchers;
import com.mikepenz.materialdrawer.AccountHeader;
import com.mikepenz.materialdrawer.Drawer;



import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import cn.pedant.SweetAlert.SweetAlertDialog;
import jp.wasabeef.recyclerview.adapters.AlphaInAnimationAdapter;

public class MainScreenActivity extends AppCompatActivity implements ItemClickListener{
    private AccountHeader headerResult = null;
    private Drawer result = null;
    List<Products> products1;
    Users user_table=new Users();
    List<Categories> categories;
    public  static String idd="";
    Details dt;
    List<Topups> ts;
    List<ServicePrices> prices;
    String vtp;
    TextView val,maxp,title;
    List<Services> sdt;
    Spinner spinner;
    ArrayList<String> serviceIds=new ArrayList<String>();
    float total;
    FloatingActionButton bt;
    ArrayList drawer=new ArrayList() ;
    String id="";
    ArrayList sub=new ArrayList() ;
    TextView tv,tv4;
    float inputv,totalq,totalp;
    ItemsAdapter adapter;
    int totalqty;
    JSONObject tups;
    List<String> productsIds=new ArrayList<String>();
    List<Products> products=new ArrayList<Products>();
    Products products_table=new Products();
    Categories categories_table=new Categories();
    Programmes programmes_table=new Programmes();
    Vouchers voucher_table=new Vouchers();
    public static String tag1="";
    Topups topups_table=new Topups();
    Typeface typeface1;
    EditText qy,cash;
    Products products0;
    ImageView icon;
   String vvalue ="0";
    MaterialDialog dialog;
    private static RecyclerView recyclerView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_screen);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        dt=new Details(MainScreenActivity.this);
        List<String> prices=new ArrayList<>();
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        overridePendingTransition(0, 0);
        Log.d("Card_Number##",dt.getCardnumber());


        vvalue =getIntent().getStringExtra("VOUCHERVALUE");
        Log.i("##EWERWRER",vvalue +"   eeeeeeeeee");
        dt.setValuevoucher(vvalue);
        val=(TextView)findViewById(R.id.val) ;
        val.setText("THB "+vvalue);
        getSupportActionBar().setTitle("Services");
        tups=dt.getTopups();
        bt=(FloatingActionButton)findViewById(R.id.floating_action_button);
        typeface1 = Typeface.createFromAsset(getAssets(), "Roboto-Regular.ttf");
        recyclerView = (RecyclerView)findViewById(R.id.recycler_view);
        recyclerView.setHasFixedSize(true);
        try {
            productsIds.clear();
            products.clear();
            serviceIds.clear();
            prices.clear();
            sdt= Services.find(Services.class,"voucher_Id =? and Programme_Id =? and ben_Group_Id =?",dt.getVoucherid(),dt.getProgrammeid(),dt.getBenGroup());

            for (int i = 0; i < sdt.size(); i++) {
                if (serviceIds.contains(sdt.get(i).getServiceId())) {

                } else {
                    serviceIds.add(sdt.get(i).getServiceId());
                }
            }

            for (int i = 0; i < serviceIds.size(); i++) {
                products.addAll(Products.find(Products.class, "productid = ?", serviceIds.get(i)));
            }
        }catch (Exception e){
            Log.i("###RERERE",e.toString());
        }
        /*products=Products.find(Products.class, "voucherid = ? and programmeid = ? and bengroup=?",dt.getVoucherid(),dt.getProgrammeid(),dt.getBenGroup());
        for(int i=0;i<products.size();i++) {
            Log.d("SSOSOSOSOS", products.get(i).getVouchid());

        }*/
        if(products.size()>0) {
            if(!(vvalue.equalsIgnoreCase("") &&vvalue.equalsIgnoreCase("0"))) {
                recyclerView.setLayoutManager(new StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL));
                adapter = new ItemsAdapter(MainScreenActivity.this, products, productsIds, prices);
                adapter.setClickListener(this);
                AlphaInAnimationAdapter alphaAdapter = new AlphaInAnimationAdapter(adapter);
                alphaAdapter.setDuration(1500);
                recyclerView.setAdapter(alphaAdapter);
                bt.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        startActivity(new Intent(MainScreenActivity.this, CartActivity.class).putExtra("VOUCHERVALUE",vvalue));
                        finish();
                    }
                });
            }else{
                Toast.makeText(MainScreenActivity.this,"No amount found",Toast.LENGTH_LONG).show();
            }
        }else{
            Toast.makeText(MainScreenActivity.this,"No services found",Toast.LENGTH_LONG).show();
        }

    }

    @Override
    public void onClick(View view, int tag) {
        id=String.valueOf(tag);
        Log.d("####ASASASASA",id);
        products1=Products.find(Products.class, "productid =?",String.valueOf(tag));
        Log.d("CALLED",  products1.get(0).getimage());

        showCustomViewvalue(String.valueOf(tag),products1.get(0).getimage());



    }


    public void   showCustomViewvalue(final String tag, String image) {
        try {
            List<String> spinnerArray =  new ArrayList<String>();
            spinnerArray.clear();

            prices =ServicePrices.find(ServicePrices.class,"serviceid =?",tag);
            spinnerArray.add("Select UOM");
            if(prices.size()>0){
                for(int i =0;i<prices.size();i++){
                    spinnerArray.add(prices.get(i).getUom());
                }

            }
            tag1 = tag;
            final String productPrice=vvalue;
            Bitmap decodedByte = null;
            Log.d("######PRODUCTTAG", tag);
            byte[] decodedString = Base64.decode(image, Base64.DEFAULT);
            decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
            Drawable d = new BitmapDrawable(getResources(), decodedByte);
            dialog = new MaterialDialog.Builder(this)
                    //.title("")
                 //   .icon(d)
                    .customView(R.layout.dialog_customviewcash, true)
                    .positiveText("Purchase")
                    .negativeText(android.R.string.cancel)
                    .autoDismiss(false)
                    .onPositive(new MaterialDialog.SingleButtonCallback() {
                                    @Override
                                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                        Log.i("#####UOMS",spinner.getSelectedItem().toString()+" DDFDGG");
                                        // Toast.makeText(MainScreenActivity.this,  spinner.getItemAtPosition(spinner.getSelectedItemPosition()).toString(),Toast.LENGTH_LONG).show();
                                        if (spinner.getSelectedItem().toString().toString().trim() !="Select UOM" && spinner.getSelectedItem().toString().toString().length() >0 && !qy.getText().toString().isEmpty() &&!cash.getText().toString().isEmpty()) {
                                            prices =ServicePrices.find(ServicePrices.class, "uom =? and serviceid=?", spinner.getSelectedItem().toString(),tag);
                                            String inputcash=cash.getText().toString().trim();

                                            if(isValidAmount(inputcash) && Float.parseFloat(prices.get(0).getMaxprice())>=Float.parseFloat(cash.getText().toString())) {
                                                inputv = Float.parseFloat(cash.getText().toString()) * Float.parseFloat(qy.getText().toString());
                                                List<PurchasedProducts> purchased = PurchasedProducts.find(PurchasedProducts.class, "productId = ? and voucherid =? and cardnumber =?", tag1, dt.getVoucherid(), dt.getCardnumber());
                                                Log.d("PSPSPSP", String.valueOf(purchased.size()) + "wwiwiw");
                                                if (productPrice.length() > 0) {
                                                    //totalqty = Math.round(Float.parseFloat(ts.get(0).getProductQuantity()));
                                                    totalp = Float.parseFloat(productPrice);
                                                }
                                                if (purchased.size() > 0) {
                                                    float total = Float.parseFloat(purchased.get(0).getTotalprice()) + inputv;

                                                    Log.i("###3PSPSPSP", String.valueOf(totalp));
                                                    Log.i("###PSPSPSP", String.valueOf(total) + "total");
                                                    if (productPrice.length() > 0) {
                                                        if (inputv <= totalp) {
                                                            vvalue =String.valueOf(totalp - inputv);
                                                            dt.setValuevoucher(vvalue);
                                                            val.setText("THB " +String.format("%.02f", Float.parseFloat(dt.getValuevoucher())));
                                                            purchased.get(0).setProductId(tag1);
                                                            purchased.get(0).setUom(spinner.getSelectedItem().toString().toString());
                                                            purchased.get(0).setVoucherid(dt.getVoucherid());
                                                            purchased.get(0).setTotalprice(String.valueOf(total));
                                                            purchased.get(0).setProgrammeid(dt.getProgrammeid());
                                                            purchased.get(0).setQuantitydeducted(String.valueOf(Float.parseFloat(purchased.get(0).getQuantitydeducted()) + Float.parseFloat(qy.getText().toString())));
                                                            purchased.get(0).setCardnumber(dt.getCardnumber());
                                                            purchased.get(0).save();

                                                        } else {
                                                            new SweetAlertDialog(MainScreenActivity.this, SweetAlertDialog.WARNING_TYPE)
                                                                    .setTitleText("Sorry.")
                                                                    .setContentText(getString(R.string.exceeds))
                                                                    .setConfirmText("Ok")
                                                                    .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                                                        @Override
                                                                        public void onClick(SweetAlertDialog sDialog) {
                                                                            sDialog.dismissWithAnimation();

                                                                        }
                                                                    })
                                                                    .show();
                                                        }
                                                    }
                                                } else {
                                                    Log.d("PSPSPSP", String.valueOf(totalp));
                                                    Log.d("PSPSPSP", String.valueOf(inputv) + "totalw");
                                                    if (totalp >= inputv){
                                                        Log.d("PSPSPSP", String.valueOf(inputv) + "yes");
                                                        PurchasedProducts purchase_table = new PurchasedProducts();
                                                        // ts.get(0).setProductPrice(String.valueOf(totalp - inputv));
                                                        vvalue =String.valueOf(totalp - inputv);
                                                        dt.setValuevoucher(vvalue);
                                                        val.setText("THB " +String.format("%.02f", Float.parseFloat(dt.getValuevoucher())));
                                                        Log.d("PSPSPSP", String.valueOf(totalp - inputv) + "new");
                                                        // purchase_table.setQuantity(qy.getText().toString());
                                                        purchase_table.setProductId(tag1);
                                                        purchase_table.setUom(spinner.getSelectedItem().toString().toString());
                                                        purchase_table.setCardnumber(dt.getCardnumber());
                                                        purchase_table.setVoucherid(dt.getVoucherid());
                                                        purchase_table.setQuantitydeducted(qy.getText().toString());
                                                        //purchased.get(0).setProgrammeid(dt.getProgrammeid());
                                                        Log.d("sdsdsdsdsreee", qy.getText().toString());
                                                        purchase_table.setTotalprice(String.valueOf(inputv));
                                                        //ts.get(0).save();
                                                        purchase_table.save();
                                                        // adapter.notifyDataSetChanged();

                                                    } else {
                                                        new SweetAlertDialog(MainScreenActivity.this, SweetAlertDialog.WARNING_TYPE)
                                                                .setTitleText("Sorry.")
                                                                .setContentText("The total price exceeds the maximum allocated amount")
                                                                .setConfirmText("Ok")
                                                                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                                                    @Override
                                                                    public void onClick(SweetAlertDialog sDialog) {
                                                                        sDialog.dismissWithAnimation();
                                                                    }
                                                                })
                                                                .show();
                                                    }
                                                }
                                            }else{
                                                if(!(isValidAmount(inputcash)))
                                                  new SweetAlertDialog(MainScreenActivity.this, SweetAlertDialog.WARNING_TYPE)
                                                        .setTitleText("Sorry.")
                                                        .setContentText("Please input the correct amount")
                                                        .setConfirmText("Ok")
                                                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                                            @Override
                                                            public void onClick(SweetAlertDialog sDialog) {
                                                                sDialog.dismissWithAnimation();

                                                            }
                                                        })
                                                        .show();
                                                 else{
                                                    new SweetAlertDialog(MainScreenActivity.this, SweetAlertDialog.WARNING_TYPE)
                                                            .setTitleText("Sorry.")
                                                            .setContentText("The total price exceeds the maximum allocated amount")
                                                            .setConfirmText("Ok")
                                                            .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                                                @Override
                                                                public void onClick(SweetAlertDialog sDialog) {
                                                                    sDialog.dismissWithAnimation();

                                                                }
                                                            })
                                                            .show();
                                                }
                                            }
                                            dialog.dismiss();

                                        } else {
                                            new SweetAlertDialog(MainScreenActivity.this, SweetAlertDialog.WARNING_TYPE)
                                                    .setTitleText("Sorry.")
                                                    .setContentText("Please input all the fields")
                                                    .setConfirmText("Ok")
                                                    .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                                        @Override
                                                        public void onClick(SweetAlertDialog sDialog) {
                                                            sDialog.dismissWithAnimation();

                                                        }
                                                    })
                                                    .show();
                                        }
                                    }
                                }
                    )
                    .onNegative(new MaterialDialog.SingleButtonCallback() {
                        @Override
                        public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {

                            dialog.dismiss();

                        }
                    })
                    .build();

            cash = (EditText) dialog.getCustomView().findViewById(R.id.cash);
            qy = (EditText) dialog.getCustomView().findViewById(R.id.qty);
            title = (TextView) dialog.getCustomView().findViewById(R.id.title);
            maxp = (TextView) dialog.getCustomView().findViewById(R.id.maxp);
            icon = (ImageView) dialog.getCustomView().findViewById(R.id.icon);
            icon.setImageDrawable(d);
            title.setText(products1.get(0).getproductname());
            Typeface typeface = Typeface.createFromAsset(getAssets(), "Roboto-Thin.ttf");


            ArrayAdapter<String> adapter = new ArrayAdapter<String>(
                    this, android.R.layout.simple_spinner_item, spinnerArray);
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            spinner = (Spinner)dialog.getCustomView().findViewById(R.id.mySpinner);
            spinner.setAdapter(adapter);
            spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener()
            {
                @Override
                public void onItemSelected(AdapterView adapter, View v, int i, long lng) {
                    try {
                        if (spinner.getItemAtPosition(spinner.getSelectedItemPosition()).toString().trim() != "Select UOM") {
                            Log.i("###SELECTED UOM",spinner.getItemAtPosition(spinner.getSelectedItemPosition()).toString());
                            prices = ServicePrices.find(ServicePrices.class, "uom =? and serviceid=?", spinner.getSelectedItem().toString(),tag);
                            maxp.setText("Max Price: "+prices.get(0).getMaxprice());
                            maxp.setVisibility(View.VISIBLE);
                        }else{
                            maxp.setVisibility(View.GONE);
                        }
                    }catch (Exception e){

                    }
                }
                @Override
                public void onNothingSelected(AdapterView<?> parentView)
                {

                }
            });

            dialog.show();

        }catch (Exception e){
            new SweetAlertDialog(MainScreenActivity.this, SweetAlertDialog.WARNING_TYPE)
                    .setTitleText("INVAlID INPUTS")
                    .setContentText(e.toString())
                    .setConfirmText("Ok")
                    .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                        @Override
                        public void onClick(SweetAlertDialog sDialog) {
                            sDialog.dismissWithAnimation();

                        }
                    })
                    .show();
        }


    }
    /* public void   showCustomViewCash(String tag) {
         try {
             tag1 = tag;
             ts = Topups.find(Topups.class, "voucherid=? and cardnumber =? and bengroup=?", dt.getVoucherid(), dt.getCardnumber(),dt.getBenGroup());
             dialog = new MaterialDialog.Builder(this)
                     .title("Cash Voucher")

                     .customView(R.layout.dialog_customviewcashh, true)
                     .positiveText("Ok")
                     .negativeText(android.R.string.cancel)
                     .autoDismiss(false)
                     .onPositive(new MaterialDialog.SingleButtonCallback() {
                                     @Override
                                     public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                         if (cash.getText().toString().isEmpty() == false) {
                                             inputv = Integer.parseInt(cash.getText().toString());
                                             List<PurchasedProducts> purchased = PurchasedProducts.find(PurchasedProducts.class, "productId = ? and voucherid =? and cardnumber =?", tag1, dt.getVoucherid(), dt.getCardnumber());
                                             Log.d("PSPSPSP", String.valueOf(purchased.size()) + "wwiwiw");
                                             if (ts.size() > 0) {
                                                 //totalqty = Math.round(Float.parseFloat(ts.get(0).getProductQuantity()));
                                                 totalp = Math.round((Float.parseFloat(ts.get(0).getVouchervalue())));
                                             }
                                             if (purchased.size() > 0) {
                                                 Float total = Float.parseFloat(purchased.get(0).getTotalprice()) + inputv;

                                                 Log.d("PSPSPSP", String.valueOf(totalp));
                                                 Log.d("PSPSPSP", String.valueOf(total) + "total");


                                                 if (ts.size() > 0) {
                                                     if (total <= totalp) {
                                                         //ts.get(0).setProductPrice(String.valueOf(totalp - inputv));
                                                         purchased.get(0).setProductId(tag1);
                                                         purchased.get(0).setVoucherid(dt.getVoucherid());
                                                         purchased.get(0).setProgrammeid(dt.getProgrammeid());
                                                         purchased.get(0).setTotalprice(String.valueOf(total));
                                                         purchased.get(0).setCardnumber(dt.getCardnumber());
                                                         ts.get(0).save();
                                                         purchased.get(0).save();
                                                         adapter.notifyDataSetChanged();
                                                     } else {
                                                         new SweetAlertDialog(MainScreenActivity.this, SweetAlertDialog.WARNING_TYPE)
                                                                 .setTitleText("Sorry.")


                                                                 .setContentText("The total exceeds the cash value")
                                                                 .setConfirmText("Ok")
                                                                 .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                                                     @Override
                                                                     public void onClick(SweetAlertDialog sDialog) {
                                                                         sDialog.dismissWithAnimation();

                                                                     }
                                                                 })
                                                                 .show();
                                                     }
                                                 }
                                             } else {
                                                 Log.d("PSPSPSP", String.valueOf(totalp));
                                                 Log.d("PSPSPSP", String.valueOf(inputv) + "totalw");
                                                 if (totalp >= inputv) {
                                                     Log.d("PSPSPSP", String.valueOf(inputv) + "yes");
                                                     PurchasedProducts purchase_table = new PurchasedProducts();
                                                    // ts.get(0).setProductPrice(String.valueOf(totalp - inputv));
                                                     Log.d("PSPSPSP", String.valueOf(totalp - inputv) + "new");
                                                     // purchase_table.setQuantity(qy.getText().toString());
                                                     purchase_table.setProductId(tag1);
                                                     purchase_table.setVoucherid(dt.getVoucherid());
                                                     purchase_table.setCardnumber(dt.getCardnumber());
                                                     purchase_table.setProgrammeid(dt.getProgrammeid());
                                                     purchase_table.setTotalprice(String.valueOf(inputv));
                                                     ts.get(0).save();
                                                     purchase_table.save();
                                                     adapter.notifyDataSetChanged();
                                                 } else {
                                                     new SweetAlertDialog(MainScreenActivity.this, SweetAlertDialog.WARNING_TYPE)
                                                             .setTitleText("Sorry.")
                                                             .setContentText("The input exceeds the cash value")
                                                             .setConfirmText("Ok")
                                                             .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                                                 @Override
                                                                 public void onClick(SweetAlertDialog sDialog) {
                                                                     sDialog.dismissWithAnimation();

                                                                 }
                                                             })
                                                             .show();
                                                 }
                                             }
                                             dialog.dismiss();
                                         } else {
                                             new SweetAlertDialog(MainScreenActivity.this, SweetAlertDialog.WARNING_TYPE)
                                                     .setTitleText("Sorry.")
                                                     .setContentText("Empty fields")
                                                     .setConfirmText("Ok")
                                                     .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                                         @Override
                                                         public void onClick(SweetAlertDialog sDialog) {
                                                             sDialog.dismissWithAnimation();

                                                         }
                                                     })
                                                     .show();
                                         }

                                     }
                                 }
                     )
                     .onNegative(new MaterialDialog.SingleButtonCallback() {
                         @Override
                         public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {

                             dialog.dismiss();

                         }
                     })
                     .build();

             cash = (EditText) dialog.getCustomView().findViewById(R.id.amount);

             Typeface typeface = Typeface.createFromAsset(getAssets(), "Roboto-Thin.ttf");


             dialog.show();

         }catch (Exception e) {
             new SweetAlertDialog(MainScreenActivity.this, SweetAlertDialog.WARNING_TYPE)
                     .setTitleText("INVAlID INPUTS")
                     .setContentText("")
                     .setConfirmText("Ok")
                     .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                         @Override
                         public void onClick(SweetAlertDialog sDialog) {
                             sDialog.dismissWithAnimation();

                         }
                     })
                     .show();
         }

     }*/
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == android.R.id.home) {
           // reset();

            List<PurchasedProducts> pd= PurchasedProducts.listAll(PurchasedProducts.class);
            if(!(pd.size()>0)) {
                Intent intent1 = new Intent(this, VouchersActivity.class).putExtra("VOUCHERVALUE",vvalue);
                startActivity(intent1);
                finish();
            }else{
                new SweetAlertDialog(MainScreenActivity.this, SweetAlertDialog.WARNING_TYPE)
                        .setTitleText("Sorry")
                        .setContentText("Please submit or you remove the selected items")
                        .setConfirmText("Ok")
                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sDialog) {
                                sDialog.dismissWithAnimation();

                            }
                        })
                        .show();
            }

        }
        return super.onOptionsItemSelected(item);
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
       /* List<PurchasedProducts> productss;
        getMenuInflater().inflate(R.menu.main, menu);
        MenuItem itemCart = menu.findItem(R.id.action_cart);
        LayerDrawable icon = (LayerDrawable) itemCart.getIcon();
        totalq=0;
        productss = PurchasedProducts.listAll(PurchasedProducts.class);
        if(productss.size()>0) {
            for (int i = 0; i < productss.size(); i++) {
                //pds=Products.find(Products.class,"productid = ? and voucherid = ?",products.get(i).getProductId(),dt.getVoucherid());

                totalq += Float.parseFloat(productss.get(i).getQuantity());
            }
            // Inflate the menu; this adds items to the action bar if it is present.

            setBadgeCount(this, icon, String.valueOf(Math.round(totalq)));
        }*/
        return true;
    }


    @Override
    public void onBackPressed() {
      //  reset();
        List<PurchasedProducts> pd= PurchasedProducts.listAll(PurchasedProducts.class);
        if(!(pd.size()>0)) {
            Intent intent1 = new Intent(this, VouchersActivity.class).putExtra("VOUCHERVALUE",vvalue);
            startActivity(intent1);
            finish();
        }else{
            new SweetAlertDialog(MainScreenActivity.this, SweetAlertDialog.WARNING_TYPE)
                    .setTitleText("Sorry")
                    .setContentText("Please submit or you remove the selected items")
                    .setConfirmText("Ok")
                    .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                        @Override
                        public void onClick(SweetAlertDialog sDialog) {
                            sDialog.dismissWithAnimation();

                        }
                    })
                    .show();
        }


    }
    public void reset(){
        List<PurchasedProducts> pd= PurchasedProducts.listAll(PurchasedProducts.class);
        Log.d("CALLED",String.valueOf(pd.size()));
        String pi,qy;
        if(pd.size()>0) {

            for (int i = 0; i < pd.size(); i++) {
                dt.setValuevoucher(String.valueOf(Float.valueOf(dt.getValuevoucher()) +Float.valueOf( pd.get(i).getTotalprice())));
           /* Topups ts = Topups.find(Topups.class, "voucherid = ? and cardnumber =? and bengroup=?",dt.getVoucherid(),dt.getCardnumber(),dt.getBenGroup()).get(0);
            Log.d("CALLED",  ts.getProductQuantity().toString()+"ll");

            String vtp=ts.getvocherType();

            if(vtp.equalsIgnoreCase("CM")){
                qy = pd.get(i).getQuantitydeducted();
                //Log.d("CALLED",  qy);
                Log.d("CALLED",pd.get(i).getQuantitydeducted().toString()+"p");
                Log.d("CALLED",pd.get(i).getQuantitydeducted().toString()+"p");
                ts.setProductQuantity(String.valueOf(Float.valueOf(ts.getProductQuantity()) +Float.valueOf(qy)));
            }else {
               // ts.setProductPrice(String.valueOf(Float.valueOf(ts.getProductPrice()) +Float.valueOf( pd.get(i).getTotalprice())));
                dt.setValuevoucher(String.valueOf(Float.valueOf(ts.getProductPrice()) +Float.valueOf( pd.get(i).getTotalprice())));
            }

            ts.save();*/
            }
            PurchasedProducts.deleteAll(PurchasedProducts.class);
        }

    }

    /*headerResult = new AccountHeaderBuilder()
                .withActivity(this)
                .withTranslucentStatusBar(true)
                .withHeaderBackground(R.drawable.sd).withPaddingBelowHeader(true)
                .withSavedInstance(savedInstanceState)
                .build();


        for(Categories c: categories){
            sub.add(new SecondaryDrawerItem().withName(c.getname()).withLevel(3).withIcon(FontAwesome.Icon.faw_glass).withIdentifier(c.getId()).withTypeface(typeface1).withTextColor(getResources().getColor(R.color.b)));

        }
        drawer.add( new ExpandableDrawerItem().withName("Categories").withIcon(R.drawable.m).withIdentifier(20).withSelectable(true).withTextColor(getResources().getColor(R.color.b)).withSubItems(sub));
        result = new DrawerBuilder()
                .withActivity(this)
                .withToolbar(toolbar)
                .withHasStableIds(true)
                .withItemAnimator(new AlphaCrossFadeAnimator())
                .withAccountHeader(headerResult)
                .withDrawerItems(drawer)

                .withOnDrawerItemClickListener(new Drawer.OnDrawerItemClickListener() {
                    @Override
                    public boolean onItemClick(View view, int position, IDrawerItem drawerItem) {
                        //check if the drawerItem is set.
                        //there are different reasons for the drawerItem to be null
                        //--> click on the header
                        //--> click on the footer
                        //those items don't contain a drawerItem

                        if (drawerItem != null) {
                            Intent intent = null;
                            if (drawerItem.getIdentifier() == 1) {
                                products=Products.find(Products.class, "categoryid  = ?", "1");
                                recyclerView.setLayoutManager(new StaggeredGridLayoutManager(3, StaggeredGridLayoutManager.VERTICAL));
                                ItemsAdapter adapter=new ItemsAdapter(MainScreenActivity.this,products);
                                AlphaInAnimationAdapter alphaAdapter = new AlphaInAnimationAdapter(adapter);
                                alphaAdapter.setDuration(3000);

                                recyclerView.setAdapter(alphaAdapter);
                                getSupportActionBar().setTitle("Drinks");
                            }else if(drawerItem.getIdentifier() == 2){
                                products=Products.find(Products.class, "categoryid  = ?", "2");
                                recyclerView.setLayoutManager(new StaggeredGridLayoutManager(3, StaggeredGridLayoutManager.VERTICAL));
                                ItemsAdapter adapter=new ItemsAdapter(MainScreenActivity.this,products);
                                AlphaInAnimationAdapter alphaAdapter = new AlphaInAnimationAdapter(adapter);
                                alphaAdapter.setDuration(3000);
                                recyclerView.setAdapter(alphaAdapter);
                                getSupportActionBar().setTitle("Jewellerly");
                            }else if(drawerItem.getIdentifier() ==3){
                                products=Products.find(Products.class, "categoryid  = ?", "3");
                                recyclerView.setLayoutManager(new StaggeredGridLayoutManager(3, StaggeredGridLayoutManager.VERTICAL));
                                ItemsAdapter adapter=new ItemsAdapter(MainScreenActivity.this,products);
                                AlphaInAnimationAdapter alphaAdapter = new AlphaInAnimationAdapter(adapter);
                                alphaAdapter.setDuration(3000);
                                recyclerView.setAdapter(alphaAdapter);
                                getSupportActionBar().setTitle("Electronics");
                            }else if(drawerItem.getIdentifier() ==5){
                                products=Products.find(Products.class, "categoryid  = ?", "4");
                                recyclerView.setLayoutManager(new StaggeredGridLayoutManager(3, StaggeredGridLayoutManager.VERTICAL));
                                ItemsAdapter adapter=new ItemsAdapter(MainScreenActivity.this,products);
                                AlphaInAnimationAdapter alphaAdapter = new AlphaInAnimationAdapter(adapter);
                                alphaAdapter.setDuration(3000);
                                recyclerView.setAdapter(alphaAdapter);
                                getSupportActionBar().setTitle("Bags");
                            }else if(drawerItem.getIdentifier() ==5){
                                recyclerView.setLayoutManager(new StaggeredGridLayoutManager(3, StaggeredGridLayoutManager.VERTICAL));
                                ItemsAdapter adapter=new ItemsAdapter(MainScreenActivity.this,products);
                                AlphaInAnimationAdapter alphaAdapter = new AlphaInAnimationAdapter(adapter);
                                alphaAdapter.setDuration(3000);
                                recyclerView.setAdapter(alphaAdapter);
                                getSupportActionBar().setTitle("Shoes");

                            }else if(drawerItem.getIdentifier() ==223222) {

                            }
                            if (intent != null) {

                            }
                        }

                        return false;
                    }
                })
                .withSavedInstance(savedInstanceState)
                .withShowDrawerOnFirstLaunch(true)
                .build();

        if (savedInstanceState == null) {


        }*/

    public static  boolean isValidAmount(String str){

            if(str.trim().length()>0 && Float.parseFloat(str.trim())>0){
                 return true;
            }
             return  false;

    }
}
