package com.evoucher.compas.evoucher.fragments;

import android.media.Image;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.ImageButton;

import com.evoucher.compas.evoucher.Details;
import com.evoucher.compas.evoucher.Network;
import com.evoucher.compas.evoucher.R;
import com.lynx.evoucher.models.Transactions;
import com.rey.material.widget.RadioButton;

import java.util.List;

import cn.pedant.SweetAlert.SweetAlertDialog;

/**
 * Created by Administrator on 10/25/2016.
 */
public class Update extends Fragment {
    ImageButton update;
    Network net;
    Button master,topups,ben;
    RadioButton rb1,rb2,rb3;
    List<Transactions> ts;
    Details dt;
    String state="";
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        dt=new Details(getContext());
        if(dt.getLevel().equalsIgnoreCase("1"))
        return inflater.inflate(R.layout.update_user, container, false);
        else
            return inflater.inflate(R.layout.update_fragment, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        dt=new Details(getActivity());
        update=(ImageButton)getView().findViewById(R.id.updatebtn) ;
       topups=(Button)getView().findViewById(R.id.topups);
        ben=(Button)getView().findViewById(R.id.ben);
        master=(Button)getView().findViewById(R.id.master);
       // update.setEnabled(false);
        if(dt.getLevel().equalsIgnoreCase("0")) {
            topups.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ts = Transactions.listAll(Transactions.class);

                    if (ts.size() == 0) {
/*
                    if(!dt.getDownloadstatus().equalsIgnoreCase("1")){
                        state="dtopups";
                        net=new Network(getActivity(),"Topups");
                        net.UpdateMaster();

                    }else{
                        new SweetAlertDialog(getActivity(), SweetAlertDialog.WARNING_TYPE)
                                .setTitleText("Sorry")
                                .setContentText("No topups to download")
                                .setConfirmText("Ok")
                                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                    @Override
                                    public void onClick(SweetAlertDialog sDialog) {
                                        sDialog.dismissWithAnimation();

                                    }
                                })
                                .show();
                    }*/

                        state = "dtopups";
                        net = new Network(getActivity(), "Topups");
                        net.UpdateMaster();
                    } else {
                        new SweetAlertDialog(getActivity(), SweetAlertDialog.WARNING_TYPE)
                                .setTitleText(getString(R.string.Sorry))
                                .setContentText(getString(R.string.PleaseUploadPendingTransaction))
                                .setConfirmText(getString(R.string.Ok))
                                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                    @Override
                                    public void onClick(SweetAlertDialog sDialog) {
                                        sDialog.dismissWithAnimation();

                                    }
                                })
                                .show();
                    }

                }
            });
        }
        if(dt.getLevel().equalsIgnoreCase("0")) {
            ben.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ts = Transactions.listAll(Transactions.class);
                    if (ts.size() == 0) {
                        state = "dben";
                        net = new Network(getActivity(), "Beneficiaries");
                        net.UpdateMaster();

                    } else {
                        new SweetAlertDialog(getActivity(), SweetAlertDialog.WARNING_TYPE)
                                .setTitleText(getString(R.string.Sorry))
                                .setContentText(getString(R.string.PleaseUploadPendingTransaction))
                                .setConfirmText(getString(R.string.Ok))
                                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                    @Override
                                    public void onClick(SweetAlertDialog sDialog) {
                                        sDialog.dismissWithAnimation();

                                    }
                                })
                                .show();
                    }

                }
            });
        }
       master.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ts=Transactions.listAll(Transactions.class);
                if(ts.size()==0){
                    state="dmaster";
                    net=new Network(getActivity(),"Master");
                    net.UpdateMaster();

                }else{
                    new SweetAlertDialog(getActivity(), SweetAlertDialog.WARNING_TYPE)
                            .setTitleText(getString(R.string.Sorry))
                            .setContentText(getString(R.string.PleaseUploadPendingTransaction))
                            .setConfirmText(getString(R.string.Ok))
                            .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sDialog) {
                                    sDialog.dismissWithAnimation();

                                }
                            })
                            .show();
                }

            }
        });


        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onStop() {
        super.onStop();
    }
}
