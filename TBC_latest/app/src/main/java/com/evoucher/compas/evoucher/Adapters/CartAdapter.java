package com.evoucher.compas.evoucher.Adapters;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Typeface;
import android.support.annotation.NonNull;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.GravityEnum;
import com.afollestad.materialdialogs.MaterialDialog;
import com.afollestad.materialdialogs.Theme;
import com.evoucher.compas.evoucher.CartActivity;
import com.evoucher.compas.evoucher.Details;
import com.lynx.evoucher.models.Products;
import com.lynx.evoucher.models.PurchasedProducts;
import com.evoucher.compas.evoucher.R;
import com.lynx.evoucher.models.Topups;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Administrator on 8/17/2016.
 */
public class CartAdapter extends BaseAdapter {
    Typeface typeface;

Details dt;
    List<Topups> ts;
    Context context;
    ArrayList<Integer> positions;
 List<Products> pdcts;

    public CartAdapter(Context context,  List<Products> products){
     pdcts=products;
        this.context=context;
         dt=new Details(context);
       typeface = Typeface.createFromAsset(context.getAssets(), "Roboto-Thin.ttf");

}
    @Override
    public int getCount() {
        return  pdcts.size();
    }

    @Override
    public Object getItem(int i) {
        return  pdcts.get(i);
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.cart_row, parent, false);

            ViewHolder holder = new ViewHolder();
            holder.iconView = (ImageView) convertView.findViewById(R.id.main);
            holder.titleTxt = (TextView) convertView.findViewById(R.id.title);
            holder.qty = (TextView) convertView.findViewById(R.id.qty);
            holder.remove = (Button) convertView.findViewById(R.id.remove);
           // holder.remove = (TextView) convertView.findViewById(R.id.remove);

            convertView.setTag(R.id.holder, holder);
        }
       // Products.find(Products.class,"productid = ? and voucherid = ?",  pdcts.get(position).getProductId(),dt.getVoucherid());
        Bitmap decodedByte=null;
        ViewHolder holder = (ViewHolder) convertView.getTag(R.id.holder);
        byte[] decodedString = Base64.decode(pdcts.get(position).getimage(), Base64.DEFAULT);
        decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
        holder.iconView.setImageBitmap( decodedByte );
       holder.titleTxt.setText(pdcts.get(position).getproductname());
      //  holder.titleTxt.setTypeface(typeface);
      //  holder.qty.setTypeface(typeface);

      // holder.remove.setTypeface(typeface);
       // ts= Topups.find(Topups.class, "voucherid=? and cardnumber =? and bengroup=?",dt.getVoucherid(),dt.getCardnumber(),dt.getBenGroup());
        //String vtp=ts.get(0).getvocherType();

        holder.remove.setTag(R.id.holderr,(PurchasedProducts.find(PurchasedProducts.class,"productid = ?",  pdcts.get(position).getProductid()).get(0)).getId());
       /* if(vtp.equalsIgnoreCase("CA")){
            holder.qty.setText("("+(PurchasedProducts.find(PurchasedProducts.class,"productid = ?",  pdcts.get(position).getProductid())).get(0).getTotalprice()+")");
        }else {*/
            holder.qty.setText("("+(PurchasedProducts.find(PurchasedProducts.class,"productid = ?",  pdcts.get(position).getProductid())).get(0).getQuantitydeducted()+")");
       // }

         holder.remove.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View v) {

        showThemed(v);

          //notifyDataSetChanged();
          //Log.d("dddddddddddrr",v.getTag().toString()+"sss");
    }
});
        return convertView;
    }
    private static class ViewHolder {
        public ImageView iconView;
        public TextView titleTxt,qty;

        public TextView unreadTxt;
        public Button remove;
    }
    public void showThemed(final View v) {
        new MaterialDialog.Builder(context)
                .title("Are you sure?")
                .content("This will remove the selected product. The action is irreversible.")
                .positiveText("Yes")
                .negativeText("No")
                .positiveColorRes(R.color.material_red_400)
                .negativeColorRes(R.color.material_red_400)
                .titleGravity(GravityEnum.CENTER)
                .titleColorRes(R.color.material_red_400)
                .contentColorRes(android.R.color.white)
                .backgroundColorRes(R.color.material_blue_grey_800)
                .dividerColorRes(R.color.material_teal_a400)
                .btnSelector(R.drawable.md_btn_selector_custom, DialogAction.POSITIVE)
                .positiveColor(Color.WHITE)
                .negativeColorAttr(android.R.attr.textColorSecondaryInverse)
                .theme(Theme.DARK)
                .onAny(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        if(which.name().equals("NEGATIVE")) {


                        }else{
                            ((CartActivity)context).update(v.getTag(R.id.holderr).toString());
                        }
                    }
                })
                .show();
    }



}