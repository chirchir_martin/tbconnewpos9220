package com.evoucher.compas.evoucher.fragments;

import android.app.AlertDialog;
import android.content.Intent;
import android.media.Image;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.evoucher.compas.evoucher.Adapters.ScrollerViewPager;
import com.evoucher.compas.evoucher.BeneficiaryList;
import com.evoucher.compas.evoucher.Details;
import com.evoucher.compas.evoucher.R;
import com.evoucher.compas.evoucher.network.BeneficiaryHttp;
import com.evoucher.compas.evoucher.ui.SpotsDialog;
import com.lynx.evoucher.models.Beneficiary;
import com.lynx.evoucher.models.FingerPrintVO;
import com.lynx.evoucher.models.Member;
import com.lynx.evoucher.models.Transactions;

import java.util.List;

import cn.pedant.SweetAlert.SweetAlertDialog;

/**
 * Created by JOHNIE on 9/23/2015.
 */
public class Fragment6 extends Fragment implements View.OnClickListener  {
    AlertDialog dialog;
    Handler handler;
    Member dt;ImageView prev;
    ImageButton savebutton,success;
   Details dtt;
    List<  Beneficiary > b;
    Beneficiary ben;
    ScrollerViewPager viewPager;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment6, container, false);

    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        dtt=new Details(getActivity());
        savebutton=(ImageButton)getView().findViewById(R.id.button1);
        savebutton.setOnClickListener(this);
        prev = (ImageView)getView().findViewById(R.id.prev);
        prev.setOnClickListener(this);
        viewPager = (ScrollerViewPager) getActivity().findViewById(R.id.view_pager);
    }


    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.button1) {
            //view.isClickable();
            showBasicIcon();
        }
        if(view.getId() == R.id.prev) {
            viewPager.setCurrentItem(4, true);
        }
    }
    private void showBasicIcon() {

        new MaterialDialog.Builder(getActivity())
                .iconRes(R.drawable.ecompass)
                // limits the displayed icon size to 48dp
                .title("")
                .content("Are you Sure??")
                .positiveText("Yes")
                .negativeText("No")
                .callback(new MaterialDialog.ButtonCallback() {
                    @Override
                    public void onPositive(MaterialDialog dialog){
                        Log.d("SAVE##",Member.right_thumb);
                        Log.d("SAVE##",Member.right_index);
                        Log.d("SAVE##",Member.left_thumb);
                        Log.d("SAVE##",Member.left_index);
                        Beneficiary beneficiary=new Beneficiary();
                        beneficiary.setFirstname(Member.firstname);
                        beneficiary.setLastname(Member.lastname);
                        beneficiary.setMiddlename(Member.middlename);
                        beneficiary.setUser_image(Member.user_image);
                        beneficiary.setSignature(Member.signature);

                        beneficiary.setNational_id(Member.national_id);
                        beneficiary.setRight_thumb(Member.right_thumb);
                        beneficiary.setRight_index(Member.right_index);
                        beneficiary.setLeft_thumb(Member.left_thumb);
                        beneficiary.setLeft_index(Member.left_index);
                        beneficiary.setTitle(Member.title);

                        beneficiary.setGender(Member.gender);
                        beneficiary.setDate_of_birth(Member.date_of_birth);
                        beneficiary.setFamily_size(Member.familysize);
                        beneficiary.setProgrammeid(Member.programmeid);
                        beneficiary.setBeneficiary_group_id(Member.beneficiary_group_id);
                        beneficiary.setIs_uploaded("0");
                        beneficiary.setCard_number("1111113333353266");
                      Log.d("#ssw",beneficiary.getRight_thumb());
                        Log.d("#ssw",beneficiary.getRight_index());
                        Log.d("#ssw",beneficiary.getLeft_thumb());
                        Log.d("#ssw",beneficiary.getLeft_index());
                        Log.d("#ssw",Member.user_image);
                        Log.d("#ssw",Member.signature);
                        beneficiary.save();
                        b= Beneficiary .find(Beneficiary.class, null, null, null, "id DESC", "1");
                        FingerPrintVO bioimage = new FingerPrintVO();
                        bioimage.setBeneficiary_id(String.valueOf(b.get(0).getId()));
                        bioimage.setImage(b.get(0).getRight_thumb());
                        bioimage.save();

                        FingerPrintVO bioimage1 = new FingerPrintVO();
                        bioimage1.setBeneficiary_id(String.valueOf(b.get(0).getId()));
                        bioimage1.setImage(b.get(0).getRight_index());
                        bioimage1.save();


                        FingerPrintVO bioimage2 = new FingerPrintVO();
                        bioimage2.setBeneficiary_id(String.valueOf(b.get(0).getId()));
                        bioimage2.setImage(b.get(0).getLeft_thumb());
                        bioimage2.save();

                        FingerPrintVO bioimage3 = new FingerPrintVO();
                        bioimage3.setBeneficiary_id(String.valueOf(b.get(0).getId()));
                        bioimage3.setImage(b.get(0).getLeft_index());
                        bioimage3.save();
                        alertSuccess();

//                        if(row>=0){
//                                alertSuccess();
//                        }
                        //showIndeterminateProgressDialog();
//                        BeneficiaryHttp beneficiaryHttp=new BeneficiaryHttp(getActivity());
//                        beneficiaryHttp.UploadBeneficiary();
                    }

                    @Override
                    public void onNegative(MaterialDialog dialog) {

                    }
                })
                .show();
    }
    public void alertSuccess(){
        new SweetAlertDialog(getActivity(), SweetAlertDialog.SUCCESS_TYPE)
                .setTitleText("DETAILS ADDED")
                .setContentText("SUCCESSFULLY")
                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sDialog) {
                        sDialog.dismissWithAnimation();
                        Intent intent=new Intent(getActivity(),BeneficiaryList.class);
                        startActivity(intent);
                        getActivity().finish();

                    }
                }).show();
    }

}
