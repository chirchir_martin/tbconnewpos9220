package com.evoucher.compas.evoucher;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Environment;
import android.os.Handler;
import android.provider.Settings;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.evoucher.compas.evoucher.fragments.SyncReports;
import com.lynx.evoucher.models.Beneficiary;
import com.lynx.evoucher.models.BeneficiaryGroup;
import com.lynx.evoucher.models.CardBlock;
import com.lynx.evoucher.models.Colors;
import com.lynx.evoucher.models.FingerPrintVO;
import com.lynx.evoucher.models.Categories;
import com.lynx.evoucher.models.Products;
import com.lynx.evoucher.models.Programmes;
import com.lynx.evoucher.models.PurchasedProducts;
import com.lynx.evoucher.models.ServicePrices;
import com.lynx.evoucher.models.Services;
import com.lynx.evoucher.models.SyncLogs;
import com.lynx.evoucher.models.Topups;
import com.lynx.evoucher.models.TopupsLogs;
import com.lynx.evoucher.models.Transactions;
import com.lynx.evoucher.models.TransactionsListProducts;
import com.lynx.evoucher.models.Users;
import com.lynx.evoucher.models.Vouchers;
import com.lynx.evoucher.models.Configuration;
import com.lynx.evoucher.models.Commodities;


import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import cn.pedant.SweetAlert.SweetAlertDialog;

/**
 * Created by Administrator on 8/31/2016.
 */
public class Network {
    ImageView logo;
    TextView tv0, tv1;

    JSONObject ja = null;
    JSONArray users, categories, products, programmes, vouchers, topup, members, bgs = null;
    Typeface typeface;

    Context context;
    Details dt;
    StringBuilder sb1 = null;
    SharedPreferences settings;
    MaterialDialog dialog, fdialog;
    String url;
    static String state = "";
    JSONArray commodities;
    JSONObject transaction;
    JSONObject archives_obj, tlogsobj;

    JSONArray cs, tr, blocked_cards, colors, productsmaster;
    String data;
    JSONObject jsonObject = new JSONObject();
    List<Configuration> config;
    public static String status = "", rensponse = "", rensponse1 = "";
    final Handler handler = new Handler();
    public static String server_protocol = "http://";
    public static String server_ip = "192.168.13.71";
    public static String server_port = ":8585";
    public static String download_url = "/compas/rest/user/downloadVo";
    public static String upload_beneficiary_url = "/compas/rest/member/updAndCustomer";

    String TAG = "##compas";

    public Network(Context context, String mode) {
        this.context = context;
        state = mode;
        dt = new Details(context);
    }

    public void fetchData() throws Exception {


        try {
            config = Configuration.listAll(Configuration.class);
            String savedurl = config.get(0).getUrl();
            String savedport = config.get(0).getPort();
//            String url= "http://192.168.13.71:8595/compas/rest/user/downloadVo";
//            String url= server_protocol+server_ip+server_port+download_url;
            String url = "http://" + savedurl + ":" + savedport + "/compas/rest/user/downloadVo";
            String data = URLEncoder.encode(dt.getMac(), "UTF-8");
            Log.i("###MAC", dt.getMac());
            final String PREFS_NAME = "MyPrefsFile";
            settings = context.getSharedPreferences(PREFS_NAME, 0);


            ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(context.CONNECTIVITY_SERVICE);
            final NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
            // Connectivity issue, we quit
            if (networkInfo == null || networkInfo.getState() != NetworkInfo.State.CONNECTED) {

                // Display a toast in that case
                handler.post(new Runnable() {
                    @Override
                    public void run() {

                        Toast.makeText(context, R.string.NetworkError, Toast.LENGTH_SHORT).show();
                    }
                });

            } else {

                if (settings.getBoolean("fistTime", true)) {
                    //  Toast.makeText(context,"downloading",Toast.LENGTH_LONG).show();
                    new Connection().execute(url, dt.getMac()).get();
                } else {
                    context.startActivity(new Intent(context, ProgrammeActivity.class));
                    LoadingActivity.fa.finish();
                }
            }
        } catch (Exception e) {
            //  Toast.makeText(context,e.toString(),Toast.LENGTH_LONG).show();

        }
    }

    private class Connection extends AsyncTask<String, String, String> {

        @Override
        protected String doInBackground(String... params) {

            BufferedReader reader = null;

            try {
                Log.d("NETWORK1", params[0]);
                // Defined URL  where to send data
                URL url = new URL(params[0]);
                URLConnection conn = url.openConnection();
                conn.setDoOutput(true);
                conn.setUseCaches(false);
                conn.setConnectTimeout(10000);
                OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());
                if (state.equalsIgnoreCase("Topups"))
                    wr.write("" + "#" + dt.getMac());
                else
                    wr.write(dt.getMac());
                wr.flush();
                // Get the server response
                reader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                sb1 = new StringBuilder();
                String line = null;
                // Read Server Response
                while ((line = reader.readLine()) != null) {

                    sb1.append(line + "");
                }

                reader.close();
                JSONObject response;
                String respCode = "";
                Log.i("d0sd0s0d0sd0sd0", sb1.length() + "fsfsfsf");
                ja = new JSONObject(sb1.toString());
                if (state.equalsIgnoreCase("Master")) {
                    respCode = ja.getString("respCode");
                    if (!(respCode.equalsIgnoreCase("400"))) {
                        users = ja.getJSONArray("users");
                        programmes = ja.getJSONArray("programmes");
                        productsmaster = ja.getJSONArray("products");
                        colors = ja.getJSONArray("colors");
                        blocked_cards = ja.getJSONArray("blockCards");
                        Programmes.deleteAll(Programmes.class);
                        BeneficiaryGroup.deleteAll(BeneficiaryGroup.class);
                        Products.deleteAll(Products.class);
                        Categories.deleteAll(Categories.class);
                        Services.deleteAll(Services.class);
                        ServicePrices.deleteAll(ServicePrices.class);
                        Programmes.deleteAll(Programmes.class);
                        Log.i("####IMAGESLENGTH", String.valueOf(colors.length()));
                        if (colors.length() > 0) {
                            Colors.deleteAll(Colors.class);
                        }
                        if (blocked_cards.length() > 0) {
                            CardBlock.deleteAll(CardBlock.class);
                        }

                        for (int i = 0; i < colors.length(); i++) {
                            Colors images_table = new Colors();
                            images_table.setImageid(colors.getJSONObject(i).getString("colorId"));
                            images_table.setColorcode(colors.getJSONObject(i).getString("colorCode"));
                            images_table.setName(colors.getJSONObject(i).getString("colorName"));
                            images_table.setColorno(colors.getJSONObject(i).getString("colorNo"));
                            images_table.save();
                        }
                        for (int i = 0; i < blocked_cards.length(); i++) {
                            CardBlock cards_table = new CardBlock();
                            cards_table.setCardno(blocked_cards.getJSONObject(i).getString("cardNo"));
                            cards_table.setRationno(blocked_cards.getJSONObject(i).getString("rationNo"));
                            cards_table.save();
                        }

                        Log.i("####BLOCKEDCARDS", String.valueOf(blocked_cards.length()) + "hghgh");
                        Vouchers.deleteAll(Vouchers.class);
                        JSONArray sprices;

                        ServicePrices prices_table;
                        Products products_table;
                        Log.i("####WSDSDSDSW", String.valueOf(productsmaster.length()) + "hghgh");
                        for (int k = 0; k < productsmaster.length(); k++) {
                            try {
                                products_table = new Products();
                                // Log.d("####DDSDSDSDSDSD", productsmaster.getJSONObject(k).getString("productId"));
                                products_table.setProductid(productsmaster.getJSONObject(k).getString("productId"));
                                List<Products> pts = Products.find(Products.class, " productid =?", productsmaster.getJSONObject(k).getString("productId"));
                                if (!(pts.size() > 0)) {
                                    products_table.setproductcode(productsmaster.getJSONObject(k).getString("productCode"));
                                    products_table.setDescription(productsmaster.getJSONObject(k).getString("productDesc"));
                                    products_table.setCategory_id(productsmaster.getJSONObject(k).getString("categoryId"));
                                    products_table.setproductname(productsmaster.getJSONObject(k).getString("productName"));
                                    products_table.setImage(productsmaster.getJSONObject(k).getString("image"));


                               /* products_table.setUom(productsmaster.getJSONObject(k).getString("uom"));
                                products_table.setQuantity(productsmaster.getJSONObject(k).getString("quantity"));
                                products_table.setMaxprice(productsmaster.getJSONObject(k).getString("maxPrice"));*/
                                    sprices = productsmaster.getJSONObject(k).getJSONArray("priceDetails");
                                    // Log.i("###serivexcxcxcxss4", String.valueOf(sprices.length()) + "  " + productsmaster.getJSONObject(k).getString("productName") + "Fdfdfd  " + productsmaster.getJSONObject(k).getString("productId"));
                                    for (int l = 0; l < sprices.length(); l++) {
                                        Log.i("###serivexcxcxcxss", String.valueOf(sprices.length()) + "  " + productsmaster.getJSONObject(k).getString("productName") + "Fdfdfd  " + productsmaster.getJSONObject(k).getString("productId") + "  max: " + sprices.getJSONObject(l).getString("maxPrice"));
                                        prices_table = new ServicePrices();
                                        prices_table.setMaxprice(sprices.getJSONObject(l).getString("maxPrice"));
                                        prices_table.setUom(sprices.getJSONObject(l).getString("quantity") + "-" + sprices.getJSONObject(l).getString("uom"));
                                        prices_table.setQuantity(sprices.getJSONObject(l).getString("quantity"));
                                        prices_table.setServiceid(productsmaster.getJSONObject(k).getString("productId"));
                                        prices_table.save();
                                    }
                                    products_table.save();
                                }
                            } catch (Exception e) {
                                Log.d("####PRODUXCXCXC", e.toString());
                            }
                        }
                        bgs = ja.getJSONArray("bnfGrps");
                        if (users.length() > 0) {
                            Users.deleteAll(Users.class);
                        }

                        for (int i = 0; i < users.length(); i++) {
                            Users user_table = new Users();
                            user_table.setUsersid(users.getJSONObject(i).getString("userId"));
                            user_table.setUsername(users.getJSONObject(i).getString("userName"));
                            user_table.setPassword(users.getJSONObject(i).getString("password"));
                            user_table.setLevel(users.getJSONObject(i).getString("level"));
                            user_table.setLocationid(users.getJSONObject(i).getString("locationId"));
                            Log.i("####RERERERE", users.getJSONObject(i).getString("userName") + "       " + users.getJSONObject(i).getString("locationId"));
                            user_table.save();
                        }
                        for (int i = 0; i < programmes.length(); i++) {
                            Programmes programmes_table = new Programmes();
                            programmes_table.setProgramid(programmes.getJSONObject(i).getString("programmeId"));
                            programmes_table.setProgrammename(programmes.getJSONObject(i).getString("programmeName"));
                            programmes_table.setStartdate(programmes.getJSONObject(i).getString("startDate"));
                            programmes_table.setEnddate(programmes.getJSONObject(i).getString("endDate"));
                            programmes_table.setBengroup(programmes.getJSONObject(i).getString("bnfGrpId"));
                            vouchers = programmes.getJSONObject(i).getJSONArray("vouchers");
                            for (int j = 0; j < vouchers.length(); j++) {
                                Vouchers voucher_table = new Vouchers();
                                products = vouchers.getJSONObject(j).getJSONArray("products");
                                voucher_table.setVouchersid(vouchers.getJSONObject(j).getString("voucherId"));
                                voucher_table.setVoucherName(vouchers.getJSONObject(j).getString("voucherName"));
                                voucher_table.setStartdate(vouchers.getJSONObject(j).getString("startDate"));
                                voucher_table.setEnddate(vouchers.getJSONObject(j).getString("endDate"));
                                voucher_table.setProgrammeid(programmes.getJSONObject(i).getString("programmeId"));
                                voucher_table.setBendroup(programmes.getJSONObject(i).getString("bnfGrpId"));
                                Log.d("###PROG", programmes.getJSONObject(i).getString("programmeId"));
                                for (int k = 0; k < products.length(); k++) {
                                    Services service_detail_table = new Services();
                                    service_detail_table.setServiceId(products.getJSONObject(k).getString("serviceId"));
                                    service_detail_table.setVoucherId(vouchers.getJSONObject(j).getString("voucherId"));
                                    service_detail_table.setProgrammeId(programmes.getJSONObject(i).getString("programmeId"));
                                    service_detail_table.setBenGroupId(programmes.getJSONObject(i).getString("bnfGrpId"));
                                    Log.d("##3#PROGdddd", vouchers.getJSONObject(j).getString("voucherId") + programmes.getJSONObject(i).getString("programmeId"));
                                    service_detail_table.save();
                                }
                                voucher_table.save();
                            }


                            programmes_table.save();

                        }

                        for (int i = 0; i < bgs.length(); i++) {
                            BeneficiaryGroup beneficiary_group_table = new BeneficiaryGroup();
                            beneficiary_group_table.setBnfGrpId(bgs.getJSONObject(i).getString("bnfGrpId"));
                            beneficiary_group_table.setBnfGrpName(bgs.getJSONObject(i).getString("bnfGrpName"));
                            beneficiary_group_table.save();

                        }
                    } else {
                        return "-1";
                    }
                } else if (state.equalsIgnoreCase("Beneficiaries")) {
                    respCode = ja.getString("respCode");
                    if (!(respCode.equalsIgnoreCase("400"))) {
                        PurchasedProducts.deleteAll(PurchasedProducts.class);
                        Beneficiary.deleteAll(Beneficiary.class);
                        members = ja.getJSONArray("beneficiaries");

                        for (int i = 0; i < members.length(); i++) {
                            Beneficiary beneficiary_table = new Beneficiary();
                            beneficiary_table.setMember_number(members.getJSONObject(i).getString("memberNo"));
                            beneficiary_table.setFirstname(members.getJSONObject(i).getString("firstName"));
                            beneficiary_table.setLastname(members.getJSONObject(i).getString("surName"));
                            beneficiary_table.setMiddlename(members.getJSONObject(i).getString("otherName"));
                            beneficiary_table.setUser_image(members.getJSONObject(i).getString("memberPic"));
                            beneficiary_table.setNational_id(members.getJSONObject(i).getString("idPassPortNo").trim());
                            beneficiary_table.setRight_thumb(members.getJSONObject(i).getString("right_thumb"));
                            beneficiary_table.setRight_index(members.getJSONObject(i).getString("right_index"));
                            beneficiary_table.setLeft_thumb(members.getJSONObject(i).getString("left_thumb"));
                            beneficiary_table.setLeft_index(members.getJSONObject(i).getString("left_index"));
                            beneficiary_table.setTitle(members.getJSONObject(i).getString("title"));
                            beneficiary_table.setGender(members.getJSONObject(i).getString("gender"));
                            beneficiary_table.setDate_of_birth(members.getJSONObject(i).getString("dateOfBirth"));
                            beneficiary_table.setFamily_size(members.getJSONObject(i).getString("familySize"));
                            beneficiary_table.setProgrammeid(members.getJSONObject(i).getString("programmeId"));
                            beneficiary_table.setBeneficiary_group_id(members.getJSONObject(i).getString("bnfGrpId"));
                            beneficiary_table.setActivation("0");

                            beneficiary_table.setCard_number(members.getJSONObject(i).getString("cardNumber"));
                            //  Log.d("##CARDNUMMBER", members.getJSONObject(i).getString("cardNumber"));
                            beneficiary_table.setCard_pin(members.getJSONObject(i).getString("cardPin"));
                            beneficiary_table.setCard_serial_number(members.getJSONObject(i).getString("serialNo"));
                            beneficiary_table.setMobile(members.getJSONObject(i).getString("cellPhone"));
                            beneficiary_table.setIs_uploaded("1");
                            beneficiary_table.save();

                            List<String> imgs = new ArrayList<>();
                            imgs.add(beneficiary_table.getRight_thumb());
                            imgs.add(beneficiary_table.getRight_index());
                            imgs.add(beneficiary_table.getLeft_thumb());
                            imgs.add(beneficiary_table.getLeft_index());
                            for (String image : imgs) {
                                FingerPrintVO bioimage = new FingerPrintVO();
                                bioimage.setBeneficiary_id(beneficiary_table.getMember_number());
                                bioimage.setImage(image);
//                            bioimage.save();
                            }

                        }
                    } else {
                        return "-1";
                    }
                } else if (state.equalsIgnoreCase("Topups")) {
                    respCode = ja.getString("respCode");
                    if (!(respCode.equalsIgnoreCase("400"))) {
                        topup = ja.getJSONArray("topupDetails");
                        if (topup.length() > 0) {
                            Log.d("###TOPUPS", String.valueOf(topup.length()));
                            Topups.deleteAll(Topups.class);
                            PurchasedProducts.deleteAll(PurchasedProducts.class);
                            for (int i = 0; i < topup.length(); i++) {
                                Topups topups_table = new Topups();
                                topups_table.setProgrammeId(topup.getJSONObject(i).getString("programmeId"));
                                topups_table.setBengroup(topup.getJSONObject(i).getString("bnfGrpId"));
                                topups_table.setBeneficiaryId(topup.getJSONObject(i).getString("beneficiaryId"));
                                topups_table.setCardNumber(topup.getJSONObject(i).getString("cardNumber"));
                                topups_table.setVoucherId(topup.getJSONObject(i).getString("voucherId"));
                                // topups_table.setProductId(topup.getJSONObject(i).getString("productId"));
                                topups_table.setVouchervalue(topup.getJSONObject(i).getString("productPrice"));
                                // topups_table.setProductQuantity(topup.getJSONObject(i).getString("productQuantity"));
                                topups_table.setvoucherType(topup.getJSONObject(i).getString("voucherType"));
                                topups_table.setVocheridno(topup.getJSONObject(i).getString("voucherIdNumber"));
                                Log.i("XXXRRRRR", topup.getJSONObject(i).getString("cardNumber"));
                                topups_table.save();
                            }

                        } else {
                            return "201";
                        }


                    } else {
                        return "-1";
                    }
                }
            } catch (Exception ex) {

                Log.i("dsssssssssdsdsd", ex.toString());
                return "0";
            }


            return "1";
        }

        protected void onPostExecute(final String resultt) {
            try {
                if (resultt.equalsIgnoreCase("0")) {
                    if (status.equalsIgnoreCase("update")) {
                        fdialog.dismiss();
                    }
                    new SweetAlertDialog(context, SweetAlertDialog.WARNING_TYPE)
                            .setTitleText(context.getString(R.string.Errorr))
                            .setContentText(context.getString(R.string.ErrorDownloading))
                            .setConfirmText(context.getString(R.string.Ok))
                            .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sDialog) {
                                    sDialog.dismissWithAnimation();
                                    if (!(status.equalsIgnoreCase("update"))) {
                                        context.startActivity(new Intent(context, UsernameActivity.class));
                                        try {
                                            LoadingActivity.fa.finish();
                                        } catch (Exception e) {

                                        }
                                    }


                                }
                            })
                            .show();
                } else if (resultt.equalsIgnoreCase("-1")) {
                    if (status.equalsIgnoreCase("update")) {
                        fdialog.dismiss();
                    }
                    SweetAlertDialog alert = new SweetAlertDialog(context, SweetAlertDialog.WARNING_TYPE)
                            .setTitleText(context.getString(R.string.Errorr))
                            .setContentText(context.getString(R.string.not_registered))
                            .setConfirmText(context.getString(R.string.Ok))
                            .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sDialog) {
                                    sDialog.dismissWithAnimation();
                                    context.startActivity(new Intent(context, UsernameActivity.class));
                                    if (status.equalsIgnoreCase("update")) {
                                        try {
                                            ProgrammeActivity.fa.finish();
                                        } catch (Exception e) {

                                        }
                                    } else {
                                        try {
                                            LoadingActivity.fa.finish();
                                        } catch (Exception e) {

                                        }
                                    }
                                }
                            });
                    alert.show();
                    alert.setCancelable(false);


                } else if (resultt.equalsIgnoreCase("1")) {
                    final Handler handler = new Handler();
                    if (status.equalsIgnoreCase("update")) {

                        if (state.equalsIgnoreCase("Topups")) {
                            if (rensponse1.equalsIgnoreCase("101")) {
                                rensponse1 = "";

                            } else {
                                rensponse = "200";
                                rensponse1 = "";
                                dt.setDownloadstatus("1");
                                Thread t = new Thread(new Runnable() {
                                    @Override
                                    public void run() {
                                        config = Configuration.listAll(Configuration.class);
                                        String savedurl = config.get(0).getUrl();
                                        String savedport = config.get(0).getPort();
                                        url = "http://" + savedurl + ":" + savedport + "/compas/rest/user/downloadTopup";
                                        try {
                                            new Connection1().execute(url, dt.getMac()).get();
                                        } catch (Exception e) {

                                        }

                                    }
                                });
                                t.start();

                            }

                        } else {
                            fdialog.dismiss();
                            SweetAlertDialog sw;
                            sw = new SweetAlertDialog(context, SweetAlertDialog.SUCCESS_TYPE)
                                    .setTitleText("")
                                    .setContentText(context.getString(R.string.Success))
                                    .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                        @Override
                                        public void onClick(SweetAlertDialog sDialog) {
                                            sDialog.dismissWithAnimation();
                                            if (state.equalsIgnoreCase("Master")) {
                                                context.startActivity(new Intent(context, UsernameActivity.class));
                                                ProgrammeActivity.fa.finish();
                                            }
                                        }
                                    });
                            sw.setCancelable(false);
                            sw.show();

                        }

                    } else {
                        List<Users> user = Users.listAll(Users.class);
                        if (user.size() > 0) {
                            settings.edit().putBoolean("fistTime", false).apply();
                            context.startActivity(new Intent(context, UsernameActivity.class));
                            LoadingActivity.fa.finish();
                        } else {
                            new SweetAlertDialog(context, SweetAlertDialog.WARNING_TYPE)
                                    .setTitleText(context.getString(R.string.Errorr))
                                    .setContentText(context.getString(R.string.NoUsersDownloaded))
                                    .setConfirmText(context.getString(R.string.Ok))
                                    .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                        @Override
                                        public void onClick(SweetAlertDialog sDialog) {
                                            sDialog.dismissWithAnimation();

                                        }
                                    })
                                    .show();
                        }


                    }


                } else if (resultt.equalsIgnoreCase("201")) {
                    if (status.equalsIgnoreCase("update")) {
                        fdialog.dismiss();
                    }
                    new SweetAlertDialog(context, SweetAlertDialog.WARNING_TYPE)
                            .setTitleText(context.getString(R.string.Errorr))
                            .setContentText(context.getString(R.string.Notopupsdownloaded))
                            .setConfirmText(context.getString(R.string.Ok))
                            .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sDialog) {
                                    sDialog.dismissWithAnimation();

                                }
                            })
                            .show();
                }
            } catch (Exception e) {

            }
        }
    }

    public void UpdateMaster() {

        String android_id = Settings.Secure.getString(context.getApplicationContext().getContentResolver(),
                Settings.Secure.ANDROID_ID);
        dt.setMac(android_id);


        status = "update";
        fdialog = new MaterialDialog.Builder(context)
                .title("Updating")
                .content("Please wait...")
                .progress(true, 0)
                .progressIndeterminateStyle(true)
                .cancelable(false)
                .show();

        try {
            config = Configuration.listAll(Configuration.class);
            String savedurl = config.get(0).getUrl();
            String savedport = config.get(0).getPort();
            Log.d("DIFIRIR", savedurl);
            Log.d("DIFIRIR", savedport);
            if (state.equalsIgnoreCase("Master")) {
                url = "http://" + savedurl + ":" + savedport + "/compas/rest/user/downloadVo";
            } else if (state.equalsIgnoreCase("Beneficiaries")) {
                url = "http://" + savedurl + ":" + savedport + "/compas/rest/user/downloadBnf";
            } else if (state.equalsIgnoreCase("Topups")) {
                url = "http://" + savedurl + ":" + savedport + "/compas/rest/user/downloadTopup";
            }
            ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(context.CONNECTIVITY_SERVICE);
            final NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
            // Connectivity issue, we quit
            if (networkInfo == null || networkInfo.getState() != NetworkInfo.State.CONNECTED) {

                // Display a toast in that case
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        fdialog.dismiss();
                        new SweetAlertDialog(context, SweetAlertDialog.WARNING_TYPE)
                                .setTitleText(context.getString(R.string.Errorr))
                                .setContentText(context.getString(R.string.DeviceNotConnected))
                                .setConfirmText(context.getString(R.string.Ok))
                                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                    @Override
                                    public void onClick(SweetAlertDialog sDialog) {
                                        sDialog.dismissWithAnimation();

                                    }
                                })
                                .show();
                    }
                });

            } else {
                final Thread t = new Thread() {
                    @Override
                    public void run() {
                        try {
                            new Connection().execute(url, dt.getMac()).get();
                        } catch (Exception e) {

                        }
                    }
                };
                t.start();

            }
        } catch (Exception e) {

        }
    }


    public void postt() {
        fdialog = new MaterialDialog.Builder(context)
                .title("Uploading Transactions")
                .content("Please wait...")
                .progress(true, 0)
                .progressIndeterminateStyle(true)
                .cancelable(false)
                .show();
        fdialog.setCancelable(false);

        cs = new JSONArray();
        transaction = new JSONObject();
        new HttpAsyncTask().execute();

    }

    private class HttpAsyncTask extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... urls) {
            config = Configuration.listAll(Configuration.class);
            String savedurl = config.get(0).getUrl().trim();
            BufferedReader bufferedReader;
            String savedport = config.get(0).getPort();
            String urll = "http://" + savedurl + ":" + savedport + "/compas/rest/user/updAndroidTrans";
            InputStream inputStream = null;
            String result = "";
            try {

                List<Transactions> transactions = Transactions.listAll(Transactions.class);
                Log.d("###TRANS", String.valueOf(transactions.size()));
                if (transactions.size() != 0) {
                    Log.d("dpsdpspdpsWE", String.valueOf(transactions.size()) + "ppppp");
                    for (int i = 0; i < transactions.size(); i++) {
                        Log.d("dpsdpspdps", transactions.get(i).getTotalvaluereamining());
                        transaction = new JSONObject();
                        commodities = new JSONArray();
                        transaction.put("voucher", transactions.get(i).getVoucheridno());
                        transaction.put("transaction_type", transactions.get(i).getTransactiontype());
                        transaction.put("cancelled_transaction", 0);
                        transaction.put("rationNo", transactions.get(i).getRationno());
                        transaction.put("receipt_number", transactions.get(i).getReceiptno());
                        transaction.put("value_remaining", transactions.get(i).getTotalvaluereamining());
                        transaction.put("total_amount_charged_by_retailer", transactions.get(i).getTotalamountchargedbyretail());
                        transaction.put("user", transactions.get(i).getUser());
                        transaction.put("locationId", transactions.get(i).getLocationid());
                        transaction.put("cardNumber", transactions.get(i).getCardno());
                        transaction.put("timestamp_transaction_created", transactions.get(i).getTimestamp());
                        transaction.put("authentication_type", 0);
                        transaction.put("pos_terminal", transactions.get(i).getDeviceid());
                        List<Commodities> tops = Commodities.find(Commodities.class, "transactionno =?", transactions.get(i).getId().toString());
                        Log.d("dpsdpspdps", String.valueOf(tops.size()));
                        Log.d("##ERROR", "DONE" + tops.size());
                        Log.d("#QRRRRRR", transactions.get(i).getId().toString());
                        for (int j = 0; j < tops.size(); j++) {
                            jsonObject = new JSONObject();
                            jsonObject.put("pos_commodity", tops.get(j).getProductid());
                            jsonObject.put("uom", tops.get(j).getUom());
                            jsonObject.put("quantity_remaining", tops.get(j).getQuantityremaining());
                            jsonObject.put("amount_charged_by_retailer", tops.get(j).getTotalamountcahgerdbyretailer());
                            jsonObject.put("deducted_quantity", tops.get(j).getQuantitydeducted());
                            commodities.put(jsonObject);
                        }
                        transaction.put("commodities", commodities);
                        cs.put(transaction);
                    }
                    Log.d("##ERROR", "DONE");
                    Log.d("as0as0a0sa0s0as", cs.toString());

                } else {
                    return "-3";

                }

                URL url = new URL(urll);
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setDoOutput(true);
                //  conn.addRequestProperty("","");
                conn.setUseCaches(false);
                conn.setConnectTimeout(10000);
                conn.setRequestProperty("Content-Type", "application/json");
                // conn.setRequestProperty("Accept", "application/json");
                conn.setRequestMethod("POST");
                //DataOutputStream wr = new DataOutputStream( conn.getOutputStream());
                OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());
                wr.write(cs.toString());
                wr.flush();
                wr.close();

                bufferedReader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                String line = "";
                // Read Server Response

                while ((line = bufferedReader.readLine()) != null)
                    result += line;
                Log.d(TAG, result);
                JSONObject upload_result = new JSONObject(result);
                if (upload_result.getInt("upload_result") == 200) {

                    Transactions.deleteAll(Transactions.class);
                    //PurchasedProducts.deleteAll(PurchasedProducts.class);
                    Commodities.deleteAll(Commodities.class);
                    SyncLogs.deleteAll(SyncLogs.class);

                    File folder = new File(Environment.getExternalStorageDirectory() +
                            File.separator + "Tbc_Uploaded_txns");
                    boolean success = true;
                    if (!folder.exists()) {
                        success = folder.mkdirs();

                    } else {

                    }

                    if (success) {
                        // Do something on success
                    } else {
                        // Do something else on failure
                    }

                    writeToFile(cs.toString().toString(), folder);
                } else if (upload_result.getInt("upload_result") == 201) {

                } else {

                    return "0";

                }

            } catch (Exception e) {

                return "0";
            }

            // 11. return result
            return result;
        }

        // onPostExecute displays the results of the AsyncTask.
        @Override
        protected void onPostExecute(String response_message) {
            fdialog.dismiss();
            // if (result.equalsIgnoreCase("0")) {
            //  dialog.dismiss();
            new SweetAlertDialog(context, SweetAlertDialog.WARNING_TYPE)
                    .setTitleText("")
                    .setContentText(response_message)
                    .setConfirmText("Ok")
                    .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                        @Override
                        public void onClick(SweetAlertDialog sDialog) {
                            sDialog.dismissWithAnimation();

                        }
                    })
                    .show();

            /*} else if (result.equalsIgnoreCase("-3")) {
                fdialog.dismiss();
                Log.d("###TRANS", "No transaction(s) found");
                new SweetAlertDialog(context, SweetAlertDialog.WARNING_TYPE)
                        .setTitleText("")
                        .setContentText("No transaction(s) found")
                        .setConfirmText("Ok")
                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sDialog) {
                                sDialog.dismissWithAnimation();

                            }
                        })
                        .show();
            } else {
                alertSuccess();
            }*/
            // }
        }


        public void UploadLogs() {

            String android_id = Settings.Secure.getString(context.getApplicationContext().getContentResolver(),
                    Settings.Secure.ANDROID_ID);
            dt.setMac(android_id);

            fdialog = new MaterialDialog.Builder(context)
                    .title("Uploading Topups Logs")
                    .content("Please wait...")
                    .progress(true, 0)
                    .progressIndeterminateStyle(true)
                    .cancelable(false)
                    .show();
            fdialog.setCancelable(false);
            new UploadTopupsLogs().execute(dt.getMac());


        }

        public void UploadArchivesTxns() {

            String android_id = Settings.Secure.getString(context.getApplicationContext().getContentResolver(),
                    Settings.Secure.ANDROID_ID);
            dt.setMac(android_id);

            fdialog = new MaterialDialog.Builder(context)
                    .title("Uploading archives transactions")
                    .content("Please wait...")
                    .progress(true, 0)
                    .progressIndeterminateStyle(true)
                    .cancelable(false)
                    .show();
            fdialog.setCancelable(false);
            new UploadTxnsArchives().execute(dt.getMac());


        }


        private class UploadTopupsLogs extends AsyncTask<String, Void, String> {
            @Override
            protected String doInBackground(String... params) {
                config = Configuration.listAll(Configuration.class);
                String savedurl = config.get(0).getUrl().trim();
                BufferedReader bufferedReader;
                String savedport = config.get(0).getPort();
                String urll = "http://" + savedurl + ":" + savedport + "/compas/rest/transaction/gtTopupLogDetails";
                InputStream inputStream = null;
                String result = "";
                try {
                    List<TopupsLogs> tlogs = TopupsLogs.listAll(TopupsLogs.class);
                    JSONArray logs = new JSONArray();
                    JSONObject obj;
                    tlogsobj = new JSONObject();
                    tlogsobj.put("deviceId", params[0]);
                    if (tlogs.size() > 0) {
                        for (int i = 0; i < tlogs.size(); i++) {
                            obj = new JSONObject();
                            obj.put("cardNo", tlogs.get(i).getCardno());
                            obj.put("oldVoucherIdNumber", tlogs.get(i).getOvoucheridno());
                            obj.put("newVoucherIdNumber", tlogs.get(i).getNvoucheridno());
                            obj.put("oldVoucherValue", tlogs.get(i).getOcardbal());
                            obj.put("newVoucherValue", tlogs.get(i).getNtopupvalue());
                            obj.put("newCardBalance", tlogs.get(i).getNcardbal());
                            obj.put("topupDate", tlogs.get(i).getTopuptime());
                            obj.put("vendorDeviceId", tlogs.get(i).getDeviceidno());
                            obj.put("userName", tlogs.get(i).getUsername());
                            obj.put("refNo", tlogs.get(i).getRefno());
                            logs.put(obj);
                        }
                        tlogsobj.put("topupLogDetails", logs);


                    } else {
                        return "-3";

                    }

                    URL url = new URL(urll);
                    HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                    conn.setDoOutput(true);
                    //  conn.addRequestProperty("","");
                    conn.setUseCaches(false);
                    conn.setConnectTimeout(10000);
                    conn.setRequestProperty("Content-Type", "application/json");
                    // conn.setRequestProperty("Accept", "application/json");
                    conn.setRequestMethod("POST");
                    //DataOutputStream wr = new DataOutputStream( conn.getOutputStream());
                    OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());
                    wr.write(tlogsobj.toString());
                    wr.flush();
                    wr.close();

                    bufferedReader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                    String line = "";
                    // Read Server Response

                    while ((line = bufferedReader.readLine()) != null)
                        result += line;
                    Log.d("#####UPTRNAS", result);

                    if (result.equalsIgnoreCase("200")) {
                        TopupsLogs.deleteAll(TopupsLogs.class);


                        File folder = new File(Environment.getExternalStorageDirectory() +
                                File.separator + "Tbc_Uploaded_TopupLogs");
                        boolean success = true;
                        if (!folder.exists()) {
                            success = folder.mkdirs();

                        } else {

                        }

                        if (success) {
                            // Do something on success
                        } else {
                            // Do something else on failure
                        }

                        writeToFile(tlogsobj.toString(), folder);

                    } else {
                        return "0";


                    }

                } catch (Exception e) {
                    Log.i("##DSDSDSDS", e.toString());

                    return "0";
                }

                // 11. return result
                return result;
            }

            // onPostExecute displays the results of the AsyncTask.
            @Override
            protected void onPostExecute(String result) {
                fdialog.dismiss();
                if (result.equalsIgnoreCase("0")) {
                    //  dialog.dismiss();
                    new SweetAlertDialog(context, SweetAlertDialog.WARNING_TYPE)
                            .setTitleText("")
                            .setContentText("Unable to upload the topup logs.")
                            .setConfirmText("Ok")
                            .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sDialog) {
                                    sDialog.dismissWithAnimation();

                                }
                            })
                            .show();

                } else if (result.equalsIgnoreCase("-3")) {
                    fdialog.dismiss();
                    Log.d("###TRANS", "No topup(s) found");
                    new SweetAlertDialog(context, SweetAlertDialog.WARNING_TYPE)
                            .setTitleText("")
                            .setContentText("No topuplog(s) found")
                            .setConfirmText("Ok")
                            .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sDialog) {
                                    sDialog.dismissWithAnimation();

                                }
                            })
                            .show();
                } else {
                    new SweetAlertDialog(context, SweetAlertDialog.SUCCESS_TYPE)
                            .setTitleText("Great!")
                            .setContentText("TOPUP LOGS UPLOADED SUCCESSFULLY")
                            .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sDialog) {
                                    sDialog.dismissWithAnimation();


                                }
                            }).show();
                }
            }
        }


        private class UploadTxnsArchives extends AsyncTask<String, Void, String> {
            @Override
            protected String doInBackground(String... params) {
                config = Configuration.listAll(Configuration.class);
                String savedurl = config.get(0).getUrl().trim();
                BufferedReader bufferedReader;
                String savedport = config.get(0).getPort();
                String urll = "http://" + savedurl + ":" + savedport + "/compas/rest/transaction/gtTransArchiveLog";
                InputStream inputStream = null;
                String result = "";
                try {
                    List<TransactionsListProducts> t_archives = TransactionsListProducts.listAll(TransactionsListProducts.class);
                    JSONArray archives_array = new JSONArray();
                    JSONObject obj;
                    archives_obj = new JSONObject();
                    archives_obj.put("masterDeviceId", params[0]);
                    if (t_archives.size() > 0) {
                        for (int i = 0; i < t_archives.size(); i++) {
                            obj = new JSONObject();
                            obj.put("serviceId", t_archives.get(i).getProductid());
                            obj.put("uom", t_archives.get(i).getUnitofmeasure());
                            obj.put("quantity", t_archives.get(i).getQuantity());
                            obj.put("value", t_archives.get(i).getVal());
                            obj.put("deviceId", t_archives.get(i).getDeviceid());
                            obj.put("transactionDate", t_archives.get(i).getTransactiondate());

                            archives_array.put(obj);
                        }
                        archives_obj.put("transList", archives_array);
                        Log.i("##DSDSDSSDXCCC", archives_array.toString());


                    } else {
                        return "-3";

                    }

                    URL url = new URL(urll);
                    HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                    conn.setDoOutput(true);
                    //  conn.addRequestProperty("","");
                    conn.setUseCaches(false);
                    conn.setConnectTimeout(10000);
                    conn.setRequestProperty("Content-Type", "application/json");
                    // conn.setRequestProperty("Accept", "application/json");
                    conn.setRequestMethod("POST");
                    //DataOutputStream wr = new DataOutputStream( conn.getOutputStream());
                    OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());
                    wr.write(archives_obj.toString());
                    wr.flush();
                    wr.close();

                    bufferedReader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                    String line = "";
                    // Read Server Response

                    while ((line = bufferedReader.readLine()) != null)
                        result += line;
                    Log.d("#####UPTRNAS", result);

                    if (result.equalsIgnoreCase("200")) {
                        // TopupsLogs.deleteAll(TopupsLogs.class);
                        TransactionsListProducts.deleteAll(TransactionsListProducts.class);

                    } else {
                        return "0";


                    }

                } catch (Exception e) {
                    Log.i("##DSDSDSDS", e.toString());

                    return "0";
                }

                // 11. return result
                return result;
            }

            // onPostExecute displays the results of the AsyncTask.
            @Override
            protected void onPostExecute(String result) {
                fdialog.dismiss();
                if (result.equalsIgnoreCase("0")) {
                    //  dialog.dismiss();
                    new SweetAlertDialog(context, SweetAlertDialog.WARNING_TYPE)
                            .setTitleText("")
                            .setContentText("Unable to upload the transactions archives.")
                            .setConfirmText("Ok")
                            .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sDialog) {
                                    sDialog.dismissWithAnimation();

                                }
                            })
                            .show();

                } else if (result.equalsIgnoreCase("-3")) {
                    fdialog.dismiss();

                    new SweetAlertDialog(context, SweetAlertDialog.WARNING_TYPE)
                            .setTitleText("")
                            .setContentText("No transactions archives found")
                            .setConfirmText("Ok")
                            .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sDialog) {
                                    sDialog.dismissWithAnimation();

                                }
                            })
                            .show();
                } else {
                    new SweetAlertDialog(context, SweetAlertDialog.SUCCESS_TYPE)
                            .setTitleText("Great!")
                            .setContentText("TRANSACTIONS ARCHIVES UPLOADED SUCCESSFULLY")
                            .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sDialog) {
                                    sDialog.dismissWithAnimation();


                                }
                            }).show();
                }
            }
        }


        public void alertSuccess() {
            new SweetAlertDialog(context, SweetAlertDialog.SUCCESS_TYPE)
                    .setTitleText("")
                    .setContentText("TRANSACTIONS UPLOADED SUCCESSFULLY")
                    .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                        @Override
                        public void onClick(SweetAlertDialog sDialog) {
                            sDialog.dismissWithAnimation();


                        }
                    }).show();
        }

        private class Connection1 extends AsyncTask<String, String, String> {

            @Override
            protected String doInBackground(String... params) {

                BufferedReader reader = null;

                try {
                    Log.d("NETWORK1", params[0]);
                    // Defined URL  where to send data
                    URL url = new URL(params[0]);
                    HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                    conn.setDoOutput(true);
                    // conn.setRe
                    conn.setRequestMethod("GET");
                    conn.setUseCaches(false);
                    conn.setConnectTimeout(5000);
                    OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());
                    wr.write("200" + "#" + dt.getMac());
                    wr.flush();
                    wr.close();
                    // Get the server response
                    reader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                    sb1 = new StringBuilder();
                    String line = null;
                    // Read Server Response
                    while ((line = reader.readLine()) != null) {


                        sb1.append(line + "");
                    }
                } catch (Exception ex) {

                    Log.i("dsssssssssdsdsd", ex.toString());
                    return "0";
                }


                return "1";
            }

            protected void onPostExecute(final String resultt) {


                fdialog.dismiss();
                new SweetAlertDialog(context, SweetAlertDialog.SUCCESS_TYPE)
                        .setTitleText("")
                        .setContentText(context.getString(R.string.Success))
                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sDialog) {
                                sDialog.dismissWithAnimation();
                            }
                        })

                        .show();
            }
        }

        private void writeToFile(String data, File folder) {
            SimpleDateFormat date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            //create a file and write tha data
            final File file = new File(folder, date.format(new Date()) + ".txt");
            try {
                file.createNewFile();
                FileOutputStream fOut = new FileOutputStream(file);
                OutputStreamWriter myOutWriter = new OutputStreamWriter(fOut);
                myOutWriter.append(data);
                myOutWriter.close();
                fOut.flush();
                fOut.close();
            } catch (IOException e) {
                Log.e("Exception", "File write failed: " + e.toString());
            }
        }

    }
}
