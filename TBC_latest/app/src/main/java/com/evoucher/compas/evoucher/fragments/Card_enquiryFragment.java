package com.evoucher.compas.evoucher.fragments;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.nfc.NdefMessage;
import android.nfc.NdefRecord;
import android.nfc.NfcAdapter;
import android.nfc.Tag;
import android.nfc.tech.Ndef;
import android.nfc.tech.NdefFormatable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.GravityEnum;
import com.afollestad.materialdialogs.MaterialDialog;
import com.afollestad.materialdialogs.Theme;
import com.evoucher.compas.evoucher.CartActivity;
import com.evoucher.compas.evoucher.Details;
import com.evoucher.compas.evoucher.MainScreenActivity;
import com.evoucher.compas.evoucher.R;
import com.lynx.evoucher.models.Topups;
import com.lynx.evoucher.models.Vouchers;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.net.URLConnection;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import cn.pedant.SweetAlert.SweetAlertDialog;

/**
 * Created by Administrator on 12/10/2016.
 */
public class Card_enquiryFragment extends Fragment {
    TextView cardno,name,bal,tap;
    ViewGroup details;
    Animation animation;
    JSONArray dd,aa;
    Typeface typeface;
    Details dt;
    String uid,cn;
   // SimpleDateFormat date = new SimpleDateFormat("yyyy-MM-dd");
    MaterialDialog fdialog;
    MaterialDialog dialog;
    Tag tag;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        return inflater.inflate(R.layout.card_enquiry_fragment, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {

       /* animation = new AlphaAnimation(1, 0);
        animation.setDuration(800);
        animation.setInterpolator(new LinearInterpolator());
        animation.setRepeatCount(Animation.INFINITE);
        animation.setRepeatMode(Animation.REVERSE);
        typeface = Typeface.createFromAsset(getActivity().getAssets(), "Roboto-Regular.ttf");*/
        tap=(TextView)getView().findViewById(R.id.tapcard) ;
      /*  tap.setTypeface(typeface);
         tap.startAnimation(animation);*/
        super.onViewCreated(view, savedInstanceState);
    }
    public void processNfc(final String cardno,final String uid,final Tag tag) {
        this.cn=cardno;
        dt=new Details(getActivity());
        this.tag=tag;
        this.uid=uid;
        Log.d("####EEREREREFDF",uid);
        fdialog=new MaterialDialog.Builder(getActivity())
                .title("Toping up the card")
                .content("Please wait...")
                .progress(true, 0)
                .progressIndeterminateStyle(true)
                .cancelable(false)
                .show();

        try {
            new Topupthread().execute(cardno,uid).get();

        }catch (Exception i){
            Log.i("##EFFDF",i.toString());

        }
    }
    private void formatTag(Tag tag, NdefMessage ndefMessage) {
        if (tag != null) {
            try {
                Ndef ndefTag = Ndef.get(tag);

                if (ndefTag == null)  {
                    // Let's try to format the Tag in NDEF
                    NdefFormatable nForm = NdefFormatable.get(tag);
                    if (nForm != null) {
                        nForm.connect();
                        nForm.format(ndefMessage);
                        nForm.close();
                        //  dialog.dismiss();
                    }
                }
                else {
                    ndefTag.connect();
                    ndefTag.writeNdefMessage(ndefMessage);
                    ndefTag.close();
                    // alertSuccess();
                    //  dialog.dismiss();
                }
            }
            catch(Exception e) {
                Log.i("###CARDERROR",e.toString());
                dt.setError("1");
                fdialog.dismiss();
                new SweetAlertDialog(getActivity(), SweetAlertDialog.WARNING_TYPE)
                        .setTitleText("Error")
                        .setContentText("Error while writing data on to the card please try again")
                        .setConfirmText("Ok")
                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sDialog) {
                                sDialog.dismissWithAnimation();

                            }
                        })
                        .show();
            }
        }

    }

    private void writeNdefMessage(Tag tag, NdefMessage ndefMessage) {

        try {

            if (tag == null) {
                Toast.makeText(getActivity(), "Tag object cannot be null", Toast.LENGTH_SHORT).show();
                return;
            }

            Ndef ndef = Ndef.get(tag);

            if (ndef == null) {
                // format tag with the ndef format and writes the message.
                formatTag(tag, ndefMessage);
            } else {
                ndef.connect();

                if (!ndef.isWritable()) {
                    Toast.makeText(getActivity(), "Tag is not writable!", Toast.LENGTH_SHORT).show();

                    ndef.close();
                    return;
                }

                ndef.writeNdefMessage(ndefMessage);
                ndef.close();
                //dialog.dismiss();
                //   alertSuccess();



            }

        } catch (Exception e) {
            Log.i("###CARDERROR",e.toString());
            fdialog.dismiss();
            dt.setError("1");
            new SweetAlertDialog(getActivity(), SweetAlertDialog.WARNING_TYPE)
                    .setTitleText("Error")
                    .setContentText("Error while writing data on to the card please try again")
                    .setConfirmText("Ok")
                    .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                        @Override
                        public void onClick(SweetAlertDialog sDialog) {
                            sDialog.dismissWithAnimation();

                        }
                    })
                    .show();
        }

    }
    private NdefRecord createTextRecord(String content) {
        try {
            byte[] language;
            language = Locale.getDefault().getLanguage().getBytes("UTF-8");

            final byte[] text = content.getBytes("UTF-8");
            final int languageSize = language.length;
            final int textLength = text.length;
            final ByteArrayOutputStream payload = new ByteArrayOutputStream(1 + languageSize + textLength);

            payload.write((byte) (languageSize & 0x1F));
            payload.write(language, 0, languageSize);
            payload.write(text, 0, textLength);

            return new NdefRecord(NdefRecord.TNF_WELL_KNOWN, NdefRecord.RTD_TEXT, new byte[0], payload.toByteArray());

        } catch (UnsupportedEncodingException e) {
            Log.i("###CARDERROR",e.toString());
            fdialog.dismiss();
            dt.setError("1");
            new SweetAlertDialog(getActivity(), SweetAlertDialog.WARNING_TYPE)
                    .setTitleText("Error")

                    .setContentText("Error while writing data on to the card please try again")
                    .setConfirmText("Ok")
                    .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                        @Override
                        public void onClick(SweetAlertDialog sDialog) {
                            sDialog.dismissWithAnimation();

                        }
                    })
                    .show();
        }
        return null;
    }


    private NdefMessage createNdefMessage(String content) {
        NdefRecord ndefRecord = createTextRecord(content);
        NdefMessage ndefMessage = new NdefMessage(new NdefRecord[]{ndefRecord});
        return ndefMessage;
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
    }
    public void alertSuccess(){
        new SweetAlertDialog(getActivity(), SweetAlertDialog.SUCCESS_TYPE)
                .setTitleText("Topup")
                .setContentText("successfully")
                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sDialog) {
                        sDialog.dismissWithAnimation();


                    }
                })

                .show();
    }


    private class Topupthread extends AsyncTask<String, String, String> {

        @Override
        protected String doInBackground (String...params){
            Log.d("##WEWEWEWEWE",params[0]);
            try {
                List<Topups> topups = Topups.find(Topups.class, "cardnumber = ?",params[0]);
                Log.d("##WEWEWEWEWE",String.valueOf(topups.size()));
                if(topups.size()>0) {
                  //  List<Vouchers> vrs=Vouchers.find(Vouchers.class,"vouchersid=?",topups.get(0).getVoucherId());
                    String total="";
                    Log.i("######CARDNUMBERjj", String.valueOf(topups.size()));
                    JSONObject tups = null;
                    aa = new JSONArray();
                    JSONObject alltps = new JSONObject();
                    dd = new JSONArray();
                    if(dt.getVoucherno().equalsIgnoreCase("")){
                        if(!dt.getValuevoucher().equalsIgnoreCase("")){
                            if(dt.getError().equalsIgnoreCase("1")) {
                                total=dt.getValuevoucher();
                            } else {
                               //total = String.valueOf(Double.parseDouble(dt.getValuevoucher()) + Double.parseDouble(topups.get(0).getProductPrice()));
                            }
                        } else {
                            //total = topups.get(0).getProductPrice();
                        }
                        alltps.put("vochervalue",total);
                        alltps.put("vocherno", topups.get(0).getVocheridno());

                        for (int i = 0; i < topups.size(); i++) {
                            tups = new JSONObject();
                            tups.put("benid", topups.get(i).getBeneficiaryId());
                            tups.put("cardno", topups.get(i).getCardNumber());
                            tups.put("programmeid", topups.get(i).getProgrammeId());
                            tups.put("voucherid", topups.get(i).getVoucherId());
                            tups.put("bengrp", topups.get(i).getBengroup());
                            tups.put("vtype", topups.get(i).getvocherType());
                            dd.put(tups);
                        }
                        alltps.put("tups", dd);
                        aa.put(alltps);
                        Log.d("######CARDNUMBERjj", String.valueOf(dd.length() + "kkk"));

                    }else if(dt.getVoucherno().equalsIgnoreCase(topups.get(0).getVocheridno())){
                        return "3";

                    }else{
                        if(!dt.getValuevoucher().equalsIgnoreCase(""))
                          //  total=String.valueOf(Double.parseDouble(dt.getValuevoucher())+Double.parseDouble(topups.get(0).getProductPrice()));

                           // total= topups.get(0).getProductPrice();
                        alltps.put("vochervalue",total);
                        alltps.put("vocherno", topups.get(0).getVocheridno());
                        for (int i = 0; i < topups.size(); i++) {
                            tups = new JSONObject();
                            tups.put("benid", topups.get(i).getBeneficiaryId());
                            tups.put("cardno", topups.get(i).getCardNumber());
                            tups.put("programmeid", topups.get(i).getProgrammeId());
                            tups.put("voucherid", topups.get(i).getVoucherId());
                            tups.put("bengrp", topups.get(i).getBengroup());
                            tups.put("vtype", topups.get(i).getvocherType());
                            dd.put(tups);
                        }
                        alltps.put("tups", dd);
                        aa.put(alltps);
                        Log.d("######CARDNUMBERjj", String.valueOf(dd.length() + "kkk"));
                    }



                }else{
                    return "1";
                }

            } catch (Exception ex) {
                return "0";
            }

            return "2";
        }


        protected void onPostExecute(final String resultt) {

            if(resultt.equalsIgnoreCase("2")){
                NdefMessage ndefMessage = createNdefMessage(uid + ";" + cn + ";" + aa.toString());
                writeNdefMessage(tag, ndefMessage);
                fdialog.dismiss();

                alertSuccess();
            }else if(resultt.equalsIgnoreCase("1")){
                fdialog.dismiss();
                new SweetAlertDialog(getActivity(), SweetAlertDialog.WARNING_TYPE)
                        .setTitleText("Error")
                        .setContentText("No topups found for this card")
                        .setConfirmText("Ok")
                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sDialog) {
                                sDialog.dismissWithAnimation();

                            }
                        })
                        .show();
            }else if(resultt.equalsIgnoreCase("0")){
                fdialog.dismiss();
                new SweetAlertDialog(getActivity(), SweetAlertDialog.WARNING_TYPE)
                        .setTitleText("Error")
                        .setContentText("Error while processing topups")
                        .setConfirmText("Ok")
                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sDialog) {
                                sDialog.dismissWithAnimation();

                            }
                        })
                        .show();

            }else if(resultt.equalsIgnoreCase("3")){
                fdialog.dismiss();
                new SweetAlertDialog(getActivity(), SweetAlertDialog.WARNING_TYPE)
                        .setTitleText("Sorry")
                        .setContentText("Top up is already done on this card")
                        .setConfirmText("Ok")
                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sDialog) {
                                sDialog.dismissWithAnimation();

                            }
                        })
                        .show();

            }

        }
    }

}
