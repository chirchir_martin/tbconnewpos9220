package com.evoucher.compas.evoucher;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.evoucher.compas.evoucher.Adapters.VouchersAdapter;
import com.lynx.evoucher.models.Products;
import com.lynx.evoucher.models.Topups;
import com.lynx.evoucher.models.Vouchers;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import jp.wasabeef.recyclerview.adapters.AlphaInAnimationAdapter;

public class VouchersActivity extends AppCompatActivity implements ItemClickListener {
    private static RecyclerView recyclerView;

    MaterialDialog fdialog;
    TextView ur;
    List<String> vouchersIds=new ArrayList<String>();;
    List<Vouchers> voucherss=new ArrayList<Vouchers>();
    public  static String id="";
    String vvalue ="0";
    JSONObject tups;
    Details dt;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vouchers);
        recyclerView = (RecyclerView)findViewById(R.id.recycler_view);
        recyclerView.setHasFixedSize(true);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        dt=new Details(VouchersActivity.this);
        ur=(TextView)findViewById(R.id.ur) ;
        ur.setText(R.string.LoggedAs +dt.getUser());
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        overridePendingTransition(0, 0);
        vvalue =getIntent().getStringExtra("VOUCHERVALUE");
        tups=dt.getTopups();
        Log.d("######DSDQRQCQ",String.valueOf(tups.length()));
        List<Vouchers> rr=Vouchers.listAll(Vouchers.class);
        for(Vouchers ff:rr)
                Log.d("##VOUCHERiD",ff.getVouchersid()+"dwww"+ff.getBendroup());
      //  vouchers=Vouchers.find(Vouchers.class,"programmeid = ? and bendroup=?",dt.getProgrammeid(),dt.getBenGroup());
        try {
            vouchersIds.clear();
            voucherss.clear();

            if (tups.length() != 0) {
                for (int i = 0; i < tups.length(); i++){
                    if( vouchersIds.contains(tups.get("voucherid").toString())) {
                    } else {
                        vouchersIds.add(tups.get("voucherid").toString());

                    }
                }
                Log.d("######DSDQRQCQ",String.valueOf(vouchersIds.get(0)));
                for (int i = 0; i <  vouchersIds.size(); i++) {
                    voucherss.addAll(Vouchers.find(Vouchers.class, "programmeid=? and vouchersid =? and bendroup=?",dt.getProgrammeid(),vouchersIds.get(i),dt.getBenGroup()));
                }
                if(voucherss.size()>0) {

                    recyclerView.setLayoutManager(new StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL));
                    VouchersAdapter adapter = new VouchersAdapter(VouchersActivity.this, voucherss);
                    adapter.setClickListener(this);
                    AlphaInAnimationAdapter alphaAdapter = new AlphaInAnimationAdapter(adapter);
                    alphaAdapter.setDuration(1500);
                    recyclerView.setAdapter(alphaAdapter);

                }else{
                    Toast.makeText(VouchersActivity.this, R.string.NoVouchers ,Toast.LENGTH_LONG).show();
                }
            }
        }catch (Exception e){
            Log.i("###RERERE",e.toString());
        }

    }

    @Override
    public void onClick(View view, int tag) {
        Log.d("#####CLICKqqqq", String.valueOf(tag));
        Log.d("#####CLICKqqqq", dt.getBenGroup());
        dt.setVoucherid(String.valueOf(tag));
        startActivity(new Intent(VouchersActivity.this, MainScreenActivity.class).putExtra("VOUCHERVALUE",vvalue));
        finish();

    }

    @Override
    public void onBackPressed() {
        Intent intent1 = new Intent(this, ProgrammeActivity.class);
        startActivity(intent1);
        finish();


    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            Intent intent1 = new Intent(this, ProgrammeActivity.class);
            startActivity(intent1);
            finish();

        }
        return super.onOptionsItemSelected(item);
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        return true;
    }


}
