package com.evoucher.compas.evoucher.Adapters;

import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.animation.DecelerateInterpolator;
import android.support.v4.view.ViewPager;
import java.lang.reflect.Field;

/**
 * Created by Kibet on 9/2/2016.
 */
public class ScrollerViewPager extends ViewPager {

    private static final String TAG = ScrollerViewPager.class.getSimpleName();
    private static boolean enabled;
    private int duration = 1000;

    public ScrollerViewPager(Context context) {
        super(context);
        this.enabled = true;

    }

    public ScrollerViewPager(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.enabled = true;

    }


    public void fixScrollSpeed(){
        fixScrollSpeed(duration);
    }

    public void fixScrollSpeed(int duration){
        this.duration = duration;
        setScrollSpeedUsingRefection(duration);
    }


    private void setScrollSpeedUsingRefection(int duration) {
        try {
            Field localField = ViewPager.class.getDeclaredField("mScroller");
            localField.setAccessible(true);
            FixedSpeedScroller scroller = new FixedSpeedScroller(getContext(), new DecelerateInterpolator(1.5F));
            scroller.setDuration(duration);
            localField.set(this, scroller);
            return;
        } catch (IllegalAccessException localIllegalAccessException) {
        } catch (IllegalArgumentException localIllegalArgumentException) {
        } catch (NoSuchFieldException localNoSuchFieldException) {
        }
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent ev) {
        if (this.enabled) {
            return super.onInterceptTouchEvent(ev);

        }
        return false;
    }
    public void setPagingEnabled(boolean enabled) {
        this.enabled = enabled;
    }
}
