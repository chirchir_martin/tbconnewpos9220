package com.evoucher.compas.evoucher;

import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.afollestad.materialdialogs.MaterialDialog;
import com.example.tscdll.TSCActivity;
import com.lynx.evoucher.models.Users;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import cn.pedant.SweetAlert.SweetAlertDialog;

public class CarryingBillPrint {


    public int  TestPrint() {

        String devAddress = BlueTooth.getAddress("BT-SPP");
        if (devAddress == null) {
            return -1;
        }

        TSCActivity tscDll = new TSCActivity();
        tscDll.openport(devAddress);
        tscDll.setup(70, 110, 4, 4, 0, 0, 0);
        tscDll.clearbuffer();
        try {

            List<Object> commands = new ArrayList<Object>();
            int x =65;
            int y = 140;
            commands.addAll(Arrays.asList(
                    "GAP 0,0\n","SET PRINTKEY OFF\n", "DIRECTION 0\n", "CLS\n",
                    "BAR 15,50,500,3\n",
                    "TEXT 185,60,\"2\",0,2,2,\"", "T B C".getBytes(),
                    "\"\n",
                    "TEXT 135,105,\"2\",0,1,1,\"", "FOOD CARD PROGRAM".getBytes(),
                    "\"\n", "BAR 15,130,500,3\n", "\"\n"
            ));

            y=y+30;
            commands.addAll(Arrays.asList(
                    "BAR 15,"+(y)+",500,3\n",
                    "TEXT 65,"+(y+20)+",\"2\",0,1,1,\"",
                    (padRight("TOTAL VALUE",15)+padLeft(" 800 TBH",10)).getBytes(),
                    "\"\n",
                    "TEXT 65,"+(y+50)+",\"2\",0,1,1,\"",
                    (padRight("BALANCE",15)+padLeft(" 1200 TBH",10)).getBytes(),
                    "\"\n",
                    "BAR 15,"+(y+80)+",500,3\n",
                    "\"\n"
            ));

            try {
                for (Object cmd : commands) {
                    if (cmd instanceof String)
                        tscDll.sendcommand(cmd.toString());

                    if (cmd instanceof byte[]) {
                        tscDll.sendcommand((byte[]) cmd);
                    }
                }
            }
            catch(Exception ex)
            {
                ex.printStackTrace();
                 return -1;
            }

            tscDll.status();
            tscDll.sendcommand("PRINT 1\n");
            tscDll.sendcommand("SET TEAR ON\n");


        } catch (Exception ex) {
            ex.printStackTrace();
            return -1;
        }
        Log.d("bluetooth print",tscDll.status());
        tscDll.closeport();
        return  0;
    }

    int y = 160;

    public int printReceipt(Details dt,List<Receipt> receipts,String charges, String bal,String RationNo,Context context,String openingbal,String vid,String receiptno) {
        String devAddress = BlueTooth.getAddress("BT-SPP");
        if (devAddress == null) {
            new SweetAlertDialog(context, SweetAlertDialog.ERROR_TYPE)
                    .setTitleText(context.getString(R.string.Sorry))
                    .setContentText(context.getString(R.string.printernotpaired))
                    .setConfirmText(context.getString(R.string.Ok))
                    .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                        @Override
                        public void onClick(SweetAlertDialog sDialog) {
                            sDialog.dismissWithAnimation();

                        }
                    })
                    .show();
            return -1;
        }

        TSCActivity tscDll = new TSCActivity();
        tscDll.openport(devAddress);
        // tscDll.nobackfeed();
        // tscDll.setup(70, 170, 4, 4, 0, 0, 0);
        tscDll.clearbuffer();
        try {
            List<Object> commands = new ArrayList<Object>();
            int x = 65;
            y=80;
            Log.i("yyyyy","  "+(y));
            tscDll.sendcommand("SET TEAR ON\n");
            commands.addAll(Arrays.asList(
                    "GAP 0,0\n","SET PRINTKEY OFF\n", "DIRECTION 0\n", "CLS\n",
                    "BAR 15,"+y+",500,3\n",
                    "TEXT 185,"+(y+10)+",\"2\",0,2,2,\"", "T B C".getBytes(),
                    "\"\n",
                    "TEXT 130,"+(y+60)+",\"2\",0,1,1,\"", "Card Holder Receipt".getBytes(),
                    "\"\n",
                    "TEXT 65,"+(y+110)+",\"2\",0,1,1,\"", ("User Name").getBytes(),
                    "\"\n",
                    "TEXT 250,"+(y+110)+",\"2\",0,1,1,\"", (": "+dt.getUser()).getBytes(),
                    "\"\n",
                    "TEXT 65,"+(y+130)+",\"2\",0,1,1,\"", ("MAC Id").getBytes(),
                    "\"\n",
                    "TEXT 250,"+(y+130)+",\"2\",0,1,1,\"", (": "+dt.getMac()).getBytes(),
                    "\"\n",
                    "TEXT 65,"+(y+150)+",\"2\",0,1,1,\"", ("Voucher Id").getBytes(),
                    "\"\n",
                    "TEXT 250,"+(y+150)+",\"2\",0,1,1,\"", (": "+vid).getBytes(),
                    "\"\n",
                    "TEXT 65,"+(y+170)+",\"2\",0,1,1,\"", "Time",
                    "\"\n",
                    "TEXT 250,"+(y+170)+",\"2\",0,1,1,\"", ": "+dt.getTime(),
                    "\"\n",
                    "TEXT 65,"+(y+190)+",\"2\",0,1,1,\"", "Date",
                    "\"\n",
                    "TEXT 250,"+(y+190)+",\"2\",0,1,1,\"", ": "+dt.getDate(),
                    "\"\n",
                    "TEXT 65,"+(y+210)+",\"2\",0,1,1,\"", "Receipt No",
                    "\"\n",
                    "TEXT 250,"+(y+210)+",\"2\",0,1,1,\"", ": "+receiptno,
                    "\"\n",
                    "TEXT 65,"+(y+230)+",\"2\",0,1,1,\"", "RATION NO",
                    "\"\n","TEXT 250,"+(y+230)+",\"2\",0,1,1,\"", ": "+RationNo,
                    "\"\n",
                    "BAR 15,"+(y+250)+",500,3\n", "\"\n",
                    "TEXT 250,"+(y+260)+",\"2\",0,1,1,\"", "ITEMS".getBytes(),
                    "\"\n",
                    "TEXT 65,360,\"2\",0,1,1,\"", "Name".getBytes(),
                    "\"\n",
                    "TEXT 280,360,\"2\",0,1,1,\"", "Qty".getBytes(),
                    "\"\n",
                    "TEXT 420,360,\"2\",0,1,1,\"", "Value".getBytes(),
                    "\"\n"


            ));

            y=370;
            for (int i = 0; i < receipts.size(); i++) {
                Receipt receipt = receipts.get(i);
                boolean hasSpace=false;
                String name=receipt.name;
                String parts[];
                if(receipt.name.contains(" ")){
                    hasSpace=true;
                    int is= receipt.name.indexOf(" ");
                    int l= receipt.name.length();
                    // name=receipt.name.substring(is+1,l);
                    //receipt.name=receipt.name.substring(is);
                    //Log.i("name1",receipt.name);
                    //Log.i("name1",name);
                    name=receipt.name.split(" ",2)[0];
                }
                y = y + 60;
                commands.addAll(Arrays.asList(
                        "TEXT " + x + "," + y + ",\"2\",0,1,1,\"",
                        padRight(name + "", 15) + padRight("" + receipt.qty + "", 8) + padLeft("" + receipt.val + " THB", 5),
                        "\"\n"
                ));
                if(hasSpace){
                    y=y+30;
                    commands.addAll(Arrays.asList(
                            "TEXT " + x + "," + y + ",\"2\",0,1,1,\"",
                            receipt.name.split(" ",2)[1],
                            "\"\n"
                    ));
                }
                y=y+30;
                commands.addAll(Arrays.asList(
                        "TEXT " + x + "," + y + ",\"2\",0,1,1,\"",
                        receipt.uom,
                        "\"\n"
                ));
            }        //padRight(receipt.name + "", 15) + padRight("" + receipt.qty + "", 8) + padLeft("" + receipt.val + "", 7),
            y = y + 30;
            commands.addAll(Arrays.asList(
                    "BAR 15," + (y) + ",500,3\n",
                    "TEXT 65,"+(y+20)+",\"2\",0,1,1,\"", "OPENING BALANCE",
                    "\"\n",
                    "TEXT 320,"+(y+20)+",\"2\",0,1,1,\"", ": "+openingbal+" THB",
                    "\"\n",
                    "TEXT 65,"+(y+70)+",\"2\",0,1,1,\"", "TRANSACTION VALUE",
                    "\"\n",
                    "TEXT 320,"+(y+70)+",\"2\",0,1,1,\"", ": "+charges+" THB",
                    "\"\n",
                    "TEXT 65,"+(y+120)+",\"2\",0,1,1,\"", "CLOSING BALANCE",
                    "\"\n",
                    "TEXT 320,"+(y+120)+",\"2\",0,1,1,\"", ": "+bal+" THB",
                    "\"\n",
                    "BAR 15," + (y + 150) + ",500,3\n",
                    "\"\n",
                    "TEXT 100,"+(y+170)+",\"2\",0,1,1,\"", "powered by Compulynx",
                    "\"\n"

            ));

            Log.i("length111"," "+commands.size());
            int setLen= (int)(commands.size() *(2));
            tscDll.setup(70, setLen-100, 4, 4, 0, 0, 0);
            try {
                for (Object cmd : commands) {
                    if (cmd instanceof String)
                        tscDll.sendcommand(cmd.toString());

                    if (cmd instanceof byte[]) {
                        tscDll.sendcommand((byte[]) cmd);
                    }
                }
            } catch (Exception ex) {
                ex.printStackTrace();
                new SweetAlertDialog(context, SweetAlertDialog.ERROR_TYPE)
                        .setTitleText(context.getString(R.string.Sorry))
                        .setContentText(context.getString(R.string.printerror))
                        .setConfirmText(context.getString(R.string.Ok))
                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sDialog) {
                                sDialog.dismissWithAnimation();

                            }
                        })
                        .show();
                 return  -1;
            }

            tscDll.status();
            tscDll.sendcommand("PRINT 1\n");
            tscDll.sendcommand("SET TEAR ON\n");


        } catch (Exception ex) {
            ex.printStackTrace();
             return -1;
        }
        Log.d("bluetooth print", tscDll.status());
        tscDll.closeport();
         return  0;
    }
    public int  printMerReceipt(Details dt,List<Receipt> receipts,String charges, String bal,String RationNo,Context context,String openingbal,String vid,String receiptno){
        String devAddress = BlueTooth.getAddress("BT-SPP");
        if (devAddress == null) {
            new SweetAlertDialog(context, SweetAlertDialog.ERROR_TYPE)
                    .setTitleText(context.getString(R.string.Sorry))
                    .setContentText(context.getString(R.string.printernotpaired))
                    .setConfirmText(context.getString(R.string.Ok))
                    .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                        @Override
                        public void onClick(SweetAlertDialog sDialog) {
                            sDialog.dismissWithAnimation();

                        }
                    })
                    .show();
            return -1;
        }

        TSCActivity tscDll = new TSCActivity();
        tscDll.openport(devAddress);
        // tscDll.nobackfeed();
        // tscDll.setup(70, 170, 4, 4, 0, 0, 0);
        tscDll.clearbuffer();
        try {
            List<Object> commands = new ArrayList<Object>();
            int x = 65;
            y=80;
            Log.i("yyyyy","  "+(y));
            commands.addAll(Arrays.asList(
                    "GAP 0,0\n","SET PRINTKEY OFF\n", "DIRECTION 0\n", "CLS\n",
                    "BAR 15,"+y+",500,3\n",
                    "TEXT 185,"+(y+10)+",\"2\",0,2,2,\"", "T B C".getBytes(),
                    "\"\n",
                    "TEXT 150,"+(y+60)+",\"2\",0,1,1,\"", "Vendor Receipt".getBytes(),
                    "\"\n",
                    "TEXT 65,"+(y+110)+",\"2\",0,1,1,\"", ("User Name").getBytes(),
                    "\"\n",
                    "TEXT 250,"+(y+110)+",\"2\",0,1,1,\"", (": "+dt.getUser()).getBytes(),
                    "\"\n",
                    "TEXT 65,"+(y+130)+",\"2\",0,1,1,\"", ("MAC Id").getBytes(),
                    "\"\n",
                    "TEXT 250,"+(y+130)+",\"2\",0,1,1,\"", (": "+dt.getMac()).getBytes(),
                    "\"\n",
                    "TEXT 65,"+(y+150)+",\"2\",0,1,1,\"", ("Voucher Id").getBytes(),
                    "\"\n",
                    "TEXT 250,"+(y+150)+",\"2\",0,1,1,\"", (": "+vid).getBytes(),
                    "\"\n",
                    "TEXT 65,"+(y+170)+",\"2\",0,1,1,\"", "Time",
                    "\"\n",
                    "TEXT 250,"+(y+170)+",\"2\",0,1,1,\"", ": "+dt.getTime(),
                    "\"\n",
                    "TEXT 65,"+(y+190)+",\"2\",0,1,1,\"", "Date",
                    "\"\n",
                    "TEXT 250,"+(y+190)+",\"2\",0,1,1,\"", ": "+dt.getDate(),
                    "\"\n",
                    "TEXT 65,"+(y+210)+",\"2\",0,1,1,\"", "Receipt No",
                    "\"\n",
                    "TEXT 250,"+(y+210)+",\"2\",0,1,1,\"", ": "+receiptno,
                    "\"\n",
                    "TEXT 65,"+(y+230)+",\"2\",0,1,1,\"", "RATION NO",
                    "\"\n","TEXT 250,"+(y+230)+",\"2\",0,1,1,\"", ": "+RationNo,
                    "\"\n",
                    "BAR 15,"+(y+250)+",500,3\n", "\"\n",
                    "TEXT 250,"+(y+260)+",\"2\",0,1,1,\"", "ITEMS".getBytes(),
                    "\"\n",
                    "TEXT 65,360,\"2\",0,1,1,\"", "Name".getBytes(),
                    "\"\n",
                    "TEXT 280,360,\"2\",0,1,1,\"", "Qty".getBytes(),
                    "\"\n",
                    "TEXT 420,360,\"2\",0,1,1,\"", "Value".getBytes(),
                    "\"\n"


            ));

            y=370;
            for (int i = 0; i < receipts.size(); i++) {
                Receipt receipt = receipts.get(i);
                boolean hasSpace=false;
                String name=receipt.name;
                String parts[];
                if(receipt.name.contains(" ")){
                    hasSpace=true;
                    int is= receipt.name.indexOf(" ");
                    int l= receipt.name.length();
                    // name=receipt.name.substring(is+1,l);
                    //receipt.name=receipt.name.substring(is);
                    //Log.i("name1",receipt.name);
                    //Log.i("name1",name);
                    name=receipt.name.split(" ",2)[0];
                }
                y = y + 60;
                commands.addAll(Arrays.asList(
                        "TEXT " + x + "," + y + ",\"2\",0,1,1,\"",
                        padRight(name + "", 15) + padRight("" + receipt.qty + "", 8) + padLeft("" + receipt.val + " THB", 5),
                        "\"\n"
                ));
                if(hasSpace){
                    y=y+30;
                    commands.addAll(Arrays.asList(
                            "TEXT " + x + "," + y + ",\"2\",0,1,1,\"",
                            receipt.name.split(" ",2)[1],
                            "\"\n"
                    ));
                }
                y=y+30;
                commands.addAll(Arrays.asList(
                        "TEXT " + x + "," + y + ",\"2\",0,1,1,\"",
                        receipt.uom,
                        "\"\n"
                ));
            }        //padRight(receipt.name + "", 15) + padRight("" + receipt.qty + "", 8) + padLeft("" + receipt.val + "", 7),
            y = y + 30;
            commands.addAll(Arrays.asList(
                    "BAR 15," + (y) + ",500,3\n",
                    "TEXT 65,"+(y+20)+",\"2\",0,1,1,\"", "OPENING BALANCE",
                    "\"\n",
                    "TEXT 320,"+(y+20)+",\"2\",0,1,1,\"", ": "+openingbal+" THB",
                    "\"\n",
                    "TEXT 65,"+(y+70)+",\"2\",0,1,1,\"", "TRANSACTION VALUE",
                    "\"\n",
                    "TEXT 320,"+(y+70)+",\"2\",0,1,1,\"", ": "+charges+" THB",
                    "\"\n",
                    "TEXT 65,"+(y+120)+",\"2\",0,1,1,\"", "CLOSING BALANCE",
                    "\"\n",
                    "TEXT 320,"+(y+120)+",\"2\",0,1,1,\"", ": "+bal+" THB",
                    "\"\n",
                    "BAR 15," + (y + 150) + ",500,3\n",
                    "\"\n",
                    "TEXT 100,"+(y+170)+",\"2\",0,1,1,\"", "powered by Compulynx",
                    "\"\n"

            ));

            Log.i("length111"," "+commands.size());
            int setLen= (int)(commands.size() *(2));
            tscDll.setup(70, setLen-100, 4, 4, 0, 0, 0);
            try {
                for (Object cmd : commands) {
                    if (cmd instanceof String)
                        tscDll.sendcommand(cmd.toString());

                    if (cmd instanceof byte[]) {
                        tscDll.sendcommand((byte[]) cmd);
                    }
                }
            } catch (Exception ex) {
                ex.printStackTrace();
                new SweetAlertDialog(context, SweetAlertDialog.ERROR_TYPE)
                        .setTitleText(context.getString(R.string.Sorry))
                        .setContentText(context.getString(R.string.printerror))
                        .setConfirmText(context.getString(R.string.Ok))
                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sDialog) {
                                sDialog.dismissWithAnimation();

                            }
                        })
                        .show();
                   return  -1;
            }

            tscDll.status();
            tscDll.sendcommand("PRINT 1\n");
            tscDll.sendcommand("SET TEAR ON\n");


        } catch (Exception ex) {
            ex.printStackTrace();
             return  -1;
        }
        Log.d("bluetooth print", tscDll.status());
        tscDll.closeport();
        return 0;
    }
    public int printXreport(final Context context, Details dt){
         boolean status=true;

        String username=" ";
        List <Users> list =Users.listAll(Users.class);
         for(Users user:list){
             if(user.getIsloggedin().equalsIgnoreCase("1")){
                 username=user.getUsername();
                  break;
             }
         }
         final MaterialDialog xReportDialogg;
        String devAddress = BlueTooth.getAddress("BT-SPP");
        if (devAddress == null) {
            //materialDialog.dismiss();
            new SweetAlertDialog(context, SweetAlertDialog.ERROR_TYPE)
                    .setTitleText(context.getString(R.string.Sorry))
                    .setContentText(context.getString(R.string.printernotpaired))
                    .setConfirmText(context.getString(R.string.Ok))
                    .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                        @Override
                        public void onClick(SweetAlertDialog sDialog) {
                            sDialog.dismissWithAnimation();

                        }
                    })
                    .show();
            return  1;

        }
        xReportDialogg = new MaterialDialog.Builder(context)
                .title(context.getString(R.string.printingxreport))
                .content(context.getString(R.string.wait))
                .progress(true, 0)
                .progressIndeterminateStyle(false)
                .show();
        TSCActivity tscDll = new TSCActivity();
        tscDll.openport(devAddress);
        tscDll.clearbuffer();
        try {
            List<Object> commands = new ArrayList<Object>();
            int x =65;
            y = y+20;
            commands.addAll(Arrays.asList(
                    "GAP 0,0\n","SET PRINTKEY OFF\n", "DIRECTION 0\n", "CLS\n",
                    "BAR 15,"+y+",500,3\n",
                    "TEXT 185,"+(y+10)+",\"2\",0,2,2,\"", "T B C".getBytes(),
                    "\"\n",
                    "TEXT 65,"+(y+70)+",\"2\",0,1,1,\"", ("User Name").getBytes(),
                    "\"\n",
                    "TEXT 250,"+(y+70)+",\"2\",0,1,1,\"", (": "+username).getBytes(),
                    "\"\n",
                    "TEXT 65,"+(y+90)+",\"2\",0,1,1,\"", ("MAC Id").getBytes(),
                    "\"\n",
                    "TEXT 250,"+(y+90)+",\"2\",0,1,1,\"", (": "+dt.getMac()).getBytes(),
                    "\"\n",
                    "TEXT 65,"+(y+110)+",\"2\",0,1,1,\"", ("Receipt Type").getBytes(),
                    "\"\n",
                    "TEXT 250,"+(y+110)+",\"2\",0,1,1,\"", (": Daily Report").getBytes(),
                    "\"\n",
                    "TEXT 65,"+(y+130)+",\"2\",0,1,1,\"", "Time",
                    "\"\n",
                    "TEXT 250,"+(y+130)+",\"2\",0,1,1,\"", ": "+dt.getTime(),
                    "\"\n",
                    "TEXT 65,"+(y+150)+",\"2\",0,1,1,\"", "Date",
                    "\"\n",
                    "TEXT 250,"+(y+150)+",\"2\",0,1,1,\"", ": "+dt.getDate(),
                    "\"\n",
                    "BAR 15,"+(y+170)+",500,3\n", "\"\n",
                    "TEXT 65,"+(y+190)+",\"2\",0,1,1,\"", ("Transaction Count").getBytes(),
                    "\"\n",
                    "TEXT 300,"+(y+190)+",\"2\",0,1,1,\"", (": "+dt.getTcount()).getBytes(),
                    "\"\n",
                    "TEXT 65,"+(y+210)+",\"2\",0,1,1,\"", ("Void Count").getBytes(),
                    "\"\n",
                    "TEXT 300,"+(y+210)+",\"2\",0,1,1,\"", (": "+dt.getVcount()).getBytes(),
                    "\"\n",
                    "TEXT 65,"+(y+230)+",\"2\",0,1,1,\"", ("Sales Amount").getBytes(),
                    "\"\n",
                    "TEXT 300,"+(y+230)+",\"2\",0,1,1,\"", (": "+dt.getSales()).getBytes(),
                    "\"\n",
                    "TEXT 65,"+(y+250)+",\"2\",0,1,1,\"", ("Voided Amount").getBytes(),
                    "\"\n",
                    "TEXT 300,"+(y+250)+",\"2\",0,1,1,\"", (": "+dt.getvAmount()).getBytes(),
                    "\"\n",
                    "TEXT 65,"+(y+270)+",\"2\",0,1,1,\"", ("Net Sales").getBytes(),
                    "\"\n",
                    "TEXT 300,"+(y+270)+",\"2\",0,1,1,\"", (": "+dt.getNetsales()).getBytes(),
                    "\"\n","\"\n",
                    "BAR 15,"+(y+290)+",500,3\n",
                    "TEXT 100,"+(y+310)+",\"2\",0,1,1,\"", ("Powered By: Compulynx").getBytes(),
                    "\"\n"
            ));
           /* y = y+130;
            for(int i=0; i<receipts.size();i++)
            {
                Receipt receipt = receipts.get(i);
                y= y+30;
                commands.addAll(Arrays.asList(
                        "TEXT "+x+","+y+",\"2\",0,1,1,\"",
                        padRight("ITEM : "+receipt.name+"",15)+padRight(""+receipt.qty+"",8)+padLeft(""+receipt.val+"",7),
                        "\"\n"
                ));
            }*/
            //y=y+30;
            /*commands.addAll(Arrays.asList(
                    "BAR 15,"+(y)+",500,3\n",
                    "TEXT 65,"+(y+20)+",\"2\",0,1,1,\"",
                    (padRight("TOTAL VALUE",15)+padLeft(" "+charges+" TBH",10)).getBytes(),
                    "\"\n",
                    "TEXT 65,"+(y+50)+",\"2\",0,1,1,\"",
                    (padRight("BALANCE",15)+padLeft(" "+bal+" TBH",10)).getBytes(),
                    "\"\n",
                    "BAR 15,"+(y+80)+",500,3\n",
                    "\"\n"
            ));*/
          // tscDll.downloadpcx("TBC");
            tscDll.setup(70, commands.size()*2-80, 4, 4, 0, 0, 0);
            try {
                for (Object cmd : commands) {
                    if (cmd instanceof String)
                        tscDll.sendcommand(cmd.toString());

                    if (cmd instanceof byte[]) {
                        tscDll.sendcommand((byte[]) cmd);
                    }
                }
            }
            catch(Exception ex)
            {

                xReportDialogg.dismiss();
                ex.printStackTrace();
                status=false;
                new SweetAlertDialog(context, SweetAlertDialog.ERROR_TYPE)
                        .setTitleText(context.getString(R.string.Sorry))
                        .setContentText(context.getString(R.string.printerror))
                        .setConfirmText(context.getString(R.string.Ok))
                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sDialog) {
                                sDialog.dismissWithAnimation();
                                xReportDialogg.dismiss();

                            }
                        })
                        .show();
            }
            tscDll.status();
            tscDll.sendcommand("PRINT 1\n");
            tscDll.sendcommand("SET TEAR ON\n");

        } catch (Exception ex) {
            ex.printStackTrace();
            status=false;
            xReportDialogg.dismiss();
            new SweetAlertDialog(context, SweetAlertDialog.ERROR_TYPE)
                    .setTitleText(context.getString(R.string.Sorry))
                    .setContentText(context.getString(R.string.printerror))
                    .setConfirmText(context.getString(R.string.Ok))
                    .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                        @Override
                        public void onClick(SweetAlertDialog sDialog) {
                            sDialog.dismissWithAnimation();

                        }
                    })
                    .show();

        }
        Log.d("bluetooth print",tscDll.status());

        tscDll.closeport();
        xReportDialogg.dismiss();
         new SweetAlertDialog(context, SweetAlertDialog.SUCCESS_TYPE)
                .setTitleText("Printed")
                .setContentText("successfully.")
                .setConfirmText(context.getString(R.string.Ok))
                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sDialog) {
                        sDialog.dismissWithAnimation();
                        Intent intent=new Intent(context,ProgrammeActivity.class);
                        context.startActivity(intent);
                       // context.this.finish();
                    }
                })
                .show();
         return 1;


    }

    public static String padRight(String s, int n) {
        return String.format("%1$-" + n + "s", s);
    }

    public static String padLeft(String s, int n) {
        return String.format("%1$" + n + "s", s);
    }

    public String split(String str){
        int l=str.indexOf(" ");
        return  str.substring(l+1);
    }


}
