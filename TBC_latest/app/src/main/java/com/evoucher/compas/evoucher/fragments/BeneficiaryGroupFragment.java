package com.evoucher.compas.evoucher.fragments;

import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.evoucher.compas.evoucher.Adapters.ScrollerViewPager;
import com.evoucher.compas.evoucher.R;
import com.lynx.evoucher.models.Member;
import com.rengwuxian.materialedittext.MaterialEditText;
import com.rey.material.widget.RadioButton;
import com.rey.material.widget.Spinner;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;
import java.util.Calendar;
import cn.pedant.SweetAlert.SweetAlertDialog;

/**
 * Created by Kibet on 9/3/2016.
 */
public class BeneficiaryGroupFragment extends Fragment implements View.OnClickListener {
  //  TextView txtHHSize,txtBgId,txtProgId,txtTitle,txtMobile;
    int n = Color.parseColor("#212121");
    private int bgRes;
    ImageView next,prev;
    ScrollerViewPager viewPager;
    Spinner spinnerBgId,spinnerProgId;
    private ImageView imageView;
    MaterialEditText houseHoldSize,title,mobile;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.beneficiary_group_fragment, container, false);

    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        houseHoldSize = ( MaterialEditText) getView().findViewById(R.id.houseHoldSize);
        /* txtBgId = (TextView) getView().findViewById(R.id.txtBgGroup);
        txtHHSize = (TextView) getView().findViewById(R.id.txtHHSize);
        txtProgId = (TextView) getView().findViewById(R.id.txtPrgId);
        txtTitle = (TextView) getView().findViewById(R.id.txtTitle);
        txtMobile = (TextView) getView().findViewById(R.id.txtMobile);*/


        title = ( MaterialEditText) getView().findViewById(R.id.title);
        mobile = ( MaterialEditText) getView().findViewById(R.id.mobile);

        next = (ImageView)getView().findViewById(R.id.next);
        prev = (ImageView)getView().findViewById(R.id.prev);
        next.setOnClickListener(this);
        prev.setOnClickListener(this);

       // error = (ImageView) getView().findViewById(R.id.error);
        viewPager = (ScrollerViewPager) getActivity().findViewById(R.id.view_pager);
        Typeface typeface1 = Typeface.createFromAsset(getActivity().getAssets(), "Roboto-Regular.ttf");

        android.widget.Spinner spinnerBgId = (android.widget.Spinner)getView(). findViewById(R.id.beneficiaryGroup);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(getActivity(),
                R.array.bgs_array, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerBgId.setAdapter(adapter);

        android.widget.Spinner spinnerProgId = (android.widget.Spinner)getView(). findViewById(R.id.programme);
        ArrayAdapter<CharSequence> adapter2 = ArrayAdapter.createFromResource(getActivity(),
                R.array.programs_array, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerProgId.setAdapter(adapter2);

     /*   Typeface typeface = Typeface.createFromAsset(getActivity().getAssets(), "Roboto-Thin.ttf");
        title.setTypeface(typeface1);
        houseHoldSize.setTypeface(typeface1);
        txtBgId.setTypeface(typeface1);
        txtTitle.setTypeface(typeface1);
        txtMobile.setTypeface(typeface1);
        txtHHSize.setTypeface(typeface1);
        txtProgId.setTypeface(typeface1);
        txtTitle.setText("Title"+Html.fromHtml("<sup>*</sup>"));
        txtMobile.setText("Mobile"+Html.fromHtml("<sup>*</sup>"));
        txtHHSize.setText("Lawyer code"+Html.fromHtml("<sup>*</sup>"));*/



    }
    public void showAlert(){
        Log.d("ALERT","ALERT");
        new SweetAlertDialog(getActivity(), SweetAlertDialog.WARNING_TYPE)
                .setTitleText("Empty fields!!")
                .setContentText("Please ensure that all fields are correctly filled")
                .setConfirmText("Ok")
                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sDialog) {
                        sDialog.dismissWithAnimation();

                    }
                })
                .show();
    }


    @Override
    public void onClick(View v) {

        if(v.getId() == R.id.next){
            if((title.getText().toString().equalsIgnoreCase("")==false)
                    &&(houseHoldSize.getText().toString().equalsIgnoreCase("")==false)&&(mobile.getText().toString().equalsIgnoreCase("")==false)){
                Member.familysize=houseHoldSize.getText().toString();
                Member.title=title.getText().toString();
                 Member.programmeid="1";
                Member.beneficiary_group_id="1";
                Member.mobile = mobile.getText().toString();
                viewPager.setCurrentItem(2, true);
            }else{
                showAlert();
            }
        }else if(v.getId() == R.id.prev) {
            viewPager.setCurrentItem(0, true);
        }

    }

}