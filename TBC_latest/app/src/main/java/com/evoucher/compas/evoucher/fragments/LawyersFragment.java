package com.evoucher.compas.evoucher.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.evoucher.compas.evoucher.Adapters.ProgrammesAdapter;
import com.evoucher.compas.evoucher.Adapters.ReportsAdapter;
import com.evoucher.compas.evoucher.BeneficiaryList;
import com.evoucher.compas.evoucher.Details;
import com.evoucher.compas.evoucher.ItemClickListener;
import com.evoucher.compas.evoucher.R;
import com.lynx.evoucher.models.Programmes;
import com.lynx.evoucher.models.Reports;

import java.util.ArrayList;
import java.util.List;

import jp.wasabeef.recyclerview.adapters.AlphaInAnimationAdapter;

/**
 * Created by Administrator on 10/29/2016.
 */
public class LawyersFragment extends Fragment implements ItemClickListener {
    RecyclerView recyclerView;
    List<Reports> reports;
    Details dt=new Details(getActivity());
    List<Programmes> programmes;
    ArrayList<String> programIDs=new ArrayList<String>();
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        return inflater.inflate(R.layout.xreport_fragment, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
       /* programmes= Programmes.find(Programmes.class, "progtype =?","IDN");
        recyclerView = (RecyclerView)getView().findViewById(R.id.recycler_vieww0);
        recyclerView.setLayoutManager(new StaggeredGridLayoutManager(3, StaggeredGridLayoutManager.VERTICAL));
        ProgrammesAdapter adapter=new ProgrammesAdapter(getActivity(),programmes);
        adapter.setClickListener(this);
        AlphaInAnimationAdapter alphaAdapter = new AlphaInAnimationAdapter(adapter);
        alphaAdapter.setDuration(1500);
        recyclerView.setAdapter(alphaAdapter);*/
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    public void onClick(View view, int pos) {
        dt.setProgrammeid(String.valueOf(pos));
        Log.d("###PROGRAMMEID",dt.getProgrammeid());
        startActivity(new Intent(getActivity(),BeneficiaryList.class));
        getActivity().finish();

    }
}
