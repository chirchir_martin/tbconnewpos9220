package com.evoucher.compas.evoucher;
import android.content.res.Configuration;
import com.lynx.evoucher.models.Programmes;
import com.orm.SugarApp;
import com.orm.SugarContext;

/**
 * Created by Kibet on 9/2/2016.
 */

/**
 * Fixes the issue of Exception finding tables when app is instantiated
 *
 * ref
 * http://stackoverflow.com/questions/33031570/android-sugar-orm-no-such-table-exception
 */
public class Application extends SugarApp {

//    @Override
//    public void onConfigurationChanged(Configuration configuration){
//        super.onConfigurationChanged(configuration);
//    }

    @Override
    public void onCreate(){
        super.onCreate();
//        SugarContext.init(getApplicationContext());
//        Programmes.findById(Programmes.class,(long) 1);
    }
//
//    @Override
//    public void onLowMemory(){
//        super.onLowMemory();
//    }
//
//    @Override
//    public void onTerminate(){
//        super.onTerminate();
//    }
}
