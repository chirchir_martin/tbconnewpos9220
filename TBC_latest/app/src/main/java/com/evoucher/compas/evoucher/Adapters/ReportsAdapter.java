package com.evoucher.compas.evoucher.Adapters;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.amulyakhare.textdrawable.TextDrawable;
import com.amulyakhare.textdrawable.util.ColorGenerator;
import com.evoucher.compas.evoucher.ItemClickListener;
import com.lynx.evoucher.models.Beneficiary;
import com.lynx.evoucher.models.Programmes;
import com.evoucher.compas.evoucher.R;
import com.lynx.evoucher.models.Reports;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Administrator on 8/17/2016.
 */
public class ReportsAdapter extends RecyclerView.Adapter<ReportsAdapter.MyViewHolder> {
    private ItemClickListener clickListener;
    private List<Programmes> programmes;
    ArrayList<String> programIDs=new ArrayList<>();
    Context context;
    String tag;
    public TextView  d;



    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        public TextView title,enroll, veify;
        ImageView im;
        CardView card;

        public MyViewHolder(View view) {
            super(view);

            im=(ImageView)view.findViewById(R.id.main);
            title=(TextView)view.findViewById(R.id.title);
          veify=(TextView)view.findViewById(R.id.verify);
            enroll=(TextView)view.findViewById(R.id.enroll);
            card=(CardView)view.findViewById(R.id.cardview);


            card.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if (clickListener != null)
                tag=view.getTag(R.id.holder).toString();
            clickListener.onClick(view,Integer.parseInt(tag));
            Log.d("#####CLICK1",view.getTag(R.id.holder).toString());

        }
    }


    public ReportsAdapter(Context context, ArrayList<String> programmesids) {

        this.programIDs = programmesids;
        this.context=context;

    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.reports_row, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {

        Typeface typeface = Typeface.createFromAsset(context.getAssets(), "Roboto-Thin.ttf");
        holder.title.setTypeface(typeface);
        // holder.price.setTypeface(typeface);
        ColorGenerator generator = ColorGenerator.DEFAULT;
        holder.card.setTag(R.id.holder,programIDs.get(position));
        System.out.println("##PROGRAM.GET");
        int color = generator.getColor(position);
        List<Programmes> programe=Programmes.find(Programmes.class, "programid = ?",programIDs.get(position));
        if(programe.get(0).getProgrammename().length()>3){
            String lettersForName =programe.get(0).getProgrammename().substring(0, 2);


            TextDrawable letterDrawable = TextDrawable.builder()
                    .buildRound(lettersForName, color);

            holder.im.setImageDrawable(letterDrawable);
        }else{
            TextDrawable letterDrawable = TextDrawable.builder()
                    .buildRound(programe.get(0).getProgrammename(), color);

            holder.im.setImageDrawable(letterDrawable);
        }
        holder.title.setText(programe.get(0).getProgrammename());
        List<Reports> reps=Reports.find(Reports.class, "programmeid = ?",programIDs.get(position));
        if(reps.size()!=0){
            holder.veify.setText("Total Verification: "+String.valueOf(reps.size()));
        }
        List<Beneficiary> ben=Beneficiary.find(Beneficiary.class, "programmeid = ?",programIDs.get(position));
        if(ben.size()!=0){
            holder.enroll.setText("Total Enrollment: "+String.valueOf(ben.size()));
        }
    }

    @Override
    public int getItemCount() {
        return programIDs.size();
    }
    public void setClickListener(ItemClickListener itemClickListener) {

        this.clickListener = itemClickListener;
    }

}