package com.evoucher.compas.evoucher;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;

import com.lynx.evoucher.models.Beneficiary;

public class LawyerDetails extends AppCompatActivity {
TextView name,gender,dob,idno;
    Beneficiary ben;
    ImageView image;
    Bitmap decodedByte=null;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lawyer_details);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Lawyer Details");
        overridePendingTransition(0, 0);
        String id=getIntent().getStringExtra("id");
        ben=Beneficiary.findById(Beneficiary.class,Long.parseLong(id));
        Log.d("####Ben",String.valueOf(ben==null));
        image=(ImageView)findViewById(R.id.userimage);
        name=(TextView)findViewById(R.id.name);
       gender=(TextView)findViewById(R.id.gender);
       dob=(TextView)findViewById(R.id.dob);
       idno=(TextView)findViewById(R.id.idno);
      //  image.setImageResource(ben.getUser_image());
         String pic="";
        try {
            pic = ben.getUser_image();
        }catch(Exception e){

        }
        Bitmap decodedByte=null;
        if(pic!=null) {
            byte[] decodedString = Base64.decode(pic, Base64.DEFAULT);
            decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
            image.setImageBitmap(decodedByte);
        }
        name.setText("Name:  "+ben.getFirstname().toUpperCase()+" "+ben.getMiddlename().toUpperCase()+" "+ben.getLastname().toUpperCase());
        gender.setText("Gender:  "+ben.getGender());
        dob.setText("Dob:  "+ben.getDate_of_birth());
        idno.setText("Id:  "+ben.getNational_id());
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.lawyerdetails_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if(id == R.id.logout){
            startActivity(new Intent(LawyerDetails.this,UsernameActivity.class));
            finish();
            return true;
        }

        if(id == android.R.id.home){
            startActivity(new Intent(LawyerDetails.this,ProgrammeActivity.class));
             finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
    @Override
    public void onBackPressed() {

        Intent intent1 = new Intent(this, ProgrammeActivity.class);
        startActivity(intent1);
        finish();


    }
}
