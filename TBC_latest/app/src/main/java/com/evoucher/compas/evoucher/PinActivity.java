package com.evoucher.compas.evoucher;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Vibrator;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.lynx.evoucher.models.Programmes;
import com.lynx.evoucher.models.Users;

import java.util.List;

import cn.pedant.SweetAlert.SweetAlertDialog;

public class PinActivity extends AppCompatActivity {

    String userEntered = "";
    String userPin = "8888";

    final int PIN_LENGTH = 4;
    boolean keyPadLockedFlag = false;
    Context appContext;
    TextView titleView;
    String result,pinerror;
    ImageView next;
    TextView statusView,user;
    Typeface typeface,typeface1;
    Button button0;
    Button button1;
    Button button2;
    Button button3;
    Button button4;
    Button button5;
    Button button6;
    Button button7;
    Button button8;
    Button button9;
    Button button10;
    Button buttonExit;
    Button buttonDelete;
    EditText passwordInput;
    ImageView backSpace;
    Vibrator vibe;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        appContext = this;
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_pin);
        overridePendingTransition(0, 0);
        typeface = Typeface.createFromAsset(getAssets(), "Roboto-Regular.ttf");
        typeface1 = Typeface.createFromAsset(getAssets(), "font0.otf");
        //statusView = (TextView) findViewById(R.id.statusview);
       // user = (TextView) findViewById(R.id.user);
        //statusView.setTypeface(typeface1);
      // user.setTypeface(typeface1);
        passwordInput = (EditText) findViewById(R.id.editText);


        vibe = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
        View.OnTouchListener otl = new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                return true;
            }
        };
        passwordInput.setOnTouchListener(otl);

        buttonExit = (Button) findViewById(R.id.buttonOk);
       /* backSpace.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                passwordInput.setText(passwordInput.getText().toString().substring(0,passwordInput.getText().toString().length()-2));
            }
        });*/
        buttonExit.setOnClickListener(new View.OnClickListener() {
                                          public void onClick(View v) {
                                              try {
                                                  vibe.vibrate(20);
                                                  Verify(passwordInput.getText().toString());
                                              } catch (Exception e) {

                                              }
                                              //Exit app
                                           /*   Intent i = new Intent();
                                              i.setAction(Intent.ACTION_MAIN);
                                              i.addCategory(Intent.CATEGORY_HOME);
                                              appContext.startActivity(i);
                                              finish();*/
                                             /* if (userEntered.equals(userPin))
                                              {
                                                  statusView.setTextColor(Color.GREEN);
                                                  statusView.setText("Correct");
                                                  Log.v("PinView", "Correct PIN");
                                                  //finish();
                                              }
                                              else
                                              {
                                                  statusView.setTextColor(Color.RED);
                                                  statusView.setText("Password is incorrect.Please try again.");
                                                   passwordInput.setBackgroundResource(R.drawable.edittextstyle_error);
                                                  passwordInput.setTextColor(Color.RED);
                                                  Log.v("PinView", "Wrong PIN");

                                                  // new LockKeyPadOperation().execute("");
                                              }*/
                                          }

                                      }
        );
        //buttonExit.setTypeface(xpressive);


        buttonDelete = (Button) findViewById(R.id.buttonDeleteBack);
        buttonDelete.setOnClickListener(new View.OnClickListener() {
                                            public void onClick(View v) {
                                                vibe.vibrate(20);
                                                Log.d("OOGDOGODOGD", String.valueOf(userEntered.length()));
                                                if (userEntered.length() > 0) {
                                                    userEntered = userEntered.substring(0, userEntered.length() - 1);
                                                    passwordInput.setText(userEntered);
                                                    //statusView.setTextColor(Color.GRAY);
                                                   // statusView.setText("Enter Pin.");
                                                    //passwordInput.setBackgroundResource(R.drawable.edittextstyle0);
                                                    passwordInput.setTextColor(Color.BLACK);

                                                }

                                            }

                                        }
        );
        buttonDelete.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                vibe.vibrate(20);
                passwordInput.setText("");
                userEntered = "";
               // statusView.setTextColor(Color.GRAY);
               // statusView.setText("Enter Pin.");
                //passwordInput.setBackgroundResource(R.drawable.edittextstyle0);
                passwordInput.setTextColor(Color.BLACK);
                return true;
            }
        });
        titleView = (TextView) findViewById(R.id.time);
        //titleView.setTypeface(xpressive);


        View.OnClickListener pinButtonHandler = new View.OnClickListener(){
            public void onClick(View v) {
                vibe.vibrate(20);
                Button pressedButton = (Button) v;
                userEntered = userEntered + pressedButton.getText();
                Log.v("PinView", "User entered=" + userEntered.length());
                //Update pin boxes
                passwordInput.setText(passwordInput.getText().toString() + "*");
                passwordInput.setSelection(passwordInput.getText().toString().length());
            }
        };


        button0 = (Button) findViewById(R.id.button0);
       // button0.setBackgroundColor(getResources().getColor(R.color.accent));

        //button0.setTypeface(typeface);
        button0.setOnClickListener(pinButtonHandler);

        button1 = (Button) findViewById(R.id.button1);
       // button1.setTypeface(typeface);
        button1.setOnClickListener(pinButtonHandler);

        button2 = (Button) findViewById(R.id.button2);
        button2.setTypeface(typeface);
        button2.setOnClickListener(pinButtonHandler);


        button3 = (Button) findViewById(R.id.button3);
       // button3.setTypeface(typeface);
        button3.setOnClickListener(pinButtonHandler);

        button4 = (Button) findViewById(R.id.button4);
        button4.setTypeface(typeface);
        button4.setOnClickListener(pinButtonHandler);

        button5 = (Button) findViewById(R.id.button5);
        //button5.setTypeface(typeface);
        button5.setOnClickListener(pinButtonHandler);

        button6 = (Button) findViewById(R.id.button6);
      //  button6.setTypeface(typeface);
        button6.setOnClickListener(pinButtonHandler);

        button7 = (Button) findViewById(R.id.button7);
       // button7.setTypeface(typeface);
        button7.setOnClickListener(pinButtonHandler);

        button8 = (Button) findViewById(R.id.button8);
       // button8.setTypeface(typeface);
        button8.setOnClickListener(pinButtonHandler);

        button9 = (Button) findViewById(R.id.button9);
        //button9.setTypeface(typeface);
        button9.setOnClickListener(pinButtonHandler);


        buttonDelete = (Button) findViewById(R.id.buttonDeleteBack);


    }
    @Override
    public void onBackPressed() {
        // TODO Auto-generated method stub
        Intent intent1 = new Intent(this, UsernameActivity.class);
        startActivity(intent1);
        finish();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        //getMenuInflater().inflate(R.menu.activity_pin_entry_view, menu);
        return true;
    }


    public void Verify(String password) throws Exception {
        Log.d("sdsdsdsds", userEntered);
        final String PREFS_NAME = "MyPrefsFile";
        SharedPreferences settings = getSharedPreferences(PREFS_NAME, 0);
        if (settings.getBoolean("fistTime", true)) {
            if (userEntered.equalsIgnoreCase("1234")) {
                Log.d("SWERRRTT","ETETGET");
                startActivity(new Intent(PinActivity.this, SettingsActivity.class));
                finish();
            } else {
                //Toast.makeText(PinActivity.this, "INVALID", Toast.LENGTH_LONG).show();
                showAlert(getString(R.string.Errorr),getString(R.string.InValidPin));
                passwordInput.setTextColor(Color.RED);
            }
        }else{

            List<Users> user = Users.listAll(Users.class);
            if (user.size() != 0) {
                for (Users ur : user) {
                    if (userEntered.equalsIgnoreCase(ur.getPassword())) {
                        startActivity(new Intent(PinActivity.this, LoadingActivity.class));
                        finish();
                        pinerror="";
                        break;
                    } else {
                        //Toast.makeText(PinActivity.this, "INVALID", Toast.LENGTH_LONG).show();
                      //  showAlert("Error","The username you entered is invalid.Please try again.");
                        pinerror="1";
                    }

                }
                if(pinerror.equalsIgnoreCase("1")){
                    showAlert(getString(R.string.Errorr),getString(R.string.InvalidUserName));
                }

            } else {
                //Toast.makeText(PinActivity.this, "NO USERS FOUND", Toast.LENGTH_LONG).show();
                showAlert(getString(R.string.Errorr),getString(R.string.NoUserFound));
            }
        }
    }
    public void showAlert(String title, String message){
        Log.d("ALERT","ALERT");
        new SweetAlertDialog(PinActivity.this, SweetAlertDialog.WARNING_TYPE)
                .setTitleText(title)
                .setContentText(message)
                .setConfirmText("Ok")
                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sDialog) {
                        sDialog.dismissWithAnimation();

                    }
                })
                .show();
    }

}

