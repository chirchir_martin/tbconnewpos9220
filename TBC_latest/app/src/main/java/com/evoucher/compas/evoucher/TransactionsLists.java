package com.evoucher.compas.evoucher;

import android.content.Context;
import android.content.Intent;
import android.icu.util.Calendar;
import android.os.Bundle;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import com.evoucher.compas.evoucher.R;

import com.evoucher.compas.evoucher.fragments.XreportFragment;
import com.lynx.evoucher.models.Transactions;
import com.lynx.evoucher.models.TransactionsListProducts;
import com.orm.util.NamingHelper;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;
import com.wdullaer.materialdatetimepicker.time.RadialPickerLayout;
import com.wdullaer.materialdatetimepicker.time.TimePickerDialog;


import org.json.JSONArray;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class TransactionsLists extends AppCompatActivity implements
        DatePickerDialog.OnDateSetListener,TimePickerDialog.OnTimeSetListener
{

    SimpleDateFormat date, date1;
    TextView DATE;

    ListView listView;
    MyCustomAdapter dataAdapter = null;
    String currentDate, selectedDate;

    List<TransactionsListProducts> dateTransactions ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_transactions_lists);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Transactions");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        listView = (ListView) findViewById(R.id.transaction_listview);
        DATE =(TextView)findViewById(R.id.date);

        displayListView();



    }



    @Override
    public void onResume() {
        super.onResume();
        DatePickerDialog dpd = (DatePickerDialog) getFragmentManager().findFragmentByTag("Datepickerdialog");
        if(dpd != null) dpd.setOnDateSetListener(this);
    }

    private void displayListView() {
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");


        currentDate = df.format(new Date());
        DATE.setText(currentDate);

        List<TransactionsListProducts> transaction= TransactionsListProducts.find(TransactionsListProducts.class, null, null, null, "id DESC", "1");

        //check whether the db is empty
        if(transaction.size()>0){

        }

        List<TransactionsListProducts> fetchedTransactions ;
        fetchedTransactions = TransactionsListProducts.find(TransactionsListProducts.class, "transactiondate = ?", currentDate);
        dataAdapter = new MyCustomAdapter(this,
                R.layout.transactions_item, fetchedTransactions);
        // Assign adapter to ListView
        listView.setAdapter(dataAdapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
            }
        });

    }



    private void displayListViewUsingDate() {

        dataAdapter = new MyCustomAdapter(this,
                R.layout.transactions_item, dateTransactions);
        // Assign adapter to ListView
        listView.setAdapter(dataAdapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
            }
        });

    }

    @Override
    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {

        date = new SimpleDateFormat("yyyy-MM-dd");
        date1 = new SimpleDateFormat("yyyy-M-dd");


        //selecteddate = (++monthOfYear)+ "/" +dayOfMonth   + "/" + year;
        final  String datee = ( year)+ "-" + (++monthOfYear)   + "-" +dayOfMonth;
        try {
            selectedDate =date.format(date1.parse(datee));


        }catch (Exception e){
            Log.i("#####selecteddateerror", e.toString());

        }
        DATE.setText(selectedDate);

        dateTransactions = TransactionsListProducts.find(TransactionsListProducts.class, "transactiondate = ?", selectedDate);

        displayListViewUsingDate();
    }

    @Override
    public void onTimeSet(RadialPickerLayout view, int hourOfDay, int minute) {

    }



    private class MyCustomAdapter extends ArrayAdapter<TransactionsListProducts> {

        private List<TransactionsListProducts> transactionList;

        public MyCustomAdapter(Context context, int textViewResourceId,
                               List<TransactionsListProducts> transactionList) {
            super(context, textViewResourceId, transactionList);
            this.transactionList = transactionList;

        }

        private class ViewHolder {

            TextView Product_;
            TextView UnitOfMeasure_;
            TextView Quantity_;
            TextView Value_;


        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            ViewHolder holder = null;
            Log.v("ConvertView", String.valueOf(position));

            if (convertView == null) {
                LayoutInflater vi = (LayoutInflater) getSystemService(
                        Context.LAYOUT_INFLATER_SERVICE);
                convertView = vi.inflate(R.layout.transactions_item, null);

                holder = new ViewHolder();
                //holder.code = (TextView) convertView.findViewById(R.id.code);
                holder.Product_ = (TextView) convertView.findViewById(R.id.product_);
                holder.UnitOfMeasure_ = (TextView) convertView.findViewById(R.id.uom_);
                holder.Quantity_ = (TextView) convertView.findViewById(R.id.quantity_);
                holder.Value_ = (TextView) convertView.findViewById(R.id.value_);


                convertView.setTag(holder);

            } else {
                holder = (ViewHolder) convertView.getTag();
            }

            TransactionsListProducts trsLists = transactionList.get(position);
            //holder.code.setText(" (" +  country.getCode() + ")");

            holder.Product_.setText(trsLists.getProductname());
            holder.UnitOfMeasure_.setText(trsLists.getUnitofmeasure());
            holder.Quantity_.setText(trsLists.getQuantity());
            holder.Value_.setText(trsLists.getVal());


            return convertView;

        }

    }

    @Override
    public void onBackPressed() {
        Intent intent=new Intent(getApplicationContext(), ProgrammeActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.transaction_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.pickdate) {
            java.util.Calendar now = java.util.Calendar.getInstance();
            DatePickerDialog dpd = DatePickerDialog.newInstance(
                    TransactionsLists.this,
                    now.get(java.util.Calendar.YEAR),
                    now.get(java.util.Calendar.MONTH),
                    now.get(java.util.Calendar.DAY_OF_MONTH)
            );

            dpd.show(this.getFragmentManager(), "Datepickerdialog");


            return true;
        }else if (id == android.R.id.home) {
            Intent intent1 = new Intent(this, ProgrammeActivity.class);
            startActivity(intent1);
            finish();

        }

        return super.onOptionsItemSelected(item);
    }


}

