package com.evoucher.compas.evoucher;

import android.util.Log;

import java.util.zip.Deflater;
import java.util.zip.Inflater;

/**
 * Created by Chirchir on 1/30/2019.
 */

public class Utils {
    public static byte[] compress(String inputString) {
        String logstr="##compress";
        try {

            byte[] input = inputString.getBytes("UTF-8");
            byte[] output = new byte[input.length];
            Deflater compresser = new Deflater();
            compresser.setInput(input);
            compresser.finish();
            int compressedDataLength = compresser.deflate(output);
            compresser.end();
            return output;
        }
        catch (java.io.UnsupportedEncodingException ex) {
            Log.d(logstr, ex.toString());
            return null;
        }
        catch (Exception ex) {
            Log.d(logstr, ex.toString());
            return null;
        }

    }

    public static String decompress(byte[] input) {
        String logstr="##decompress";
        try {

            Inflater decompresser = new Inflater();
            decompresser.setInput(input, 0, input.length);
            byte[] result = new byte[input.length];
            boolean ret=decompresser.needsInput();
            Log.d("##ret"," needs input "+ret);
            boolean ret1=decompresser.needsDictionary();
            Log.d("##ret"," needs dict" +ret1);
            int resultLength = decompresser.inflate(result);
            decompresser.end();
            String outputString = new String(result, 0, resultLength, "UTF-8");
            return outputString;

        } catch (java.io.UnsupportedEncodingException ex) {
            Log.d(logstr, ex.toString());
            return null;
        } catch (java.util.zip.DataFormatException ex) {
            Log.d(logstr, ex.toString());
            return null;
        }
        catch (Exception ex) {
            Log.d(logstr, ex.toString());
            return null;
        }
    }
}
