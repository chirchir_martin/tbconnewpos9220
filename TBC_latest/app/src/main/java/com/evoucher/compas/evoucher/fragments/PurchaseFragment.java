package com.evoucher.compas.evoucher.fragments;

import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.ColorStateList;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Typeface;
import android.hardware.usb.UsbDevice;
import android.hardware.usb.UsbManager;
import android.nfc.NdefMessage;
import android.nfc.NdefRecord;
import android.nfc.Tag;
import android.nfc.tech.Ndef;
import android.nfc.tech.NdefFormatable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Vibrator;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.evoucher.compas.evoucher.Adapters.ProgrammesAdapter;
import com.evoucher.compas.evoucher.Beneficairy_Activity;
import com.evoucher.compas.evoucher.Card_Details;
import com.evoucher.compas.evoucher.Details;
import com.evoucher.compas.evoucher.ItemClickListener;
import com.evoucher.compas.evoucher.Network;

import com.evoucher.compas.evoucher.R;
import com.evoucher.compas.evoucher.Utils;
import com.evoucher.compas.evoucher.VouchersActivity;
import com.evoucher.compas.evoucher.card.KeyInfoProvider;
import com.evoucher.compas.evoucher.card.SampleAppKeys;
import com.evoucher.compas.evoucher.ui.CustomImageView;
import com.lynx.evoucher.models.Beneficiary;
import com.lynx.evoucher.models.CardBlock;
import com.lynx.evoucher.models.Colors;
import com.lynx.evoucher.models.Configuration;
import com.lynx.evoucher.models.FingerPrintVO;
import com.lynx.evoucher.models.Programmes;
import com.lynx.evoucher.models.Topups;
import com.lynx.evoucher.models.TopupsLogs;
import com.lynx.evoucher.models.Users;
import com.newpos.libpay.device.card.CardInfo;
import com.newpos.libpay.device.card.CardListener;
import com.newpos.libpay.device.card.CardManager;
import com.nxp.nfclib.CardType;
import com.nxp.nfclib.KeyType;
import com.nxp.nfclib.NxpNfcLib;
import com.nxp.nfclib.desfire.DESFireFactory;
import com.nxp.nfclib.desfire.IDESFireEV1;
import com.nxp.nfclib.exceptions.NxpNfcLibException;
import com.nxp.nfclib.interfaces.IKeyData;
import com.rengwuxian.materialedittext.MaterialEditText;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import javax.crypto.Cipher;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.IvParameterSpec;

import SecuGen.FDxSDKPro.JSGFPLib;
import SecuGen.FDxSDKPro.SGAutoOnEventNotifier;
import SecuGen.FDxSDKPro.SGFDxDeviceName;
import SecuGen.FDxSDKPro.SGFDxErrorCode;
import SecuGen.FDxSDKPro.SGFDxSecurityLevel;
import SecuGen.FDxSDKPro.SGFDxTemplateFormat;
import SecuGen.FDxSDKPro.SGFingerInfo;
import cn.com.aratek.fp.FingerprintScanner;
import cn.pedant.SweetAlert.SweetAlertDialog;
import jp.wasabeef.recyclerview.adapters.AlphaInAnimationAdapter;

/**
 * Created by Administrator on 10/24/2016.
 */
public class PurchaseFragment extends Fragment implements ItemClickListener, View.OnClickListener {
    List<Colors> colorslist;
    Vibrator vibe;
    List<Topups> tps;
    String amt = "0";
    ArrayList<String> pin = new ArrayList<String>();
    EditText passwordInput;
    String userEntered = "";
    String rationNo;
    int count = 0;
    Button button0;
    Button button1;
    Button button2;
    Button button3;
    Button button4;
    Button button5;
    Button button6;
    Button button7;
    Button button8;
    Button button9;
    Button button10;
    Button buttonExit;
    Button buttonDelete;
    Button buttonOk;
    private IKeyData objKEY_2KTDES_ULC = null;
    private IKeyData objKEY_2KTDES = null;
    private IKeyData objKEY_AES128 = null;
    private byte[] default_ff_key = null;
    private IKeyData default_zeroes_key = null;
    private NxpNfcLib libInstance = null;
    private IDESFireEV1 desFireEV1;
    private CardType mCardType = CardType.UnknownCard;
    private Cipher cipher = null;
    private byte[] bytesKey = null;
    private IvParameterSpec iv = null;
    private static final String KEY_APP_MASTER = "This is my key  ";
    private static final String ALIAS_KEY_AES128 = "key_aes_128";
    private static final String ALIAS_KEY_2KTDES = "key_2ktdes";
    private static final String ALIAS_KEY_2KTDES_ULC = "key_2ktdes_ulc";
    private static final String ALIAS_DEFAULT_FF = "alias_default_ff";
    private static final String ALIAS_KEY_AES128_ZEROES = "alias_default_00";
    private static final String EXTRA_KEYS_STORED_FLAG = "keys_stored_flag";
    static String packageKey = "b927aab8842ab54cbaf2ea1df9917159";
    MaterialDialog pDialogg;
    Button submitcode, capture;

    byte[] appId = new byte[]{0x1, 0x00, 0x00};
    int timeOut = 2000;
    String msg = "";
    List<Topups> topups;
    MaterialDialog dialog;
    List<Programmes> pgs;
    JSONObject data, pdetails, card_pin, topup;
    String  uidhex;
    ViewGroup menus;
    TextView tap;
    String vouchervalue = "0";
    String ovoucheridno = "";
    String amount = "0";
    int status;
    List<Programmes> programs = new ArrayList<Programmes>();
    ViewGroup codeview, view1, view3, fingerprint, tapview;
    private static RecyclerView recyclerView;
    ArrayList<String> programIDs = new ArrayList<String>();
    Details dt = new Details(getActivity());
    Network net = new Network(getActivity(), "");
    Thread t;
    String bgip;

    JSONArray tups;
    CardManager cardManager;
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        return inflater.inflate(R.layout.purchase_fragment, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        submitcode = (Button) getView().findViewById(R.id.submitcode);
        menus = (ViewGroup) getView().findViewById(R.id.menus);
        menus.setVisibility(View.GONE);
        setUi();
        dt.setValuevoucher("");
        initializeLibrary();
        initializeKeys();
        initializeCipherinitVector();
        super.onViewCreated(view, savedInstanceState);
        pDialogg = new MaterialDialog.Builder(getActivity())
                .title(getString(R.string.searchcard))
                .content(getString(R.string.tapcard))
                .progress(true,0)
                .progressIndeterminateStyle(false)
                .show();
        new Thread(new Runnable() {
            @Override
            public void run() {

                read_nfc_card(com.newpos.libpay.device.card.CardType.INMODE_NFC.getVal()|com.newpos.libpay.device.card.CardType.INMODE_MAG.getVal()|com.newpos.libpay.device.card.CardType.INMODE_IC.getVal(), 10000);

            }
        }).start();

    }


    @Override
    public void onClick(View view, int id) {

        Log.d("#####CLICK", String.valueOf(id));
        dt.setProgrammeid(String.valueOf(id));
        startActivity(new Intent(getActivity(), VouchersActivity.class).putExtra("VOUCHERVALUE", vouchervalue));
        getActivity().finish();

    }

    public void showAlert(String title, String message) {
        Log.d("ALERT", "ALERT");
        new SweetAlertDialog(getActivity(), SweetAlertDialog.WARNING_TYPE)
                .setTitleText(title)
                .setContentText(message)
                .setConfirmText(getString(R.string.Ok))
                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sDialog) {
                        sDialog.dismissWithAnimation();

                    }
                })
                .show();
    }


    @Override
    public void onPause() {
//
        super.onPause();
    }

    @Override
    public void onDestroy() {
        Log.d("###Destroyed", "DESTROYED");
        super.onDestroy();
    }


    public void cardLogic(final Intent intent) {
        CardType type = CardType.UnknownCard;
        try {
            type = libInstance.getCardType(intent);
        } catch (NxpNfcLibException ex) {
            try {
                pDialogg.dismiss();
            } catch (Exception e) {

            }

           /* new SweetAlertDialog(getActivity(), SweetAlertDialog.WARNING_TYPE)
                    .setTitleText(getString(R.string.nfc_error))
                    .setContentText(getString(R.string.nfc_error_totle))
                    .setConfirmText(getString(R.string.Ok))
                    .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                        @Override
                        public void onClick(SweetAlertDialog sDialog) {
                            sDialog.dismissWithAnimation();

                        }
                    })
                    .show();*/
        }

        switch (type) {

            case DESFireEV1: {
                mCardType = CardType.DESFireEV1;
                desFireEV1 = DESFireFactory.getInstance().getDESFire(libInstance.getCustomModules());
                try {

                    desFireEV1.getReader().connect();
                    desFireEV1.getReader().setTimeout(2000);
                    pDialogg = new MaterialDialog.Builder(getActivity())
                            .title(getString(R.string.Readingcard))
                            .content(getString(R.string.DontRemoveCard))
                            .progress(true, 0)
                            .progressIndeterminateStyle(false)
                            .show();
                    new Thread(new Runnable(){
                        @Override
                        public void run() {

                            desfireEV1CardLogic();

                        }
                    }).start();

                } catch (Exception t) {
                    t.printStackTrace();
                    Log.d("##ERRORRR", t.toString());

                }
                break;
            }


        }
    }

    @TargetApi(19)
    private void initializeLibrary() {
        libInstance = NxpNfcLib.getInstance();
        try {
            libInstance.registerActivity(getActivity(), packageKey);
        } catch (NxpNfcLibException ex) {
            // Toast.makeText(this, ex.getMessage(), Toast.LENGTH_SHORT).show();
            try {
                pDialogg.dismiss();
            } catch (Exception e) {
               /* new SweetAlertDialog(getActivity(), SweetAlertDialog.WARNING_TYPE)
                        .setTitleText(getString(R.string.nfc_error))
                        .setContentText(getString(R.string.nfc_error_totle))
                        .setConfirmText(getString(R.string.Ok))
                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sDialog) {
                                sDialog.dismissWithAnimation();

                            }
                        })
                        .show();*/
            }
        }
    }

    /**
     * Initialize the Cipher and init vector of 16 bytes with 0xCD.
     */


    private void desfireEV1CardLogic() {
        String tops = "";
        data = new JSONObject();
        pdetails = new JSONObject();
        card_pin = new JSONObject();
        vouchervalue = "0";
        ovoucheridno = "";
        try {
            String uidhex = new String();
            byte[] uid = desFireEV1.getUID();
            for (int i = 0; i < uid.length; i++) {
                String x = Integer.toHexString(((int) uid[i] & 0xff));
                if (x.length() == 1) {
                    x = '0' + x;
                }
                uidhex += x;
            }
            dt.setUid(String.valueOf(uidhex));
            dt.setValuevoucher("");
            desFireEV1.getReader().setTimeout(timeOut);
            desFireEV1.authenticate(0, IDESFireEV1.AuthType.Native, KeyType.TWO_KEY_THREEDES, objKEY_2KTDES);
            desFireEV1.selectApplication(0);
            desFireEV1.selectApplication(appId);
            desFireEV1.authenticate(0, IDESFireEV1.AuthType.Native, KeyType.TWO_KEY_THREEDES, objKEY_2KTDES);
            pdetails = new JSONObject(new String(desFireEV1.readData(Card_Details.personalDetails, 0, 0)));
            card_pin = new JSONObject(new String(desFireEV1.readData(Card_Details.cpin, 0, 0)));
            Log.i("PIN & Details: ", pdetails + "&&&" + card_pin);
            List<CardBlock> cb=CardBlock.find(CardBlock.class, "cardno =?", card_pin.getString("cardNo"));
            if(!(cb.size()>0)) {

                dt.setRationNo(pdetails.getString("rationalNo"));
                rationNo=pdetails.getString("rationalNo");
                tps = Topups.find(Topups.class, "cardnumber =?", card_pin.getString("cardNo"));
                data = new JSONObject();
                try {
                    Log.i("####NOT_ACTIVATED", "2");
                    data = new JSONObject(new String(desFireEV1.readData(Card_Details.carddata, 0, 0)));
                    if (tps.size() > 0) {

                        Log.i("##DSDSDSDSSXXZZX", data.getString("voucherno") + " 1");
                        Log.i("##DSDSDSDSSXXZZX", tps.get(0).getVocheridno() + " 2");

                        if (data.getString("voucherno").equalsIgnoreCase(tps.get(0).getVocheridno())) {

                            vouchervalue = data.getString("vochervalue");
                            status = 1;
                        } else {

                                vouchervalue = data.getString("vochervalue");
                                ovoucheridno = data.getString("voucherno");
                                status = topUpCard(tps.get(0), vouchervalue, ovoucheridno);
                        }

                    } else {

                        status = 1;

                    }
                    // desFireEV1.getReader().close();
                } catch (Exception e) {

                    Log.i("####sdsdsdsdsdsds", e.toString());
                    if (tps.size() > 0) {
                        try {
                            vouchervalue = data.getString("vochervalue");
                        } catch (Exception i) {
                            vouchervalue = "0";
                        }
                        ovoucheridno = "0";
                        status = topUpCard(tps.get(0), vouchervalue, ovoucheridno);
                    } else {
                        tops = "0";
                    }


                }

                if (data.length() > 0) {
                    vouchervalue = data.getString("vochervalue");
                    //  tops.equalsIgnoreCase("0")
                    Log.i("####NOT_ACTIVATED", "4");
                    pgs = Programmes.find(Programmes.class, "programid=? and bengroup=?", data.getString("programmeid"), data.getString("bengrpId"));
                    if (pgs.size() > 0) {
                        Log.i("########iwiweoiriowri", String.valueOf(Programmes.listAll(Programmes.class).size()));
                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                pDialogg.dismiss();
                                tapview = (ViewGroup) getView().findViewById(R.id.tapview);
                                tapview.setVisibility(View.GONE);
                                menus.setVisibility(View.VISIBLE);
                            }
                        });


                    } else {
                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                pDialogg.dismiss();
                                new SweetAlertDialog(getActivity(), SweetAlertDialog.WARNING_TYPE)
                                        .setTitleText(getString(R.string.Sorry))
                                        .setContentText(getString(R.string.UnableToFetchPrograms))
                                        .setConfirmText(getString(R.string.Ok))
                                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                            @Override
                                            public void onClick(SweetAlertDialog sDialog) {
                                                sDialog.dismissWithAnimation();

                                            }
                                        })
                                        .show();

                            }
                        });
                    }
                } else {

                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            pDialogg.dismiss();
                            new SweetAlertDialog(getActivity(), SweetAlertDialog.WARNING_TYPE)
                                    .setTitleText(getString(R.string.Sorry))
                                    .setContentText(getString(R.string.no_topups))
                                    .setConfirmText(getString(R.string.Ok))
                                    .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                        @Override
                                        public void onClick(SweetAlertDialog sDialog) {
                                            sDialog.dismissWithAnimation();

                                        }
                                    })
                                    .show();

                        }
                    });
                }

            }else {

                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        pDialogg.dismiss();
                        new SweetAlertDialog(getActivity(), SweetAlertDialog.WARNING_TYPE)
                                .setTitleText("Unable to do transactions")
                                .setContentText("This card is already blocked")
                                .setConfirmText(getString(R.string.Ok))
                                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                    @Override
                                    public void onClick(SweetAlertDialog sDialog) {
                                        sDialog.dismissWithAnimation();

                                    }
                                })
                                .show();

                    }
                });

            }
        } catch (Exception e) {

            Log.i("###DSDDFDFDFD", e.toString());
            if (!(data.length() > 0)) {
                if (e.getMessage().trim().contains("Tag was lost"))
                    msg = "Card was removed before the process was complete.Please tap card again and wait";
                else msg = getString(R.string.no_topups);

                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        pDialogg.dismiss();
                        new SweetAlertDialog(getActivity(), SweetAlertDialog.WARNING_TYPE)
                                .setTitleText(getString(R.string.Sorry))

                                .setContentText(msg)

                                .setConfirmText(getString(R.string.Ok))
                                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                    @Override
                                    public void onClick(SweetAlertDialog sDialog) {
                                        sDialog.dismissWithAnimation();

                                    }
                                })
                                .show();

                    }
                });
            } else {

                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        pDialogg.dismiss();
                        new SweetAlertDialog(getActivity(), SweetAlertDialog.WARNING_TYPE)
                                .setTitleText(getString(R.string.Errorr))
                                .setContentText(getString(R.string.UnAbletoReadCard))
                                .setConfirmText(getString(R.string.Ok))
                                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                    @Override
                                    public void onClick(SweetAlertDialog sDialog) {
                                        sDialog.dismissWithAnimation();

                                    }
                                })
                                .show();

                    }
                });
            }

        }

    }
    private void mifareDesfireLogic(String dataStr){
        String tops = "";
        data = new JSONObject();
        pdetails = new JSONObject();
        card_pin = new JSONObject();
        vouchervalue = "0";
        ovoucheridno = "";
        try {
            JSONObject carddata= new JSONObject(dataStr);
            dt.setValuevoucher("");
            dt.setUid(uidhex);
            pdetails = carddata.getJSONObject("personaldetails");
            card_pin = carddata.getJSONObject("cardpin");
            data = carddata.getJSONObject("topups");
            Log.d("##data",data.toString());
            Log.i("PIN & Details: ", pdetails + "&&&" + card_pin);
            List<CardBlock> cb=CardBlock.find(CardBlock.class, "cardno =?", card_pin.getString("cardNo"));
            if(!(cb.size()>0)) {

                dt.setRationNo(pdetails.getString("rationalNo"));
                rationNo=pdetails.getString("rationalNo");
                tps = Topups.find(Topups.class, "cardnumber =?", card_pin.getString("cardNo"));
                //data = new JSONObject();
                try {
                    Log.i("####NOT_ACTIVATED", "2");
                    if (tps.size() > 0) {

                        Log.i("##DSDSDSDSSXXZZX", data.getString("voucherno") + " 1");
                        Log.i("##DSDSDSDSSXXZZX", tps.get(0).getVocheridno() + " 2");

                        if (data.getString("voucherno").equalsIgnoreCase(tps.get(0).getVocheridno())) {
                            vouchervalue = data.getString("vochervalue");
                            status = 1;
                        } else {

                            vouchervalue = data.getString("vochervalue");
                            ovoucheridno = data.getString("voucherno");
                            //status = topUpCard(tps.get(0), vouchervalue, ovoucheridno);
                        }

                    } else {

                        status = 1;

                    }
                } catch (Exception e) {

                    Log.i("####sdsdsdsdsdsds", e.toString());
                    if (tps.size() > 0) {
                        try {
                            vouchervalue = data.getString("vochervalue");
                        } catch (Exception i) {
                            vouchervalue = "0";
                        }
                        ovoucheridno = "0";
                        status = topUpCard(tps.get(0), vouchervalue, ovoucheridno);
                    } else {
                        tops = "0";
                    }


                }

                if (data.length()>0) {
                    vouchervalue = data.getString("vochervalue");
                    //  tops.equalsIgnoreCase("0")
                    Log.i("####NOT_ACTIVATED", "4");
                    pgs = Programmes.find(Programmes.class, "programid=? and bengroup=?", data.getString("programmeid"), data.getString("bengrpId"));
                    if (pgs.size() > 0) {
                        Log.i("########iwiweoiriowri", String.valueOf(Programmes.listAll(Programmes.class).size()));
                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                pDialogg.dismiss();
                                tapview = (ViewGroup) getView().findViewById(R.id.tapview);
                                tapview.setVisibility(View.GONE);
                                menus.setVisibility(View.VISIBLE);
                            }
                        });


                    } else {
                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                pDialogg.dismiss();
                                new SweetAlertDialog(getActivity(), SweetAlertDialog.WARNING_TYPE)
                                        .setTitleText(getString(R.string.Sorry))
                                        .setContentText(getString(R.string.UnableToFetchPrograms))
                                        .setConfirmText(getString(R.string.Ok))
                                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                            @Override
                                            public void onClick(SweetAlertDialog sDialog) {
                                                sDialog.dismissWithAnimation();

                                            }
                                        })
                                        .show();

                            }
                        });
                    }
                } else {

                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            pDialogg.dismiss();
                            new SweetAlertDialog(getActivity(), SweetAlertDialog.WARNING_TYPE)
                                    .setTitleText(getString(R.string.Sorry))
                                    .setContentText(getString(R.string.no_topups))
                                    .setConfirmText(getString(R.string.Ok))
                                    .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                        @Override
                                        public void onClick(SweetAlertDialog sDialog) {
                                            sDialog.dismissWithAnimation();

                                        }
                                    })
                                    .show();

                        }
                    });
                }

            }else {

                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        pDialogg.dismiss();
                        new SweetAlertDialog(getActivity(), SweetAlertDialog.WARNING_TYPE)
                                .setTitleText("Unable to do transactions")
                                .setContentText("This card is already blocked")
                                .setConfirmText(getString(R.string.Ok))
                                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                    @Override
                                    public void onClick(SweetAlertDialog sDialog) {
                                        sDialog.dismissWithAnimation();

                                    }
                                })
                                .show();

                    }
                });

            }
        } catch (Exception e) {

            Log.i("###DSDDFDFDFD", e.toString());
            if (!(data.length() > 0)) {
                if (e.getMessage().trim().contains("Tag was lost"))
                    msg = "Card was removed before the process was complete.Please tap card again and wait";
                else msg = getString(R.string.no_topups);

                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        pDialogg.dismiss();
                        new SweetAlertDialog(getActivity(), SweetAlertDialog.WARNING_TYPE)
                                .setTitleText(getString(R.string.Sorry))

                                .setContentText(msg)

                                .setConfirmText(getString(R.string.Ok))
                                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                    @Override
                                    public void onClick(SweetAlertDialog sDialog) {
                                        sDialog.dismissWithAnimation();

                                    }
                                })
                                .show();

                    }
                });
            } else {

                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        pDialogg.dismiss();
                        new SweetAlertDialog(getActivity(), SweetAlertDialog.WARNING_TYPE)
                                .setTitleText(getString(R.string.Errorr))
                                .setContentText(getString(R.string.UnAbletoReadCard))
                                .setConfirmText(getString(R.string.Ok))
                                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                    @Override
                                    public void onClick(SweetAlertDialog sDialog) {
                                        sDialog.dismissWithAnimation();

                                    }
                                })
                                .show();

                    }
                });
            }

        }

    }
    private void initializeKeys() {
        KeyInfoProvider infoProvider = KeyInfoProvider.getInstance(getActivity().getApplicationContext());
        infoProvider.setKey(ALIAS_KEY_2KTDES, SampleAppKeys.EnumKeyType.EnumDESKey, SampleAppKeys.KEY_2KTDES);
        infoProvider.setKey(ALIAS_KEY_AES128, SampleAppKeys.EnumKeyType.EnumAESKey, SampleAppKeys.KEY_AES128);
        infoProvider.setKey(ALIAS_KEY_AES128_ZEROES, SampleAppKeys.EnumKeyType.EnumAESKey, SampleAppKeys.KEY_AES128_ZEROS);
        infoProvider.setKey(ALIAS_DEFAULT_FF, SampleAppKeys.EnumKeyType.EnumMifareKey, SampleAppKeys.KEY_DEFAULT_FF);
        objKEY_2KTDES_ULC = infoProvider.getKey(ALIAS_KEY_2KTDES_ULC, SampleAppKeys.EnumKeyType.EnumDESKey);
        objKEY_2KTDES = infoProvider.getKey(ALIAS_KEY_2KTDES, SampleAppKeys.EnumKeyType.EnumDESKey);
        objKEY_AES128 = infoProvider.getKey(ALIAS_KEY_AES128, SampleAppKeys.EnumKeyType.EnumAESKey);
        default_zeroes_key = infoProvider.getKey(ALIAS_KEY_AES128_ZEROES, SampleAppKeys.EnumKeyType.EnumAESKey);
        default_ff_key = infoProvider.getMifareKey(ALIAS_DEFAULT_FF);
    }


    private void initializeCipherinitVector() {

		/* Initialize the Cipher */
        try {
            cipher = Cipher.getInstance("AES/CBC/NoPadding");
        } catch (NoSuchAlgorithmException e) {

            e.printStackTrace();
        } catch (NoSuchPaddingException e) {
            e.printStackTrace();
        }

		/* set Application Master Key */
        bytesKey = KEY_APP_MASTER.getBytes();

		/* Initialize init vector of 16 bytes with 0xCD. It could be anything */
        byte[] ivSpec = new byte[16];
        Arrays.fill(ivSpec, (byte) 0xCD);
        iv = new IvParameterSpec(ivSpec);

    }

    void setUi() {
        button0 = (Button) getView().findViewById(R.id.button0);
        button0.setOnClickListener(this);
        button1 = (Button) getView().findViewById(R.id.button1);
        button1.setOnClickListener(this);
        button2 = (Button) getView().findViewById(R.id.button2);
        button2.setOnClickListener(this);
        button3 = (Button) getView().findViewById(R.id.button3);
        button3.setOnClickListener(this);
        button4 = (Button) getView().findViewById(R.id.button4);
        button4.setOnClickListener(this);
        button5 = (Button) getView().findViewById(R.id.button5);
        button5.setOnClickListener(this);
        button6 = (Button) getView().findViewById(R.id.button6);
        button6.setOnClickListener(this);
        button7 = (Button) getView().findViewById(R.id.button7);
        button7.setOnClickListener(this);
        button8 = (Button) getView().findViewById(R.id.button8);
        button8.setOnClickListener(this);
        button9 = (Button) getView().findViewById(R.id.button9);
        button9.setOnClickListener(this);

        buttonOk = (Button) getView().findViewById(R.id.buttonOk);
        buttonOk.setOnClickListener(new View.OnClickListener() {
                                        public void onClick(View v) {
                                            try {
                                                vibe.vibrate(100);
                                                Verify(passwordInput.getText().toString());
                                            } catch (Exception e) {
                                                //  .i("")
                                            }

                                        }

                                    }
        );
        buttonDelete = (Button) getView().findViewById(R.id.buttonDeleteBack);
        passwordInput = (EditText) getView().findViewById(R.id.editText);
        vibe = (Vibrator) getActivity().getSystemService(Context.VIBRATOR_SERVICE);
        View.OnTouchListener otl = new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                return true;
            }
        };
        passwordInput.setOnTouchListener(otl);
        colorslist = Colors.listAll(Colors.class);
        Log.i("#####IMGELIST", String.valueOf(colorslist.size()));
        if (colorslist.size() >= 9) {
            try {
                //   Log.i("#####COLORSS",colorslist.get(9).getColorcode());
                //   button0.setBackgroundTintList(ColorStateList.valueOf(Color.parseColor("#"+colorslist.get(1).getColorno())));
                button1.setBackgroundTintList(ColorStateList.valueOf(Color.parseColor(colorslist.get(0).getColorcode())));
                button2.setBackgroundTintList(ColorStateList.valueOf(Color.parseColor(colorslist.get(1).getColorcode())));
                button3.setBackgroundTintList(ColorStateList.valueOf(Color.parseColor(colorslist.get(2).getColorcode())));
                button4.setBackgroundTintList(ColorStateList.valueOf(Color.parseColor(colorslist.get(3).getColorcode())));
                button5.setBackgroundTintList(ColorStateList.valueOf(Color.parseColor(colorslist.get(4).getColorcode())));
                button6.setBackgroundTintList(ColorStateList.valueOf(Color.parseColor(colorslist.get(5).getColorcode())));
                button7.setBackgroundTintList(ColorStateList.valueOf(Color.parseColor(colorslist.get(6).getColorcode())));
                button8.setBackgroundTintList(ColorStateList.valueOf(Color.parseColor(colorslist.get(7).getColorcode())));
                button9.setBackgroundTintList(ColorStateList.valueOf(Color.parseColor(colorslist.get(8).getColorcode())));

            } catch (Exception e) {
                Log.i("####ERROR", e.toString());
            }

        } else {
            Log.i("#####IMGELIST", String.valueOf(colorslist.size()));
        }

        buttonDelete = (Button) getView().findViewById(R.id.buttonDeleteBack);
        buttonDelete.setOnClickListener(new View.OnClickListener() {
                                            public void onClick(View v) {
                                                vibe.vibrate(100);
                                                Log.d("OOGDOGODOGD", String.valueOf(userEntered.length()));
                                                if (userEntered.length() > 0) {
                                                    userEntered = userEntered.substring(0, userEntered.length() - 1);
                                                    passwordInput.setText(userEntered);
                                                    //statusView.setTextColor(Color.GRAY);
                                                    // statusView.setText("Enter Pin.");
                                                    //passwordInput.setBackgroundResource(R.drawable.edittextstyle0);
                                                    passwordInput.setTextColor(Color.BLACK);

                                                }

                                            }

                                        }
        );
        buttonDelete.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                vibe.vibrate(200);
                passwordInput.setText("");
                userEntered = "";
                // statusView.setTextColor(Color.GRAY);
                // statusView.setText("Enter Pin.");
                //passwordInput.setBackgroundResource(R.drawable.edittextstyle0);
                passwordInput.setTextColor(Color.BLACK);
                return true;
            }
        });
    }

    @Override
    public void onClick(View v) {
        String input = "";
        if (userEntered.trim().length() <= 3) {
            vibe.vibrate(100);
            switch (v.getId()) {
                case R.id.button0: {
                    // input= colorslist.get(0).getColorno();
                    break;

                }
                case R.id.button1: {

                    input = colorslist.get(0).getColorno();
                    break;

                }
                case R.id.button2: {
                    input = colorslist.get(1).getColorno();
                    break;

                }
                case R.id.button3: {
                    input = colorslist.get(2).getColorno();
                    break;

                }
                case R.id.button4: {
                    input = colorslist.get(3).getColorno();
                    break;
                }
                case R.id.button5: {
                    input = colorslist.get(4).getColorno();
                    break;
                }
                case R.id.button6: {
                    input = colorslist.get(5).getColorno();
                    break;
                }
                case R.id.button7: {
                    input = colorslist.get(6).getColorno();
                    break;
                }
                case R.id.button8: {
                    input = colorslist.get(7).getColorno();
                    break;
                }
                case R.id.button9: {
                    input = colorslist.get(8).getColorno();
                    break;
                }
                default: {

                }
            }

            userEntered = userEntered + input;
            Log.i("####PinView", "User entered=" + input);
            //Update pin boxes
            passwordInput.setText(passwordInput.getText().toString() + "*");
            passwordInput.setSelection(passwordInput.getText().toString().length());
        }
    }

    public void Verify(String password) throws Exception {
        Log.i("##FGFGFCVCVC", password + "dvdvd");
        if (card_pin.getString("pin").trim().equalsIgnoreCase(userEntered.trim())) {
          /*  startActivity(new Intent(getActivity(),Beneficairy_Activity.class));
            getActivity().finish();*/
            view3 = (ViewGroup) getView().findViewById(R.id.recyclerlayout);

            recyclerView = (RecyclerView) getView().findViewById(R.id.recycler_viewq);

            recyclerView.setLayoutManager(new StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL));
            ProgrammesAdapter adapter = new ProgrammesAdapter(getActivity(), pgs);
            adapter.setClickListener(PurchaseFragment.this);
            AlphaInAnimationAdapter alphaAdapter = new AlphaInAnimationAdapter(adapter);
            alphaAdapter.setDuration(1500);
            recyclerView.setAdapter(alphaAdapter);
            menus.setVisibility(View.GONE);
            view3.setVisibility(View.VISIBLE);
            dt.setTopups(data);
            dt.setValuevoucher("");
            try {

                dt.setBenGroup(data.getString("bengrpId"));
                dt.setValuevoucher(vouchervalue);
                dt.setVoucherno(data.getString("voucherno"));
                dt.setCardnumber(card_pin.getString("cardNo"));

            } catch (Exception e) {
                Log.i("#####VOUCHERVALUE", e.toString());

            }
        } else {
            passwordInput.setTextColor(Color.RED);
            showAlert(getString(R.string.Errorr), getString(R.string.inValPin));
        }
    }

    int topUpCard(Topups ts, String value, String ovoucherno) {
        String android_id = Settings.Secure.getString(getActivity().getApplicationContext().getContentResolver(),
                Settings.Secure.ANDROID_ID);
        SimpleDateFormat date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Log.i("##DWEWERWREWD", ts.getVouchervalue() + "  ffff");
        Log.i("##DWEWERWREWD", vouchervalue + "  ffffaa");

        //create a new db record
        TopupsLogs topupsLogs = new TopupsLogs();
        topupsLogs.setOcardbal(value);
        topupsLogs.setIsuploaded("0");
        topupsLogs.setNtopupvalue(ts.getVouchervalue());
        topupsLogs.setDeviceidno(android_id);
        topupsLogs.setOvoucheridno(ovoucherno);
        topupsLogs.setRefno(android_id + android_id + System.currentTimeMillis());
        topupsLogs.setNvoucheridno(ts.getVocheridno());
        topupsLogs.setTopuptime(date.format(new Date()));
        topupsLogs.setCardno(ts.getCardNumber());

        vouchervalue = String.valueOf(Float.parseFloat(value) + Float.parseFloat(ts.getVouchervalue()));

        topupsLogs.setNcardbal(vouchervalue);
        String username, locationid;
        try {
            username = Users.find(Users.class, "isloggedin =?", "1").get(0).getUsername();
            locationid = Users.find(Users.class, "isloggedin =?", "1").get(0).getLocationid();

        } catch (Exception e) {
            username = "";
            locationid = "";
        }
        topupsLogs.setUsername(username);
        topupsLogs.save();


        try {

            topup = new JSONObject();
            topup.put("vochervalue", vouchervalue);
            topup.put("voucherno", ts.getVocheridno());
            topup.put("programmeid", ts.getProgrammeId());
            topup.put("voucherid", ts.getVoucherId());
            topup.put("benid", ts.getBeneficiaryId());
            topup.put("cardno", ts.getCardNumber());
            topup.put("bengrpId", ts.getBengroup());
            data = topup;

            desFireEV1.getReader().setTimeout(timeOut);
            desFireEV1.authenticate(0, IDESFireEV1.AuthType.Native, KeyType.TWO_KEY_THREEDES, objKEY_2KTDES);
            desFireEV1.selectApplication(0);
            desFireEV1.selectApplication(appId);
            desFireEV1.authenticate(0, IDESFireEV1.AuthType.Native, KeyType.TWO_KEY_THREEDES, objKEY_2KTDES);
            desFireEV1.writeData(Card_Details.carddata, 0, topup.toString().trim().getBytes());
            return 1;

        } catch (Exception e) {
            Log.i("####ERROACTIVAINGVSARD", e.toString());
            return 0;
        }

    }
    public CardManager read_nfc_card(int mode, int timeout) {
        final CardInfo cInfo = new CardInfo();
        CardManager cardManager = CardManager.getInstance(getActivity(), mode,"read",null);
        cardManager.getCard(timeout, new CardListener() {
            @Override
            public void callback(CardInfo cardInfo) {

               Log.d("##callback"," card info "+ cardInfo.getCarddata());
                try{

                     mifareDesfireLogic(cardInfo.getCarddata());

                }catch(Exception e){

                    Log.d("##callback"," card info "+e.toString());

                }

            }
        });

        Log.d("##callback"," About to return the instance of the card manager ");
        return cardManager;
    }
}
