package com.evoucher.compas.evoucher;

/**
 * Created by Martin on 2/8/2017.
 */


import android.content.Context;
import android.os.AsyncTask;
import android.os.Environment;
import android.text.format.DateFormat;
import android.widget.Toast;

import com.lynx.evoucher.models.Transactions;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.Buffer;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Backup extends AsyncTask<Void,Void,Void> {
    String folderName;
    String file;
    Context context;
    public Backup(String folder, String fileName, Context context1){
        folderName=folder;
        file=fileName;
        context=context1;
    }
    @Override
    protected Void doInBackground(Void... params)   {
        try {
            BackupDatabase(folderName,file);
        }catch (Exception e){

        }


        return null;
    }
    public static  void outPutLogs(String logString,String remedy,Context context){

        if(!isExternalStorageWritable())
            return;
        boolean success =false;
        File logFile = null;
        File dirDocFile= Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOCUMENTS);
        if(!dirDocFile.exists()){
            success=dirDocFile.mkdir();
        }
        File dirFile=new File(dirDocFile,"TBC_Trans_Logs");
        Date d = new Date();
        CharSequence sequence = DateFormat.format("yyyy-MM-dd hh:mm:ss", d.getTime());
        //logString="##LOG-> Date_and_Time :"+sequence+" Reason :"+logString+"  in component named:"+context.getClass().getName()+" Remedy :"+remedy;
        if (dirFile.exists()) {
            logFile = new File(dirFile,"transactionLogFile");
            if (logFile.exists()) {

                if (logFile.length() >5000000) {
                    logFile.delete();
                    logFile= new File(dirFile,"transactionLogFile");
                    try {
                        logFile.createNewFile();
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                    if(logFile.exists()){
                        success=true;
                    }
                    else {

                        success=false;
                    }
                }else {
                    success=true;
                }
            }else{
                try {
                    logFile.createNewFile();
                }catch (Exception e){
                    e.printStackTrace();
                }
                if(logFile.exists()){
                    success=true;
                }
            }
        }
        else
        {   try {
            dirFile.mkdir();
            logFile=new File(dirFile,"transactionLogFile");
            success=logFile.createNewFile();
        }catch (Exception e){
            e.printStackTrace();
        }
        }

        if (success)
        {   OutputStream output=null;

            try {

                output =  new FileOutputStream(logFile,true);
                output.write(logString.getBytes());
                output.write("\n\r".getBytes());
                output.flush();
                output.close();
            }catch (Exception e){
                e.printStackTrace();
            }


        }
    }
    public  void BackupDatabase(String folder,String fileName) throws IOException {
        if(!isExternalStorageWritable())
            return;
        boolean success =true;
        File file = null;File file1=null;
        file1 = Environment.getExternalStoragePublicDirectory("AppData");
        if(!file1.exists()){
            file1.mkdir();
        }
        file=      Environment.getExternalStoragePublicDirectory("AppData/"+folder);
        if (file.exists())
        {
            success =true;
        }
        else
        {
            success = file.mkdir();
        }

        if (success)
        {
            File dbFile=context.getDatabasePath("krcs.db");
            FileInputStream fis = new FileInputStream(dbFile);
            String outFileName = Environment.getExternalStoragePublicDirectory("AppData")+"/"+folder+"/"+fileName;
            OutputStream output = new FileOutputStream(outFileName);
            byte[] buffer = new byte[1024];
            int length;
            while ((length = fis.read(buffer))>0) {
                output.write(buffer, 0, length);
            }
            output.flush();
            output.close();
            fis.close();
        }
    }
    public  void read_database_folder(String folder,String fileName) throws IOException {
        if(!isExternalStorageWritable())
            return;
        boolean success =true;
        File file = null;File file1=null;
        file1 = Environment.getExternalStoragePublicDirectory("AppData");
        if(!file1.exists()){
            file1.mkdir();
        }
        file= Environment.getExternalStoragePublicDirectory("AppData/"+folder+"/"+fileName);
        if (file.exists())
        {
            success =true;
        }
        else
        {
            // success = file.mkdir();
        }

        if (success)
        {
            String database_file=context.getDatabasePath("krcs.db").getAbsolutePath();
            FileInputStream fis = new FileInputStream(file);
            //String outFileName = Environment.getExternalStoragePublicDirectory("AppData")+"/"+folder+"/"+fileName;
            OutputStream output = new FileOutputStream(database_file);
            byte[] buffer = new byte[1024];
            int length;
            while ((length = fis.read(buffer))>0) {
                output.write(buffer, 0, length);
            }
            output.flush();
            output.close();
            fis.close();
        }
    }
    public static boolean isExternalStorageWritable() {
        String state = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED.equals(state)) {
            return true;
        }
        return false;
    }
    public void backup() {
        try {
            File sdcard = Environment.getExternalStoragePublicDirectory("AppData");
            File outputFile = new File(sdcard, "MyAppDb.bak");

            if (!outputFile.exists())
                outputFile.createNewFile();

            File data = Environment.getDataDirectory();
            File inputFile = new File(data,
                    "data/com.example.administrator.learningtest/databases/sugar.sqlite");
            InputStream input = new FileInputStream(inputFile);
            OutputStream output = new FileOutputStream(outputFile);
            byte[] buffer = new byte[1024];
            int length;
            while ((length = input.read(buffer)) > 0) {
                output.write(buffer, 0, length);
            }
            output.flush();
            output.close();
            input.close();
        } catch (IOException e) {
            e.printStackTrace();
            //throw new Error("Copying Failed");
        }
    }
    public static  void  outPutTopupLogs(String logString,String remedy,Context context){

        if(!isExternalStorageWritable())
            return;
        boolean success =false;
        File logFile = null;
        File dirDocFile= Environment.getExternalStoragePublicDirectory("AppData");
        if(!dirDocFile.exists()){
            success=dirDocFile.mkdir();
        }
        File dirFile=new File(dirDocFile,"PBU_Logs");
        Date d = new Date();
        CharSequence sequence = DateFormat.format("yyyy-MM-dd hh:mm:ss", d.getTime());
        logString="##LOG-> Date_and_Time :"+sequence+" Reason :"+logString;
        if (dirFile.exists()) {
            logFile = new File(dirFile,"logfile_+"+sequence);
            if (logFile.exists()) {

                if (logFile.length() >5000000) {
                    logFile.delete();
                    logFile= new File(dirFile,"logfile_+"+sequence);
                    try {
                        logFile.createNewFile();
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                    if(logFile.exists()){
                        success=true;
                    }
                    else {

                        success=false;
                    }
                }else {
                    success=true;
                }
            }else{
                try {
                    logFile.createNewFile();
                }catch (Exception e){
                    e.printStackTrace();
                }
                if(logFile.exists()){
                    success=true;
                }
            }
        }
        else
        {   try {
            dirFile.mkdir();
            logFile=new File(dirFile,"logfile_+"+sequence);
            success=logFile.createNewFile();
        }catch (Exception e){
            e.printStackTrace();
        }
        }

        if (success)
        {   OutputStream output=null;

            try {

                output =  new FileOutputStream(logFile,true);
                output.write(logString.getBytes());
                output.write("\n\r".getBytes());
                output.flush();
                output.close();
            }catch (Exception e){
                e.printStackTrace();
            }


        }
    }
    public static  void logTransactions(List<Transactions> transactions, String fileName, Context context){

        if(!isExternalStorageWritable())
            return;
        boolean success =false;
        File logFile = null;
        File dirDocFile= Environment.getExternalStoragePublicDirectory("AppData");
        if(!dirDocFile.exists()){
            success=dirDocFile.mkdir();
        }
        File dirFile=new File(dirDocFile,"PBU_Logs/transactions");
        Date d = new Date();
        CharSequence sequence = DateFormat.format("yyyy-MM-dd hh:mm:ss", d.getTime());
        //logString="##LOG-> Date_and_Time :"+sequence+" Reason :"+logString+"  in component named:"+context.getClass().getName()+" Remedy :"+remedy;
        if (dirFile.exists()) {
            logFile = new File(dirFile,fileName);
            if (logFile.exists()) {

                if (logFile.length() >5000000) {
                    logFile.delete();
                    logFile= new File(dirFile, fileName);
                    try {
                        logFile.createNewFile();
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                    if(logFile.exists()){
                        success=true;
                    }
                    else {

                        success=false;
                    }
                }else {
                    success=true;
                }
            }else{
                try {
                    logFile.createNewFile();
                }catch (Exception e){
                    e.printStackTrace();
                }
                if(logFile.exists()){
                    success=true;
                }
            }
        }
        else
        {   try {
            dirFile.mkdir();
            logFile=new File(dirFile, fileName);
            success=logFile.createNewFile();
        }catch (Exception e){
            e.printStackTrace();
        }
        }

        if (success)
        {   OutputStream output=null;

            try {
                String logString=" ";
                output =  new FileOutputStream(logFile,true);
                for(Transactions txn:transactions) {
                   /* JSONObject jo= new JSONObject().put("Account",txn.getAccountno()).put("Amount",txn.getTotalamountchargedbyretail()).put("Cycle",txn.getCycle()).put("VoucherId",txn.getVoucheridno())
                            .put("Receipt_no",txn.getReceiptno()).put("Teller Id",txn.getUser());
                    //logString="Account :"+txn.getAccountno()+" Amount : "+txn.getTotalamountchargedbyretail()+" Cycle :"+txn.getCycle()+ "Teller Id : "+txn.getUserid()+" Voucher Id :"+txn.getVoucheridno();
                    output.write(jo.toString().getBytes());
                    output.write("\n\r".getBytes());*/
                }
                output.flush();
                output.close();
            }catch (Exception e){
                e.printStackTrace();
            }


        }
    }

    public static  List<String> read_missing_transactions_file(Context context, String fileName){
        if(!isExternalStorageWritable())
            return null;
        List<String>  missing_transactions_account= new ArrayList<>();
        boolean success =false;
        File logFile = null;
        File dirDocFile= Environment.getExternalStoragePublicDirectory("AppData");
        if(!dirDocFile.exists()){
            success=dirDocFile.mkdir();
        }
        File dirFile=new File(dirDocFile,"missing_transactions");
        Date d = new Date();
        //logString="##LOG-> Date_and_Time :"+sequence+" Reason :"+logString+"  in component named:"+context.getClass().getName()+" Remedy :"+remedy;
        if (dirFile.exists()) {
            logFile = new File(dirFile,fileName);
            if (logFile.exists()) {
                success=true;
            }else{
                Toast.makeText(context,"Missing transactions file does not exist",Toast.LENGTH_LONG);
                return  missing_transactions_account;
            }
        }
        else
        {   try {
            dirFile.mkdir();
            logFile=new File(dirFile, fileName);
            success=logFile.createNewFile();
        }catch (Exception e){
            e.printStackTrace();
            Toast.makeText(context,"Error reading the external directory",Toast.LENGTH_LONG);
            return  missing_transactions_account;
        }
        }
        if (success){
            FileReader input_reader;
            BufferedReader  reader;
            try {
                input_reader =new FileReader(logFile);
                reader= new BufferedReader(input_reader);
                String account_line=null;
                for(;(account_line = reader.readLine()) != null; ) {
                    account_line=account_line.trim();
                    if(account_line.length()>0)
                        missing_transactions_account.add(account_line);
                }
            } catch (Exception e){

            }

        }
        return  missing_transactions_account;

    }


}