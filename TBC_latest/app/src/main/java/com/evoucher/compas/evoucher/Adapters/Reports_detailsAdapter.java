package com.evoucher.compas.evoucher.Adapters;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.amulyakhare.textdrawable.TextDrawable;
import com.amulyakhare.textdrawable.util.ColorGenerator;
import com.evoucher.compas.evoucher.ItemClickListener;
import com.evoucher.compas.evoucher.R;
import com.lynx.evoucher.models.Beneficiary;
import com.lynx.evoucher.models.Programmes;
import com.lynx.evoucher.models.Reports;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Administrator on 10/29/2016.
 */
import android.widget.BaseAdapter;
import com.evoucher.compas.evoucher.Details;

import com.lynx.evoucher.models.Topups;



/**
 * Created by Administrator on 8/17/2016.
 */
public class Reports_detailsAdapter extends BaseAdapter {
    Typeface typeface;

    Details dt;
    List<Topups> ts;
    Context context;
    ArrayList<Integer> positions;
    List<Reports> reports;

    public Reports_detailsAdapter(Context context,  List<Reports> reports){
       this. reports=reports;
        this.context=context;
        dt=new Details(context);
        typeface = Typeface.createFromAsset(context.getAssets(), "Roboto-Thin.ttf");

    }
    @Override
    public int getCount() {
        return   reports.size();
    }

    @Override
    public Object getItem(int i) {
        return    reports.get(i);
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.reportsdetails_row, parent, false);
            Typeface typeface = Typeface.createFromAsset(context.getAssets(), "Roboto-Regular.ttf");

            ViewHolder holder = new ViewHolder();

            holder.lawyercode = (TextView) convertView.findViewById(R.id.lawyer_code);
            holder.lawyername = (TextView) convertView.findViewById(R.id.lawyername);
            holder.date = (TextView) convertView.findViewById(R.id.date);
            holder.verifiedby = (TextView) convertView.findViewById(R.id.verifiedby);
            holder.report_layout = (ViewGroup) convertView.findViewById(R.id.reports_layout);
            convertView.setTag(R.id.holder, holder);
        }

        ViewHolder holder = (ViewHolder) convertView.getTag(R.id.holder);
        if(position % 2==0){
            holder.report_layout.setBackgroundResource(R.color.even);
        }else{
            holder.report_layout.setBackgroundResource(R.color.odd);
        }
        holder.lawyercode.setTypeface(typeface);
        holder.lawyername.setTypeface(typeface);
        holder.lawyercode.setTypeface(typeface);
        holder.date.setTypeface(typeface);
        holder.verifiedby.setTypeface(typeface);

        holder.lawyercode.setText(reports.get(position).getLawyercode());
        holder.lawyername.setText(reports.get(position).getLawyername().toUpperCase());
        holder.date.setText(reports.get(position).getDate());
        holder.verifiedby.setText(reports.get(position).getVerifiedby());

        return convertView;
    }
    private static class ViewHolder {

        public TextView lawyercode,lawyername,date,verifiedby;
        ViewGroup report_layout;


    }

}