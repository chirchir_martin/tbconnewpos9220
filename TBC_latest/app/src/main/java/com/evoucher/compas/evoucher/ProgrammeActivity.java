package com.evoucher.compas.evoucher;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.hardware.usb.UsbDevice;
import android.hardware.usb.UsbManager;
import android.nfc.NdefMessage;
import android.nfc.NdefRecord;
import android.nfc.NfcAdapter;
import android.nfc.Tag;
import android.nfc.tech.Ndef;
import android.nfc.tech.NdefFormatable;
import android.os.Build;
import android.os.Environment;
import android.os.Handler;
import android.os.Parcelable;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.evoucher.compas.evoucher.Adapters.ProgrammesAdapter;
import com.evoucher.compas.evoucher.fragments.CardActivation;
import com.evoucher.compas.evoucher.fragments.CardBalanceFragment;
import com.evoucher.compas.evoucher.fragments.Card_enquiryFragment;
import com.evoucher.compas.evoucher.fragments.FormatFragment;
import com.evoucher.compas.evoucher.fragments.LawyersFragment;

import com.evoucher.compas.evoucher.fragments.PendingSyncFragment;

import com.evoucher.compas.evoucher.fragments.PasswordChangeFragment;

import com.evoucher.compas.evoucher.fragments.PurchaseFragment;
import com.evoucher.compas.evoucher.fragments.Settings_fragment;
import com.evoucher.compas.evoucher.fragments.SyncReports;
import com.evoucher.compas.evoucher.fragments.SynchronizationFragment;
import com.evoucher.compas.evoucher.fragments.Update;
import com.evoucher.compas.evoucher.fragments.Verification_Fragment;
import com.evoucher.compas.evoucher.fragments.VoidFragment;
import com.evoucher.compas.evoucher.fragments.XreportFragment;
import com.evoucher.compas.evoucher.fragments.ZreportFragment;
import com.evoucher.compas.evoucher.p2p.SyncActivity;
import com.evoucher.compas.evoucher.ui.CustomImageView;
import com.google.common.base.Strings;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;
import com.lynx.evoucher.models.Beneficiary;
import com.lynx.evoucher.models.FingerPrintVO;
import com.lynx.evoucher.models.Member;
import com.lynx.evoucher.models.Programmes;
import com.lynx.evoucher.models.Reports;
import com.lynx.evoucher.models.Reprints;
import com.lynx.evoucher.models.SyncLogs;
import com.lynx.evoucher.models.Topups;
import com.lynx.evoucher.models.Configuration;
import com.lynx.evoucher.models.TransactionsListProducts;
import com.lynx.evoucher.models.Vouchers;
import com.mikepenz.itemanimators.AlphaCrossFadeAnimator;
import com.mikepenz.materialdrawer.AccountHeader;
import com.mikepenz.materialdrawer.AccountHeaderBuilder;
import com.mikepenz.materialdrawer.Drawer;
import com.mikepenz.materialdrawer.DrawerBuilder;
import com.mikepenz.materialdrawer.model.ExpandableDrawerItem;
import com.mikepenz.materialdrawer.model.ProfileDrawerItem;
import com.mikepenz.materialdrawer.model.SecondaryDrawerItem;
import com.mikepenz.materialdrawer.model.interfaces.IDrawerItem;

import com.newpos.libpay.PaySdk;
import com.newpos.libpay.device.card.CardInfo;
import com.newpos.libpay.device.card.CardListener;
import com.newpos.libpay.device.card.CardManager;
import com.newpos.libpay.device.printer.Quantitydetails;
import com.newpos.libpay.device.printer.TBCTransactionReceipt;
import com.rey.material.widget.RadioButton;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.nio.channels.FileChannel;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import SecuGen.FDxSDKPro.JSGFPLib;
import SecuGen.FDxSDKPro.SGAutoOnEventNotifier;
import SecuGen.FDxSDKPro.SGFDxDeviceName;
import SecuGen.FDxSDKPro.SGFDxErrorCode;
import SecuGen.FDxSDKPro.SGFDxSecurityLevel;
import SecuGen.FDxSDKPro.SGFDxTemplateFormat;
import SecuGen.FDxSDKPro.SGFingerInfo;
import cn.com.aratek.fp.FingerprintScanner;
import cn.pedant.SweetAlert.SweetAlertDialog;
import jp.wasabeef.recyclerview.adapters.AlphaInAnimationAdapter;

public class ProgrammeActivity extends AppCompatActivity implements ItemClickListener, View.OnClickListener {
    private static RecyclerView recyclerView;
    List<Programmes> programs;
    ArrayList<String> programIDs = new ArrayList<String>();
    private AccountHeader headerResult = null;
    Typeface typeface1;
    private Drawer result = null;
    private static final int REQUEST_WRITE_STORAGE = 112;
    MaterialDialog xReportDialog;
    NfcAdapter nfcAdapter;
    PurchaseFragment purchaseFragment;
    ViewGroup container, view1, view2, viewsettings, xreport, zreport, phone;
    TextView ur;
    Network net;
    ImageView home;
    public static Activity fa;
    Details dt;
    Network nt;
    PurchaseFragment newFragment;
    XreportFragment xreportFragment;
    ZreportFragment zreportFragment;
    PendingSyncFragment pendingSyncFragment;
    FormatFragment formatFragment;
    CardActivation cardActivationfragment;
    Settings_fragment settings_fragment;
    CardBalanceFragment cardbalance;
    SyncReports syncReports;
    VoidFragment voidFragment;
    PasswordChangeFragment passwordChangeFragment;
    SynchronizationFragment synchronizationFragment;
    String cycle;
    ViewGroup views;
    Card_enquiryFragment card_enquiryFragment;
    Verification_Fragment verification_fragment;
    Update update_fragment;
    Toolbar toolbar;
    String voucheridno = "";
    CardView txn_card, syncreport_card, pass_change, cardactivation_card, tdetail, peningsync_card, sync_card, cardbalance_card, update_card, format_card, settings_card, summary_card, xreports_card, void_txnsCard;
     CardManager cardManager;
    Activity instance;
    public static PaySdk sdkInstance;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        dt = new Details(ProgrammeActivity.this);
        if (dt.getLevel().equalsIgnoreCase("1")) {

            setContentView(R.layout.activity_programme_user);
            setData();

        } else if (dt.getLevel().equalsIgnoreCase("0")) {
            setContentView(R.layout.activity_programme);

            format_card = (CardView) findViewById(R.id.format_card);
            format_card.setOnClickListener(this);

            syncreport_card = (CardView) findViewById(R.id.syncreport_card);
            syncreport_card.setOnClickListener(this);

            peningsync_card = (CardView) findViewById(R.id.ptxn_card);
            peningsync_card.setOnClickListener(this);
        } else {

            startActivity(new Intent(ProgrammeActivity.this, UsernameActivity.class));
            finish();

        }
        fa = this;

        if (dt.getLevel().equalsIgnoreCase("0")) {
            cardactivation_card = (CardView) findViewById(R.id.cardactivation_card);
            cardactivation_card.setOnClickListener(this);
        }
        try {

            toolbar = (Toolbar) findViewById(R.id.toolbar);
            Drawable drawable = getResources().getDrawable(R.drawable.home_icon);
            Bitmap bitmap = ((BitmapDrawable) drawable).getBitmap();
            Drawable newdrawable = new BitmapDrawable(getResources(), Bitmap.createScaledBitmap(bitmap, 50, 50, true));
            toolbar.setNavigationIcon(newdrawable);
            setSupportActionBar(toolbar);
            getSupportActionBar().setTitle("Menus");
            overridePendingTransition(0, 0);
            nfcAdapter = NfcAdapter.getDefaultAdapter(this);
            ur = (TextView) findViewById(R.id.urb);
            dt = new Details(ProgrammeActivity.this);
            net = new Network(ProgrammeActivity.this, "");
            views = (ViewGroup) findViewById(R.id.views);
            container = (ViewGroup) findViewById(R.id.fragment_container);
            views.setVisibility(View.VISIBLE);
            container.setVisibility(View.GONE);

            txn_card = (CardView) findViewById(R.id.txn_card);
            txn_card.setOnClickListener(this);

            home = (ImageView) findViewById(R.id.home_menu);
            home.setOnClickListener(this);

            cardbalance_card = (CardView) findViewById(R.id.cardbalance_card);
            cardbalance_card.setOnClickListener(this);

            pass_change = (CardView) findViewById(R.id.changepassword);
            pass_change.setOnClickListener(this);

            update_card = (CardView) findViewById(R.id.update_card);
            update_card.setOnClickListener(this);

            settings_card = (CardView) findViewById(R.id.settings_card);
            settings_card.setOnClickListener(this);

            tdetail = (CardView) findViewById(R.id.tdetailtxn_card);
            tdetail.setOnClickListener(this);

            summary_card = (CardView) findViewById(R.id.summary_card);
            summary_card.setOnClickListener(this);

            xreports_card = (CardView) findViewById(R.id.xreports_card);
            xreports_card.setOnClickListener(this);

            sync_card = (CardView) findViewById(R.id.sync_card);
            sync_card.setOnClickListener(this);

            void_txnsCard = (CardView) findViewById(R.id.void_txnCard);
            void_txnsCard.setOnClickListener(this);
            // remove void functionality
            //void_txnsCard.setVisibility(View.GONE);
            ur.setText("Logged in as " + dt.getUser());
            ur.setText("Logged in as " + dt.getUser());

            typeface1 = Typeface.createFromAsset(getAssets(), "Roboto-Regular.ttf");
            recyclerView = (RecyclerView) findViewById(R.id.recycler_view);

        } catch (Exception e) {


        }

        boolean hasPermission = (ContextCompat.checkSelfPermission(ProgrammeActivity.this,
                Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED);
        if (!hasPermission) {
            ActivityCompat.requestPermissions(ProgrammeActivity.this,
                    new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                    REQUEST_WRITE_STORAGE);
        }
       instance=this;
        Activity  instance;
        instance=this;
        sdkInstance=PaySdk.getInstance();
        try {
            sdkInstance.init(instance);
        }catch (Exception e){

        }
        new Thread(new Runnable() {
            @Override
            public void run() {
                cardManager = read_nfc_card(com.newpos.libpay.device.card.CardType.INMODE_NFC.getVal(), 10000);
                if (cardManager.getCardStatus()) {
                    try {

                        JSONObject master_card_json = new JSONObject(cardManager.returnData());
                        JSONObject personaldetails = new JSONObject(master_card_json.get("personaldetails").toString());
                        Log.d("##log"," "+personaldetails.toString());

                    }catch(Exception e){

                        Log.d("##log"," "+e.toString());

                    }

                } else {

                    Log.d("##log"," The card status is false ");

                }
            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        if (dt.getLevel().equalsIgnoreCase("0")) {
            getMenuInflater().inflate(R.menu.admin_menu, menu);
        } else {
            getMenuInflater().inflate(R.menu.menu_user, menu);
        }

        return true;
    }

    @Override
    public void onClick(View view, int id) {
        Log.d("#####CLICK", String.valueOf(id));
        dt.setProgrammeid(String.valueOf(id));

        startActivity(new Intent(ProgrammeActivity.this, VouchersActivity.class));
        finish();

    }

    public void showAlert(String title, String message) {
        Log.d("ALERT", "ALERT");

        new SweetAlertDialog(ProgrammeActivity.this, SweetAlertDialog.WARNING_TYPE)
                .setTitleText(title)
                .setContentText(message)
                .setConfirmText("Ok")
                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sDialog) {
                        sDialog.dismissWithAnimation();

                    }
                })
                .show();
    }

    @Override
    public boolean onOptionsItemSelected(final MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.logout) {
            Intent intent1 = new Intent(this, UsernameActivity.class);
            startActivity(intent1);
            finish();

        }
        if (id == R.id.trans) {
            net.postt();
        }

        if (id == R.id.logs) {
            net.UploadLogs();
        }

        if (id == R.id.upload_archives) {
            net.UploadArchivesTxns();
        }

        if (id == R.id.backup) {

            final ProgressDialog progressDialog = ProgressDialog.show(ProgrammeActivity.this, "Backing up the transactions", "Please wait", true,
                    true, new DialogInterface.OnCancelListener() {
                        @Override
                        public void onCancel(DialogInterface dialog) {

                        }
                    });
            progressDialog.setCancelable(false);

            new Thread(new Runnable() {
                @Override
                public void run() {
                    final int res = BackupTxns();

                    ProgrammeActivity.this.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            progressDialog.dismiss();
                            if (res == 0) {
                                new SweetAlertDialog(ProgrammeActivity.this, SweetAlertDialog.ERROR_TYPE)
                                        .setTitleText("Error")
                                        .setContentText("Unable to backup the transactions")
                                        .setConfirmText("Ok")
                                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                            @Override
                                            public void onClick(SweetAlertDialog sDialog) {
                                                sDialog.dismissWithAnimation();

                                            }
                                        })
                                        .show();

                            } else if (res == 1) {
                                new SweetAlertDialog(ProgrammeActivity.this, SweetAlertDialog.SUCCESS_TYPE)
                                        .setTitleText("Great")
                                        .setContentText("Transaction backed up successfully")
                                        .setConfirmText("Ok")
                                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                            @Override
                                            public void onClick(SweetAlertDialog sDialog) {
                                                sDialog.dismissWithAnimation();

                                            }
                                        })
                                        .show();

                            } else if (res == 2) {
                                new SweetAlertDialog(ProgrammeActivity.this, SweetAlertDialog.ERROR_TYPE)
                                        .setTitleText("Sorry")
                                        .setContentText("No transaction to back up")
                                        .setConfirmText("Ok")
                                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                            @Override
                                            public void onClick(SweetAlertDialog sDialog) {
                                                sDialog.dismissWithAnimation();

                                            }
                                        })
                                        .show();

                            }
                        }
                    });
                }
            }).start();
        }
        /*if(id==R.id.printer_test){
            final ProgressDialog progressDialog = ProgressDialog.show(ProgrammeActivity.this, "Printer Test", "Please wait", true,
                    true, new DialogInterface.OnCancelListener() {
                        @Override
                        public void onCancel(DialogInterface dialog) {

                        }
                    });
            progressDialog.setCancelable(false);

            new Thread(new Runnable() {
                @Override
                public void run() {
                    CarryingBillPrint print= new CarryingBillPrint();
                    final int res= print.TestPrint();

                    ProgrammeActivity.this.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            progressDialog.dismiss();
                            if (res == 0) {
                                new SweetAlertDialog(ProgrammeActivity.this, SweetAlertDialog.SUCCESS_TYPE)
                                        .setTitleText("Great")
                                        .setContentText("Print test sucessful")
                                        .setConfirmText("Ok")
                                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                            @Override
                                            public void onClick(SweetAlertDialog sDialog) {
                                                sDialog.dismissWithAnimation();

                                            }
                                        })
                                        .show();

                            } else if (res == -1) {
                                new SweetAlertDialog(ProgrammeActivity.this, SweetAlertDialog.ERROR_TYPE)
                                        .setTitleText("Oops!")
                                        .setContentText("Error doing print test")
                                        .setConfirmText("Ok")
                                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                            @Override
                                            public void onClick(SweetAlertDialog sDialog) {
                                                sDialog.dismissWithAnimation();

                                            }
                                        })
                                        .show();

                            }
                        }
                    });
                }
            }).start();
        }*/
        if(id==R.id.reprint){
            final ProgressDialog progressDialog = ProgressDialog.show(ProgrammeActivity.this, "Reprinting last receipt", "Please wait", true,
                    true, new DialogInterface.OnCancelListener() {
                        @Override
                        public void onCancel(DialogInterface dialog) {

                        }
                    });
            progressDialog.setCancelable(false);

            new Thread(new Runnable() {
                @Override
                public void run() {
                    CarryingBillPrint print= new CarryingBillPrint();
                     List<Reprints> reprintsList= Reprints.listAll(Reprints.class);
                     Reprints r=null;
                     Context c= ProgrammeActivity.this;
                     int result=0;
                     if(reprintsList.size()>0) {
                         r=reprintsList.get(0);
                         String items[]=r.getItems().split("###");
                         List receiptsList= new ArrayList<Quantitydetails>();
                         for(int i =0;i<items.length; i++){
                              String item[]= items[i].split("##");
                              Receipt receipt= new Receipt();
                              receipt.name=item[0];
                              receipt.qty=item[1];
                              receipt.val=item[2];
                              receipt.uom=item[3];
                              receiptsList.add(receipt);
                         }

                          Details dt= new Details(ProgrammeActivity.this);
                          dt.setUser(r.getUsername());
                          dt.setTime(r.getTime());
                          dt.setMac(r.getMacid());
                         final  TBCTransactionReceipt mreceipt= new TBCTransactionReceipt();
                         mreceipt.setDate(r.getDate());
                         mreceipt.setCloseval(r.getClosebal());
                         mreceipt.setOpenbal(r.getOpenbal());
                         mreceipt.setTransval(r.getCharges());
                         mreceipt.setDetails(receiptsList);
                         mreceipt.setVoucherid(r.getVoucherid());
                         mreceipt.setTime(r.getTime());
                         mreceipt.setReceiptNo(r.getReceiptno());
                         mreceipt.setMacid(r.getMacid());
                         mreceipt.setUsername(r.getUsername());
                         mreceipt.setRationNo(r.getRationno());

                         new Thread(new Runnable() {
                             @Override
                             public void run() {
                                 mreceipt.setOwner("Card Holder Copy");
                                 sdkInstance.printReceipt(mreceipt, PaySdk.ReceiptType.tbc_txn,"Card Holder Copy"," ");
                                 mreceipt.setOwner("Merchant Copy");
                                 sdkInstance.printReceipt(mreceipt, PaySdk.ReceiptType.tbc_txn,"Merchant Copy"," ");
                                 new SweetAlertDialog(ProgrammeActivity.this, SweetAlertDialog.SUCCESS_TYPE)
                                         .setTitleText("Great")
                                         .setContentText("Receipt reprint sucessful")
                                         .setConfirmText("Ok")
                                         .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                             @Override
                                             public void onClick(SweetAlertDialog sDialog) {
                                                 sDialog.dismissWithAnimation();

                                             }
                                         })
                                         .show();
                                /* runOnUiThread(new Runnable() {
                                     @Override
                                     public void run() {
                                         alertSuccess();
                                     }
                                 });*/
                             }

                         }).start();
                          //result = res1+res2;
                     }else{
                         result=-3;
                     }
                    final int res=result;
                    ProgrammeActivity.this.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            progressDialog.dismiss();
                            if (res == 0) {
                                new SweetAlertDialog(ProgrammeActivity.this, SweetAlertDialog.SUCCESS_TYPE)
                                        .setTitleText("Great")
                                        .setContentText("Receipt reprint sucessful")
                                        .setConfirmText("Ok")
                                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                            @Override
                                            public void onClick(SweetAlertDialog sDialog) {
                                                sDialog.dismissWithAnimation();

                                            }
                                        })
                                        .show();

                            } else if (res == -1 || res ==-2) {
                                new SweetAlertDialog(ProgrammeActivity.this, SweetAlertDialog.ERROR_TYPE)
                                        .setTitleText("Oops!")
                                        .setContentText("Error doing receipt reprint")
                                        .setConfirmText("Ok")
                                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                            @Override
                                            public void onClick(SweetAlertDialog sDialog) {
                                                sDialog.dismissWithAnimation();

                                            }
                                        })
                                        .show();

                            }
                            else if (res == -3) {
                                new SweetAlertDialog(ProgrammeActivity.this, SweetAlertDialog.ERROR_TYPE)
                                        .setTitleText("Oops!")
                                        .setContentText("No receipt record found")
                                        .setConfirmText("Ok")
                                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                            @Override
                                            public void onClick(SweetAlertDialog sDialog) {
                                                sDialog.dismissWithAnimation();

                                            }
                                        })
                                        .show();

                            }
                        }
                    });
                }
            }).start();
        }
        if (id == android.R.id.home) {
            views.setVisibility(View.VISIBLE);
            container.setVisibility(View.GONE);
            getSupportActionBar().setTitle("Menus");
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {

     /*   Intent intent1 = new Intent(this, UsernameActivity.class);
        startActivity(intent1);
        finish();*/


    }

    @Override
    protected void onResume() {
        super.onResume();
        try {
            enableForegroundDispatchSystem();
        } catch (Exception e) {

        }
    }

    @Override
    public void onPause() {

        super.onPause();
        try {
            disableForegroundDispatchSystem();
        } catch (Exception e) {

        }
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        setIntent(intent);
        PurchaseFragment frag = (PurchaseFragment) getSupportFragmentManager().findFragmentByTag("purchasef");
        Card_enquiryFragment frag1 = (Card_enquiryFragment) getSupportFragmentManager().findFragmentByTag("cardenquiryf");
        CardBalanceFragment frag2 = (CardBalanceFragment) getSupportFragmentManager().findFragmentByTag("cardbal");
        CardActivation frag3 = (CardActivation) getSupportFragmentManager().findFragmentByTag("cardactivation");
        VoidFragment frag4 = (VoidFragment) getSupportFragmentManager().findFragmentByTag("void_trans");
        FormatFragment frag5 = (FormatFragment) getSupportFragmentManager().findFragmentByTag("format");
        PasswordChangeFragment frag6 = (PasswordChangeFragment) getSupportFragmentManager().findFragmentByTag("passwordchange");

        if (!(frag == null)) {
            frag.cardLogic(intent);
        } else if (!(frag1 == null)) {
            // frag.cardLogic(intent);

        } else if (!(frag2 == null)) {
            frag2.cardLogic(intent);

        } else if (!(frag3 == null)) {
            // frag3.cardLogic(intent);

        } else if (!(frag4 == null)) {
            frag4.cardLogic(intent);

        } else if (!(frag5 == null)) {
            frag5.cardLogic(intent);

        } else if (!(frag6 == null)) {
            frag6.cardLogic(intent);

        }
    }


    private void enableForegroundDispatchSystem() {

        Intent intent = new Intent(this, ProgrammeActivity.class).addFlags(Intent.FLAG_RECEIVER_REPLACE_PENDING);

        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, 0);

        IntentFilter[] intentFilters = new IntentFilter[]{};

        nfcAdapter.enableForegroundDispatch(this, pendingIntent, intentFilters, null);
    }

    private void disableForegroundDispatchSystem() {
        nfcAdapter.disableForegroundDispatch(this);
    }


    @Override
    public void onDestroy() {

        super.onDestroy();
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        IntentResult result = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);
        if (result != null) {

            //ScannedCode.setText(result.getContents());
           // cardActivationfragment = new CardActivation();
            try {
                cardActivationfragment.validateCode(result.getContents());
            }catch (Exception e){
                Log.d("##log","  exception "+e.toString());
            }

        } else {

            Toast.makeText(ProgrammeActivity.this, "error while scanning the qr code", Toast.LENGTH_LONG).show();

        }
    }

    @Override
    public void onClick(View view) {
        //Toast.makeText(ProgrammeActivity.this,"CLICKED",Toast.LENGTH_LONG).show();
        switch (view.getId()) {
            case R.id.txn_card: {

                views.setVisibility(View.GONE);
                container.setVisibility(View.VISIBLE);
                getSupportActionBar().setTitle("Purchase");
                newFragment = new PurchaseFragment();
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, newFragment, "purchasef").commit();

                break;

            }
            case R.id.cardactivation_card: {

                views.setVisibility(View.GONE);
                container.setVisibility(View.VISIBLE);
                cardActivationfragment = new CardActivation();
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, cardActivationfragment).commit();
                getSupportActionBar().setTitle("Card Activation");
                break;

                
            }
            case R.id.cardbalance_card: {

                views.setVisibility(View.GONE);
                container.setVisibility(View.VISIBLE);
                cardbalance = new CardBalanceFragment();
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, cardbalance, "cardbal").commit();
                getSupportActionBar().setTitle("Card Balance");
                break;

            }
            case R.id.update_card: {

                getSupportActionBar().setTitle("Update");
                dt.setStatus("update");
                views.setVisibility(View.GONE);
                container.setVisibility(View.VISIBLE);
                update_fragment = new Update();
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, update_fragment).commit();
                break;

            }
            case R.id.settings_card: {

                views.setVisibility(View.GONE);
                container.setVisibility(View.VISIBLE);
                settings_fragment = new Settings_fragment();
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, settings_fragment).commit();
                getSupportActionBar().setTitle("Settings");
                dt.setStatus("settings");
                break;

            }
            case R.id.summary_card: {

                views.setVisibility(View.GONE);
                container.setVisibility(View.VISIBLE);
                zreportFragment = new ZreportFragment();
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, zreportFragment).commit();
                getSupportActionBar().setTitle("Reports");
                break;

            }
            case R.id.xreports_card: {
                views.setVisibility(View.GONE);
                container.setVisibility(View.VISIBLE);
                xreportFragment = new XreportFragment();
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, xreportFragment).commit();
                getSupportActionBar().setTitle("Reports");
                break;

            }
            case R.id.sync_card: {
                startActivity(new Intent(ProgrammeActivity.this, SyncActivity.class));
                finish();
                break;

            }
            case R.id.void_txnCard: {

                views.setVisibility(View.GONE);
                container.setVisibility(View.VISIBLE);
                voidFragment = new VoidFragment();
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, voidFragment, "void_trans").commit();
                getSupportActionBar().setTitle("Void Transaction");
                break;

            }
            case R.id.format_card: {

                views.setVisibility(View.GONE);
                container.setVisibility(View.VISIBLE);
                formatFragment = new FormatFragment();
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, formatFragment, "format").commit();
                getSupportActionBar().setTitle("Format Card");
                break;

            }
            case R.id.tdetailtxn_card: {

                Intent intent1 = new Intent(this, TransactionsLists.class);
                startActivity(intent1);
                finish();
                break;

            }

            case R.id.ptxn_card: {

                views.setVisibility(View.GONE);
                container.setVisibility(View.VISIBLE);
                pendingSyncFragment = new PendingSyncFragment();
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, pendingSyncFragment).commit();
                getSupportActionBar().setTitle("Pending sync");
                break;

            }
            case R.id.syncreport_card: {

                views.setVisibility(View.GONE);
                container.setVisibility(View.VISIBLE);
                syncReports = new SyncReports();
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, syncReports).commit();
                getSupportActionBar().setTitle("Sync Reports");
                break;

            }
            case R.id.changepassword: {
                views.setVisibility(View.GONE);
                container.setVisibility(View.VISIBLE);
                passwordChangeFragment = new PasswordChangeFragment();
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, passwordChangeFragment, "passwordchange").commit();
                getSupportActionBar().setTitle("Change Password");
                break;

            }


        }

    }


    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case REQUEST_WRITE_STORAGE: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    setData();
                } else {
                    Toast.makeText(ProgrammeActivity.this, "The app was not allowed to write to your storage. Hence, it cannot function properly. Please consider granting it this permission", Toast.LENGTH_LONG).show();
                }
            }
        }

    }

    private void setData() {
        Log.i("#####fething", "callledd");

        List<TransactionsListProducts> transaction = TransactionsListProducts.find(TransactionsListProducts.class, null, null, null, "id DESC", "1");

        Log.i("##DDSDSDSDS", String.valueOf(transaction.size()));
        //check whether the db is empty
        if (!(transaction.size() > 0)) {
            final ProgressDialog progressDialog = ProgressDialog.show(ProgrammeActivity.this, "Fetching your transactions", "Please wait", true,
                    true, new DialogInterface.OnCancelListener() {
                        @Override
                        public void onCancel(DialogInterface dialog) {

                        }
                    });
            progressDialog.setCancelable(false);


            new Thread(new Runnable() {
                @Override
                public void run() {
                    final int res = ReadTxns();
                    Log.i("##WDSDSDSD", String.valueOf(res));


                    ProgrammeActivity.this.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            progressDialog.dismiss();
                            if (res > 0) {
                                new SweetAlertDialog(ProgrammeActivity.this, SweetAlertDialog.SUCCESS_TYPE)
                                        .setTitleText("Great")
                                        .setContentText(res + " Transaction has been retrieved")
                                        .setConfirmText("Ok")
                                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                            @Override
                                            public void onClick(SweetAlertDialog sDialog) {
                                                sDialog.dismissWithAnimation();

                                            }
                                        })
                                        .show();
                            } else {

                            }


                        }
                    });

                }
            }).start();


        } else {

        }


    }

    int ReadTxns() {
        String android_id = Settings.Secure.getString(getApplicationContext().getContentResolver(),
                Settings.Secure.ANDROID_ID);
        try {
            File folder = new File(Environment.getExternalStorageDirectory() +
                    File.separator + "Tbc_all_txns");

//Get the text file
            // SimpleDateFormat date = new SimpleDateFormat("yyyy-MM-dd");
            File file = new File(folder, "all_txns.txt");

//Read text from file
            StringBuilder sb1 = new StringBuilder();
            BufferedReader br = new BufferedReader(new FileReader(file));
            String line;

            while ((line = br.readLine()) != null) {
                sb1.append(line + "");

            }
            br.close();
            Log.i("##DSDSDSDSDSS", sb1 + "dfdfd");
            JSONArray ja = new JSONArray(sb1.toString());


            TransactionsListProducts all_txns;

            for (int i = 0; i < ja.length(); i++) {
                all_txns = new TransactionsListProducts();
                all_txns.setProductid(ja.getJSONObject(i).getString("serviceId"));
                all_txns.setProductname(ja.getJSONObject(i).getString("serviceName"));
                all_txns.setQuantity(ja.getJSONObject(i).getString("quantity"));
                all_txns.setTransactiondate(ja.getJSONObject(i).getString("date"));
                all_txns.setUnitofmeasure(ja.getJSONObject(i).getString("Uom"));
                all_txns.setVal(ja.getJSONObject(i).getString("value"));
                all_txns.setDeviceid(android_id);
                all_txns.save();


            }
            if (ja.length() > 0) {
                return ja.length();
            } else {
                return -3;
            }

        } catch (Exception e) {

            Log.i("##DSDSDSDSDSS", e.toString() + "dfdfd");
            return -1;
            //You'll need to add proper error handling here
        }


    }

    int BackupTxns() {
        Log.i("###SDSDSDSD", "callled");
        String android_id = Settings.Secure.getString(getApplicationContext().getContentResolver(),
                Settings.Secure.ANDROID_ID);
        dt.setMac(android_id);
        List<TransactionsListProducts> td = TransactionsListProducts.listAll(TransactionsListProducts.class);
        if (td.size() > 0) {

            try {

                JSONObject obj = new JSONObject();
                JSONArray txnsarray = new JSONArray();
                for (TransactionsListProducts ts : td) {

                    obj = new JSONObject();
                    obj.put("serviceId", ts.getProductid());
                    obj.put("Uom", ts.getUnitofmeasure());
                    obj.put("quantity", ts.getQuantity());
                    obj.put("value", ts.getVal());
                    obj.put("serviceName", ts.getProductname());
                    obj.put("date", ts.getTransactiondate());
                    obj.put("deviceId", android_id);
                    txnsarray.put(obj);

                }
                File folder = new File(Environment.getExternalStorageDirectory() +
                        File.separator + "Tbc_all_txns");

                boolean success = true;

                if (!folder.exists()) {
                    success = folder.mkdirs();

                } else {

                }

                if (success) {
                    // Do something on success
                } else {
                    // Do something else on failure
                }

                SimpleDateFormat date = new SimpleDateFormat("yyyy-MM-dd");
                //create a file and write tha data
                final File file = new File(folder, "all_txns.txt");
                try {
                    if (!file.exists()) {
                        file.createNewFile();
                    }
                    FileOutputStream fOut = new FileOutputStream(file);
                    OutputStreamWriter myOutWriter = new OutputStreamWriter(fOut);
                    myOutWriter.append(txnsarray.toString());
                    myOutWriter.close();
                    fOut.flush();
                    fOut.close();

                    return 1;
                } catch (IOException e) {
                    Log.e("Exception", "File write failed: " + e.toString());
                    return 0;
                }

            } catch (Exception e) {
                Log.i("######ldsldlsl", e.toString());
                return 0;

            }

        } else {
            return 2;
        }


    }
    public CardManager read_nfc_card(int mode, int timeout) {
        final CardInfo cInfo = new CardInfo();
        CardManager cardManager = CardManager.getInstance(instance, mode);
        cardManager.getCard(timeout, new CardListener() {
            @Override
            public void callback(CardInfo cardInfo) {

            }
        });
        return cardManager;
    }

}
