package com.evoucher.compas.evoucher.fragments;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import com.evoucher.compas.evoucher.Adapters.ScrollerViewPager;
import com.evoucher.compas.evoucher.R;
import com.lynx.evoucher.models.Member;


/**
 * Created by JOHNIE on 9/3/2015.
 */
public class Fragment2 extends Fragment implements OnClickListener {
    ScrollerViewPager viewPager;
   ImageView prev,next,error,error1;
    final int CAMERA_CAPTURE = 1;
    final int CAMERA_CAPTURE1 =2;
    private static final String STATE_IMAGE = "STATE_IMAGE";
    //keep track of cropping intent
    final int PIC_CROP = 3;
    final int PIC_CROP1 = 4; private Uri mUri;
    String front_encoded,back_encoded; Bitmap thePic,saved,saved1,thePic2;
    //captured picture uri
    private Uri picUri;
    ImageView picVieww,picView;
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fagment2, container, false);

    }
    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);


    }
    @Override
    public void onClick(View v) {
        Member dt=new Member();
        ScrollerViewPager viewPager = (ScrollerViewPager) getActivity().findViewById(R.id.view_pager);
        if (v.getId() == R.id.text) {
            Intent captureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            //we will handle the returned data in onActivityResult
            startActivityForResult(captureIntent, CAMERA_CAPTURE);
        } else if (v.getId() == R.id.text2) {


            try {
                //use standard intent to capture an image
                Intent captureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                //we will handle the returned data in onActivityResult
                startActivityForResult(captureIntent, CAMERA_CAPTURE1);
            } catch (ActivityNotFoundException anfe) {
                anfe.printStackTrace();
                //display an error message
                String errorMessage = "Whoops - your device doesn't support capturing images!";
                Toast toast = Toast.makeText(getActivity(), errorMessage, Toast.LENGTH_SHORT);
                toast.show();
            }


        } else if (v.getId() == R.id.next) {
            viewPager.setCurrentItem(2, true);
        } else if (v.getId() == R.id.prev){
//            if(picView.getDrawable()!=null&&picVieww.getDrawable()!=null){
                error1.setVisibility(View.GONE);
                Member.front_national_id=front_encoded;
                Member.back_national_id=back_encoded;
                viewPager.setCurrentItem(2, true);

//            }else {
//                if (picView.getDrawable() == null) {
//                    error.setVisibility(View.VISIBLE);
//
//                } else {
//                    error.setVisibility(View.GONE);
//
//                }
//                 if(picVieww.getDrawable() == null) {
//                    error1.setVisibility(View.VISIBLE);
//                }else{
//                     error1.setVisibility(View.GONE);
//                 }
//            }

    }}
    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        //retrieve a reference to the UI button
       ImageView captureBtn = ( ImageView)getView().findViewById(R.id.text);
        ImageView captureBtn1 = ( ImageView)getView().findViewById(R.id.text2);
        //handle button clicks
        captureBtn.setOnClickListener(this);
        captureBtn1.setOnClickListener(this);
        picVieww = (ImageView)getView().findViewById(R.id.picture1);
        picView = (ImageView)getView().findViewById(R.id.picture);
        error = (ImageView)getView().findViewById(R.id.error2);
        error1 = (ImageView)getView().findViewById(R.id.error3);

        prev=(ImageView)getView().findViewById(R.id.next);
        next=(ImageView)getView().findViewById(R.id.prev);
        prev.setOnClickListener(this);
        next.setOnClickListener(this);
        viewPager = (ScrollerViewPager) getActivity().findViewById(R.id.view_pager);
      
    }


   public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == getActivity().RESULT_OK) {
            //user is returning from capturing an image using the camera
            if(requestCode == CAMERA_CAPTURE){
                //get the Uri for the captured image
                picUri = data.getData();
                //carry out the crop operation
                //performCrop();
                // this below code will use for display image without cropping.

                Bitmap bitmap = BitmapFactory.decodeFile(picUri.getPath());
                picView.setImageBitmap(bitmap);
            }
            //user is returning from cropping the image
//            else if(requestCode == PIC_CROP){
//                //get the returned data
//                Bundle extras = data.getExtras();
//                //get the cropped bitmap
//               thePic2 = extras.getParcelable("data");
//                //retrieve a reference to the ImageView
//
//                //display the returned cropped image
//                picView.setImageBitmap(thePic2);
//                ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
//                thePic2.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream);
//                byte[] byteArray = byteArrayOutputStream .toByteArray();
//                front_encoded = Base64.encodeToString(byteArray, Base64.DEFAULT);
//                error.setVisibility(View.GONE);
//            }
            else if(requestCode == CAMERA_CAPTURE1){
                picUri = data.getData();
                //carry out the crop operation
                //performCrop1();
                Bitmap bitmap = BitmapFactory.decodeFile(picUri.getPath());
                picVieww.setImageBitmap(bitmap);

            }
//            else if(requestCode == PIC_CROP1){
//                //get the returned data
//                Bundle extras = data.getExtras();
//                //get the cropped bitmap
//               thePic = extras.getParcelable("data");
//
//                //retrieve a reference to the ImageView
//
//                //display the returned cropped image
//
//                picVieww.setImageBitmap(thePic);
//                ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
//                thePic.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream);
//                byte[] byteArray = byteArrayOutputStream .toByteArray();
//                 back_encoded = Base64.encodeToString(byteArray, Base64.DEFAULT);
//                error1.setVisibility(View.GONE);
//
//        }
    }}

    private void performCrop(){
        //take care of exceptions
        try {
            //call the standard crop action intent (the user device may not support it)
            Intent cropIntent = new Intent("com.android.camera.action.CROP");
            //indicate image type and Uri
            cropIntent.setDataAndType(picUri, "image/*");
            //set crop properties
            cropIntent.putExtra("crop", "true");
            //indicate aspect of desired crop
            cropIntent.putExtra("aspectX", 1);
            cropIntent.putExtra("aspectY", 1);
            //indicate output X and Y
            cropIntent.putExtra("outputX", 410);
            cropIntent.putExtra("outputY", 220);
            //retrieve data on return
            cropIntent.putExtra("return-data", true);
            //start the activity - we handle returning in onActivityResult
            startActivityForResult(cropIntent, PIC_CROP);
        }
        //respond to users whose devices do not support the crop action
        catch(ActivityNotFoundException anfe){
            //display an error message
            String errorMessage = "Whoops - your device doesn't support the crop action!";
            Toast toast = Toast.makeText(getActivity(), errorMessage, Toast.LENGTH_SHORT);
            toast.show();
        }
    }

    private void performCrop1(){
        //take care of exceptions
        try {
            //call the standard crop action intent (the user device may not support it)
            Intent cropIntent = new Intent("com.android.camera.action.CROP");
            //indicate image type and Uri
            cropIntent.setDataAndType(picUri, "image/*");
            //set crop properties
            cropIntent.putExtra("crop", "true");
            //indicate aspect of desired crop
            cropIntent.putExtra("aspectX", 1);
            cropIntent.putExtra("aspectY", 1);
            //indicate output X and Y
            cropIntent.putExtra("outputX", 410);
            cropIntent.putExtra("outputY", 220);
            //retrieve data on return
            cropIntent.putExtra("return-data", true);
            //start the activity - we handle returning in onActivityResult
            startActivityForResult(cropIntent, PIC_CROP1);
        }
        //respond to users whose devices do not support the crop action
        catch(ActivityNotFoundException anfe){
            //display an error message
            String errorMessage = "Whoops - your device doesn't support the crop action!";
            Toast toast = Toast.makeText(getActivity(), errorMessage, Toast.LENGTH_SHORT);
            toast.show();
        }


    }
    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        if (savedInstanceState != null) {
            thePic =(Bitmap)savedInstanceState.getParcelable("BitmapImage");
            thePic2 =(Bitmap)savedInstanceState.getParcelable("BitmapImage2");
            if(thePic!=null) {
                picVieww.setImageBitmap(thePic);



            }
            if(thePic2!=null) {
                picView.setImageBitmap(thePic2);
            }

        }

    }
    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelable("BitmapImage", thePic);
        outState.putParcelable("BitmapImage2", thePic2);


    }


}
