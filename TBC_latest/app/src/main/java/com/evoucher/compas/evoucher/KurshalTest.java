package com.evoucher.compas.evoucher;

import android.annotation.TargetApi;
import android.app.PendingIntent;
import android.content.Intent;
import android.content.IntentFilter;
import android.nfc.NfcAdapter;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.afollestad.materialdialogs.MaterialDialog;
import com.evoucher.compas.evoucher.card.KeyInfoProvider;
import com.evoucher.compas.evoucher.card.SampleAppKeys;
import com.nxp.nfclib.CardType;
import com.nxp.nfclib.KeyType;
import com.nxp.nfclib.NxpNfcLib;
import com.nxp.nfclib.desfire.DESFireFactory;
import com.nxp.nfclib.desfire.DESFireFile;
import com.nxp.nfclib.desfire.EV1ApplicationKeySettings;
import com.nxp.nfclib.desfire.IDESFireEV1;
import com.nxp.nfclib.exceptions.NxpNfcLibException;
import com.nxp.nfclib.interfaces.IKeyData;

import org.json.JSONObject;

import java.security.NoSuchAlgorithmException;
import java.util.Arrays;

import javax.crypto.Cipher;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.IvParameterSpec;

public class KurshalTest extends AppCompatActivity {

    private IKeyData objKEY_2KTDES_ULC = null;
    private IKeyData objKEY_2KTDES = null;
    private IKeyData objKEY_AES128 = null;
    private byte[] default_ff_key = null;
    private IKeyData default_zeroes_key = null;
    private NxpNfcLib libInstance = null;
    private IDESFireEV1 desFireEV1;
    private CardType mCardType = CardType.UnknownCard;
    private Cipher cipher = null;
    private byte[] bytesKey = null;
    private IvParameterSpec iv = null;
    MaterialDialog pDialogg;
    private static final String KEY_APP_MASTER = "This is my key  ";
    private static final String ALIAS_KEY_AES128 = "key_aes_128";
    private static final String ALIAS_KEY_2KTDES = "key_2ktdes";
    private static final String ALIAS_KEY_2KTDES_ULC = "key_2ktdes_ulc";
    private static final String ALIAS_DEFAULT_FF = "alias_default_ff";
    private static final String ALIAS_KEY_AES128_ZEROES = "alias_default_00";
    private static final String EXTRA_KEYS_STORED_FLAG = "keys_stored_flag";
    static String packageKey = "b927aab8842ab54cbaf2ea1df9917159";
    byte[] appId = new byte[]{0x1, 0x00, 0x00};
    int fileSize = 100;
    byte[] data = new byte[]{0x11, 0x11, 0x11, 0x11,
            0x11};
    int timeOut = 4000;
    int fileNo = 0;
    /**
     * Flag to indicate whether card should be activated....
     */
    boolean aflag = false;
    NfcAdapter nfcAdapter;
    MaterialDialog dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initializeLibrary();
        initializeKeys();
        initializeCipherinitVector();

        try {
            nfcAdapter = NfcAdapter.getDefaultAdapter(this);


        } catch (Exception e) {
            Log.i("###EEEEE", e.toString());
        }

    }

    @Override
    protected void onPause() {
        libInstance.stopForeGroundDispatch();
        super.onPause();
    }


    @Override
    protected void onResume() {
        libInstance.startForeGroundDispatch();
        super.onResume();
    }


    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        setIntent(intent);
        if (aflag) {
            CardType type = CardType.UnknownCard;
            try {
                type = libInstance.getCardType(intent);
            } catch (NxpNfcLibException ex) {
                // handle exception
            }

            switch (type) {

                case DESFireEV1: {
                    mCardType = CardType.DESFireEV1;
                    desFireEV1 = DESFireFactory.getInstance().getDESFire(libInstance.getCustomModules());
                    try {
                        try {
                            dialog.dismiss();
                        } catch (Exception e) {

                        }
                        desFireEV1.getReader().connect();
                        desFireEV1.getReader().setTimeout(2000);
                        desfireEV1CardLogic();


                    } catch (Exception t) {
                        // handle exception here

                    }
                    break;
                }
                case DESFireEV2:{
                     //
                }
                case MIFAREClassic:{
                    //
                }
                case MIFAREClassicEV1:{
                    //
                }


            }
        }
    }


    private void enableForegroundDispatchSystem() {

        Intent intent = new Intent(this, Beneficairy_Activity.class).addFlags(Intent.FLAG_RECEIVER_REPLACE_PENDING);

        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, 0);

        IntentFilter[] intentFilters = new IntentFilter[]{};

        nfcAdapter.enableForegroundDispatch(this, pendingIntent, intentFilters, null);
    }

    @TargetApi(19)
    private void initializeLibrary() {

        libInstance = NxpNfcLib.getInstance();
        try {
            libInstance.registerActivity(KurshalTest.this, packageKey);
        } catch (NxpNfcLibException ex) {
            // Toast.makeText(this, ex.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    /**
     * Initialize the Cipher and init vector of 16 bytes with 0xCD.
     */


    private void desfireEV1CardLogic() {
        pDialogg = new MaterialDialog.Builder(KurshalTest.this)
                .title("Card Activation")
                .content("Please dont remove the card...")
                .progress(true, 0)
                .progressIndeterminateStyle(false)
                .show();
        pDialogg.setCancelable(false);
        new ActivateCard().execute();


    }

    private void initializeKeys() {
        KeyInfoProvider infoProvider = KeyInfoProvider.getInstance(KurshalTest.this.getApplicationContext());
        infoProvider.setKey(ALIAS_KEY_2KTDES, SampleAppKeys.EnumKeyType.EnumDESKey, SampleAppKeys.KEY_2KTDES);
        infoProvider.setKey(ALIAS_KEY_AES128, SampleAppKeys.EnumKeyType.EnumAESKey, SampleAppKeys.KEY_AES128);
        infoProvider.setKey(ALIAS_KEY_AES128_ZEROES, SampleAppKeys.EnumKeyType.EnumAESKey, SampleAppKeys.KEY_AES128_ZEROS);
        infoProvider.setKey(ALIAS_DEFAULT_FF, SampleAppKeys.EnumKeyType.EnumMifareKey, SampleAppKeys.KEY_DEFAULT_FF);
        objKEY_2KTDES_ULC = infoProvider.getKey(ALIAS_KEY_2KTDES_ULC, SampleAppKeys.EnumKeyType.EnumDESKey);
        objKEY_2KTDES = infoProvider.getKey(ALIAS_KEY_2KTDES, SampleAppKeys.EnumKeyType.EnumDESKey);
        objKEY_AES128 = infoProvider.getKey(ALIAS_KEY_AES128, SampleAppKeys.EnumKeyType.EnumAESKey);
        default_zeroes_key = infoProvider.getKey(ALIAS_KEY_AES128_ZEROES, SampleAppKeys.EnumKeyType.EnumAESKey);
        default_ff_key = infoProvider.getMifareKey(ALIAS_DEFAULT_FF);
    }


    private void initializeCipherinitVector() {

		/* Initialize the Cipher */
        try {
            cipher = Cipher.getInstance("AES/CBC/NoPadding");
        } catch (NoSuchAlgorithmException e) {

            e.printStackTrace();
        } catch (NoSuchPaddingException e) {
            e.printStackTrace();
        }

		/* set Application Master Key */
        bytesKey = KEY_APP_MASTER.getBytes();

		/* Initialize init vector of 16 bytes with 0xCD. It could be anything */
        byte[] ivSpec = new byte[16];
        Arrays.fill(ivSpec, (byte) 0xCD);
        iv = new IvParameterSpec(ivSpec);

    }


    private class ActivateCard extends AsyncTask<String, String, String> {

        @Override
        protected String doInBackground(String... params) {
            // get the card UID here

            String uidhex = new String();
            byte[] uid = desFireEV1.getUID();
            for (int i = 0; i < uid.length; i++) {
                String x = Integer.toHexString(((int) uid[i] & 0xff));
                if (x.length() == 1) {
                    x = '0' + x;
                }
                uidhex += x;
            }

            return null;
        }


        protected void onPostExecute(final String result) {

            if (result.equalsIgnoreCase("Ok")) {
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        try {

                            // when writing/ creating structures
                            desFireEV1.getReader().setTimeout(timeOut);
                            desFireEV1.selectApplication(0);
                            desFireEV1.selectApplication(appId);
                            //Do authentication here
                            desFireEV1.authenticate(0, IDESFireEV1.AuthType.Native, KeyType.TWO_KEY_THREEDES, objKEY_2KTDES);

                            // you could format if its a new card
                            /*desFireEV1.selectApplication(0);
                            desFireEV1.authenticate(0, IDESFireEV1.AuthType.Native, KeyType.THREEDES, objKEY_2KTDES);
                            desFireEV1.format();*/

                            EV1ApplicationKeySettings.Builder appsetbuilder = new EV1ApplicationKeySettings.Builder();
                            EV1ApplicationKeySettings appsettings = appsetbuilder.setAppKeySettingsChangeable(true)
                                    .setAppMasterKeyChangeable(true)
                                    .setAuthenticationRequiredForApplicationManagement(false)
                                    .setAuthenticationRequiredForDirectoryConfigurationData(false)
                                    .setKeyTypeOfApplicationKeys(KeyType.TWO_KEY_THREEDES).build();
                            //// when writing/ creating sectors
                            desFireEV1.createApplication(appId, appsettings);
                            desFireEV1.selectApplication(appId);
                             // create sector0
                            desFireEV1.createFile(0, new DESFireFile.StdDataFileSettings(
                                    IDESFireEV1.CommunicationType.Plain, (byte) 0, (byte) 0, (byte) 0, (byte) 0, 1000));
                            // create sector1
                            desFireEV1.createFile(1, new DESFireFile.StdDataFileSettings(
                                    IDESFireEV1.CommunicationType.Plain, (byte) 0, (byte) 0, (byte) 0, (byte) 0, 1000));
                            // create sector2
                            desFireEV1.createFile(2, new DESFireFile.StdDataFileSettings(
                                    IDESFireEV1.CommunicationType.Plain, (byte) 0, (byte) 0, (byte) 0, (byte) 0, 100));

                            //write Data
                            desFireEV1.authenticate(0, IDESFireEV1.AuthType.Native, KeyType.TWO_KEY_THREEDES, objKEY_2KTDES);
                            desFireEV1.writeData(0, 0, "provide your personal deatils here".trim().getBytes());
                            desFireEV1.writeData(1, 0, "Provide your card pin details here".trim().getBytes());
                            desFireEV1.getReader().close();
                        // reading card data
                            desFireEV1.authenticate(0, IDESFireEV1.AuthType.Native, KeyType.TWO_KEY_THREEDES, objKEY_2KTDES);
                             JSONObject  results0 =new JSONObject(new String(desFireEV1.readData(0, 0, 0)));
                             JSONObject  results1 =new JSONObject(new String(desFireEV1.readData(1, 0, 0)));

                        } catch (final Exception err) {
                            //handle exception here
                        }
                    }
                }).start();


            } else if (result.equalsIgnoreCase("0")) {
                //  handel the scenario here

            }
        }

    }
}
