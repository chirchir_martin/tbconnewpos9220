package com.evoucher.compas.evoucher.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import com.evoucher.compas.evoucher.Adapters.CartAdapter;
import com.evoucher.compas.evoucher.Adapters.SyncAdapter;
import com.evoucher.compas.evoucher.CartActivity;
import com.evoucher.compas.evoucher.Details;
import com.evoucher.compas.evoucher.ItemClickListener;
import com.evoucher.compas.evoucher.R;
import com.lynx.evoucher.models.Beneficiary;
import com.lynx.evoucher.models.Products;
import com.lynx.evoucher.models.SyncLogs;
import com.lynx.evoucher.models.Topups;
import com.lynx.evoucher.models.Transactions;

import java.util.ArrayList;
import java.util.List;

import jp.wasabeef.recyclerview.adapters.AlphaInAnimationAdapter;

/**
 * Created by Administrator on 10/25/2016.
 */
public class SyncReports extends Fragment implements ItemClickListener {
    RecyclerView recyclerView;
    List<Transactions> trans;
    TextView count, bid, samt, vamt, nsales, no;
    Details dt;
    ViewGroup x;
    ArrayList<String> programIDs = new ArrayList<String>();

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.syncreport_fragment, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        ViewGroup empty = (ViewGroup) getView().findViewById(R.id.empty);
        ViewGroup vlist = (ViewGroup) getView().findViewById(R.id.lview);


        try {
            List<SyncLogs> synclogs = SyncLogs.listAll(SyncLogs.class);

            if (synclogs.size() > 0) {

                RecyclerView tv = (RecyclerView) getView().findViewById(R.id.sync_list);
                tv.setHasFixedSize(true);
                SyncAdapter ad = new SyncAdapter(getActivity(), synclogs);


                tv.setLayoutManager(new LinearLayoutManager(getActivity().getApplicationContext()));

                AlphaInAnimationAdapter alphaAdapter = new AlphaInAnimationAdapter(ad);
                alphaAdapter.setDuration(1500);
                tv.setAdapter(alphaAdapter);


                tv.setAdapter(ad);
                vlist.setVisibility(View.VISIBLE);
                empty.setVisibility(View.GONE);
            } else {
                vlist.setVisibility(View.GONE);
                empty.setVisibility(View.VISIBLE);
            }
        } catch (Exception e) {

        }
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    public void onClick(View view, int pos) {


    }
}
