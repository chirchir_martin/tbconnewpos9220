package com.evoucher.compas.evoucher.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.evoucher.compas.evoucher.Adapters.ProgrammesAdapter;
import com.evoucher.compas.evoucher.Adapters.ReportsAdapter;
import com.evoucher.compas.evoucher.BeneficiaryList;
import com.evoucher.compas.evoucher.Details;
import com.evoucher.compas.evoucher.ItemClickListener;
import com.evoucher.compas.evoucher.R;
import com.evoucher.compas.evoucher.ReportsActivity;
import com.lynx.evoucher.models.Beneficiary;
import com.lynx.evoucher.models.BeneficiaryGroup;
import com.lynx.evoucher.models.CardBlock;
import com.lynx.evoucher.models.Products;
import com.lynx.evoucher.models.Programmes;
import com.lynx.evoucher.models.Reports;
import com.lynx.evoucher.models.Topups;
import com.lynx.evoucher.models.TopupsLogs;
import com.lynx.evoucher.models.Transactions;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import jp.wasabeef.recyclerview.adapters.AlphaInAnimationAdapter;

/**
 * Created by Administrator on 10/25/2016.
 */
public class ZreportFragment extends Fragment  implements ItemClickListener {
    RecyclerView recyclerView;
    List<Transactions> trans;
    TextView count,bid,samt,tps,vamt,nsales,no,tc,cb;
    Details dt;
    ViewGroup x;
    ArrayList<String> programIDs=new ArrayList<String>();
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.zreport_fragment, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        dt= new Details(getActivity());
        count = (TextView) getView().findViewById(R.id.count);
        bid = (TextView) getView().findViewById(R.id.bid);
        samt = (TextView) getView().findViewById(R.id.bgrp);
        samt.setVisibility(View.GONE);
        vamt = (TextView) getView().findViewById(R.id.vrs);
        tps = (TextView) getView().findViewById(R.id.svs);

       tc = (TextView) getView().findViewById(R.id.txn_count);
       cb = (TextView) getView().findViewById(R.id.cards_blocked);
        try {
            vamt.setText("Total Commodities             " + String.valueOf(Products.count(Products.class, null, null)));
            count.setText("Total Card Holders:             " + String.valueOf(Beneficiary.count(Beneficiary.class, null, null)));
            bid.setText("Total topups                        " + String.valueOf(Topups.count(Topups.class, null, null)));
            tc.setText("Total transactions                " + String.valueOf(Transactions.count(Transactions.class, null, null)));
            cb.setText("Blocked Cards                      " + String.valueOf(CardBlock.count(CardBlock.class, null, null)));

            String[] params = {"0"};
            long ptxns = TopupsLogs.count(TopupsLogs.class, "isuploaded =?", params);
            tps.setText("Total Topups Logs              " + String.valueOf(ptxns >0? ptxns:0));

        }catch (Exception e){

        }
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    public void onClick(View view, int pos) {


    }
}
