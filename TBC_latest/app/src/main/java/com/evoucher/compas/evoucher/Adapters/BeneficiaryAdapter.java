package com.evoucher.compas.evoucher.Adapters;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.evoucher.compas.evoucher.BeneficiaryList;
import com.evoucher.compas.evoucher.R;
import com.lynx.evoucher.models.Beneficiary;
import com.lynx.evoucher.models.Member;

import java.util.ArrayList;
import java.util.List;

import cn.pedant.SweetAlert.SweetAlertDialog;


/**
 * Created by Kibet on 9/2/2016.
 */
public class BeneficiaryAdapter extends ArrayAdapter<Beneficiary> {

    private List<Beneficiary> beneficiaries;
    Context context;
    String tag;


    public class MyViewHolder {
        public ImageView main_icon;
        public TextView name;
    }

    public BeneficiaryAdapter(Context context,ArrayList<Beneficiary> beneficiaries) {
        super(context, R.layout.beneficiary_list_row, beneficiaries);
        this.beneficiaries = beneficiaries;
        this.context=context;

    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // Get the data item for this position
        // Check if an existing view is being reused, otherwise inflate the view
        MyViewHolder viewHolder; // view lookup cache stored in tag
        if (convertView == null) {
            // If there's no view to re-use, inflate a brand new view for row
            viewHolder = new MyViewHolder();
            LayoutInflater inflater = LayoutInflater.from(getContext());
            convertView = inflater.inflate(R.layout.beneficiary_list_row, parent, false);

            viewHolder.main_icon=(ImageView) convertView.findViewById(R.id.user);
            viewHolder.name=(TextView)convertView.findViewById(R.id.beneficiary_name);
            // Cache the viewHolder object inside the fresh view
            convertView.setTag(viewHolder);
        } else {
            // View is being recycled, retrieve the viewHolder object from tag
            viewHolder = (MyViewHolder) convertView.getTag();
        }
        // Populate the data into the template view using the data object
        String pic=beneficiaries.get(position).getUser_image();
        Bitmap decodedByte=null;
        if(pic!=null) {
            byte[] decodedString = Base64.decode(pic, Base64.DEFAULT);
            decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
            viewHolder.main_icon.setImageBitmap(decodedByte);

        }else{
            viewHolder.main_icon.setImageResource(R.drawable.user4);
        }

        Typeface typeface1 = Typeface.createFromAsset(context.getAssets(), "Roboto-Regular.ttf");
        viewHolder.name.setTypeface(typeface1);
       // viewHolder.name.setText(beneficiaries.get(position).getLastname().toUpperCase()+" "+beneficiaries.get(position).getFirstname().toUpperCase());
        // Return the completed view to render on screen
        return convertView;
    }

}

