package com.evoucher.compas.evoucher.ui;

import android.content.Context;
import android.widget.Toast;

import com.android.desert.keyboard.InputInfo;
import com.android.desert.keyboard.InputManager;
import com.newpos.libpay.device.card.CardInfo;
import com.newpos.libpay.device.pinpad.PinInfo;
import com.newpos.libpay.device.pinpad.PinType;
import com.newpos.libpay.device.pinpad.PinpadListener;
import com.newpos.libpay.device.scanner.QRCInfo;
import com.newpos.libpay.presenter.TransInterface;
import com.newpos.libpay.trans.translog.TransLogData;

/**
 * Created by Chirchir on 5/18/2018.
 */

public class PrintingHandler  implements  TransInterface {
    Context mContext ;
    public PrintingHandler(Context  context){
        mContext=context;
    }

    public  InputInfo getInput(InputManager.Mode type){
        return  null;
    }


    public CardInfo getCard(int mode){
        return  null;
    }

    public QRCInfo getQRCInfo(InputManager.Style mode){
        return  null;
    }

    /**
     * get PIN from PIN PAD
     * @param type refer to @{@link PinType}
     * @return PinInfo @{@link PinInfo}
     * @attention  deal with PIN pad, refer to@{@link com.newpos.libpay.device.pinpad.PinpadManager}
     * @attention  @{@link com.newpos.libpay.device.pinpad.PinpadManager#getPin(int, PinType, PinpadListener)}
     */
    public PinInfo getPinpadPin(PinType type) {
        return  null;
    }
    /**
     * notice user confirm card number
     * @param cn card number
     * @return 0:user confirm  others:user cancel
     */
    public int confirmCardNO(String cn) {
        return 0;
    }
    /**
     * show and select card app
     * @param list card app list
     * @return card app index
     * @attention index start with 0
     */
    public int choseAppList(String[] list){
        return  0;
    }

    /**
     * before GPO of EMV process.
     * this callback to the user's purpose is to take into account a lot of special EMV process requirements
     */
    public void beforeGPO(){

    }

    /**
     * show transaction status
     * @param status Trans Status
     */
    public void handling(int status){
        if(status==327){
            Toast.makeText(mContext,"Paper lack",Toast.LENGTH_LONG);
        }if(status==0){
            Toast.makeText(mContext,"Error printing the receipt",Toast.LENGTH_LONG);
        }
    }

    /**
     * 人机交互显示接口(确认原交易信息)notice user confirm transaction information
     * @param logData refer to @{@link TransLogData}
     * @return 0:user confirm  others:user cancel
     */
    public int confirmTransInfo(TransLogData logData){
        return  0;
    }

    /**
     * check card identity information
     * @param info card identity information
     * @return 0:user confirm  others:user cancel
     */
    public int confirmCardVerifyCert(String info) {
        return  0;
    }

    /**
     * show transaction succeed
     * @param code transaction code @{@link com.newpos.libpay.trans.Tcode}
     * @param args addition parameters
     */
    public void trannSuccess(int code, String... args) {

    }
    /**
     * show error information
     * @param errcode refer to @{@link com.newpos.libpay.trans.Tcode}
     */
    public void showError(int errcode){

    }
}

