package com.evoucher.compas.evoucher.fragments;

import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.evoucher.compas.evoucher.Adapters.ScrollerViewPager;
import com.evoucher.compas.evoucher.R;
import com.lynx.evoucher.models.Member;
import com.rengwuxian.materialedittext.MaterialEditText;
import com.rey.material.widget.RadioButton;
import com.rey.material.widget.Spinner;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;
import java.util.Calendar;
import cn.pedant.SweetAlert.SweetAlertDialog;

/**
 * Created by JOHNIE on 9/3/2015.
 */
public class Fragment1 extends Fragment implements DatePickerDialog.OnDateSetListener,View.OnClickListener {

  //  TextView date,tv,tv1,tv2,tv3,tv4,tv5,tv6,tv7;
    TextView date,tv7;
    int n = Color.parseColor("#212121");
    private int bgRes;
    ImageView ib,error;
    String dateee;
    ScrollerViewPager viewPager;
    Spinner spinner;
    private ImageView imageView;

  MaterialEditText  me;

    MaterialEditText middle;

    MaterialEditText lastname;



    MaterialEditText id;
   // MaterialEditText nationality;
    MaterialEditText title;
    RadioButton rb1;
    RadioButton rb2;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment1, container, false);

    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        rb1 = (RadioButton)getView().findViewById(R.id.switches_rb1);
        rb2 = (RadioButton)getView().findViewById(R.id.switches_rb2);


        CompoundButton.OnCheckedChangeListener listener = new CompoundButton.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked){
                    rb1.setChecked(rb1 == buttonView);
                    rb2.setChecked(rb2 == buttonView);

                }

            }

        };

        rb1.setOnCheckedChangeListener(listener);
        rb2.setOnCheckedChangeListener(listener);

       me = ( MaterialEditText) getView().findViewById(R.id.firstname);
/*
        tv = (TextView) getView().findViewById(R.id.lab);
        tv1 = (TextView) getView().findViewById(R.id.lab1);
        tv2 = (TextView) getView().findViewById(R.id.lab2);
        tv3 = (TextView) getView().findViewById(R.id.lab3);
        tv4 = (TextView) getView().findViewById(R.id.lab4);*/
//        tv6 = (TextView) getView().findViewById(R.id.lab6);
        tv7 = (TextView) getView().findViewById(R.id.rad);
     id = ( MaterialEditText) getView().findViewById(R.id.textVie);

    middle = ( MaterialEditText) getView().findViewById(R.id.middlename);
      //  nationality = (MaterialEditText) getView().findViewById(R.id.nationality);

        //tv5 = (TextView) getView().findViewById(R.id.lab5);
       lastname = ( MaterialEditText) getView().findViewById(R.id.lastname);
        ib =
                (ImageView) getView().findViewById(R.id.next);
      //  error = (ImageView) getView().findViewById(R.id.error);
        viewPager = (ScrollerViewPager) getActivity().findViewById(R.id.view_pager);
        ib.setOnClickListener(this);
        Typeface typeface1 = Typeface.createFromAsset(getActivity().getAssets(), "Roboto-Regular.ttf");
        android.widget.Spinner spinnerr = (android.widget.Spinner)getView(). findViewById(R.id.nationality0);
        // Create an ArrayAdapter using the string array and a default spinner layout
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(getActivity(),
                R.array.planets_arrayy, android.R.layout.simple_spinner_item);
// Specify the layout to use when the list of choices appears

        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        tv7.setText("Gender"+Html.fromHtml("<sup>*</sup>"));
        spinnerr.setAdapter(adapter);

//        TextView myTextView=(TextView)getView().findViewById(R.id.textView0);
        Typeface typeface = Typeface.createFromAsset(getActivity().getAssets(), "Roboto-Thin.ttf");
      me.setTypeface(typeface1);
       id.setTypeface(typeface1);
     //  title.setTypeface(typeface1);
        rb1.setTypeface(typeface1);
        rb2.setTypeface(typeface1);
       /* tv.setTypeface(typeface1);
        tv1.setTypeface(typeface1);
        tv2.setTypeface(typeface1);
        tv3.setTypeface(typeface1);
        tv4.setTypeface(typeface1);
        tv5.setTypeface(typeface1);*/
//        tv6.setTypeface(typeface1);
        tv7.setTypeface(typeface1);
     //nationality.setTypeface(typeface);

       // tv3.setText("PassportID/NationalID"+Html.fromHtml("<sup>*</sup>"));
        lastname.setTypeface(typeface1);
        middle.setTypeface(typeface1);
        // myTextView.setTypeface(typeFace);

        date = (TextView) getView().findViewById(R.id.dob);
        date.setTypeface(typeface1);
        date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar now = Calendar.getInstance();
                DatePickerDialog dpd = DatePickerDialog.newInstance(
                        Fragment1.this,
                        now.get(Calendar.YEAR),
                        now.get(Calendar.MONTH),
                        now.get(Calendar.DAY_OF_MONTH)
                );

                dpd.show(getActivity().getFragmentManager(), "Datepickerdialog");
            }
        });
        // spinner = (Spinner) getView().findViewById(R.id.department);
// Create an ArrayAdapter using the string array and a default spinner layout
        // ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(getActivity(),
        //   R.array.planets_array, R.layout.row_spn);
// Specify the layout to use when the list of choices appears
        // adapter.setDropDownViewResource(R.layout.row_spn_dropdown);
// Apply the adapter to the spinner
        //  spinner.setAdapter(adapter);


    }
    public void showAlert(){
        Log.d("ALERT","ALERT");
        new SweetAlertDialog(getActivity(), SweetAlertDialog.WARNING_TYPE)
                .setTitleText("Empty fields!!")
                .setContentText("Please ensure that all fields are correctly filled")
                .setConfirmText("Ok")
                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sDialog) {
                        sDialog.dismissWithAnimation();

                    }
                })
                .show();
    }
    @Override
    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
        dateee = (++monthOfYear)+ "/" +dayOfMonth   + "/" + year;
        Log.d("#####DATE",dateee);

        date.setText(dateee);
        date.setTextColor(n);


    }

    @Override
    public void onClick(View v) {
        if((me.getText().toString().equalsIgnoreCase("")==false)
                &&(middle.getText().toString().equalsIgnoreCase("")==false)
                &&(lastname.getText().toString().equalsIgnoreCase("")==false)
                &&(rb1.isChecked()==true||rb2.isChecked()==true)){
            if(rb1.isChecked()){
                Member.gender ="M";
                Log.d("SSSISISIISS", String.valueOf(rb1.isChecked()+"male"));
            }else if(rb2.isChecked()){
                Member.gender ="F";
                Log.d("SSSISISIISS", String.valueOf(rb1.isChecked()+"female"));
            }
            Member.firstname=me.getText().toString();
            Member.middlename=middle.getText().toString();
            Member.lastname=lastname.getText().toString();
            Member.date_of_birth=dateee;
           // Member.title=title.getText().toString();
             //dt.setNationality(nationality.getText().toString());
            Member.national_id=id.getText().toString();
             //dt.setOccupation(occupation.getText().toString());

            viewPager.setCurrentItem(1, true);
        }else{
            showAlert();
        }

    }

}