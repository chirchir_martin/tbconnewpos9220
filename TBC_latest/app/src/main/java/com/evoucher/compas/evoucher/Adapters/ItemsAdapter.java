package com.evoucher.compas.evoucher.Adapters;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.evoucher.compas.evoucher.Details;
import com.evoucher.compas.evoucher.ItemClickListener;
import com.lynx.evoucher.models.Products;
import com.lynx.evoucher.models.Topups;
import com.evoucher.compas.evoucher.R;

import org.json.JSONArray;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Administrator on 8/17/2016.
 */
public class ItemsAdapter extends RecyclerView.Adapter<ItemsAdapter.MyViewHolder> {
    private ItemClickListener clickListener;
    private List<Products> products;
    Context context;
    List<String> productsIds,prices;
    CardView card;
    List<Topups> tps;
    JSONArray tops;
    public TextView  d;
    String tag;
    Details dt;

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        public TextView title, price, d,qty;
        ImageView im;
        CardView card;
        public MyViewHolder(View view) {
            super(view);

            im=(ImageView)view.findViewById(R.id.main);
            title=(TextView)view.findViewById(R.id.title);
            price=(TextView)view.findViewById(R.id.price);
            qty=(TextView)view.findViewById(R.id.qty);
            card=(CardView)view.findViewById(R.id.cardview);
            card.setOnClickListener(this);

        }

        @Override
        public void onClick(View view) {
            if (clickListener != null)
                tag=view.getTag(R.id.holder).toString();
            clickListener.onClick(view,Integer.parseInt(tag));
        }
    }


    public ItemsAdapter(Context context, List<Products> products,List<String> pids,List<String> ppprices) {

        this.products = products;
        this.context=context;
        this.productsIds=pids;
        this.prices=ppprices;
        dt=new Details(context);
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_row, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {

        Typeface typeface = Typeface.createFromAsset(context.getAssets(), "Roboto-Thin.ttf");
        holder.title.setTypeface(typeface);
        holder.price.setTypeface(typeface);
        holder.qty.setTypeface(typeface);
        holder.title.setText(products.get(position).getproductname());
        holder.price.setText("(" +products.get(position).getUom()+")");
       /* tps=Topups.find(Topups.class, "voucherid = ? and cardnumber = ?",dt.getVoucherid(),dt.getCardnumber());
        String vtp=tps.get(0).getvocherType();
        if(tps.size()>0) {

            if (vtp.equalsIgnoreCase("CM")) {
                holder.qty.setText("(" + tps.get(0).getProductQuantity() + ")");
                holder.price.setText("Ksh " + tps.get(0).getProductPrice());
            } else {
                holder.price.setText("(Ksh " + dt.getValuevoucher() + ")");

            }
        }*/
        holder.card.setTag(R.id.holder,products.get(position).getProductid());
        try {
            Log.d("sdsd0dsdsd", products.get(position).getimage() + position);
            Bitmap decodedByte = null;
            byte[] decodedString = Base64.decode(products.get(position).getimage(), Base64.DEFAULT);
            decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
            holder.im.setImageBitmap(decodedByte);
        }catch (Exception e){

        }



    }

    @Override
    public int getItemCount() {
        return products.size();
    }
    public void setClickListener(ItemClickListener itemClickListener) {

        this.clickListener = itemClickListener;
    }

}