package com.evoucher.compas.evoucher;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.DatePicker;

import com.evoucher.compas.evoucher.Adapters.PageAdapter;
import com.evoucher.compas.evoucher.Adapters.ScrollerViewPager;
import com.evoucher.compas.evoucher.ui.SpringIndicator;
import com.google.common.collect.Lists;
import com.lynx.evoucher.models.Beneficiary;

import java.text.SimpleDateFormat;
import java.util.List;


/**
 * Created by Kibet on 9/2/2016.
 */
public class BeneficiaryCreation extends AppCompatActivity
        implements DatePickerDialog.OnDateSetListener{
    SimpleDateFormat ft=new SimpleDateFormat("HH");
    SimpleDateFormat ftt=new SimpleDateFormat("mm");
    int n= Color.parseColor("#212121");
    ScrollerViewPager viewPager;
    private boolean mCanQuit = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.beneficiary_creation);
        overridePendingTransition(0, 0);
        try {
            if (getIntent().getAction().equalsIgnoreCase("android.hardware.usb.action.USB_DEVICE_ATTACHED")) {
                try {

                    if (getIntent().getStringExtra("w").equalsIgnoreCase("YES")) {

                    } else {
                          /*  startActivity(new Intent(MainActivity.this, Login.class));
                            finish();*/
                    }

                } catch (Exception e) {
                    startActivity(new Intent(BeneficiaryCreation.this, ProgrammeActivity.class));
                    finish();
                }
            }
        } catch (Exception e) {

        }
        System.out.println("Current Intent## "+getIntent().getAction());
        viewPager = (ScrollerViewPager) findViewById(R.id.view_pager);
        SpringIndicator springIndicator = (SpringIndicator) findViewById(R.id.indicator);
        Toolbar toolbar = (Toolbar) findViewById(R.id.tool_bar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(false);

        PageAdapter adap=new PageAdapter(getSupportFragmentManager());
        viewPager.setAdapter(adap);
        viewPager.fixScrollSpeed();
        viewPager.setOffscreenPageLimit(5);
        viewPager.setPagingEnabled(true);
        // viewPager.setPageTransformer(true, new DepthPageTransformer());

        // just set viewPager
        springIndicator.setViewPager(viewPager);

    }

    private List<String> getTitles(){
        return Lists.newArrayList("1", "2", "3", "4","5","6");
    }

    private List<Integer> getBgRes(){
        return Lists.newArrayList(R.drawable.bg1, R.drawable.bg2, R.drawable.bg3, R.drawable.bg4);
    }



    @Override
    public void onBackPressed() {
        Intent intent1 = new Intent(this, BeneficiaryList.class);
        startActivity(intent1);
        finish();


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        //getMenuInflater().inflate(R.menu.beneficiary_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if(id == android.R.id.home){
            startActivity(new Intent(BeneficiaryCreation.this,BeneficiaryList.class));
            BeneficiaryCreation.this.finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void finish() {

        super.finish();
        return;

    }

    @Override
    public void onDateSet(DatePicker datePicker, int i, int i1, int i2) {

    }
}
