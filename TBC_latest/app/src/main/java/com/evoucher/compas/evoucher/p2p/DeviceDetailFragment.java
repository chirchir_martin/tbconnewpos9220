/*
 * Copyright (C) 2011 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.evoucher.compas.evoucher.p2p;

import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.net.wifi.WpsInfo;
import android.net.wifi.p2p.WifiP2pConfig;
import android.net.wifi.p2p.WifiP2pDevice;
import android.net.wifi.p2p.WifiP2pInfo;
import android.net.wifi.p2p.WifiP2pManager.ConnectionInfoListener;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.provider.Settings;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.evoucher.compas.evoucher.Details;
import com.evoucher.compas.evoucher.PinActivity;
import com.evoucher.compas.evoucher.R;
import com.evoucher.compas.evoucher.TransactionsLists;
import com.google.gson.JsonArray;
import com.lynx.evoucher.models.CardBlock;
import com.lynx.evoucher.models.Commodities;
import com.lynx.evoucher.models.SyncLogs;
import com.lynx.evoucher.models.Topups;
import com.lynx.evoucher.models.TopupsLogs;
import com.lynx.evoucher.models.Transactions;
import com.lynx.evoucher.models.TransactionsListProducts;

import org.apache.commons.io.IOUtils;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.nio.charset.StandardCharsets;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import cn.pedant.SweetAlert.SweetAlertDialog;

/**
 * A fragment that manages a particular peer and allows interaction with device
 * i.e. setting up network connection and transferring data.
 */
public class DeviceDetailFragment extends Fragment implements ConnectionInfoListener {
    String state = "";
    boolean b = false;
    ServerSocket serverSocket;
    BufferedReader reader;
    Socket socket;
    DataOutputStream stream;
    float totalamount = 0;

    SyncLogs synclogs_table;
    protected static final int CHOOSE_FILE_RESULT_CODE = 20;
    private View mContentView = null;
    boolean x = false;
    private WifiP2pDevice device;
    private WifiP2pInfo info;
    List<Topups> topups = null;
    List<CardBlock> cblock = null;
    boolean isactive = false;
    Details dt;
    JSONObject ja;
    String size = "";
    List<Transactions> txns, transactions;
    JSONArray topups_array, topupslogs_array,blockedcards_array, txnsarchives_array;
    String status = "";
    private static final int SOCKET_TIMEOUT = 5000;
    ProgressDialog progressDialog;
    JSONArray commodities, cs;
    JSONObject transaction, jsonObject;
    JSONObject topup,card_block, topupLogs, data;
    List<TopupsLogs> topupslogs;
    List<TransactionsListProducts> txns_list;

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {

        super.onActivityCreated(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {


        mContentView = inflater.inflate(R.layout.device_detail, null);
        mContentView.findViewById(R.id.btn_start_client).setVisibility(View.GONE);
        mContentView.findViewById(R.id.btn_start_client1).setVisibility(View.GONE);
        mContentView.findViewById(R.id.btn_start_client2).setVisibility(View.GONE);
        mContentView.findViewById(R.id.sync_archives).setVisibility(View.GONE);
        mContentView.findViewById(R.id.sync_master).setVisibility(View.GONE);


        mContentView.findViewById(R.id.btn_connect).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                WifiP2pConfig config = new WifiP2pConfig();

                config.deviceAddress = device.deviceAddress;
                config.groupOwnerIntent = 0;
                config.wps.setup = WpsInfo.PBC;
                mContentView.findViewById(R.id.btn_start_client).setClickable(true);
                mContentView.findViewById(R.id.btn_start_client).setAlpha(1f);


                mContentView.findViewById(R.id.btn_start_client1).setClickable(true);
                mContentView.findViewById(R.id.btn_start_client1).setAlpha(1f);

                mContentView.findViewById(R.id.btn_start_client2).setClickable(true);
                mContentView.findViewById(R.id.btn_start_client2).setAlpha(1f);

                mContentView.findViewById(R.id.sync_archives).setClickable(true);
                mContentView.findViewById(R.id.sync_archives).setAlpha(1f);

                mContentView.findViewById(R.id.sync_master).setClickable(true);
                mContentView.findViewById(R.id.sync_master).setAlpha(1f);

                if (progressDialog != null && progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }
                progressDialog = ProgressDialog.show(getActivity(), "Press back to cancel",
                        "Connecting to :" + device.deviceAddress, true, true
//                        new DialogInterface.OnCancelListener() {
//
//                            @Override
//                            public void onCancel(DialogInterface dialog) {
//                                ((DeviceActionListener) getActivity()).cancelDisconnect();
//                            }
//                        }
                );
                ((DeviceListFragment.DeviceActionListener) getActivity()).connect(config);

            }
        });

        mContentView.findViewById(R.id.btn_disconnect).setOnClickListener(
                new View.OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        mContentView.findViewById(R.id.btn_start_client).setVisibility(View.GONE);
                        mContentView.findViewById(R.id.btn_start_client1).setVisibility(View.GONE);
                        mContentView.findViewById(R.id.btn_start_client2).setVisibility(View.GONE);
                        mContentView.findViewById(R.id.sync_archives).setVisibility(View.GONE);
                        mContentView.findViewById(R.id.sync_master).setVisibility(View.GONE);

                        ((DeviceListFragment.DeviceActionListener) getActivity()).disconnect("button");
                    }
                });

       /* mContentView.findViewById(R.id.delete_txns).setOnClickListener(
                new View.OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        new MaterialDialog.Builder(getActivity())
                                .iconRes(R.drawable.ecompass)
                                .limitIconToDefaultSize()
                                .title("")
                                .content("Are you Sure?")
                                .positiveText("Yes")
                                .negativeText("No")
                                .callback(new MaterialDialog.ButtonCallback() {
                                    @Override
                                    public void onPositive(MaterialDialog dialog) {
                                        for (Transactions tns : transactions) {
                                            List<Commodities> tops = Commodities.find(Commodities.class, "transactionno =?", tns.getId().toString());
                                            if (tops.size() > 0) {
                                                tops.get(0).delete();
                                            }
                                            tns.delete();
                                        }
                                        ((DeviceListFragment.DeviceActionListener) getActivity()).disconnect("delete");
                                        mContentView.findViewById(R.id.btn_disconnect).setClickable(true);
                                        mContentView.findViewById(R.id.btn_disconnect).setAlpha(1f);
                                        new SweetAlertDialog(getActivity(), SweetAlertDialog.SUCCESS_TYPE)
                                                .setTitleText("Great")
                                                .setContentText("Transaction deleted successfully")
                                                .setConfirmText("Ok")
                                                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                                    @Override
                                                    public void onClick(SweetAlertDialog sDialog) {
                                                        sDialog.dismissWithAnimation();

                                                    }
                                                })
                                                .show();
                                    }

                                    @Override
                                    public void onNegative(MaterialDialog dialog) {

                                    }
                                })
                                .show();

                    }
                });
*/
        mContentView.findViewById(R.id.btn_start_client).setOnClickListener(
                new View.OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        mContentView.findViewById(R.id.btn_start_client1).setVisibility(View.GONE);
                        mContentView.findViewById(R.id.btn_start_client2).setVisibility(View.GONE);
                        mContentView.findViewById(R.id.sync_archives).setVisibility(View.GONE);
                        mContentView.findViewById(R.id.sync_master).setVisibility(View.GONE);
                        status = "0";
                        dt.setStatus(status);
                        fetchData();

                    }
                });

        mContentView.findViewById(R.id.btn_start_client1).setOnClickListener(
                new View.OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        mContentView.findViewById(R.id.btn_start_client).setVisibility(View.GONE);
                        mContentView.findViewById(R.id.btn_start_client2).setVisibility(View.GONE);
                        mContentView.findViewById(R.id.sync_archives).setVisibility(View.GONE);
                        mContentView.findViewById(R.id.sync_master).setVisibility(View.GONE);
                        status = "1";
                        fetchData();
                        dt.setStatus(status);
                    }
                });

        mContentView.findViewById(R.id.btn_start_client2).setOnClickListener(
                new View.OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        mContentView.findViewById(R.id.btn_start_client).setVisibility(View.GONE);
                        mContentView.findViewById(R.id.btn_start_client1).setVisibility(View.GONE);
                        mContentView.findViewById(R.id.sync_archives).setVisibility(View.GONE);
                        mContentView.findViewById(R.id.sync_master).setVisibility(View.GONE);
                        status = "2";
                        fetchData();
                        dt.setStatus(status);
                    }
                });

        mContentView.findViewById(R.id.sync_archives).setOnClickListener(
                new View.OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        mContentView.findViewById(R.id.btn_start_client).setVisibility(View.GONE);
                        mContentView.findViewById(R.id.btn_start_client1).setVisibility(View.GONE);
                        mContentView.findViewById(R.id.btn_start_client2).setVisibility(View.GONE);
                        mContentView.findViewById(R.id.sync_master).setVisibility(View.GONE);
                        status = "3";
                        fetchData();
                        dt.setStatus(status);
                    }
                });

        mContentView.findViewById(R.id.sync_master).setOnClickListener(
                new View.OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        mContentView.findViewById(R.id.btn_start_client).setVisibility(View.GONE);
                        mContentView.findViewById(R.id.btn_start_client1).setVisibility(View.GONE);
                        mContentView.findViewById(R.id.btn_start_client2).setVisibility(View.GONE);
                        mContentView.findViewById(R.id.sync_archives).setVisibility(View.GONE);
                        status = "4";
                        fetchData();
                        dt.setStatus(status);
                    }
                });

        return mContentView;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        // User has picked an image. Transfer it to group owner i.e peer using
        // FileTransferService.
        Uri uri = data.getData();
        TextView statusText = (TextView) mContentView.findViewById(R.id.status_text);
        statusText.setText("Sending: " + uri);
        Log.d(SyncActivity.TAG, "Intent----------- " + uri);
        Intent serviceIntent = new Intent(getActivity(), FileTransferService.class);
        serviceIntent.setAction(FileTransferService.ACTION_SEND_FILE);
        serviceIntent.putExtra(FileTransferService.EXTRAS_FILE_PATH, uri.toString());
        serviceIntent.putExtra(FileTransferService.EXTRAS_GROUP_OWNER_ADDRESS,
                info.groupOwnerAddress.getHostAddress());
        serviceIntent.putExtra(FileTransferService.EXTRAS_GROUP_OWNER_PORT, 8988);
        getActivity().startService(serviceIntent);
    }

    @Override
    public void onConnectionInfoAvailable(final WifiP2pInfo info) {
        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.dismiss();
        }
        this.info = info;
        this.getView().setVisibility(View.VISIBLE);

        // The owner IP is now known.
        TextView view = (TextView) mContentView.findViewById(R.id.group_owner);
        view.setText(getResources().getString(R.string.group_owner_text)
                + ((info.isGroupOwner == true) ? getResources().getString(R.string.yes)
                : getResources().getString(R.string.no)));

        // InetAddress from WifiP2pInfo struct.
        view = (TextView) mContentView.findViewById(R.id.device_info);
        view.setText("Group Owner IP - " + info.groupOwnerAddress.getHostAddress());

        // After the group negotiation, we assign the group owner as the file
        // server. The file server is single threaded, single connection server
        // socket.
        if (info.groupFormed && info.isGroupOwner) {
            new FileServerAsyncTask(getActivity(), mContentView.findViewById(R.id.status_text))
                    .execute();
        } else if (info.groupFormed) {
            // The other device acts as the client. In this case, we enable the
            // get file button.
            mContentView.findViewById(R.id.btn_start_client).setVisibility(View.VISIBLE);
            mContentView.findViewById(R.id.btn_start_client1).setVisibility(View.VISIBLE);
            mContentView.findViewById(R.id.btn_start_client2).setVisibility(View.VISIBLE);
            mContentView.findViewById(R.id.sync_master).setVisibility(View.VISIBLE);
            mContentView.findViewById(R.id.sync_archives).setVisibility(View.VISIBLE);

            //((TextView) mContentView.findViewById(R.id.status_text)).setText(getResources()
            //   .getString(R.string.client_text));
        }

        // hide the connect button
        mContentView.findViewById(R.id.btn_connect).setVisibility(View.GONE);
    }

    /**
     * Updates the UI with device data
     *
     * @param device the device to be displayed
     */
    public void showDetails(WifiP2pDevice device) {
        this.device = device;
        this.getView().setVisibility(View.VISIBLE);
        TextView view = (TextView) mContentView.findViewById(R.id.device_address);
        view.setText(device.deviceAddress);
        view = (TextView) mContentView.findViewById(R.id.device_info);
        view.setText(device.toString());
        refreshViews();
    }

    void refreshViews() {
        mContentView.findViewById(R.id.btn_start_client).setVisibility(View.GONE);
        mContentView.findViewById(R.id.btn_start_client).setClickable(true);
        mContentView.findViewById(R.id.btn_start_client).setAlpha(1f);

        mContentView.findViewById(R.id.btn_start_client1).setVisibility(View.GONE);
        mContentView.findViewById(R.id.btn_start_client1).setClickable(true);
        mContentView.findViewById(R.id.btn_start_client1).setAlpha(1f);

        mContentView.findViewById(R.id.btn_start_client2).setVisibility(View.GONE);
        mContentView.findViewById(R.id.btn_start_client2).setClickable(true);
        mContentView.findViewById(R.id.btn_start_client2).setAlpha(1f);

        mContentView.findViewById(R.id.sync_archives).setVisibility(View.GONE);
        mContentView.findViewById(R.id.sync_archives).setClickable(true);
        mContentView.findViewById(R.id.sync_archives).setAlpha(1f);

    }

    /**
     * Clears the UI fields after a disconnect or direct mode disable operation.
     */
    public void resetViews() {
        mContentView.findViewById(R.id.btn_connect).setVisibility(View.VISIBLE);
        TextView view = (TextView) mContentView.findViewById(R.id.device_address);
        view.setText(R.string.empty);
        view = (TextView) mContentView.findViewById(R.id.device_info);
        view.setText(R.string.empty);
        view = (TextView) mContentView.findViewById(R.id.group_owner);
        view.setText(R.string.empty);
        view = (TextView) mContentView.findViewById(R.id.status_text);
        view.setText(R.string.empty);
        mContentView.findViewById(R.id.btn_start_client).setVisibility(View.GONE);
        this.getView().setVisibility(View.GONE);
    }

    /**
     * A simple server socket that accepts connection and writes some data on
     * the stream.
     */
    private class FileServerAsyncTask extends AsyncTask<Void, Void, String> {

        private Context context;
        private TextView statusText;

        /**
         * @param context
         * @param statusText
         */
        public FileServerAsyncTask(Context context, View statusText) {
            this.context = context;
            this.statusText = (TextView) statusText;
        }

        @Override
        protected String doInBackground(Void... params) {
            try {

                BufferedReader reader;
                final StringBuilder sb1 = new StringBuilder();
                ServerSocket serverSocket = new ServerSocket(8988);
                Log.d(SyncActivity.TAG, "Server: Socket opened");
                Socket client = serverSocket.accept();
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        mContentView.findViewById(R.id.btn_disconnect).setAlpha(0.5f);
                        mContentView.findViewById(R.id.btn_disconnect).setClickable(false);
                    }
                });
                Log.d(SyncActivity.TAG, "Server: connection done");
                reader = new BufferedReader(new InputStreamReader(client.getInputStream()));

                JSONObject ja;
                String line = null;
                // Read Server Response
                while ((line = reader.readLine()) != null) {
                    sb1.append(line + "");
                }
                reader.close();
                if (sb1.length() > 0) {
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            progressDialog = ProgressDialog.show(getActivity(), "Receiving the data", "Please wait", true,
                                    true, new DialogInterface.OnCancelListener() {
                                        @Override
                                        public void onCancel(DialogInterface dialog) {

                                        }
                                    });
                            progressDialog.setCancelable(false);
                            // Toast.makeText(getActivity(), sb1.length(), Toast.LENGTH_LONG).show();
                        }
                    });
                    Log.i("#######DATA_RECEIVED", sb1.toString() + "gffgfgfgfrg");
                    ja = new JSONObject(sb1.toString());
                    state = ja.getString("status");
                    if (state.equalsIgnoreCase("0")) {
                        Topups topups_table;
                        topups_array = ja.getJSONArray("content");
                        size = String.valueOf(topups_array.length());
                        if (topups_array.length() > 0) {
                            Topups.deleteAll(Topups.class);
                            for (int i = 0; i < topups_array.length(); i++) {
                                topups_table = new Topups();
                                topups_table.setProgrammeId(topups_array.getJSONObject(i).getString("programmeId"));
                                topups_table.setBengroup(topups_array.getJSONObject(i).getString("bnfGrpId"));
                                topups_table.setVouchervalue(topups_array.getJSONObject(i).getString("voucherValue"));
                                topups_table.setBeneficiaryId(topups_array.getJSONObject(i).getString("beneficiaryId"));
                                topups_table.setCardNumber(topups_array.getJSONObject(i).getString("cardNumber"));
                                topups_table.setVoucherId(topups_array.getJSONObject(i).getString("voucherId"));
                                topups_table.setVocheridno(topups_array.getJSONObject(i).getString("voucherIdNo"));
                                Log.i("XXXRRRRR", topups_array.getJSONObject(i).getString("cardNumber"));
                                topups_table.save();
                            }

                        }

                    } else if (state.equalsIgnoreCase("1")) {
                        Transactions txns_table;
                        Commodities commodities_table;
                        JSONArray txns_array, commodities;

                        txns_array = ja.getJSONArray("content");
                        if (txns_array.length() > 0) {
                            int count = 0;
                            String deviceId = "";
                            totalamount = 0;
                            size = String.valueOf(txns_array.length());
                            for (int i = 0; i < txns_array.length(); i++) {
                                txns = Transactions.find(Transactions.class, "receiptno =? and deviceid =?", txns_array.getJSONObject(i).getString("receipt_number"), txns_array.getJSONObject(i).getString("pos_terminal"));
                                if (!(txns.size() > 0)) {
                                    txns_table = new Transactions();
                                    commodities = new JSONArray(txns_array.getJSONObject(i).getString("commodities"));
                                    txns_table.setVoucheridno(txns_array.getJSONObject(i).getString("voucher"));
                                    txns_table.setTransactiontype(txns_array.getJSONObject(i).getString("transaction_type"));


                                    Log.i("##DSDSADDADA", txns_array.getJSONObject(i).getString("transaction_type") + "  sss");

                                    txns_table.setReceiptno(txns_array.getJSONObject(i).getString("receipt_number"));
                                    txns_table.setTotalvaluereamining(txns_array.getJSONObject(i).getString("value_remaining"));
                                    txns_table.setCardno(txns_array.getJSONObject(i).getString("cardNumber"));
                                    txns_table.setLocationid(txns_array.getJSONObject(i).getString("locationID"));
                                    txns_table.setIsuploaded("0");
                                    txns_table.setVoucheridno(txns_array.getJSONObject(i).getString("voucherIdNo"));

                                    count = count + 1;
                                    totalamount = totalamount + Float.parseFloat(txns_array.getJSONObject(i).getString("total_amount_charged_by_retailer"));
                                    txns_table.setDate(txns_array.getJSONObject(i).getString("date"));
                                    txns_table.setTotalamountchargedbyretail(txns_array.getJSONObject(i).getString("total_amount_charged_by_retailer"));
                                    txns_table.setUser(txns_array.getJSONObject(i).getString("user"));
                                    txns_table.setRationno(txns_array.getJSONObject(i).getString("rationNo"));
                                    txns_table.setTimestamp(txns_array.getJSONObject(i).getString("timestamp_transaction_created"));
                                    deviceId = txns_array.getJSONObject(i).getString("pos_terminal");
                                    txns_table.setDeviceid(deviceId);
                                    txns_table.save();

                                    List<Transactions> transaction = Transactions.find(Transactions.class, null, null, null, "id DESC", "1");
                                    for (int j = 0; j < commodities.length(); j++) {
                                        commodities_table = new Commodities();
                                        commodities_table.setUom(commodities.getJSONObject(j).getString("uom"));
                                        commodities_table.setTransactionno(transaction.get(0).getId().toString());
                                        commodities_table.setProductid(commodities.getJSONObject(j).getString("pos_commodity"));
                                        commodities_table.setQuantityremaining(commodities.getJSONObject(j).getString("quantity_remaining"));
                                        commodities_table.setTotalamountcahgerdbyretailer(commodities.getJSONObject(j).getString("amount_charged_by_retailer"));
                                        commodities_table.setQuantitydeducted(commodities.getJSONObject(j).getString("deducted_quantity"));
                                        commodities_table.save();
                                    }


                                }



                            }
                            SimpleDateFormat date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                            synclogs_table = new SyncLogs();
                            synclogs_table.setDeviceid(deviceId);
                            synclogs_table.setNooftxns(String.valueOf(count));
                            synclogs_table.setDate(date.format(new Date()));
                            synclogs_table.setTotalamt(String.valueOf(totalamount));
                            synclogs_table.save();
                            serverSocket.close();
                        }

                    } else if (state.equalsIgnoreCase("2")) {
                        Log.i("###TOPESSSSS", "sendingggg");
                        TopupsLogs topupslogs_table;
                        topupslogs_array = ja.getJSONArray("content");

                        if (topupslogs_array.length() > 0) {
                            for (int i = 0; i < topupslogs_array.length(); i++) {
                                topupslogs_table = new TopupsLogs();
                                topupslogs_table.setCardno(topupslogs_array.getJSONObject(i).getString("cardNumber"));
                                topupslogs_table.setNtopupvalue(topupslogs_array.getJSONObject(i).getString("newTopupValue"));
                                topupslogs_table.setNvoucheridno(topupslogs_array.getJSONObject(i).getString("newVoucherNo"));
                                topupslogs_table.setNcardbal(topupslogs_array.getJSONObject(i).getString("newCardBal"));
                                topupslogs_table.setOvoucheridno(topupslogs_array.getJSONObject(i).getString("oldVoucherNo"));
                                topupslogs_table.setOcardbal(topupslogs_array.getJSONObject(i).getString("oldCardBal"));
                                topupslogs_table.setTopuptime(topupslogs_array.getJSONObject(i).getString("topupTime"));
                                topupslogs_table.setDeviceidno(topupslogs_array.getJSONObject(i).getString("deviceId"));
                                topupslogs_table.setIsuploaded("0");
                                topupslogs_table.setUsername(topupslogs_array.getJSONObject(i).getString("username"));
                                topupslogs_table.setRefno(topupslogs_array.getJSONObject(i).getString("refno"));
                                topupslogs_table.save();

                            }


                        }

                    } else if (state.equalsIgnoreCase("3")) {
                        String android_id = Settings.Secure.getString(getActivity().getApplicationContext().getContentResolver(),
                                Settings.Secure.ANDROID_ID);
                        TransactionsListProducts txnsarchives_table;
                        txnsarchives_array = ja.getJSONArray("content");

                        if (txnsarchives_array.length() > 0) {
                            for (int i = 0; i < txnsarchives_array.length(); i++) {
                                txnsarchives_table = new TransactionsListProducts();
                                txnsarchives_table.setProductid(txnsarchives_array.getJSONObject(i).getString("serviceId"));
                                txnsarchives_table.setDeviceid(txnsarchives_array.getJSONObject(i).getString("deviceId"));
                                txnsarchives_table.setUnitofmeasure(txnsarchives_array.getJSONObject(i).getString("uom"));
                                txnsarchives_table.setQuantity(txnsarchives_array.getJSONObject(i).getString("quantity"));
                                txnsarchives_table.setVal(txnsarchives_array.getJSONObject(i).getString("value"));
                                txnsarchives_table.setTransactiondate(txnsarchives_array.getJSONObject(i).getString("transactionDate"));
                                txnsarchives_table.save();

                            }


                        }

                    }else  if (state.equalsIgnoreCase("4")) {
                        CardBlock cardBlock_table;
                        blockedcards_array = ja.getJSONArray("content");
                        size = String.valueOf(blockedcards_array.length());
                        if (blockedcards_array.length() > 0) {
                           CardBlock.deleteAll(CardBlock.class);
                            for (int i = 0; i < blockedcards_array.length(); i++) {
                                cardBlock_table = new CardBlock();
                                cardBlock_table.setRationno(blockedcards_array.getJSONObject(i).getString("rationNo"));
                                cardBlock_table.setCardno(blockedcards_array.getJSONObject(i).getString("cardNumber"));
                                cardBlock_table.save();
                            }

                        }

                    }
                    serverSocket.close();


                } else {

                }

                return "1";
            } catch (Exception e) {
                Log.i("##FFDGGFGFGgf", e.toString());
                return null;
            }
        }

        /*
         * (non-Javadoc)
         * @see android.os.AsyncTask#onPostExecute(java.lang.Object)
         */
        @Override
        protected void onPostExecute(String result) {
            try {
                Log.i("###SDSDSDSDS", "uiiii");
                if (result != null) {
                    progressDialog.dismiss();

                    if (state.equalsIgnoreCase("0"))
                        alertSuccess("Success", "Topups Updated successfully");
                    else if (state.equalsIgnoreCase("1"))
                        alertSuccess("Success", " Transactions Updated successfully");
                    else if (state.equalsIgnoreCase("2"))
                        alertSuccess("Success", "Topups Logs saved successfully");
                    else if (state.equalsIgnoreCase("3"))
                        alertSuccess("Success", "Transactions archives saved successfully");
                    else if (state.equalsIgnoreCase("4"))
                        alertSuccess("Success", "Blocked cards saved successfully");


                } else {
                    progressDialog.dismiss();
                    alertError("Error", "ERROR RECEIVING THE DATA");
                    //Toast.makeText(context, "ERROR RECEIVING THE DATA", Toast.LENGTH_LONG).show();
                }
            } catch (Exception e) {

            }

        }

        /*
         * (non-Javadoc)
         * @see android.os.AsyncTask#onPreExecute()
         */
        @Override
        protected void onPreExecute() {

        }

    }

    public boolean copyFile(JSONObject tps, OutputStream out) {

        try {
            out.write((tps.toString().getBytes()));
            out.flush();
            out.close();

        } catch (Exception e) {

            Log.i("###WPDsdsdsd", e.toString());
            // Log.i("###SASADSDSDSd",e.toString());
            e.printStackTrace();
        }
        return true;
    }

    void fetchData() {


        try {

            progressDialog = ProgressDialog.show(getActivity(), "Sending the data", "Please wait", true,
                    true, new DialogInterface.OnCancelListener() {
                        @Override
                        public void onCancel(DialogInterface dialog) {

                        }
                    });
            progressDialog.setCancelable(false);

            new Thread(new Runnable() {
                @Override
                public void run() {
                    try {


                        String host = info.groupOwnerAddress.getHostAddress();
                        isactive = true;
                        socket = new Socket();
                        int port = 8988;
                        Log.d(SyncActivity.TAG, "Opening client socket - ");
                        socket.bind(null);
                        socket.connect((new InetSocketAddress(host, port)), SOCKET_TIMEOUT);

                        Log.d(SyncActivity.TAG, "Client socket - " + socket.isConnected());

                        OutputStream stream = socket.getOutputStream();


                        data = new JSONObject();
                        JSONArray topUps = new JSONArray();
                        if (status.equalsIgnoreCase("0")) {
                            //data = new JSONObject();
                            data.put("status", "0");
                            topups = Topups.listAll(Topups.class);
                            //topups= Topups.find(Topups.class, null, null, null, "id DESC", "1");
                            if (topups.size() > 0) {
                                for (int i = 0; i < topups.size(); i++) {
                                    topup = new JSONObject();
                                    topup.put("beneficiaryId", topups.get(i).getBeneficiaryId());
                                    topup.put("cardNumber", topups.get(i).getCardNumber());
                                    topup.put("voucherValue", topups.get(i).getVouchervalue());
                                    topup.put("programmeId", topups.get(i).getProgrammeId());
                                    topup.put("voucherIdNo", topups.get(i).getVocheridno());
                                    topup.put("voucherId", topups.get(i).getVoucherId());
                                    topup.put("bnfGrpId", topups.get(i).getBengroup());
                                    topUps.put(topup);
                                }

                                data.put("content", topUps);
                                Log.i("###TRTRTRTer", topUps.toString());
                                copyFile(data, stream);


                                getActivity().runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        progressDialog.dismiss();
                                        mContentView.findViewById(R.id.btn_start_client).setClickable(false);
                                        mContentView.findViewById(R.id.btn_start_client).setAlpha(0.5f);

                                    }
                                });


                            } else {
                                getActivity().runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        getActivity().runOnUiThread(new Runnable() {
                                            @Override
                                            public void run() {
                                                alertError("Sorry", "No Topups found");
                                                //  Toast.makeText(getActivity(),,Toast.LENGTH_LONG).show();
                                                progressDialog.dismiss();
                                            }
                                        });
                                    }
                                });

                            }

                        } else if (status.equalsIgnoreCase("1")) {
                            data.put("status", "1");
                            cs = new JSONArray();
                            transaction = new JSONObject();
                            transactions = Transactions.find(Transactions.class, "isuploaded =?", "0");
                            Log.d("###TRANS", String.valueOf(transactions.size()));
                            if (transactions.size() != 0) {
                                Log.d("dpsdpspdpsWE", String.valueOf(transactions.size()) + "ppppp");
                                for (int i = 0; i < transactions.size(); i++) {
                                    Log.d("dpsdpspdps", transactions.get(i).getTotalvaluereamining());
                                    transaction = new JSONObject();
                                    commodities = new JSONArray();
                                    transaction.put("voucher", transactions.get(i).getVoucheridno());
                                    transaction.put("transaction_type", transactions.get(i).getTransactiontype());
                                    transaction.put("voucherIdNo", transactions.get(i).getVoucheridno());
                                    transaction.put("cancelled_transaction", 0);
                                    transaction.put("receipt_number", transactions.get(i).getReceiptno());
                                    transaction.put("value_remaining", transactions.get(i).getTotalvaluereamining());
                                    transaction.put("total_amount_charged_by_retailer", transactions.get(i).getTotalamountchargedbyretail());
                                    transaction.put("user", transactions.get(i).getUser());
                                    transaction.put("rationNo", transactions.get(i).getRationno());
                                    transaction.put("cardNumber", transactions.get(i).getCardno());
                                    transaction.put("date", transactions.get(i).getDate());
                                    transaction.put("locationID", transactions.get(i).getLocationid());
                                    transaction.put("timestamp_transaction_created", transactions.get(i).getTimestamp());
                                    Log.d("###transactioncreated", transactions.get(i).getTimestamp());
                                    transaction.put("authentication_type", 0);
                                    transaction.put("pos_terminal", transactions.get(0).getDeviceid());
                                    List<Commodities> tops = Commodities.find(Commodities.class, "transactionno =?", transactions.get(i).getId().toString());
                                    Log.d("dpsdpspdps", String.valueOf(tops.size()));
                                    Log.d("##ERROR", "DONE" + tops.size());
                                    Log.d("#QRRRRRR", transactions.get(i).getId().toString());
                                    for (int j = 0; j < tops.size(); j++) {
                                        jsonObject = new JSONObject();
                                        jsonObject.put("pos_commodity", tops.get(j).getProductid());
                                        jsonObject.put("uom", tops.get(j).getUom());
                                        jsonObject.put("quantity_remaining", tops.get(j).getQuantityremaining());
                                        jsonObject.put("amount_charged_by_retailer", tops.get(j).getTotalamountcahgerdbyretailer());
                                        jsonObject.put("deducted_quantity", tops.get(j).getQuantitydeducted());
                                        commodities.put(jsonObject);
                                    }
                                    transaction.put("commodities", commodities);
                                    cs.put(transaction);

                                }
                                data.put("content", cs);
                                copyFile(data, stream);
                                getActivity().runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {

                                        // mContentView.findViewById(R.id.btn_disconnect).setClickable(false);
                                        // mContentView.findViewById(R.id.btn_disconnect).setAlpha(0.5f);
                                        mContentView.findViewById(R.id.btn_start_client1).setClickable(false);
                                        mContentView.findViewById(R.id.btn_start_client1).setAlpha(0.5f);
                                        // mContentView.findViewById(R.id.delete_txns).setVisibility(View.VISIBLE);
                                        progressDialog.dismiss();

                                    }
                                });

                                for (Transactions tns : transactions) {
                                    tns.setIsuploaded("1");
                                    tns.save();
                                }


                            } else {
                                getActivity().runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        alertError("Sorry", "No transactions found");
                                        progressDialog.dismiss();
                                    }
                                });

                            }
                        } else if (status.equalsIgnoreCase("2")) {
                            JSONArray topUpsLogs = new JSONArray();
                            data = new JSONObject();
                            data.put("status", "2");
                            topupslogs = TopupsLogs.find(TopupsLogs.class, "isuploaded =?", "0");

                            if (topupslogs.size() > 0) {
                                for (int i = 0; i < topupslogs.size(); i++) {
                                    topupLogs = new JSONObject();
                                    topupLogs.put("cardNumber", topupslogs.get(i).getCardno());
                                    topupLogs.put("newTopupValue", topupslogs.get(i).getNtopupvalue());
                                    topupLogs.put("newVoucherNo", topupslogs.get(i).getNvoucheridno());
                                    topupLogs.put("newCardBal", topupslogs.get(i).getNcardbal());
                                    topupLogs.put("oldVoucherNo", topupslogs.get(i).getOvoucheridno());
                                    topupLogs.put("oldCardBal", topupslogs.get(i).getOcardbal());
                                    topupLogs.put("topupTime", topupslogs.get(i).getTopuptime());
                                    topupLogs.put("deviceId", topupslogs.get(i).getDeviceidno());
                                    topupLogs.put("refno", topupslogs.get(i).getRefno());
                                    topupLogs.put("username", topupslogs.get(i).getUsername());
                                    topUpsLogs.put(topupLogs);
                                }

                                data.put("content", topUpsLogs);
                                Log.i("###TRTRTRTer", data.toString());
                                copyFile(data, stream);

                                for (TopupsLogs tlogs : topupslogs) {
                                    tlogs.setIsuploaded("1");
                                    tlogs.save();
                                }

                                getActivity().runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {

                                        //  mContentView.findViewById(R.id.btn_disconnect).setClickable(false);
                                        //  mContentView.findViewById(R.id.btn_disconnect).setAlpha(0.5f);

                                        mContentView.findViewById(R.id.btn_start_client2).setClickable(false);
                                        mContentView.findViewById(R.id.btn_start_client2).setAlpha(0.5f);
                                        //mContentView.findViewById(R.id.delete_txns).setVisibility(View.VISIBLE);
                                        progressDialog.dismiss();

                                    }
                                });

                            } else {
                                getActivity().runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        getActivity().runOnUiThread(new Runnable() {
                                            @Override
                                            public void run() {
                                                alertError("Sorry", "No TopupsLogs found");
                                                //  Toast.makeText(getActivity(),,Toast.LENGTH_LONG).show();
                                                progressDialog.dismiss();
                                            }
                                        });
                                    }
                                });

                            }

                        } else if (status.equalsIgnoreCase("3")){
                            String android_id = Settings.Secure.getString(getActivity().getApplicationContext().getContentResolver(),
                                    Settings.Secure.ANDROID_ID);
                            JSONObject txns_obj = null;
                            JSONArray txnArchives = new JSONArray();
                            data = new JSONObject();
                            data.put("status", "3");
                            txns_list = TransactionsListProducts.listAll(TransactionsListProducts.class);

                            if (txns_list.size() > 0) {
                                for (int i = 0; i < txns_list.size(); i++) {
                                    txns_obj = new JSONObject();
                                    txns_obj.put("deviceId", android_id);
                                    txns_obj.put("serviceId", txns_list.get(i).getProductid());
                                    txns_obj.put("uom", txns_list.get(i).getUnitofmeasure());
                                    txns_obj.put("quantity", txns_list.get(i).getQuantity());
                                    txns_obj.put("value", txns_list.get(i).getVal());
                                    txns_obj.put("transactionDate", txns_list.get(i).getTransactiondate());

                                    txnArchives.put(txns_obj);
                                }
                                data.put("content", txnArchives);
                                Log.i("###TRTRTRTer", data.toString());
                                copyFile(data, stream);


                                getActivity().runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {

                                        mContentView.findViewById(R.id.sync_archives).setClickable(false);
                                        mContentView.findViewById(R.id.sync_archives).setAlpha(0.5f);
                                        progressDialog.dismiss();

                                    }
                                });

                            } else {
                                getActivity().runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        getActivity().runOnUiThread(new Runnable() {
                                            @Override
                                            public void run() {
                                                progressDialog.dismiss();
                                                alertError("Sorry", "No Transactions Archives found");
                                                //  Toast.makeText(getActivity(),,Toast.LENGTH_LONG).show();

                                            }
                                        });
                                    }
                                });

                            }

                        } else if (status.equalsIgnoreCase("4")) {
                            JSONArray cardBlock = new JSONArray();
                            //data = new JSONObject();
                            data.put("status", "4");
                            cblock = CardBlock.listAll(CardBlock.class);
                            if ( cblock.size() > 0) {
                                for (int i = 0; i < cblock.size(); i++) {
                                    card_block = new JSONObject();
                                    card_block.put("rationNo",  cblock.get(i).getRationno());
                                    card_block.put("cardNumber",  cblock.get(i).getCardno());

                                    cardBlock.put(card_block);
                                }

                                data.put("content", cardBlock);
                                Log.i("###TRTRTRTer", cardBlock.toString());
                                copyFile(data, stream);


                                getActivity().runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        progressDialog.dismiss();
                                        mContentView.findViewById(R.id.sync_master).setClickable(false);
                                        mContentView.findViewById(R.id.sync_master).setAlpha(0.5f);

                                    }
                                });


                            } else {
                                getActivity().runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        getActivity().runOnUiThread(new Runnable() {
                                            @Override
                                            public void run() {
                                                alertError("Sorry", "No Blocked cards found");
                                                //  Toast.makeText(getActivity(),,Toast.LENGTH_LONG).show();
                                                progressDialog.dismiss();
                                            }
                                        });
                                    }
                                });

                            }
                        }



                        } catch (Exception e) {
                        Log.i("###WEWEWEW", e.toString());
                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                alertError("Error", "Error processing data.Refresh and try again");
                                // Toast.makeText(getActivity(),"Error fetching data",Toast.LENGTH_LONG).show();
                                progressDialog.dismiss();
                            }
                        });

                    }

                }
            }).start();
            Log.d(SyncActivity.TAG, "Client: Data written");
        } catch (Exception e) {
            Log.i("###WEWEWEW", e.toString());
        } finally {
            if (socket != null) {
                if (socket.isConnected()) {
                    try {
                        socket.close();
                    } catch (IOException e) {
                        // Give up
                        e.printStackTrace();
                    }
                }
            }


        }
    }

    void alertSuccess(String title, String msg) {

        new SweetAlertDialog(getActivity(), SweetAlertDialog.SUCCESS_TYPE)
                .setTitleText(title)
                .setContentText(msg)
                .setConfirmText("Ok")
                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sDialog) {
                        if (state.equalsIgnoreCase("0"))
                            ((DeviceListFragment.DeviceActionListener) getActivity()).disconnect("success");
                        sDialog.dismissWithAnimation();

                    }
                })
                .show();
    }

    void alertError(String title, String msg) {

        new SweetAlertDialog(getActivity(), SweetAlertDialog.WARNING_TYPE)
                .setTitleText(title)
                .setContentText(msg)
                .setConfirmText("Ok")
                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sDialog) {
                        sDialog.dismissWithAnimation();

                    }
                })
                .show();
    }
}
