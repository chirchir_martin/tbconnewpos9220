package com.evoucher.compas.evoucher;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ListView;
import com.evoucher.compas.evoucher.Adapters.Reports_detailsAdapter;
import com.lynx.evoucher.models.Reports;

import java.util.List;

public class ReportsActivity extends AppCompatActivity {
    Details dt;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reports);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        dt = new Details(ReportsActivity.this);
        getSupportActionBar().setTitle("Reports");
        ListView tv = (ListView) findViewById(R.id.items);
        List<Reports> reports = Reports.find(Reports.class, "programmeid =?", dt.getProgrammeid());
        Log.d("#####EFERERE", dt.getProgrammeid() + "sixe" + reports.size());
        Reports_detailsAdapter ad = new Reports_detailsAdapter(this, reports);
        tv.setAdapter(ad);
    }

    @Override
    public void onBackPressed() {

        Intent intent1 = new Intent(this, ProgrammeActivity.class);
        startActivity(intent1);
        finish();


    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.lawyerdetails_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.logout) {
            startActivity(new Intent(ReportsActivity.this, UsernameActivity.class));
            finish();
            return true;
        }

        if (id == android.R.id.home) {
            startActivity(new Intent(ReportsActivity.this, ProgrammeActivity.class));
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

}