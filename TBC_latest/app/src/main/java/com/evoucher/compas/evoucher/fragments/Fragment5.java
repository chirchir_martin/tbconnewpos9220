package com.evoucher.compas.evoucher.fragments;

import android.app.AlertDialog;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.ColorMatrix;
import android.graphics.ColorMatrixColorFilter;
import android.graphics.Paint;
import android.hardware.usb.UsbDevice;
import android.hardware.usb.UsbManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.evoucher.compas.evoucher.Adapters.ScrollerViewPager;
import com.evoucher.compas.evoucher.R;
import com.evoucher.compas.evoucher.ui.CustomImageView;
import com.lynx.evoucher.models.Member;

import java.nio.ByteBuffer;
//
import SecuGen.FDxSDKPro.JSGFPLib;
import SecuGen.FDxSDKPro.SGAutoOnEventNotifier;
import SecuGen.FDxSDKPro.SGFDxDeviceName;
import SecuGen.FDxSDKPro.SGFDxErrorCode;
import SecuGen.FDxSDKPro.SGFDxSecurityLevel;
import SecuGen.FDxSDKPro.SGFDxTemplateFormat;
import SecuGen.FDxSDKPro.SGFingerInfo;
import SecuGen.FDxSDKPro.SGFingerPresentEvent;
import cn.com.aratek.fp.FingerprintScanner;
import cn.pedant.SweetAlert.SweetAlertDialog;



/**
 * Created by JOHNIE on 9/3/2015.
 */
public class Fragment5 extends Fragment   implements View.OnClickListener, Runnable,SGFingerPresentEvent  {
    TextView b1;
    ImageView imageView,sl0,sl1,sr0,sr1,r0,r1,l0,l1;
    CustomImageView img0,img1,img2,img3;
    Button mCapture;
    FloatingActionButton add;
    private static final String ACTION_USB_PERMISSION = "com.android.example.USB_PERMISSION";

    ImageView error1,next,prev;
    private EditText mEditLog;
    private boolean mAutoOnEnabled;
    private String state;
    private int mImageWidth;
    public static String status="";
    private String encoded_lthumb,encoded_lindex,encoded_rthumb,encoded_rindex;
    private int LTHUMB=0,LINDEX=1,RTHUMB=2,RINDEX=3;
    private int mImageHeight;
    boolean feed;
    private int[] mMaxTemplateSize;
    private int[] grayBuffer;
    private PendingIntent mPermissionIntent;
    private Bitmap grayBitmap;
    private IntentFilter filter; //2014-04-11
    private SGAutoOnEventNotifier autoOn;
    private boolean mLed;
    private boolean Ready=false;
    private byte[] mRegisterTemplate0,mRegisterTemplate1,mRegisterTemplate2,mRegisterTemplate3;
    ScrollerViewPager  viewPager;
    private JSGFPLib sgfplib;
     Animation animation;
    private static final String TAG = "SecuGenUSB";
    private FingerprintScanner scanner = FingerprintScanner.getInstance();


    private void debugMessage(String message) {
        Log.d(TAG,message);
        //this.mEditLog.append(message);
        //this.mEditLog.invalidate(); //TODO trying to get Edit log to update after each line written
    }

    private final BroadcastReceiver mUsbReceiver = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            Log.d("","Enter mUsbReceiver.onReceive()");
            if (ACTION_USB_PERMISSION.equals(action)) {
                synchronized (this) {
                    UsbDevice device = (UsbDevice)intent.getParcelableExtra(UsbManager.EXTRA_DEVICE);
                    if (intent.getBooleanExtra(UsbManager.EXTRA_PERMISSION_GRANTED, false)) {
                        if(device != null){
                            Log.d(TAG, "Vendor ID : " + device.getVendorId() + "\n");
                            Log.d(TAG, "Product ID: " + device.getProductId() + "\n");
    						debugMessage("Vendor ID : " + device.getVendorId() + "\n");
    						debugMessage("Product ID: " + device.getProductId() + "\n");
                        }
                        else
                            Log.e(TAG, "mUsbReceiver.onReceive() Device is null");
                    }
                    else
                        Log.e(TAG, "mUsbReceiver.onReceive() permission denied for device " + device);
                }
            }
        }
    };

    public Handler fingerDetectedHandler = new Handler() {
        // @Override
        public void handleMessage(Message msg) {
            //Handle the message


        }
    };

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mPermissionIntent = PendingIntent.getBroadcast(getActivity(), 0, new Intent(ACTION_USB_PERMISSION), 0);
        //mLed = false;

        return inflater.inflate(R.layout.fragment5, container, false);

    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        animation = new AlphaAnimation(1, 0);
        animation.setDuration(200);
        animation.setInterpolator(new LinearInterpolator());
        animation.setRepeatCount(Animation.INFINITE);
        animation.setRepeatMode(Animation.REVERSE);

        l0 = (ImageView)getView().findViewById(R.id.l0);
        // add = (FloatingActionButton)getView().findViewById(R.id.floating_action_button);
        sl0 = (ImageView)getView().findViewById(R.id.sl0);
        l1 = (ImageView)getView().findViewById(R.id.l1);
        sl1 = (ImageView)getView().findViewById(R.id.sl1);
        r0 = (ImageView)getView().findViewById(R.id.r0);
        sr0 = (ImageView)getView().findViewById(R.id.sr0);
        r1 = (ImageView)getView().findViewById(R.id.r1);
        sr1 = (ImageView)getView().findViewById(R.id.sr1);
        next = (ImageView)getView().findViewById(R.id.next);
        prev = (ImageView)getView().findViewById(R.id.prev);
        img0 = (CustomImageView)getView().findViewById(R.id.img0);
        img1 = ( CustomImageView )getView().findViewById(R.id.img1);
        img2 = ( CustomImageView )getView().findViewById(R.id.img2);
        img3 = ( CustomImageView )getView().findViewById(R.id.img3);
        prev.setOnClickListener(this);
        next.setOnClickListener(this);
        img0.setOnClickListener(this);
        img1.setOnClickListener(this);
        img2.setOnClickListener(this);
        img3.setOnClickListener(this);
        img0.setEnabled(false);
        img1.setEnabled(false);
        img2.setEnabled(false);
        img3.setEnabled(false);

        l0.startAnimation(animation);
        l1.startAnimation(animation);
        r0.startAnimation(animation);
        r1.startAnimation(animation);

        img0.setEnabled(true);
        l0.setVisibility(View.VISIBLE);

        viewPager = (ScrollerViewPager) getActivity().findViewById(R.id.view_pager);
        sgfplib = new JSGFPLib((UsbManager)getActivity().getSystemService(Context.USB_SERVICE));
        filter = new IntentFilter(ACTION_USB_PERMISSION);
        getActivity().registerReceiver(mUsbReceiver, filter);
        mMaxTemplateSize = new int[1];
        final MaterialDialog pDialog = new MaterialDialog.Builder(getActivity())
                .title("Initializing fingerprint Scanner")
                .content("Please wait...")
                .progress(true, 0)
                .progressIndeterminateStyle(false)
                .show();
      Thread t= new Thread(new Runnable() {
            @Override
            public void run() {
                sgfplib = new JSGFPLib((UsbManager)getActivity().getSystemService(Context.USB_SERVICE));
                mMaxTemplateSize = new int[1];
                  initLib();
                 scanner.powerOn();

                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        pDialog.dismiss();


                    }
                });

            }
        });
        t.start();


        //USB Permissions
      //  filter = new IntentFilter(ACTION_USB_PERMISSION);
       // getActivity().registerReceiver(mUsbReceiver, filter);
     //  sgfplib = new JSGFPLib((UsbManager) getActivity().getSystemService(Context.USB_SERVICE));
//        this.mToggleButtonSmartCapture.toggle();
//        this.mToggleButtonPowerControl.toggle();
       // scanner.powerOn();  //powerOn
      /*  try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }*/

        Log.d(TAG,"Initialize Secugen Lib$");
        //initLib();

     //   debugMessage("jnisgfplib version: " + sgfplib.Version() + "\n");
      //  mLed = false;
       // autoOn = new SGAutoOnEventNotifier (sgfplib, this);
       // mAutoOnEnabled = false;

    }
    public void onClick(View v) {
        if(v.getId()==R.id.img0){
            state="";
            CaptureFingerPrint(LTHUMB);
            Log.d(TAG,"Current State##"+state);
            if(state.equalsIgnoreCase("low")) {
                //Toast.makeText(getActivity(), "Please Recapture again ", Toast.LENGTH_LONG).show();
                showAlert("QUALITY ERROR","Please Recapture again!!");

            }else if(state.equalsIgnoreCase("okay")){
                l0.clearAnimation();
                l0.setVisibility(View.GONE);

                sl0.setVisibility(View.VISIBLE);

                l1.startAnimation(animation);
                l1.setVisibility(View.VISIBLE);

                img0.setEnabled(false);
                img1.setEnabled(true);
            }
        }else if(v.getId()==R.id.img1){
            state="";
            CaptureFingerPrint(LINDEX);
            if(state.equalsIgnoreCase("nomatch") ){
                l1.clearAnimation();
                l1.setVisibility(View.GONE);

                sl1.setVisibility(View.VISIBLE);

                r0.startAnimation(animation);
                r0.setVisibility(View.VISIBLE);

                img1.setEnabled(false);
                img2.setEnabled(true);
            }else if(state.equalsIgnoreCase("match")){
               // Toast.makeText(getActivity(),"This fingerprint already Captured ",Toast.LENGTH_LONG).show();
                showAlert("DUPLICATE ERROR","This fingerprint already Captured!!");
            }else if(state.equalsIgnoreCase("low")){
              //  Toast.makeText(getActivity(), "Please Recapture again ", Toast.LENGTH_LONG).show();
                showAlert("QUALITY ERROR","Please Recapture again!!");
            }
        }else if(v.getId()==R.id.img2){
            state="";
            CaptureFingerPrint(RTHUMB);
            if(state.equalsIgnoreCase("nomatch")) {
                r0.clearAnimation();
                r0.setVisibility(View.GONE);

                sr0.setVisibility(View.VISIBLE);

                r1.startAnimation(animation);
                r1.setVisibility(View.VISIBLE);
                img2.setEnabled(false);
                img3.setEnabled(true);
            } else if(state.equalsIgnoreCase("match")){
              //  Toast.makeText(getActivity(),"This Fingerprint already Captured ",Toast.LENGTH_LONG).show();
                showAlert("DUPLICATE ERROR","This fingerprint already Captured!!");
            }else if(state.equalsIgnoreCase("low")){
               // Toast.makeText(getActivity(), "Please Recapture again ", Toast.LENGTH_LONG).show();
                showAlert("QUALITY ERROR","Please Recapture again!!");
            }
        }else if(v.getId()==R.id.img3) {
            state="";
            CaptureFingerPrint(RINDEX);

            if (state.equalsIgnoreCase("nomatch")) {
                r1.clearAnimation();
                r1.setVisibility(View.GONE);

                sr1.setVisibility(View.VISIBLE);
                img3.setEnabled(false);
            } else if(state.equalsIgnoreCase("match")){
                //Toast.makeText(getActivity(), "This fingerprint already Captured ", Toast.LENGTH_LONG).show();
                showAlert("DUPLICATE ERROR","This fingerprint already Captured!!");
            }else if(state.equalsIgnoreCase("low")){
              //  Toast.makeText(getActivity(), "Please Recapture again ", Toast.LENGTH_LONG).show();
                showAlert("QUALITY ERROR","Please Recapture again!!");
            }
        }
        if(v.getId() == R.id.button1){
            img0.setImageBitmap(null);
            img0.setImageResource(R.drawable.noimg);
            img1.setImageBitmap(null);
            status="";
            img1.setImageResource(R.drawable.noimg);
            img2.setImageBitmap(null);
            img2.setImageResource(R.drawable.noimg);
            img3.setImageBitmap(null);
            img3.setImageResource(R.drawable.noimg);
            img0.setEnabled(true);
            img1.setEnabled(false);
            img2.setEnabled(false);
            img3.setEnabled(false);
            img0.setEnabled(false);
            l0.startAnimation(animation);
            l0.setVisibility(View.VISIBLE);
            l1.clearAnimation();
            l1.setVisibility(View.GONE);
            r1.clearAnimation();
            r1.setVisibility(View.GONE);
            r0.clearAnimation();
            r0.setVisibility(View.GONE);
            sl0.setVisibility(View.GONE);
            sl1.setVisibility(View.GONE);
            sr0.setVisibility(View.GONE);
            sr1.setVisibility(View.GONE);
        }
        if(v.getId() == R.id.next){
           /* if(status.equalsIgnoreCase("")){*/
                viewPager.setCurrentItem(5, true);
            /*}else{
                showAlert("ERROR!!","Please ensure that all fingerprints are captured!!");
            }*/



        }else if(v.getId() == R.id.prev) {
            Log.d("dddsdsdsd","clicked");
            viewPager.setCurrentItem(3, true);

        }
    }
    @Override
    public void onResume(){
        Log.d("#####WEWRWREWRWR", "onResume()");
        getActivity().registerReceiver(mUsbReceiver, filter);
       // initLib();
        super.onResume();
        //getActivity().registerReceiver(mUsbReceiver, filter);
       /* long error = sgfplib.Init( SGFDxDeviceName.SG_DEV_AUTO);
        if (error != SGFDxErrorCode.SGFDX_ERROR_NONE){
            AlertDialog.Builder dlgAlert = new AlertDialog.Builder(getActivity());
            if (error == SGFDxErrorCode.SGFDX_ERROR_DEVICE_NOT_FOUND)
                dlgAlert.setMessage("The attached fingerprint device is not supported on Android");
            else
                dlgAlert.setMessage("Fingerprint device initialization failed!");
            dlgAlert.setTitle("SecuGen Fingerprint SDK");
            dlgAlert.setPositiveButton("OK",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog,int whichButton){
                           getActivity(). finish();
                            return;
                        }
                    }
            );
            dlgAlert.setCancelable(false);
            dlgAlert.create().show();
        }
        else {
            UsbDevice usbDevice = sgfplib.GetUsbDevice();
            if (usbDevice == null){
                AlertDialog.Builder dlgAlert = new AlertDialog.Builder(getActivity());
                dlgAlert.setMessage("SDU04P or SDU03P fingerprint sensor not found!");
                dlgAlert.setTitle("SecuGen Fingerprint SDK");
                dlgAlert.setPositiveButton("OK",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,int whichButton){
                                getActivity().finish();
                                return;
                            }
                        }
                );
                dlgAlert.setCancelable(false);
                dlgAlert.create().show();
            }
            else {
                sgfplib.GetUsbManager().requestPermission(usbDevice, mPermissionIntent);
                error = sgfplib.OpenDevice(0);
                debugMessage("OpenDevice() ret: " + error + "\n");
                SecuGen.FDxSDKPro.SGDeviceInfoParam deviceInfo = new SecuGen.FDxSDKPro.SGDeviceInfoParam();
                error = sgfplib.GetDeviceInfo(deviceInfo);
                debugMessage("GetDeviceInfo() ret: " + error + "\n");
                mImageWidth = deviceInfo.imageWidth;
                mImageHeight= deviceInfo.imageHeight;
                debugMessage("Image width: " + mImageWidth + "\n");
                debugMessage("Image height: " + mImageHeight + "\n");
                debugMessage("Serial Number: " + new String(deviceInfo.deviceSN()) + "\n");
                sgfplib.SetTemplateFormat(SGFDxTemplateFormat.TEMPLATE_FORMAT_SG400);
                sgfplib.GetMaxTemplateSize(mMaxTemplateSize);
                debugMessage("TEMPLATE_FORMAT_SG400 SIZE: " + mMaxTemplateSize[0] + "\n");
                mRegisterTemplate0 = new byte[mMaxTemplateSize[0]];
                mRegisterTemplate1 = new byte[mMaxTemplateSize[0]];
                mRegisterTemplate2 = new byte[mMaxTemplateSize[0]];
                mRegisterTemplate3 = new byte[mMaxTemplateSize[0]];
                //Thread thread = new Thread(this);
                //thread.start();
            }
        }*/
    }

    @Override
    public void onPause() {
        Log.d("#####DDKKDKDDK", "onPause()");
        getActivity().unregisterReceiver(mUsbReceiver);
        super.onPause();
    }

    @Override
    public void onDestroy(){
        //getActivity().unregisterReceiver(mUsbReceiver);


        super.onDestroy();
    }


    //Converts image to grayscale (NEW)
    public Bitmap toGrayscale(byte[] mImageBuffer)
    {
        byte[] Bits = new byte[mImageBuffer.length * 4];
        for (int i = 0; i < mImageBuffer.length; i++) {
            Bits[i * 4] = Bits[i * 4 + 1] = Bits[i * 4 + 2] = mImageBuffer[i]; // Invert the source bits
            Bits[i * 4 + 3] = -1;// 0xff, that's the alpha.
        }

        Bitmap bmpGrayscale = Bitmap.createBitmap(mImageWidth, mImageHeight, Bitmap.Config.ARGB_8888);
        //Bitmap bm contains the fingerprint img
        bmpGrayscale.copyPixelsFromBuffer(ByteBuffer.wrap(Bits));
        return bmpGrayscale;
    }


    //Converts image to grayscale (NEW)
    public Bitmap toGrayscale(Bitmap bmpOriginal)
    {
        int width, height;
        height = bmpOriginal.getHeight();
        width = bmpOriginal.getWidth();
        Bitmap bmpGrayscale = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
        for (int y=0; y< height; ++y) {
            for (int x=0; x< width; ++x){
                int color = bmpOriginal.getPixel(x, y);
                int r = (color >> 16) & 0xFF;
                int g = (color >> 8) & 0xFF;
                int b = color & 0xFF;
                int gray = (r+g+b)/3;
                color = Color.rgb(gray, gray, gray);
                //color = Color.rgb(r/3, g/3, b/3);
                bmpGrayscale.setPixel(x, y, color);
            }
        }
        return bmpGrayscale;
    }

    //Converts image to binary (OLD)
    public Bitmap toBinary(Bitmap bmpOriginal)
    {
        int width, height;
        height = bmpOriginal.getHeight();
        width = bmpOriginal.getWidth();
        Bitmap bmpGrayscale = Bitmap.createBitmap(width, height, Bitmap.Config.RGB_565);
        Canvas c = new Canvas(bmpGrayscale);
        Paint paint = new Paint();
        ColorMatrix cm = new ColorMatrix();
        cm.setSaturation(0);
        ColorMatrixColorFilter f = new ColorMatrixColorFilter(cm);
        paint.setColorFilter(f);
        c.drawBitmap(bmpOriginal, 0, 0, paint);
        return bmpGrayscale;
    }
    /**
     *
     * @param requestCode
     */
    public void CaptureFingerPrint(int requestCode){
        try{
            if(sgfplib!=null){
                long dwTimeStart = 0, dwTimeEnd = 0, dwTimeElapsed = 0;
                byte[] buffer = new byte[mImageWidth*mImageHeight];
                dwTimeStart = System.currentTimeMillis();
                long result = sgfplib.GetImage(buffer);
//	    DumpFile("capture.raw", buffer);
                dwTimeEnd = System.currentTimeMillis();
                dwTimeElapsed = dwTimeEnd-dwTimeStart;
                debugMessage("getImage() ret:" + result + " [" + dwTimeElapsed + "ms]\n");
                Bitmap b = Bitmap.createBitmap(mImageWidth,mImageHeight, Bitmap.Config.ARGB_8888);
                b.setHasAlpha(false);
                int[] intbuffer = new int[mImageWidth*mImageHeight];
                for (int i=0; i<intbuffer.length; ++i)
                    intbuffer[i] = (int) buffer[i];
                b.setPixels(intbuffer, 0, mImageWidth, 0, 0, mImageWidth, mImageHeight);

                result = sgfplib.SetTemplateFormat(SecuGen.FDxSDKPro.SGFDxTemplateFormat.TEMPLATE_FORMAT_SG400);
                dwTimeEnd = System.currentTimeMillis();
                dwTimeElapsed = dwTimeEnd-dwTimeStart;
                debugMessage("SetTemplateFormat(SG400) ret:" +  result + " [" + dwTimeElapsed + "ms]\n");
                SGFingerInfo fpInfo = new SGFingerInfo();

                boolean[] matched = new boolean[1];
                boolean[] matched1 = new boolean[1];
                boolean[] matched2 = new boolean[1];
                int[] quality = new int[1];

                sgfplib.GetImageQuality(mImageWidth, mImageHeight, buffer, quality);
                if (quality[0] > 60) {
                    if(requestCode == LTHUMB){
                        img0.setImageBitmap(this.toGrayscale(b));
                        for (int i=0; i< mRegisterTemplate0.length; ++i)
                            mRegisterTemplate0[i] = 0;
                        dwTimeStart = System.currentTimeMillis();
                        result = sgfplib.CreateTemplate(fpInfo, buffer, mRegisterTemplate0);

                        //DumpFile("register.min", mRegisterTemplate0);
                        dwTimeEnd = System.currentTimeMillis();
                        dwTimeElapsed = dwTimeEnd-dwTimeStart;
                        debugMessage("CreateTemplate() ret:" + result + " [" + dwTimeElapsed + "ms]\n");
                        encoded_lthumb = Base64.encodeToString(mRegisterTemplate0, Base64.DEFAULT);
                        Member.left_thumb=encoded_lthumb;
                        state = "okay";
                    }

                    if (requestCode == LINDEX) {
                        img1.setImageBitmap(this.toGrayscale(b));
                        for (int i=0; i< mRegisterTemplate1.length; ++i)
                            mRegisterTemplate1[i] = 0;
                        dwTimeStart = System.currentTimeMillis();
                        result = sgfplib.CreateTemplate(fpInfo, buffer, mRegisterTemplate1);

                        //DumpFile("register.min", mRegisterTemplate0);
                        dwTimeEnd = System.currentTimeMillis();
                        dwTimeElapsed = dwTimeEnd-dwTimeStart;
                        debugMessage("CreateTemplate() ret:" + result + " [" + dwTimeElapsed + "ms]\n");

                        result = sgfplib.MatchTemplate(mRegisterTemplate0, mRegisterTemplate1, SGFDxSecurityLevel.SL_NORMAL, matched);
                        if (matched[0]) {
                            state = "match";
                        } else {
                            img1.setImageBitmap(this.toGrayscale(b));
                            encoded_lindex = Base64.encodeToString(mRegisterTemplate1, Base64.DEFAULT);
                            Member.left_index=encoded_lindex;
                            state = "nomatch";
                        }
                    }

                    if (requestCode == RTHUMB) {
                        img2.setImageBitmap(this.toGrayscale(b));
                        state = "nomatch";
                        for (int i=0; i< mRegisterTemplate2.length; ++i)
                            mRegisterTemplate2[i] = 0;
                        dwTimeStart = System.currentTimeMillis();
                        result = sgfplib.CreateTemplate(fpInfo, buffer, mRegisterTemplate2);

                        //DumpFile("register.min", mRegisterTemplate0);
                        dwTimeEnd = System.currentTimeMillis();
                        dwTimeElapsed = dwTimeEnd-dwTimeStart;
                        debugMessage("CreateTemplate() ret:" + result + " [" + dwTimeElapsed + "ms]\n");

                        result = sgfplib.MatchTemplate(mRegisterTemplate2, mRegisterTemplate1, SGFDxSecurityLevel.SL_NORMAL, matched);
                        result = sgfplib.MatchTemplate(mRegisterTemplate2, mRegisterTemplate0, SGFDxSecurityLevel.SL_NORMAL, matched1);
                        if (!matched[0] && !matched1[0]) {
                            img2.setImageBitmap(this.toGrayscale(b));
                            encoded_rthumb = Base64.encodeToString(mRegisterTemplate2, Base64.DEFAULT);
                            Member.right_thumb=encoded_rthumb;
                            state = "nomatch";

                        } else {
                            state = "match";
                        }
                    }

                    if (requestCode == RINDEX) {
                        img3.setImageBitmap(this.toGrayscale(b));
                        for (int i = 0; i < mRegisterTemplate3.length; ++i)
                            mRegisterTemplate3[i] = 0;
                        dwTimeStart = System.currentTimeMillis();
                        result = sgfplib.CreateTemplate(fpInfo, buffer, mRegisterTemplate3);

                        //DumpFile("register.min", mRegisterTemplate0);
                        dwTimeEnd = System.currentTimeMillis();
                        dwTimeElapsed = dwTimeEnd - dwTimeStart;
                        debugMessage("CreateTemplate() ret:" + result + " [" + dwTimeElapsed + "ms]\n");

                        result = sgfplib.MatchTemplate(mRegisterTemplate3, mRegisterTemplate2, SGFDxSecurityLevel.SL_NORMAL, matched);
                        result = sgfplib.MatchTemplate(mRegisterTemplate3, mRegisterTemplate1, SGFDxSecurityLevel.SL_NORMAL, matched1);
                        result = sgfplib.MatchTemplate(mRegisterTemplate3, mRegisterTemplate0, SGFDxSecurityLevel.SL_NORMAL, matched2);

                        if (!matched[0] && !matched1[0] && !matched2[0]) {
                            img3.setImageBitmap(this.toGrayscale(b));
                            encoded_rindex = Base64.encodeToString(mRegisterTemplate3, Base64.DEFAULT);
                            Member.right_index =encoded_rindex;
                            status="all";
                            state = "nomatch";
                        } else {
                            state = "match";
                        }
                    }
                } else{
                    state = "low";
                   // fingershowAlert("CAPTURE ERROR","Please Recapture FingerPrint");
                    initLib();
                    //restartCapture();
                }
            } else {
                initLib();
                //restartCapture();
                fingershowAlert("CAPTURE ERROR","Please Recapture FingerPrint");
                state = "";
            }

        }catch(Exception ex){
            state = "";
            Log.d("######EXR",ex.toString());
           /*
            final MaterialDialog pDialog = new MaterialDialog.Builder(getActivity())
                    .title("Initializing fingerprint Scanner")
                    .content("Please wait...")
                    .progress(true, 0)
                    .progressIndeterminateStyle(false)
                    .show();
            Thread h= new Thread(new Runnable() {
                @Override
                public void run() {

                   // scanner.powerOn();
                    initLib();
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            pDialog.dismiss();
                        }
                    });

                }
            });
           h.start();*/
            fingershowAlert("CAPTURE ERROR","Please Capture again");
            initLib();
            scanner.powerOn();
            //restartCapture();
            //
           // Toast.makeText(getActivity(),"Please try again",Toast.LENGTH_SHORT).show();
        }
    }
    public void fingershowAlert(String title, String message){
        Log.d("ALERT","ALERT");
        new SweetAlertDialog(getActivity(), SweetAlertDialog.WARNING_TYPE)
                .setTitleText(title)
                .setContentText(message)
                .setConfirmText("Ok")
                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sDialog) {
                        sDialog.dismissWithAnimation();

                        //   showCustomViewFingerprint();

                    }
                })
                .show();
    }

    /**
     *
     * @param message
     */
    public void showAlert(String title,String message){
        Log.d("ALERT","ALERT");
        new SweetAlertDialog(getActivity(), SweetAlertDialog.WARNING_TYPE)
                .setTitleText(title)
                .setContentText(message)
                .setConfirmText("Ok")
                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sDialog) {
                        sDialog.dismissWithAnimation();
                    }
                })
                .show();
    }


    @Override
    public void run() {
        Log.d(TAG, "Enter run()");
        //ByteBuffer buffer = ByteBuffer.allocate(1);
        //UsbRequest request = new UsbRequest();
        //request.initialize(mSGUsbInterface.getConnection(), mEndpointBulk);
        //byte status = -1;
        while (true) {
            // queue a request on the interrupt endpoint
            //request.queue(buffer, 1);
            // send poll status command
            //  sendCommand(COMMAND_STATUS);
            // wait for status event
            /*
            if (mSGUsbInterface.getConnection().requestWait() == request) {
                byte newStatus = buffer.get(0);
                if (newStatus != status) {
                    Log.d(TAG, "got status " + newStatus);
                    status = newStatus;
                    if ((status & COMMAND_FIRE) != 0) {
                        // stop firing
                        sendCommand(COMMAND_STOP);
                    }
                }
                try {
                    Thread.sleep(100);
                } catch (InterruptedException e) {
                }
            } else {
                Log.e(TAG, "requestWait failed, exiting");
                break;
            }
            */
    }
    }


    public void SGFingerPresentCallback (){
        //autoOn.stop();
        //fingerDetectedHandler.sendMessage(new Message());
    }

    private void restartCapture(){
        img0.setEnabled(false);
        img1.setEnabled(false);
        img2.setEnabled(false);
        img3.setEnabled(false);

        l0.startAnimation(animation);
        l1.startAnimation(animation);
        r0.startAnimation(animation);
        r1.startAnimation(animation);

        img0.setEnabled(true);
        l0.setVisibility(View.VISIBLE);
    }

    /**
     *
     */
    private void initLib() {
        sgfplib = new JSGFPLib((UsbManager)getActivity().getSystemService(Context.USB_SERVICE));
        long error = sgfplib.Init( SGFDxDeviceName.SG_DEV_AUTO);
        if (error != SGFDxErrorCode.SGFDX_ERROR_NONE){
          final  AlertDialog.Builder dlgAlert = new AlertDialog.Builder(getActivity());
            if (error == SGFDxErrorCode.SGFDX_ERROR_DEVICE_NOT_FOUND)
                dlgAlert.setMessage("The attached fingerprint device is not supported on Android");
            else
                dlgAlert.setMessage("Fingerprint device initialization failed!");
            dlgAlert.setTitle("SecuGen Fingerprint SDK");
            dlgAlert.setPositiveButton("OK",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog,int whichButton){
                            getActivity().finish();
                            return;
                        }
                    }
            );
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    dlgAlert.setCancelable(true);
                    dlgAlert.create().show();
                }
            });
        }
        else {
            UsbDevice usbDevice = sgfplib.GetUsbDevice();
            if (usbDevice == null){
               final AlertDialog.Builder dlgAlert = new AlertDialog.Builder(getActivity());
                dlgAlert.setMessage("SDU04P or SDU03P fingerprint sensor not found!");
                dlgAlert.setTitle("SecuGen Fingerprint SDK");
                dlgAlert.setPositiveButton("OK",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,int whichButton){
                                getActivity().finish();
                                return;
                            }
                        }
                );
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        dlgAlert.setCancelable(true);
                        dlgAlert.create().show();
                    }
                });
            }
            else {
                try {

                    sgfplib.GetUsbManager().requestPermission(usbDevice, mPermissionIntent);
                    error = sgfplib.OpenDevice(0);
                    debugMessage("OpenDevice() ret: " + error + "\n");
                    SecuGen.FDxSDKPro.SGDeviceInfoParam deviceInfo = new SecuGen.FDxSDKPro.SGDeviceInfoParam();
                    error = sgfplib.GetDeviceInfo(deviceInfo);
                    debugMessage("GetDeviceInfo() ret: " + error + "\n");
                    mImageWidth = deviceInfo.imageWidth;
                    mImageHeight= deviceInfo.imageHeight;
                    debugMessage("Image width: " + mImageWidth + "\n");
                    debugMessage("Image height: " + mImageHeight + "\n");
                    debugMessage("Serial Number: " + new String(deviceInfo.deviceSN()) + "\n");
                    sgfplib.SetTemplateFormat(SGFDxTemplateFormat.TEMPLATE_FORMAT_SG400);
                    sgfplib.GetMaxTemplateSize(mMaxTemplateSize);
                    debugMessage("TEMPLATE_FORMAT_SG400 SIZE: " + mMaxTemplateSize[0] + "\n");
                    mRegisterTemplate0 = new byte[mMaxTemplateSize[0]];
                    mRegisterTemplate1 = new byte[mMaxTemplateSize[0]];
                    mRegisterTemplate2 = new byte[mMaxTemplateSize[0]];
                    mRegisterTemplate3 = new byte[mMaxTemplateSize[0]];
                    //Thread thread = new Thread(this);
                    //thread.start();
                }catch (Exception e){

                }

//                mRegisterTemplate = new byte[mMaxTemplateSize[0]];
//                mVerifyTemplate = new byte[mMaxTemplateSize[0]];
//                boolean smartCaptureEnabled = this.mToggleButtonSmartCapture.isChecked();
//                if (smartCaptureEnabled)
//                    sgfplib.WriteData((byte)5, (byte)1);
//                else
//                    sgfplib.WriteData((byte)5, (byte)0);
//                if (mAutoOnEnabled){
//                    autoOn.start();
//                    DisableControls();
//                }
                Log.d(TAG, "Init finish");
            }
        }
    }
}
