package com.evoucher.compas.evoucher.fragments;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import com.evoucher.compas.evoucher.Adapters.ScrollerViewPager;
import com.evoucher.compas.evoucher.CaptureSignature;
import com.evoucher.compas.evoucher.R;
import com.lynx.evoucher.models.Member;

import java.io.ByteArrayOutputStream;
import cn.pedant.SweetAlert.SweetAlertDialog;


/**
 * Created by JOHNIE on 9/3/2015.
 */
public class Fragment4 extends Fragment implements View.OnClickListener{
   TextView b1;
    ImageView signImage;
    Bitmap b;
    private String encoded_signature;
    ScrollerViewPager viewPager;
    ImageView error1,next,prev;
    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        if (savedInstanceState != null) {
            b =(Bitmap)savedInstanceState.getParcelable("BitmapImage");
        }
    }
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment4, container, false);

    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        b1 = (TextView)getView().findViewById(R.id.getSign);
        signImage = (ImageView)getView().findViewById(R.id.imageView1);
        b1.setOnClickListener(onButtonClick);
        error1 = (ImageView)getView().findViewById(R.id.error1);
        next = (ImageView)getView().findViewById(R.id.next);
        prev = (ImageView)getView().findViewById(R.id.prev);

        next.setOnClickListener(this);
        prev.setOnClickListener(this);
        viewPager = (ScrollerViewPager) getActivity().findViewById(R.id.view_pager);
    }
    Button.OnClickListener onButtonClick = new Button.OnClickListener() {

        @Override
        public void onClick(View v) {
            // TODO Auto-generated method stub
            Intent i = new Intent(getActivity(), CaptureSignature.class);
            startActivityForResult(i, 0);
        }
    };

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        // TODO Auto-generated method stub
        if (resultCode == 1) {

            b = BitmapFactory.decodeByteArray(
                    data.getByteArrayExtra("byteArray"), 0,
                    data.getByteArrayExtra("byteArray").length);
            signImage.setImageBitmap(b);
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            b.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream);
            byte[] byteArray = byteArrayOutputStream .toByteArray();
            encoded_signature = Base64.encodeToString(byteArray, Base64.DEFAULT);
            //error1.setVisibility(View.GONE);
        }
    }
    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        if (savedInstanceState != null) {
           b = (Bitmap) savedInstanceState.getParcelable("BitmapImage");
            if (b != null) {
                signImage.setImageBitmap(b);
            }
        }
    }
    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelable("BitmapImage", b);



    }

    @Override
    public void onClick(View v) {
        Member dt=new Member();
         if(v.getId() == R.id.next){
             if(signImage.getDrawable()!=null) {
                 Member.signature=encoded_signature;
                 viewPager.setCurrentItem(4, true);
             }else{
                 showAlert();
             }
        }else if(v.getId() == R.id.prev) {
             Log.d("dddsdsdsd","clicked");
            viewPager.setCurrentItem(2, true);

        }
    }
    public void showAlert(){
        Log.d("ALERT","ALERT");
        new SweetAlertDialog(getActivity(), SweetAlertDialog.WARNING_TYPE)
                .setTitleText("Empty Signature")
                .setContentText("Please ensure that the client's signature is captured.")
                .setConfirmText("Ok")
                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sDialog) {
                        sDialog.dismissWithAnimation();

                    }
                })
                .show();
    }
}