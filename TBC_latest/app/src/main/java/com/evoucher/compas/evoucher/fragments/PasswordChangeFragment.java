package com.evoucher.compas.evoucher.fragments;

import android.annotation.TargetApi;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.evoucher.compas.evoucher.Beneficairy_Activity;
import com.evoucher.compas.evoucher.Card_Details;
import com.evoucher.compas.evoucher.CartActivity;
import com.evoucher.compas.evoucher.Details;
import com.evoucher.compas.evoucher.ProgrammeActivity;
import com.evoucher.compas.evoucher.R;
import com.evoucher.compas.evoucher.Utils;
import com.evoucher.compas.evoucher.card.KeyInfoProvider;
import com.evoucher.compas.evoucher.card.SampleAppKeys;
import com.lynx.evoucher.models.Programmes;
import com.lynx.evoucher.models.Topups;
import com.newpos.libpay.device.card.CardInfo;
import com.newpos.libpay.device.card.CardListener;
import com.newpos.libpay.device.card.CardManager;
import com.nxp.nfclib.CardType;
import com.nxp.nfclib.KeyType;
import com.nxp.nfclib.NxpNfcLib;
import com.nxp.nfclib.desfire.DESFireFactory;
import com.nxp.nfclib.desfire.IDESFireEV1;
import com.nxp.nfclib.exceptions.NxpNfcLibException;
import com.nxp.nfclib.interfaces.IKeyData;

import org.json.JSONObject;

import java.security.NoSuchAlgorithmException;
import java.util.Arrays;

import javax.crypto.Cipher;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.IvParameterSpec;

import cn.pedant.SweetAlert.SweetAlertDialog;

/**
 * Created by Chirchir on 8/18/2017.
 */

public class PasswordChangeFragment extends Fragment {

    private IKeyData objKEY_2KTDES_ULC = null;
    private IKeyData objKEY_2KTDES = null;
    private IKeyData objKEY_AES128 = null;
    private byte[] default_ff_key = null;
    private IKeyData default_zeroes_key = null;
    private NxpNfcLib libInstance = null;
    private IDESFireEV1 desFireEV1;
    private CardType mCardType = CardType.UnknownCard;
    private Cipher cipher = null;
    private byte[] bytesKey = null;
    JSONObject card_pin = null;
    JSONObject cardbalance = null;
    JSONObject personal_data = null;
    private IvParameterSpec iv = null;
    MaterialDialog pDialogg;
    private static final String KEY_APP_MASTER = "This is my key  ";
    private static final String ALIAS_KEY_AES128 = "key_aes_128";
    private static final String ALIAS_KEY_2KTDES = "key_2ktdes";
    private static final String ALIAS_KEY_2KTDES_ULC = "key_2ktdes_ulc";
    private static final String ALIAS_DEFAULT_FF = "alias_default_ff";
    private static final String ALIAS_KEY_AES128_ZEROES = "alias_default_00";
    private static final String EXTRA_KEYS_STORED_FLAG = "keys_stored_flag";
    static String packageKey = "b927aab8842ab54cbaf2ea1df9917159";
    byte[] appId = new byte[]{0x1, 0x00, 0x00};
    int fileSize = 100;
    byte[] data = new byte[]{0x11, 0x11, 0x11, 0x11,
            0x11};
    int timeOut = 2000;
    boolean changePin = false;
    String bgroupId;
    Details dt;
    String paswword = "";
    MaterialDialog dialog;

    EditText newPassword, confirmPassword;
    Button change;
    TextView name, ration;
    ViewGroup changepinview, tapview;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        return inflater.inflate(R.layout.passwordchange, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        dt = new Details(getActivity());
        newPassword = (EditText) (getView().findViewById(R.id.input_password1));
        confirmPassword = (EditText) getView().findViewById(R.id.input_password2);
        change = (Button) getView().findViewById(R.id.change);
        name = (TextView) getView().findViewById(R.id.name);
        ration = (TextView) getView().findViewById(R.id.ration);

        changepinview = (ViewGroup) getView().findViewById(R.id.changepin);
        tapview = (ViewGroup) getView().findViewById(R.id.tapview);

        tapview.setVisibility(View.VISIBLE);
        changepinview.setVisibility(View.GONE);

        change.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (!(newPassword.getText().toString().equalsIgnoreCase("")) && !(confirmPassword.getText().toString().equalsIgnoreCase(""))) {

                    if (newPassword.getText().toString().equalsIgnoreCase(confirmPassword.getText().toString())) {
                        paswword = newPassword.getText().toString();

                        try {
                            changePin = true;
                            dialog = new MaterialDialog.Builder(getActivity())
                                    .title(getString(R.string.TapToUpdate))
                                    .content("")
                                    .progress(true, 0)
                                    .progressIndeterminateStyle(true)
                                    .cancelable(false)
                                    .show();
                            final Handler handler = new Handler();
                            handler.postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    changePin = false;
                                    dialog.dismiss();
                                }

                            }, 10000);
                            new Thread(new Runnable() {
                                @Override
                                public void run() {

                                    update_nfc_card(com.newpos.libpay.device.card.CardType.INMODE_NFC.getVal(), 10000);

                                }
                            }).start();

                        } catch (Exception e) {

                        }


                    } else {
                        new SweetAlertDialog(getContext(), SweetAlertDialog.WARNING_TYPE)
                                .setTitleText("Password")
                                .setContentText("Change not successful.The passwords do not match")
                                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                    @Override
                                    public void onClick(SweetAlertDialog sDialog) {
                                        sDialog.dismissWithAnimation();
                                        //  startActivity(new Intent(getContext(), ProgrammeActivity.class));
                                    }
                                }).show();
                    }
                } else {
                    new SweetAlertDialog(getContext(), SweetAlertDialog.WARNING_TYPE)
                            .setTitleText("Error")
                            .setContentText("Please input both fields")
                            .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sDialog) {
                                    sDialog.dismissWithAnimation();
                                    //  startActivity(new Intent(getContext(), ProgrammeActivity.class));
                                }
                            }).show();
                }
            }
        });


        initializeLibrary();
        initializeKeys();
        initializeCipherinitVector();
        pDialogg = new MaterialDialog.Builder(getActivity())
                .title(getString(R.string.searchcard))
                .content(getString(R.string.tapcard))
                .progress(true, 0)
                .progressIndeterminateStyle(false)
                .show();
        new Thread(new Runnable() {
            @Override
            public void run() {

                read_nfc_card(com.newpos.libpay.device.card.CardType.INMODE_NFC.getVal(), 10000);

            }
        }).start();

    }

    public void cardLogic(final Intent intent) {
        CardType type = CardType.UnknownCard;
        try {
            type = libInstance.getCardType(intent);
        } catch (NxpNfcLibException ex) {
            try {
                pDialogg.dismiss();
            } catch (Exception e) {

            }

            new SweetAlertDialog(getActivity(), SweetAlertDialog.WARNING_TYPE)
                    .setTitleText(getString(R.string.nfc_error))
                    .setContentText(getString(R.string.nfc_error_totle))
                    .setConfirmText(getString(R.string.Ok))
                    .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                        @Override
                        public void onClick(SweetAlertDialog sDialog) {
                            sDialog.dismissWithAnimation();

                        }
                    })
                    .show();
        }

        switch (type) {

            case DESFireEV1: {
                mCardType = CardType.DESFireEV1;
                desFireEV1 = DESFireFactory.getInstance().getDESFire(libInstance.getCustomModules());
                try {

                    desFireEV1.getReader().connect();
                    desFireEV1.getReader().setTimeout(2000);
                    pDialogg = new MaterialDialog.Builder(getActivity())
                            .title(getString(R.string.Readingcard))
                            .content(getString(R.string.DontRemoveCard))
                            .progress(true, 0)
                            .progressIndeterminateStyle(false)
                            .show();
                    new Thread(new Runnable() {
                        @Override
                        public void run() {
                            desfireEV1CardLogic();
                        }
                    }).start();

                } catch (Exception t) {
                    t.printStackTrace();
                    Log.d("##ERRORRR", t.toString());

                }
                break;
            }


        }
    }

    private void desfireEV1CardLogic() {
        String uidhex = new String();
        byte[] uid = desFireEV1.getUID();
        for (int i = 0; i < uid.length; i++) {
            String x = Integer.toHexString(((int) uid[i] & 0xff));
            if (x.length() == 1) {
                x = '0' + x;
            }
            uidhex += x;
        }

        if (changePin) {
            if (uidhex.equalsIgnoreCase(dt.getUid())) {
                try {
                    card_pin.put("pin", paswword);
                } catch (Exception e) {
                    Log.i("##DSDSDSD", e.toString());

                }

                desFireEV1.getReader().setTimeout(timeOut);
                desFireEV1.authenticate(0, IDESFireEV1.AuthType.Native, KeyType.TWO_KEY_THREEDES, objKEY_2KTDES);
                desFireEV1.selectApplication(0);
                desFireEV1.selectApplication(appId);
                desFireEV1.authenticate(0, IDESFireEV1.AuthType.Native, KeyType.TWO_KEY_THREEDES, objKEY_2KTDES);
                desFireEV1.writeData(Card_Details.cpin, 0, card_pin.toString().getBytes());
                changePin = false;
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        dialog.dismiss();

                        name.setText("");
                        ration.setText("");

                        showAlert("Great!", "Password changed successfully");
                    }
                });

            } else {
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        pDialogg.dismiss();
                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                pDialogg.dismiss();
                                new SweetAlertDialog(getActivity(), SweetAlertDialog.WARNING_TYPE)
                                        .setTitleText("Error")
                                        .setContentText("Please tap the right card")
                                        .setConfirmText(getString(R.string.Ok))
                                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                            @Override
                                            public void onClick(SweetAlertDialog sDialog) {
                                                sDialog.dismissWithAnimation();

                                            }
                                        })
                                        .show();

                            }
                        });

                    }
                });
            }


        } else {

            card_pin = new JSONObject();
            personal_data = new JSONObject();
            try {

                dt.setUid(uidhex);

                desFireEV1.getReader().setTimeout(timeOut);
                desFireEV1.authenticate(0, IDESFireEV1.AuthType.Native, KeyType.TWO_KEY_THREEDES, objKEY_2KTDES);
                desFireEV1.selectApplication(0);
                desFireEV1.selectApplication(appId);
                desFireEV1.authenticate(0, IDESFireEV1.AuthType.Native, KeyType.TWO_KEY_THREEDES, objKEY_2KTDES);

                card_pin = new JSONObject(new String(desFireEV1.readData(Card_Details.cpin, 0, 0)));
                personal_data = new JSONObject(new String(desFireEV1.readData(Card_Details.personalDetails, 0, 0)));
                changePin = true;
                Log.i("##SWDSDSDS", card_pin.toString());

                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        try {
                            name.setText("Name:  " + personal_data.getString("name").replace("   ", " "));
                            ration.setText("Ration No: " + personal_data.getString("rationalNo"));
                        } catch (Exception e) {

                        }
                        tapview.setVisibility(View.GONE);
                        changepinview.setVisibility(View.VISIBLE);
                        pDialogg.dismiss();

                    }
                });


            } catch (Exception e) {

                //the card is not activated
                pDialogg.dismiss();
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        pDialogg.dismiss();
                        new SweetAlertDialog(getActivity(), SweetAlertDialog.WARNING_TYPE)
                                .setTitleText("Error")
                                .setContentText("Unable to read the data.Please ensure that this card is activated")
                                .setConfirmText(getString(R.string.Ok))
                                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                    @Override
                                    public void onClick(SweetAlertDialog sDialog) {
                                        sDialog.dismissWithAnimation();

                                    }
                                })
                                .show();

                    }
                });

            }

        }


    }


    @TargetApi(19)
    private void initializeLibrary() {
        libInstance = NxpNfcLib.getInstance();
        try {
            libInstance.registerActivity(getActivity(), packageKey);
        } catch (NxpNfcLibException ex) {
            // Toast.makeText(this, ex.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    private void initializeKeys() {

        KeyInfoProvider infoProvider = KeyInfoProvider.getInstance(getActivity().getApplicationContext());
        infoProvider.setKey(ALIAS_KEY_2KTDES, SampleAppKeys.EnumKeyType.EnumDESKey, SampleAppKeys.KEY_2KTDES);
        infoProvider.setKey(ALIAS_KEY_AES128, SampleAppKeys.EnumKeyType.EnumAESKey, SampleAppKeys.KEY_AES128);
        infoProvider.setKey(ALIAS_KEY_AES128_ZEROES, SampleAppKeys.EnumKeyType.EnumAESKey, SampleAppKeys.KEY_AES128_ZEROS);
        infoProvider.setKey(ALIAS_DEFAULT_FF, SampleAppKeys.EnumKeyType.EnumMifareKey, SampleAppKeys.KEY_DEFAULT_FF);
        objKEY_2KTDES_ULC = infoProvider.getKey(ALIAS_KEY_2KTDES_ULC, SampleAppKeys.EnumKeyType.EnumDESKey);
        objKEY_2KTDES = infoProvider.getKey(ALIAS_KEY_2KTDES, SampleAppKeys.EnumKeyType.EnumDESKey);
        objKEY_AES128 = infoProvider.getKey(ALIAS_KEY_AES128, SampleAppKeys.EnumKeyType.EnumAESKey);
        default_zeroes_key = infoProvider.getKey(ALIAS_KEY_AES128_ZEROES, SampleAppKeys.EnumKeyType.EnumAESKey);
        default_ff_key = infoProvider.getMifareKey(ALIAS_DEFAULT_FF);

    }


    private void initializeCipherinitVector() {

		/* Initialize the Cipher */
        try {
            cipher = Cipher.getInstance("AES/CBC/NoPadding");
        } catch (NoSuchAlgorithmException e) {

            e.printStackTrace();
        } catch (NoSuchPaddingException e) {
            e.printStackTrace();
        }

		/* set Application Master Key */
        bytesKey = KEY_APP_MASTER.getBytes();

		/* Initialize init vector of 16 bytes with 0xCD. It could be anything */
        byte[] ivSpec = new byte[16];
        Arrays.fill(ivSpec, (byte) 0xCD);
        iv = new IvParameterSpec(ivSpec);

    }

    public void showAlert(String title, String message) {


        Log.d("ALERT", "ALERT");
        new SweetAlertDialog(getActivity(), SweetAlertDialog.SUCCESS_TYPE)
                .setTitleText(title)
                .setContentText(message)
                .setConfirmText(getString(R.string.Ok))
                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sDialog) {
                        sDialog.dismissWithAnimation();
                        startActivity(new Intent(getContext(), ProgrammeActivity.class));

                    }
                })
                .show();

    }

    public CardManager read_nfc_card(int mode, int timeout) {
        final CardInfo cInfo = new CardInfo();
        CardManager cardManager = CardManager.getInstance(getActivity(), mode, "read", null);
        cardManager.getCard(timeout, new CardListener() {
            @Override
            public void callback(CardInfo cardInfo) {

                Log.d("##callback", " card info " + cardInfo.getCarddata());

                try {

                    read_card_personaldetails(cardInfo.getCarddata());

                } catch (Exception e) {

                    Log.d("##callback", " card info " + e.toString());

                }

            }
        });

        Log.d("##callback", " About to return the instance of the card manager ");
        return cardManager;
    }
    public CardManager update_nfc_card(int mode, int timeout) {
        final CardInfo cInfo = new CardInfo();
        try {
            card_pin.put("pin", paswword);
        } catch (Exception e) {
            Log.i("##DSDSDSD", e.toString());

        }
        CardManager cardManager = CardManager.getInstance(getActivity(), mode, "update", card_pin.toString().getBytes());
        cardManager.getCard(timeout, new CardListener() {
            @Override
            public void callback(CardInfo cardInfo) {
               if(cardInfo.isResultFalg()){
                   getActivity().runOnUiThread(new Runnable() {
                       @Override
                       public void run() {

                           changePin=false;
                           dialog.dismiss();
                           name.setText("");
                           ration.setText("");
                           showAlert("Great!", "Password changed successfully");

                       }
                   });
               }else{
                   Log.d("##log"," error updating card  ");
               }

            }
        });

        Log.d("##callback", " About to return the instance of the card manager ");
        return cardManager;
    }

    public void read_card_personaldetails(String datastr) {

        try {

            JSONObject carddata = new JSONObject(datastr);
            dt.setValuevoucher("");
            personal_data = carddata.getJSONObject("personaldetails");
            card_pin = carddata.getJSONObject("cardpin");
            cardbalance = carddata.getJSONObject("topups");
            bgroupId = personal_data.getString("groupId");
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {

                    try {
                        name.setText("Name:  " + personal_data.getString("name").replace("   ", " "));
                        ration.setText("Ration No: " + personal_data.getString("rationalNo"));
                        changePin=true;
                    } catch (Exception e) {

                    }
                    tapview.setVisibility(View.GONE);
                    changepinview.setVisibility(View.VISIBLE);
                    pDialogg.dismiss();

                }
            });
        } catch (Exception e) {

           Log.d("##log","  exception thrown "+e.toString());

        }

    }
}
