package com.evoucher.compas.evoucher.network;

import android.annotation.TargetApi;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Handler;
import android.telecom.Connection;
import android.util.Log;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.evoucher.compas.evoucher.Network;
import com.lynx.evoucher.models.Beneficiary;
import com.lynx.evoucher.models.Categories;
import com.lynx.evoucher.models.Configuration;
import com.lynx.evoucher.models.Products;
import com.lynx.evoucher.models.Programmes;
import com.lynx.evoucher.models.PurchasedProducts;
import com.lynx.evoucher.models.Topups;
import com.lynx.evoucher.models.Users;
import com.lynx.evoucher.models.Vouchers;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URLEncoder;
import java.util.List;

import cn.pedant.SweetAlert.SweetAlertDialog;

/**
 * Created by Kibet on 9/3/2016.
 */
public class BeneficiaryHttp {
    Context context;
    JSONObject  beneficiaryUpload = null;
    JSONArray beneficiariesJson;
    MaterialDialog dialog;
    List<Configuration> config;
    final Handler handler = new Handler();

    public BeneficiaryHttp(Context context) {
        this.context = context;
    }

    public  void UploadBeneficiary(){
        dialog=new MaterialDialog.Builder(context)
                .title("Uploading Beneficiary")
                .content("Please wait...")
                .progress(true, 0)
                .progressIndeterminateStyle(true)
                .cancelable(false)
                .show();
        try {
            ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(context.CONNECTIVITY_SERVICE);
            final NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
            // Connectivity issue, we quit
            if (networkInfo == null || networkInfo.getState() != NetworkInfo.State.CONNECTED) {

                // Display a toast in that case
                handler.post(new Runnable() {
                    @Override
                    public void run() {

                        Toast.makeText(context, "Network Error", Toast.LENGTH_SHORT).show();
                    }
                });

            } else {
                final Thread t = new Thread() {
                    @TargetApi(Build.VERSION_CODES.M)
                    @Override
                    public void run() {
                        try {
                            new MemberUploadAsyncTask().execute().get();
                        } catch (Exception e) {

                        }
                    }
                };
                t.start();

            }
        } catch (Exception e) {

        }
    }

    private class MemberUploadAsyncTask extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... urls) {
            config= Configuration.listAll(Configuration.class);
            String savedurl =config.get(0).getUrl();
            String savedport =config.get(0).getPort();
            String url= Network.server_protocol+savedurl+":"+savedport+Network.upload_beneficiary_url;
            Log.d("BENEFICIARY_POST$$",url);
            InputStream inputStream = null;
            String result = "";
            try {
                // 1. create HttpClient
                HttpClient httpclient = new DefaultHttpClient();
                // 2. make POST request to the given URL
                HttpPost httpPost = new HttpPost(url);
                beneficiaryUpload=new JSONObject();
                beneficiariesJson=new JSONArray();
//                List<Beneficiary> beneficiaryList=Beneficiary.listAll(Beneficiary.class);
                List<Beneficiary> beneficiaryList=Beneficiary.find(Beneficiary.class,"isuploaded=?","0");
                if (beneficiaryList.size() > 0){
                    for(Beneficiary beneficiary: beneficiaryList){
                        JSONArray bio_images=new JSONArray();
                        JSONObject image=new JSONObject();
                        beneficiaryUpload.put("memberNo",beneficiary.getNational_id()+beneficiary.getId() );
                        beneficiaryUpload.put("surName", beneficiary.getFirstname());
                        beneficiaryUpload.put("title", beneficiary.getTitle());
                        beneficiaryUpload.put("firstName", beneficiary.getLastname());
                        beneficiaryUpload.put("otherName", beneficiary.getMiddlename());
                        beneficiaryUpload.put("idPassPortNo", beneficiary.getNational_id());
                        beneficiaryUpload.put("gender", beneficiary.getGender());
                        beneficiaryUpload.put("dateOfBirth",beneficiary.getDate_of_birth());
                        beneficiaryUpload.put("nationality", "Kenyan");
                        beneficiaryUpload.put("memberPic", beneficiary.getUser_image());
                        beneficiaryUpload.put("familySize", beneficiary.getFamily_size());
                        beneficiaryUpload.put("bnfGrpId", beneficiary.getBeneficiary_group_id());
                        beneficiaryUpload.put("programmeId", beneficiary.getProgrammeid());
                        beneficiaryUpload.put("cellPhone", beneficiary.getMobile());
                        image.put("image",beneficiary.getLeft_thumb());
                        image.put("image",beneficiary.getLeft_index());
                        image.put("image",beneficiary.getRight_thumb());
                        image.put("image",beneficiary.getRight_index());
                        bio_images.put(image);
                        beneficiaryUpload.put("bioimages",bio_images);
                        beneficiariesJson.put(beneficiaryUpload);
                        Log.d("#####size",String.valueOf(bio_images.length()));
                        Log.d("sdsdsdsder3ty5t3",beneficiary.getLeft_thumb()+"dsds");
                        Log.d("sdsdsdsder3ty5t3",beneficiary.getLeft_index()+"ssd");
                        Log.d("sdsdsdsder3ty5t3",beneficiary.getRight_thumb()+"ddsd");
                        Log.d("sdsdsdsder3ty5t3",beneficiary.getRight_index()+"hjhjhj");
                    }

                                                          /* ;
                jsonObject.put("name", person.getName());
                jsonObject.put("country", person.getCountry());
                jsonObject.put("twitter", person.getTwitter());*/

                    // 4. convert JSONObject to JSON to String
                    // json = jsonObject.toString();

                    // ** Alternative way to convert Person object to JSON string usin Jackson Lib
                    // ObjectMapper mapper = new ObjectMapper();
                    // json = mapper.writeValueAsString(person);

                    // 5. set json to StringEntity
                    StringEntity se = new StringEntity(beneficiariesJson.toString());


                    // 6. set httpPost Entity
                    httpPost.setEntity(se);

                    // 7. Set some headers to inform server about the type of the content
                    httpPost.setHeader("Accept", "application/json");
                    httpPost.setHeader("Content-type", "application/json");

                    // 8. Execute POST request to the given URL
                    HttpResponse httpResponse = httpclient.execute(httpPost);

                    // 9. receive response as inputStream
                    inputStream = httpResponse.getEntity().getContent();
                    // 10. convert inputstream to string
                    if(inputStream != null) {
                        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
                        String line = "";
                        while ((line = bufferedReader.readLine()) != null)
                            result += line;

                        inputStream.close();
                    }else{

                    }

                    System.out.println("POST_RESULT## "+result);
                    JSONObject jsonResult=new JSONObject(result);
                    if (jsonResult.getString("respCode").trim().equals("200")){
                        for (Beneficiary ben: beneficiaryList){
                            ben.setIs_uploaded("1");
                            ben.save();
                        }
                        result ="1";
                    }
                } else{
                    Log.d("POST_RESULT##","No Beneficiaries to Upload");
                    result ="-1";
                }

            } catch (Exception e) {
                e.printStackTrace();
                return "0";
            }


            return result;
        }
        // onPostExecute displays the results of the AsyncTask.
        @Override
        protected void onPostExecute(String result) {
            dialog.dismiss();
            if(result.equalsIgnoreCase("0")) {
                new SweetAlertDialog(context, SweetAlertDialog.WARNING_TYPE)
                        .setTitleText("")
                        .setContentText("Unable to upload the Beneficiaries")
                        .setConfirmText("Ok")
                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sDialog) {
                                sDialog.dismissWithAnimation();
                            }
                        })
                        .show();

            } else if(result.equalsIgnoreCase("-1")) {
                new SweetAlertDialog(context, SweetAlertDialog.WARNING_TYPE)
                        .setTitleText("")
                        .setContentText("No Beneficiaries to Upload!!")
                        .setConfirmText("Ok")
                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sDialog) {
                                sDialog.dismissWithAnimation();
                            }
                        })
                        .show();

            }else{
                alertSuccess();
            }
        }
    }

    public void alertSuccess(){
        new SweetAlertDialog(context, SweetAlertDialog.SUCCESS_TYPE)
                .setTitleText("")
                .setContentText("BENEFICIARY UPLOADED SUCCESSFULLY!!")
                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sDialog) {
                        sDialog.dismissWithAnimation();
                    }
                })

                .show();
    }
}
