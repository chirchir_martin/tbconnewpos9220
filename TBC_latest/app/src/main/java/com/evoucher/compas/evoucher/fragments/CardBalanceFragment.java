package com.evoucher.compas.evoucher.fragments;

import android.annotation.TargetApi;
import android.content.Intent;
import android.graphics.Typeface;
import android.nfc.Tag;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.evoucher.compas.evoucher.Card_Details;
import com.evoucher.compas.evoucher.Details;
import com.evoucher.compas.evoucher.R;
import com.evoucher.compas.evoucher.Utils;
import com.evoucher.compas.evoucher.card.KeyInfoProvider;
import com.evoucher.compas.evoucher.card.SampleAppKeys;
import com.lynx.evoucher.models.Beneficiary;
import com.lynx.evoucher.models.BeneficiaryGroup;
import com.lynx.evoucher.models.Programmes;
import com.newpos.libpay.device.card.CardInfo;
import com.newpos.libpay.device.card.CardListener;
import com.newpos.libpay.device.card.CardManager;
import com.nxp.nfclib.CardType;
import com.nxp.nfclib.KeyType;
import com.nxp.nfclib.NxpNfcLib;
import com.nxp.nfclib.desfire.DESFireFactory;
import com.nxp.nfclib.desfire.IDESFireEV1;
import com.nxp.nfclib.exceptions.NxpNfcLibException;
import com.nxp.nfclib.interfaces.IKeyData;

import org.json.JSONArray;
import org.json.JSONObject;

import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.List;

import javax.crypto.Cipher;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.IvParameterSpec;

import cn.pedant.SweetAlert.SweetAlertDialog;

/**
 * Created by Administrator on 12/13/2016.
 */
public class CardBalanceFragment extends Fragment {
    private IKeyData objKEY_2KTDES_ULC = null;
    private IKeyData objKEY_2KTDES = null;
    private IKeyData objKEY_AES128 = null;
    private byte[] default_ff_key = null;
    private IKeyData default_zeroes_key = null;
    private NxpNfcLib libInstance = null;
    private IDESFireEV1 desFireEV1;
    private CardType mCardType = CardType.UnknownCard;
    private Cipher cipher = null;
    private byte[] bytesKey = null;
    private IvParameterSpec iv = null;
    private static final String KEY_APP_MASTER = "This is my key  ";
    private static final String ALIAS_KEY_AES128 = "key_aes_128";
    private static final String ALIAS_KEY_2KTDES = "key_2ktdes";
    private static final String ALIAS_KEY_2KTDES_ULC = "key_2ktdes_ulc";
    private static final String ALIAS_DEFAULT_FF = "alias_default_ff";
    private static final String ALIAS_KEY_AES128_ZEROES = "alias_default_00";
    private static final String EXTRA_KEYS_STORED_FLAG = "keys_stored_flag";
    static String packageKey = "b927aab8842ab54cbaf2ea1df9917159";

      MaterialDialog pDialogg;

    JSONObject data =new JSONObject(),pdetails,card_pin;
    TextView cardno,name,bal,tap;
    ViewGroup details;
    Details dt;
    String msg="";
    Animation animation;
    Typeface typeface;
    String bgip="";
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        return inflater.inflate(R.layout.cardbalance, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        initializeLibrary();
        initializeKeys();
        initializeCipherinitVector();
        tap=(TextView)getView().findViewById(R.id.tapcard) ;
        /*tap.setTypeface(typeface);

        tap.startAnimation(animation);*/
        dt=new Details(getActivity());
        super.onViewCreated(view, savedInstanceState);
        pDialogg = new MaterialDialog.Builder(getActivity())
                .title(getString(R.string.searchcard))
                .content(getString(R.string.tapcard))
                .progress(true,0)
                .progressIndeterminateStyle(false)
                .show();
        new Thread(new Runnable() {
            @Override
            public void run() {

                read_nfc_card(com.newpos.libpay.device.card.CardType.INMODE_NFC.getVal(), 10000);

            }
        }).start();
    }
    public void processNfc(final String cardno,String bal) {
        String bgip;
        JSONArray tups=new JSONArray();
       // tups=dt.getTopups();
        try {
            List<Beneficiary> ben = Beneficiary.find(Beneficiary.class, "cardnumber =?", cardno);
            //  Toast.makeText(getActivity(),ben.get(0).getFirstname()+"BAL:"+bal,Toast.LENGTH_LONG).show();

            List<BeneficiaryGroup> bg=BeneficiaryGroup.find(BeneficiaryGroup.class,"bnf_Grp_Id =?",ben.get(0).getBeneficiary_group_id());
            if(bg.size()>0){
                bgip=bg.get(0).getBnfGrpName();
            }else{
                bgip="";
            }
            new MaterialDialog.Builder(getActivity())
                    .title("Name "+": " + ben.get(0).getFirstname().toUpperCase())
                    .content(getString(R.string.CardNo)+": " + cardno + " " + '\n' + "Card Bal: " + bal+ '\n' +"VoucherId No: " +dt.getVoucherno() + " " + '\n'+"Programme Name: " + Programmes.find(Programmes.class," programid =?",dt.getTopups().get("programmeid").toString()).get(0).getProgrammename() + " " + '\n'+"Beneficiary Group Name: " +bgip )
                    .positiveText(getString(R.string.Ok))
                    .show();
        }catch (Exception e){
            new SweetAlertDialog(getActivity(), SweetAlertDialog.WARNING_TYPE)
                    .setTitleText(getString(R.string.Sorry))
                    .setContentText(getString(R.string.BenefiaciaryNotFound))
                    .setConfirmText(getString(R.string.Ok))
                    .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                        @Override
                        public void onClick(SweetAlertDialog sDialog) {
                            sDialog.dismissWithAnimation();

                        }
                    })
                    .show();
        }


    }

    public void cardLogic(final Intent intent) {
        CardType type = CardType.UnknownCard;
        try {
            type = libInstance.getCardType(intent);
        } catch (NxpNfcLibException ex) {
             Toast.makeText(getActivity(), ex.getMessage(), Toast.LENGTH_SHORT).show();
        }

        switch (type) {

            case DESFireEV1: {
                mCardType = CardType.DESFireEV1;
                desFireEV1 = DESFireFactory.getInstance().getDESFire(libInstance.getCustomModules());
                try {

                    desFireEV1.getReader().connect();
                    desFireEV1.getReader().setTimeout(2000);
                    pDialogg = new MaterialDialog.Builder(getActivity())
                            .title(R.string.Readingcard)
                            .content(R.string.DontRemoveCard)
                            .progress(true, 0)
                            .progressIndeterminateStyle(false)
                            .show();
                    new Thread(new Runnable() {
                        @Override
                        public void run() {
                            desfireEV1CardLogic();
                        }
                    }).start();

                } catch (Exception t) {
                    t.printStackTrace();
                    Log.d("##ERRORRR", t.toString());

                }
                break;
            }


        }
    }

    @TargetApi(19)
    private void initializeLibrary() {
        libInstance = NxpNfcLib.getInstance();
        try {
            libInstance.registerActivity(getActivity(), packageKey);
        } catch (NxpNfcLibException ex) {
             Toast.makeText(getActivity(), ex.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    /**
     * Initialize the Cipher and init vector of 16 bytes with 0xCD.
     */


    private void desfireEV1CardLogic() {
        byte[] appId = new byte[]{0x1, 0x00, 0x00};
        int timeOut = 2000;

        try {
            desFireEV1.getReader().setTimeout(timeOut);
            desFireEV1.authenticate(0, IDESFireEV1.AuthType.Native, KeyType.TWO_KEY_THREEDES, objKEY_2KTDES);
            desFireEV1.selectApplication(0);
            desFireEV1.selectApplication(appId);
            desFireEV1.authenticate(0, IDESFireEV1.AuthType.Native, KeyType.TWO_KEY_THREEDES, objKEY_2KTDES);
            data = new JSONObject(new String(desFireEV1.readData(Card_Details.carddata, 0, 0)));
            Log.i("###Rerererer",data.toString()+"uuuuuu");
            Log.i("###Rerererer",data.getString("programmeid")+"uuuuuu");
            pdetails =new JSONObject(new String(desFireEV1.readData(Card_Details.personalDetails, 0, 0)));
            card_pin =new JSONObject(new String(desFireEV1.readData(Card_Details.cpin, 0, 0)));
            desFireEV1.getReader().close();

         /*   List<BeneficiaryGroup> bg=BeneficiaryGroup.find(BeneficiaryGroup.class,"bnf_Grp_Id =?",data.getString("bengrpId"));
            if(bg.size()>0){
                bgip=bg.get(0).getBnfGrpName();
            }else{
                bgip="";
            }*/
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    pDialogg.dismiss();
                    try {

                        new MaterialDialog.Builder(getActivity())
                                .title(getString(R.string.Name)+": " + pdetails.getString("name").replace("   "," "))
                                .content(getString(R.string.RatioNo)  + pdetails.getString("rationalNo") + " " + '\n' + "Card Bal: " + data.getString("vochervalue") + '\n' + "VoucherId No: " +data.getString("voucherno") + " " + '\n' + "Programme Name: " + Programmes.find(Programmes.class, " programid =?", data.getString("programmeid")).get(0).getProgrammename() + " " + '\n' + "Card Holder Group Name: " + bgip)
                                .positiveText(R.string.Ok)
                                .show();
                    }catch (Exception e){
                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                pDialogg.dismiss();
                                new SweetAlertDialog(getActivity(), SweetAlertDialog.WARNING_TYPE)
                                        .setTitleText("Sorry")
                                        .setContentText("Please ensure the card is correctly topped up")
                                        .setConfirmText(getString(R.string.Ok))
                                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                            @Override
                                            public void onClick(SweetAlertDialog sDialog) {
                                                sDialog.dismissWithAnimation();

                                            }
                                        })
                                        .show();

                            }
                        });

                    }
                }
            });

        }catch (Exception e){
            if(!(data.length()>0)){
                if (e.getMessage().trim().contains("Tag was lost"))
                    msg = "Card was removed before the process was complete.Please tap card again and wait";
                else msg = getString(R.string.no_topups);
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        pDialogg.dismiss();

                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                pDialogg.dismiss();
                                new SweetAlertDialog(getActivity(), SweetAlertDialog.WARNING_TYPE)
                                        .setTitleText(getString(R.string.Sorry))
                                        .setContentText(msg)
                                        .setConfirmText(getString(R.string.Ok))
                                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                            @Override
                                            public void onClick(SweetAlertDialog sDialog) {
                                                sDialog.dismissWithAnimation();

                                            }
                                        })
                                        .show();

                            }
                        });


                    }
                });
            }else{
                Log.i("#####ERRORRERe",e.toString());
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        pDialogg.dismiss();
                        new SweetAlertDialog(getActivity(), SweetAlertDialog.WARNING_TYPE)
                                .setTitleText(getString(R.string.Errorr))
                                .setContentText(getString(R.string.UnAbletoReadCard))
                                .setConfirmText(getString(R.string.Ok))
                                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                    @Override
                                    public void onClick(SweetAlertDialog sDialog) {
                                        sDialog.dismissWithAnimation();

                                    }
                                })
                                .show();

                    }
                });
            }


        }
        //fetch the data

    }
    private void initializeKeys() {
        KeyInfoProvider infoProvider = KeyInfoProvider.getInstance(getActivity().getApplicationContext());
        infoProvider.setKey(ALIAS_KEY_2KTDES, SampleAppKeys.EnumKeyType.EnumDESKey, SampleAppKeys.KEY_2KTDES);
        infoProvider.setKey(ALIAS_KEY_AES128, SampleAppKeys.EnumKeyType.EnumAESKey, SampleAppKeys.KEY_AES128);
        infoProvider.setKey(ALIAS_KEY_AES128_ZEROES, SampleAppKeys.EnumKeyType.EnumAESKey, SampleAppKeys.KEY_AES128_ZEROS);
        infoProvider.setKey(ALIAS_DEFAULT_FF, SampleAppKeys.EnumKeyType.EnumMifareKey, SampleAppKeys.KEY_DEFAULT_FF);
        objKEY_2KTDES_ULC = infoProvider.getKey(ALIAS_KEY_2KTDES_ULC, SampleAppKeys.EnumKeyType.EnumDESKey);
        objKEY_2KTDES = infoProvider.getKey(ALIAS_KEY_2KTDES, SampleAppKeys.EnumKeyType.EnumDESKey);
        objKEY_AES128 = infoProvider.getKey(ALIAS_KEY_AES128, SampleAppKeys.EnumKeyType.EnumAESKey);
        default_zeroes_key = infoProvider.getKey(ALIAS_KEY_AES128_ZEROES, SampleAppKeys.EnumKeyType.EnumAESKey);
        default_ff_key = infoProvider.getMifareKey(ALIAS_DEFAULT_FF);
    }


    private void initializeCipherinitVector() {

		/* Initialize the Cipher */
        try {
            cipher = Cipher.getInstance("AES/CBC/NoPadding");
        } catch (NoSuchAlgorithmException e) {

            e.printStackTrace();
        } catch (NoSuchPaddingException e) {
            e.printStackTrace();
        }

		/* set Application Master Key */
        bytesKey = KEY_APP_MASTER.getBytes();

		/* Initialize init vector of 16 bytes with 0xCD. It could be anything */
        byte[] ivSpec = new byte[16];
        Arrays.fill(ivSpec, (byte) 0xCD);
        iv = new IvParameterSpec(ivSpec);

    }
    public CardManager read_nfc_card(int mode, int timeout) {
        final CardInfo cInfo = new CardInfo();
        CardManager cardManager = CardManager.getInstance(getActivity(), mode,"read",null);
        cardManager.getCard(timeout, new CardListener() {
            @Override
            public void callback(CardInfo cardInfo) {

                Log.d("##callback"," card info "+ cardInfo.getCarddata());
                try{

                    showCardBalance(cardInfo.getCarddata());

                }catch(Exception e){

                    Log.d("##callback"," card info "+e.toString());

                }

            }
        });

        Log.d("##callback"," About to return the instance of the card manager ");
        return cardManager;
    }
    public void showCardBalance(String dataStr){

        try{
            JSONObject carddata= new JSONObject(dataStr);
            dt.setValuevoucher("");
            pdetails = carddata.getJSONObject("personaldetails");
            card_pin = carddata.getJSONObject("cardpin");
            data = carddata.getJSONObject("topups");
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    pDialogg.dismiss();
                    try {

                        new MaterialDialog.Builder(getActivity())
                                .title(getString(R.string.Name)+": " + pdetails.getString("name").replace("   "," "))
                                .content(getString(R.string.RatioNo)  + pdetails.getString("rationalNo") + " " + '\n' + "Card Bal: " + data.getString("vochervalue") + '\n' + "VoucherId No: " +data.getString("voucherno") + " " + '\n' + "Programme Name: " + Programmes.find(Programmes.class, " programid =?", data.getString("programmeid")).get(0).getProgrammename() + " " + '\n' + "Card Holder Group Name: " + bgip)
                                .positiveText(R.string.Ok)
                                .show();
                    }catch (Exception e){
                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                pDialogg.dismiss();
                                new SweetAlertDialog(getActivity(), SweetAlertDialog.WARNING_TYPE)
                                        .setTitleText("Sorry")
                                        .setContentText("Please ensure the card is correctly topped up")
                                        .setConfirmText(getString(R.string.Ok))
                                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                            @Override
                                            public void onClick(SweetAlertDialog sDialog) {
                                                sDialog.dismissWithAnimation();

                                            }
                                        })
                                        .show();

                            }
                        });

                    }
                }
            });
        }catch (Exception e){
            if(!(data.length()>0)){
                if (e.getMessage().trim().contains("Tag was lost"))
                    msg = "Card was removed before the process was complete.Please tap card again and wait";
                else msg = getString(R.string.no_topups);
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        pDialogg.dismiss();

                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                pDialogg.dismiss();
                                new SweetAlertDialog(getActivity(), SweetAlertDialog.WARNING_TYPE)
                                        .setTitleText(getString(R.string.Sorry))
                                        .setContentText(msg)
                                        .setConfirmText(getString(R.string.Ok))
                                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                            @Override
                                            public void onClick(SweetAlertDialog sDialog) {
                                                sDialog.dismissWithAnimation();

                                            }
                                        })
                                        .show();

                            }
                        });


                    }
                });
            }else{

                Log.i("#####ERRORRERe",e.toString());
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        pDialogg.dismiss();
                        new SweetAlertDialog(getActivity(), SweetAlertDialog.WARNING_TYPE)
                                .setTitleText(getString(R.string.Errorr))
                                .setContentText(getString(R.string.UnAbletoReadCard))
                                .setConfirmText(getString(R.string.Ok))
                                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                    @Override
                                    public void onClick(SweetAlertDialog sDialog) {
                                        sDialog.dismissWithAnimation();

                                    }
                                })
                                .show();

                    }
                });
            }

        }
    }

}
