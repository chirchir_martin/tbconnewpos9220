package com.evoucher.compas.evoucher.fragments;

import android.Manifest;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.nfc.NdefMessage;
import android.nfc.NdefRecord;
import android.nfc.Tag;
import android.nfc.tech.Ndef;
import android.nfc.tech.NdefFormatable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.evoucher.compas.evoucher.Details;
import com.evoucher.compas.evoucher.R;


import org.json.JSONArray;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.UnsupportedEncodingException;
import java.util.List;
import java.util.Locale;

import cn.pedant.SweetAlert.SweetAlertDialog;

/**
 * Created by Administrator on 12/10/2016.
 */
public class SynchronizationFragment extends Fragment implements View.OnClickListener{
    /*public static final String TAG = "###SalutTestApp";
    public SalutDataReceiver dataReceiver;
    public SalutServiceData serviceData;
    public Salut network;
    public Button hostingBtn;
    public Button discoverBtn;
    SalutDataCallback callback;*/
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        return inflater.inflate(R.layout.card_enquiry_fragment, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
       /* hostingBtn = (Button)getView().findViewById(R.id.hosting_button);
        discoverBtn = (Button)getView(). findViewById(R.id.discover_services);
        Toast.makeText(getActivity(),String.valueOf(check()),Toast.LENGTH_LONG).show();

        hostingBtn.setOnClickListener(this);
      //  hostingBtn.setAlpha(0.5f);
        discoverBtn.setOnClickListener(this);
         checkPermissions();
        *//*Create a data receiver object that will bind the callback
        with some instantiated object from our app. *//*
        callback = new SalutDataCallback() {
             @Override public void onDataReceived(Object data) {

             }
       };


         *//*Create a data receiver object that will bind the callback
         with some instantiated object from our app. *//*
                                       dataReceiver = new SalutDataReceiver(getActivity(), callback);



        *//*Populate the details for our awesome service. *//*
        serviceData = new SalutServiceData("testAwesomeService", 60606,
                "HOST");

        *//*Create an instance of the Salut class, with all of the necessary data from before.
        * We'll also provide a callback just in case a device doesn't support WiFi Direct, which
        * Salut will tell us about before we start trying to use methods.*//*
        network = new Salut(dataReceiver, serviceData, new SalutCallback() {
            @Override
            public void call() {
                Log.e(TAG, "Sorry, but this device does not support WiFi Direct.");
            }
        });
*/
        super.onViewCreated(view, savedInstanceState);
    }


    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
    }



    public void alertSuccess(){
        new SweetAlertDialog(getActivity(), SweetAlertDialog.SUCCESS_TYPE)
                .setTitleText("Topup")
                .setContentText("successfully")
                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sDialog) {
                        sDialog.dismissWithAnimation();


                    }
                })

                .show();
    }

    @Override
    public void onClick(View v) {

    }





   /* private void setupNetwork()

    {
        Log.d("#####ISRUNNINGASSHOST",String.valueOf(network.isRunningAsHost));
        if(!network.isRunningAsHost)
        {
            network.startNetworkService(new SalutDeviceCallback() {
                @Override
                public void call(SalutDevice salutDevice) {
                    Toast.makeText(getActivity().getApplicationContext(), "Device: " + salutDevice.instanceName + " connected.", Toast.LENGTH_SHORT).show();
                }
            });

            hostingBtn.setText("Stop Service");
            discoverBtn.setAlpha(0.5f);
            discoverBtn.setClickable(false);
        }
        else
        {
            network.stopNetworkService(false);
            hostingBtn.setText("Start Service");
            discoverBtn.setAlpha(1f);
            discoverBtn.setClickable(true);
        }
    }

    private void discoverServices()
    {
        if(!network.isRunningAsHost && !network.isDiscovering){
         *//*   network.discoverWithTimeout(new SalutCallback() {
                @Override
                public void call() {
                   Toast.makeText(getActivity(),network.foundDevices.toString(),Toast.LENGTH_LONG).show();
                    network.registerWithHost(network.foundDevices.get(0), new SalutCallback() {
                        @Override
                        public void call() {
                            Toast.makeText(getActivity(),"connected. ",Toast.LENGTH_LONG).show();
                        }
                    }, new SalutCallback() {
                        @Override
                        public void call() {
                            Toast.makeText(getActivity(),"failed connection. ",Toast.LENGTH_LONG).show();
                        }
                    });
                    Log.i(TAG, "wwwLook at all thsssese devices! " + network.foundDevices.get(0));
                }
            }, new SalutCallback() {
                @Override
                public void call() {
                    Toast.makeText(getActivity(),"Bummer, we didn't find anyone. ",Toast.LENGTH_LONG).show();
                    Log.i(TAG, "wwwLook at all thsssese devices! " + network.foundDevices.get(0));
                }
            }, 20000);*//*

            network.discoverWithTimeout(new SalutCallback() {
                @Override
                public void call() {
                    Toast.makeText(getActivity(),"connected. "+network.foundDevices.size(),Toast.LENGTH_LONG).show();
                    network.registerWithHost(network.foundDevices.get(0), new SalutCallback(){
                        @Override
                        public void call() {
                            Toast.makeText(getActivity(),"connected. ",Toast.LENGTH_LONG).show();
                        }
                    }, new SalutCallback() {
                        @Override
                        public void call() {
                            Toast.makeText(getActivity(),"failed connection. ",Toast.LENGTH_LONG).show();
                        }
                    });
                    Toast.makeText(getActivity(),network.foundDevices.toString(),Toast.LENGTH_LONG).show();
                }
            }, new SalutCallback() {
                @Override
                public void call() {
                    discoverBtn.setText("Discover Services");
                    hostingBtn.setAlpha(1f);
                    hostingBtn.setClickable(true);
                    Toast.makeText(getActivity(),"Bummer, we didn't find anyone. ",Toast.LENGTH_LONG).show();
                }
            }, 10000);


            discoverBtn.setText("Stop Discovery");
            hostingBtn.setAlpha(0.5f);
            hostingBtn.setClickable(false);
        }
        else
        {
            network.stopServiceDiscovery(true);
            discoverBtn.setText("Discover Services");
            hostingBtn.setAlpha(1f);
            hostingBtn.setClickable(false);
        }



    }*/


   /* @Override
    public void onClick(View v) {

        if(!Salut.isWiFiEnabled(getActivity().getApplicationContext()))
        {
            Toast.makeText(getActivity().getApplicationContext(), "Please enable WiFi first.", Toast.LENGTH_SHORT).show();
            return;
        }

        if(v.getId() == R.id.hosting_button)
        {
            setupNetwork();
        }
        else if(v.getId() == R.id.discover_services)
        {
            discoverServices();
        }
    }
*/
    boolean check(){
        String permission = "android.permission.ACCESS_WIFI_STATE";
        int res = getContext().checkCallingOrSelfPermission(permission);
        return (res == PackageManager.PERMISSION_GRANTED);
    }


    void checkPermissions(){
        // No explanation needed, we can request the permission.
        ActivityCompat.requestPermissions(getActivity(),
                        new String[]{Manifest.permission.ACCESS_COARSE_LOCATION},
                        1);




    }

    @Override
    public void onRequestPermissionsResult(int requestCode,String permissions[], int[] grantResults) {
        switch (requestCode) {
            case 1: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.

                } else {

                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                return;
            }

            // other 'case' lines to check for other
            // permissions this app might request
        }
    }
}
