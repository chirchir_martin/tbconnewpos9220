package com.evoucher.compas.evoucher;

import android.view.View;

/**
 * Created by Administrator on 8/17/2016.
 */
public interface ItemClickListener {
    void onClick(View view, int position);
}