package com.newpos.libpay.trans;

import android.content.Context;

import com.android.newpos.libemv.PBOCManager;
import com.newpos.libpay.Logger;
import com.newpos.libpay.PaySdk;
import com.newpos.libpay.global.TMConfig;
import com.newpos.libpay.global.TMConstants;
import com.newpos.libpay.helper.iso8583.ISO8583;
import com.newpos.libpay.helper.ssl.NetworkHelper;
import com.newpos.libpay.paras.EmvAidInfo;
import com.newpos.libpay.paras.EmvCapkInfo;
import com.newpos.libpay.presenter.TransInterface;
import com.newpos.libpay.utils.ISOUtil;
import com.newpos.libpay.utils.PAYUtils;

import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.Properties;

/**
 * Created by zhouqiang on 2017/10/11.
 * @author zhouqiang
 * transaction abstract class
 */
public abstract class Trans {

	/** -------------------------field define start-----------------------------*/
	/**
	 * 0* message type
	 */
	protected String MsgID;

	/**
	 * 2* Primary Account Number
	 */
	protected String Pan;

	/**
	 * 3* Transaction Processing Code
	 */
	protected String ProcCode;

	/**
	 * 4* Amount Of Transactions
	 */
	protected long Amount;

	/**
	 * 11* System Trace Audit Number
	 */
	protected String TraceNo;

	/**
	 * 12* Local Time Of Transaction
	 */
	protected String LocalTime;

	/**
	 * 13* Local Date Of Transaction
	 */
	protected String LocalDate;

	/**
	 * 14* Date Of Expired
	 */
	protected String ExpDate;

	/**
	 * 15 Date Of Settlement
	 */
	protected String SettleDate;

	/**
	 * 22* Point Of Service Entry Mode
	 */
	protected String EntryMode;

	/**
	 * 23* Card Sequence Number
	 */
	protected String PanSeqNo;

	/**
	 * 25* Point Of Service Condition Mode
	 */
	protected String SvrCode;

	/**
	 * 26* Point Of Service PIN Capture Code
	 */
	protected String CaptureCode;

	/**
	 * 32* Acquiring Institution Identification Code
	 */
	protected String AcquirerID;

	/**
	 * Track 1 Data (no use)
	 */
	protected String Track1;

	/**
	 * 35* Track 2 Data
	 */
	protected String Track2;

	/**
	 * 36* Track 3 Data
	 */
	protected String Track3;

	/**
	 * 37* Retrieval Reference Number
	 */
	protected String RRN;

	/**
	 * 38* Authorization Identification Response Code
	 */
	protected String AuthCode;

	/**
	 * 39* Response Code
	 */
	protected String RspCode;

	/**transInterface
	 * 41* Card Acceptor Terminal Identification
	 */
	protected String TermID;

	/**
	 * 42* Card Acceptor Identification Code
	 */
	protected String MerchID;

	/**
	 * 44* Additional Response Data
	 */
	protected String Field44;

	/**
	 * 48* Additional Data - Private
	 */
	protected String Field48;

	/**
	 * 49* Currency Code Of Transaction
	 */
	protected String CurrencyCode;

	/**
	 * 52* PIN Data(PIN block)
	 */
	protected String PIN;

	/**
	 * 53* Security Related Control Information
	 */
	protected String SecurityInfo;

	/**
	 * 54* Balance Amount
	 */
	protected String ExtAmount;

	/**
	 * 55* Integrated Circuit Card System Related Data
	 */
	protected byte[] ICCData = null ;

	/**
	 * 58* PBOC_ELECTRONIC_DATA
	 */
	protected String Field58 ;

	/**
	 * 60* field 60 Reserved Private
	 * ——field length			        			N3
	 * ——60.1  message type code		    		N2
	 * ——60.2  batch number		        			N6
	 * ——60.3  Network management information code	N3
	 * ——60.4  terminal ability	      				N1
	 * ——60.5  ICC condition code					N1
	 * ——60.6  Support for partial deductions and return balance marks 				N1
	 * ——60.7  balance type 						N3
	 *
	 * Note:for more detail information,please refer to QCUP009.1-2014
	 */
	protected String Field60;
	/**
	 * whether use 60.1 and 60.1 of original trans
	 */
	protected boolean isUseOrg_603_601 = false;
	/**
	 * field 60.1
	 */
	protected String F60_1;
	/**
	 * field 60.2
	 */
	protected String BatchNo;
	/**
	 * field 60.3
	 */
	protected String F60_3;

	/**
	 * 61* Original Message
	 */
	protected String Field61;

	/**
	 * 62* Reserved Private
	 * Note:for more detail information,please refer to QCUP009.1-2014
	 */
	protected String Field62;

	/**
	 * 63* Reserved Private
	 * Note:for more detail information,please refer to QCUP009.1-2014
	 */
	protected String Field63;

	/**
	 * 64* Message Authentication Code (MAC)
	 */
	protected String Field64;

	/** -------------------------field define end---------------------------*/


	/** -------------------------common parameters of transaction--------------------*/
	/**
	 * Context object
	 */
	protected Context context;

	/**
	 * ISO8583 object
	 */
	protected ISO8583 iso8583;

	/**
	 * Network Helper
	 */
	protected NetworkHelper netWork;

	/**
	 * terminal configure
	 */
	protected TMConfig cfg;

	/**
	 * Presenter of MVP mode
	 */
	protected TransInterface transInterface ;

	/**
	 * transaction chinese name
	 */
	protected String TransCName;

	/**
	 * transaction english name, refer to @{@link Type}
	 */
	protected String TransEName;

	/**
	 * whether increase voucher NO.
	 */
	protected boolean isTraceNoInc;

	/**
	 * success if response code(response data in field 39) is "00"
	 */
	protected String RSP_00_SUCCESS = "00" ;

	/**
	 * PBOC library manager
	 */
	protected PBOCManager pbocManager ;
	/** -------------------------end of common parameters of transaction----------------*/

	/***
	 * Trans object
	 * @param ctx Context @{@link Context}
	 * @param ename transaction name @{@link Type}
	 * @param tt Presenter of MVP mode
	 */
	public Trans(Context ctx, String ename , TransInterface tt) {
		this.context = ctx ;
		this.TransEName = ename ;
		this.transInterface = tt ;
		this.pbocManager = PBOCManager.getInstance();
		this.pbocManager.setDEBUG(true);
		loadConfig();
		loadEMVConfig();
	}

	/**
	 * load transaction parameters from terminal configure
	 */
	private void loadConfig() {
		Logger.debug("==Trans->loadConfig==");

		cfg = TMConfig.getInstance();
		TermID = cfg.getTermID();
		MerchID = cfg.getMerchID();
		CurrencyCode = cfg.getCurrencyCode();
		BatchNo = ISOUtil.padleft("" + cfg.getBatchNo(), 6, '0');
		TraceNo = ISOUtil.padleft("" + cfg.getTraceNo(), 6, '0');

		//Init NetworkHelper
		boolean isPub = cfg.getPubCommun() ;
		String ip = isPub?cfg.getIp():cfg.getIP2();
		int port = Integer.parseInt(isPub?cfg.getPort():cfg.getPort2());
		netWork = new NetworkHelper(ip, port, cfg.getTimeout(), this.context);

		//Init ISO8583
		String tpdu = cfg.getTpdu();
		String header = cfg.getHeader();
		iso8583 = new ISO8583(this.context, tpdu, header);
		setFixedDatas();
	}

	/**
	 * load EMV transaction configure
	 */
	private void loadEMVConfig(){
		String aidFilePath = TMConfig.getRootFilePath() + EmvAidInfo.FILENAME;
		Logger.debug("load aid from path = "+aidFilePath);
		File aidFile = new File(aidFilePath);
		if (aidFile.exists()) {
			try {
				EmvAidInfo aidInfo = (EmvAidInfo) PAYUtils.file2Object(aidFilePath);
				if (aidInfo != null && aidInfo.getAidInfoList() != null) {
					for (byte[] item : aidInfo.getAidInfoList()) {
						Logger.debug("load aid:"+ISOUtil.byte2hex(item));
						pbocManager.setEmvParas(item);
					}
				}
			} catch (IOException e) {
				e.printStackTrace();
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
			}
		}

		String capkFilePath = TMConfig.getRootFilePath()+ EmvCapkInfo.FILENAME;
		Logger.debug("load capk from path = "+capkFilePath);
		File capkFile = new File(capkFilePath);
		if (capkFile.exists()) {
			try {
				EmvCapkInfo capk = (EmvCapkInfo) PAYUtils.file2Object(capkFilePath);
				if (capk != null && capk.getCapkList() != null) {
					for (byte[] item : capk.getCapkList()) {
						Logger.debug("load capk:"+ISOUtil.byte2hex(item));
						pbocManager.setEmvCapks(item);
					}
				}
			} catch (IOException e) {
				e.printStackTrace();
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
			}
		}
	}

	protected void setFixedDatas() {
		Logger.debug("==Trans->setFixedDatas==");
		if (null == TransEName) {
			return;
		}
		Properties pro = PAYUtils.lodeConfig(context, TMConstants.TRANS);
		if (pro == null) {
			return;
		}
		String prop = pro.getProperty(TransEName);
		String[] propGroup = prop.split(",");
		if (!PAYUtils.isNullWithTrim(propGroup[0])){
			MsgID = propGroup[0];
		}else{
			MsgID = null;
		}
		if (!isUseOrg_603_601) {
			if (!PAYUtils.isNullWithTrim(propGroup[1])){
				ProcCode = propGroup[1];
			}else{
				ProcCode = null;
			}
		}
		if (!PAYUtils.isNullWithTrim(propGroup[2])){
			SvrCode = propGroup[2];
		}else{
			SvrCode = null;
		}
		if (!isUseOrg_603_601) {
			if (!PAYUtils.isNullWithTrim(propGroup[3])){
				F60_1 = propGroup[3];
			}else{
				F60_1 = null;
			}
		}
		if (!PAYUtils.isNullWithTrim(propGroup[4])) {
			F60_3 = propGroup[4];
		}else {
			F60_3 = null;
		}
		if (F60_1 != null && F60_3 != null){
			Field60 = F60_1 + cfg.getBatchNo() + F60_3;
		}
		try {
			TransCName = new String(propGroup[5].getBytes("ISO-8859-1"), "utf-8");
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
	}

	/**
	 * set whether increase voucher NO.
	 * @param isTraceNoInc
     */
	public void setTraceNoInc(boolean isTraceNoInc) {
		this.isTraceNoInc = isTraceNoInc;
	}

	/**
	 * append field 60 data
	 * @param f60
     */
	protected void appendField60(String f60) {
		Field60 = Field60 + f60;
	}

	/**
	 * format response code
	 * @param rsp
	 * @return @{@link Tcode}
	 */
	protected int formatRsp(String rsp){
		String[] stand_rsp = {"5A","5B","6A","A0","D1","D2","D3","D4","N6","N7"} ;
		int START = 200 ;
		boolean finded = false ;
		for (int i = 0 ; i < stand_rsp.length ; i++){
			if(stand_rsp[i].equals(rsp)){
				START += i ;
				finded = true ;
				break;
			}
		}
		if(finded){

			return START ;

		}else {

			return Integer.parseInt(rsp) ;

		}
	}

	/**
	 * create socket and connect
	 * @return
	 */
	protected int connect() {
		return netWork.Connect();
	}

	/**
	 * packet iso8583 data and send
	 * @return
	 */
	protected int send() {
		byte[] pack = iso8583.packetISO8583();
		if (pack == null) {
			return -1;
		}
		Logger.debug(TransEName+"->send:"+ ISOUtil.hexString(pack));
		return netWork.Send(pack);
	}

	/**
	 * receive data
	 * @return
	 */
	protected byte[] receive() {
		byte[] recive = null;
		try {

			recive = netWork.Recive(2048, cfg.getTimeout());

		} catch (IOException e) {

			Logger.debug("receive->IOException:"+e.toString());
			return null;

		}
		if(recive!=null){

			Logger.debug(TransEName+"->receive:"+ISOUtil.hexString(recive));

		}
		return recive;
	}
}
