package com.newpos.libpay.device.card;

import android.app.Activity;
import android.content.Context;
import android.util.Log;

import com.newpos.libpay.Logger;
import com.newpos.libpay.R;
import com.newpos.libpay.trans.Tcode;
import com.pos.device.SDKException;
import com.pos.device.beeper.Beeper;
import com.pos.device.icc.ContactCard;
import com.pos.device.icc.IccReader;
import com.pos.device.icc.IccReaderCallback;
import com.pos.device.icc.OperatorMode;
import com.pos.device.icc.SlotType;
import com.pos.device.icc.VCC;
import com.pos.device.magcard.MagCardCallback;
import com.pos.device.magcard.MagCardReader;
import com.pos.device.magcard.MagneticCard;
import com.pos.device.magcard.TrackInfo;
import com.pos.device.picc.EmvContactlessCard;
import com.pos.device.picc.MifareClassic;
//import com.pos.device.picc.MifareDesfire;
//import com.pos.device.picc.MifareDesfire;
import com.pos.device.picc.MifareDesfire;
import com.pos.device.picc.MifareUltralight;
import com.pos.device.picc.PiccReader;
import com.pos.device.picc.PiccReaderCallback;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.zip.Deflater;
import java.util.zip.Inflater;

/**
 * Created by zhouqiang on 2017/3/14.
 * @author zhouqiang
 * card manage
 */

public class CardManager {
    String TAG="##mifaredesfire";
    private static CardManager instance ;
    private static int mode ;
    private static Context mContext;
    boolean card_status;
    String  description="card_wait_timed_out";
    String mData=" No data returned ";
    static byte[] updatedata;
    byte[] appId = new byte[]{0x1, 0x00, 0x00};
    static  String operation;
    String cardUID;
    public String getAccount_details() {
        return account_details;
    }

    public void setAccount_details(String account_details) {
        this.account_details = account_details;
    }

    String account_details;
    public boolean getUpdate_card() {
        return update_card;
    }

    public void setUpdate_card(boolean update_card) {
        this.update_card = update_card;
    }

    boolean update_card;
    int   result_code;
    private CardManager(){}

    public static CardManager getInstance(int m){
        mode = m ;
        if(null == instance){
            instance = new CardManager();
        }
        return instance ;
    }
    public static CardManager getInstance(Context context,int m){
        mode = m ;
        if(null == instance){
            instance = new CardManager();
             mContext=context;
        }
        return instance ;
    }
    public static CardManager getInstance(Context context,int m ,String opr,byte[] data){
        mode = m ;
        operation=opr;
        updatedata=data;
        if(null == instance){

            instance = new CardManager();
            mContext=context;

        }
        return instance ;
    }
    public String returnData(){

         return mData;
    }
     public boolean getCardStatus(){
        return  card_status;
    }
    public void  setCardStatus(boolean status){
        card_status=status;
    }
      public String getDescription(){
          return  description;
      }

    private MagCardReader magCardReader ;
    private IccReader iccReader ;
    private PiccReader piccReader ;

    private void init(){
        //Log.d("yuan", "1-->"+(mode & CardType.INMODE_MAG.getVal()));
        if( (mode & CardType.INMODE_MAG.getVal() ) != 0 ){
            //Log.d("yuan", "4-->");
            magCardReader = MagCardReader.getInstance();
            //Log.d("yuan", magCardReader.getClass().getName());
        }
        //Log.d("yuan", "2-->"+(mode & CardType.INMODE_IC.getVal()));
        if( (mode & CardType.INMODE_IC.getVal()) != 0 ){
            iccReader = IccReader.getInstance(SlotType.USER_CARD);
        }
        //Log.d("yuan", "3-->"+(mode & CardType.INMODE_NFC.getVal()));
        if( (mode & CardType.INMODE_NFC.getVal()) != 0 ){
            piccReader = PiccReader.getInstance();
        }
        isEnd = false ;
    }

    private void stopMAG(){
        try {
            if(magCardReader!=null){
                magCardReader.stopSearchCard();
            }
        } catch (SDKException e) {
            e.printStackTrace();
        }
    }

    private void stopICC(){
        if(iccReader!=null){
            try {
                iccReader.stopSearchCard();
            } catch (SDKException e) {
                e.printStackTrace();
            }
        }
    }

    private void stopPICC(){
        if(piccReader!=null){
            piccReader.stopSearchCard();
            try {
                piccReader.release();
            } catch (SDKException e) {
                e.printStackTrace();
            }
        }
    }

    public void releaseAll(){
        isEnd = true ;
        try {
            if(magCardReader!=null){
                magCardReader.stopSearchCard();
                Logger.debug("mag stop");
            }
            if(iccReader!=null){
                iccReader.stopSearchCard();
                iccReader.release();
                Logger.debug("icc stop");
            }
            if(piccReader!=null){
                piccReader.stopSearchCard();
                piccReader.release();
                Logger.debug("picc stop");
            }
        } catch (SDKException e) {
            e.printStackTrace();
        }
    }

    private CardListener listener ;
    private boolean isEnd = false ;
    public CardInfo getCard(final int timeout , CardListener l){
        Logger.debug("CardManager>>getCard>>timeout="+timeout);
        init();
        final CardInfo info = new CardInfo() ;
        if(null == l){
            info.setResultFalg(false);
            info.setErrno(Tcode.SEARCH_CARD_FAIL);
            listener.callback(info);
        }else {

            this.listener = l ;
            new Thread(){
                @Override
                public void run(){
                    try{
                        if( (mode & CardType.INMODE_MAG.getVal()) != 0 ){
                            Logger.debug("CardManager>>getCard>>MAG");

                            magCardReader.startSearchCard(timeout, new MagCardCallback() {
                                @Override
                                public void onSearchResult(int i, MagneticCard magneticCard) {
                                    if(!isEnd){
                                        Logger.debug("CardManager>>getCard>>MAG>>i="+i);
                                        isEnd = true ;
                                        stopICC();
                                        stopPICC();
                                        if( 0 == i ){
                                            listener.callback(handleMAG(magneticCard));
                                        }else {
                                            info.setResultFalg(false);
                                            info.setErrno(Tcode.SEARCH_CARD_FAIL);
                                            listener.callback(info);
                                        }
                                    }
                                }
                            });
                        }if( (mode & CardType.INMODE_IC.getVal()) != 0 ){
                            Logger.debug("CardManager>>getCard>>ICC");
                            iccReader.startSearchCard(timeout, new IccReaderCallback() {
                                @Override
                                public void onSearchResult(int i) {
                                    if(!isEnd){
                                        Logger.debug("CardManager>>getCard>>ICC>>i="+i);
                                        isEnd = true ;
                                        stopMAG();
                                        stopPICC();
                                        if( 0 == i ){
                                            try {
                                                listener.callback(handleICC());
                                            } catch (SDKException e) {
                                                info.setResultFalg(false);
                                                info.setErrno(Tcode.SEARCH_CARD_FAIL);
                                                listener.callback(info);
                                            }
                                        }else {
                                            info.setResultFalg(false);
                                            info.setErrno(Tcode.SEARCH_CARD_FAIL);
                                            listener.callback(info);
                                        }
                                    }
                                }
                            });
                        }if( (mode & CardType.INMODE_NFC.getVal()) != 0 ){
                            Log.d("##log", " interacting with the NFC cards");
                            piccReader.startSearchCard(timeout, new PiccReaderCallback() {
                                @Override
                                public void onSearchResult(int i, int i1) {
                                    try {
                                        Thread.sleep(400);
                                    } catch (InterruptedException e) {
                                        e.printStackTrace();
                                    }
                                    if(!isEnd){
                                        Logger.debug("CardManager>>getCard>>NFC>>i= nfc beep"+i);
                                        isEnd = true ;
                                        stopICC();
                                        stopMAG();
                                        if(0 == i){
                                            try {

                                                Beeper.getInstance().beep(3000 , 500);

                                            } catch (SDKException e) {

                                                Logger.debug("CardManager>>getCard>>NFC>>i="+ e.toString());
                                                e.printStackTrace();

                                            }

                                            Logger.debug("CardManager>> before");
                                                 Log.d("##log", " is reading the card ");
                                                 listener.callback(handlePICC(i1));
                                            Logger.debug("CardManager> after");
                                        } else {
                                            Log.d("##log", " The return value is not 0");
                                            info.setResultFalg(false);
                                            info.setErrno(Tcode.SEARCH_CARD_FAIL);
                                            listener.callback(info);


                                        }
                                    }else{
                                        Log.d("##log", " is end ");
                                    }
                                }
                            });
                        }
                    }catch (SDKException sdk){
                        Logger.debug("SDKException="+sdk.getMessage().toString());
                        releaseAll();
                        info.setResultFalg(false);
                        info.setErrno(Tcode.SEARCH_CARD_FAIL);
                        listener.callback(info);

                    }

                }
            }.start();

        }

        return info;

    }

    private CardInfo handleMAG(MagneticCard card){

        CardInfo info = new CardInfo() ;
        info.setResultFalg(true);
        info.setCardType(CardType.INMODE_MAG);
        TrackInfo ti_1 = card.getTrackInfos(MagneticCard.TRACK_1);
        TrackInfo ti_2 = card.getTrackInfos(MagneticCard.TRACK_2);
        TrackInfo ti_3 = card.getTrackInfos(MagneticCard.TRACK_3);
        info.setTrackNo(new String[]{ti_1.getData() , ti_2.getData() , ti_3.getData()});
        return info ;

    }

    private CardInfo handleICC() throws SDKException {
        CardInfo info = new CardInfo();
        info.setCardType(CardType.INMODE_IC);
        if (iccReader.isCardPresent()) {
            ContactCard contactCard = iccReader.connectCard(VCC.VOLT_5 , OperatorMode.EMV_MODE);
            byte[] atr = contactCard.getATR() ;
            if (atr.length != 0) {
                info.setResultFalg(true);
                info.setCardAtr(atr);
            } else {
                info.setResultFalg(false);
                info.setErrno(Tcode.SEARCH_CARD_FAIL);
            }
        } else {
            info.setResultFalg(false);
            info.setErrno(Tcode.SEARCH_CARD_FAIL);
        }
        return info;
    }

      private CardInfo handlePICC(int nfcType){
       CardInfo info = new CardInfo();
       info.setResultFalg(true);
       info.setCardType(CardType.INMODE_NFC);
       info.setNfcType(nfcType);
          Log.d("##desfirelog","Entering a zone of handling the picc");
       if (nfcType ==PiccReader.MIFARE_ONE_S50||nfcType ==PiccReader.MIFARE_ONE_S70
               ||nfcType==PiccReader.UNKNOWN_TYPEA){
           Log.d("##desfirelog","Entering  execution block of mifare classic");
           try {

               MifareClassic classic = MifareClassic.connect();
               if (classic !=null){
                   byte[] uid = classic.getUID();
                   //info.setNfcUid(.byte2hex(uid));
               }
           } catch (SDKException e) {

               e.printStackTrace();

           }
       }else if(nfcType == PiccReader.MIFARE_ULTRALIGHT){
             try {
               Log.d("##desfirelog","Entering a zone of mifare ultralight");
                 MifareUltralight mifareUltralight = MifareUltralight.connect();
               if (mifareUltralight!=null){
                   //byte[] uid = mifareUltralight.getUID();
                  // info.setNfcUid(ISOUtil.byte2hex(uid));
               }
           } catch (SDKException e) {
               e.printStackTrace();
           }
       }else if(nfcType == PiccReader.MIFARE_DESFIRE){

           Log.e(TAG, "SAK====>" + PiccReader.getInstance().getSAK());
           try {

               MifareDesfire desfire = MifareDesfire.connect();
               Card_Structure card_structure= new Card_Structure();
               JSONObject object = new JSONObject();
               int keyNo = 0;
               int ret;
               byte[] key = new byte[16];
               boolean card_read_status=true;
               int selectApp0 = desfire.selectApplication(new byte[]{0x00, 0x00, 0x00});
               byte[] uid=desfire.getUID();
               cardUID=byteToString(uid);
               Log.d("##log","  on selecting the app0  "+selectApp0);
               Log.d("##log","  on selecting the app0  "+cardUID);
               boolean onauthentication = desfire.authenticate(keyNo, key);
               Log.d("##log"," on authenticating the app0 "+onauthentication);
               byte[] appId = new byte[]{0x1, 0x00, 0x00};
               if(!(operation=="personalise"))
               ret = desfire.selectApplication(appId);
               else
                    ret=0;
               Log.d("##log"," on selecting app1  "+desfire.authenticate(keyNo, key));
               if(!(ret==0)){

                   description="Card not honoured";
                   this.card_status=card_read_status;
                   return  null;
               }
                  if(operation=="read") {
                      try {

                          String personaldetails = read_personaldetails_file(desfire, 0);
                          String cardpin = read_cardpin_file(desfire, 2);
                          String carddata = read_carddata_file(desfire, 7);
                          Log.d(TAG, "After reading the card ");
                          object.put("personaldetails", new JSONObject(personaldetails));
                          object.put("cardpin", new JSONObject(cardpin));
                          object.put("topups", new JSONObject(carddata));
                          object.put("uid", cardUID);
                          card_status = true;
                          info.setCarddata(object.toString());
                          info.setStatus(true);
                      } catch (Exception e) {

                          this.card_status = false;
                          description = "This card has not been activated";
                          Log.d(TAG, "Error reading details from the card" + e.toString());
                          return null;

                      }
                  }else if(operation=="write"){

                       int status= update_tbc_cardbalance(desfire,7,updatedata);
                       Log.d("##log","return value from reading the card "+ret);
                      if(status==0)
                           info.setResultFalg(true);
                       else
                           info.setResultFalg(false);

                  }else if(operation=="update"){

                      int status= update_tbc_cardbalance(desfire,2,updatedata);
                      Log.d("##log","return value from reading the card "+ret);
                      if(status==0)
                          info.setResultFalg(true);
                      else
                          info.setResultFalg(false);
                  } else if(operation =="personalise"){
                      String personaldetails=null;
                      String cardpin=null;
                      try {
                          JSONObject data = new JSONObject(new String(updatedata));
                          cardpin=data.getJSONObject("cardpin").toString();
                          personaldetails=data.getJSONObject("personaldetails").toString();
                      }catch (Exception e){
                          info.setResultFalg(false);
                          Log.d("##log","Exception "+e.toString());
                          stopPICC();
                           return info;
                      }
                      MifareDesfire.DesfireAppInfo appinfo= new MifareDesfire.DesfireAppInfo();
                      appinfo.AID=new byte[]{0x1, 0x00, 0x00};
                      appinfo.masterKeySetting=0;
                      appinfo.cryptoMode=0;
                      appinfo.numberOfKey=1;
                      MifareDesfire.DesfireStdBackupfileInfo fileSettings= new MifareDesfire.DesfireStdBackupfileInfo();
                      fileSettings.changeAccessRightKeyNo=0;
                      fileSettings.communicationSettings=0;
                      fileSettings.readAndWriteAccessRightKeyNo=0;
                      ret=desfire.createApplication(appinfo);
                      Log.d("##log","Return value on creating application   "+ret);
                      onauthentication = desfire.authenticate(keyNo, key);
                      Log.d("##log"," on authenticating the app1 "+onauthentication);
                      fileSettings.fileSize=1000;
                      ret=desfire.createStdDatafile(0,fileSettings);
                      Log.d("##log","Return value on creating file0   "+ret);
                      fileSettings.fileSize=1000;
                      ret=desfire.createStdDatafile(1,fileSettings);
                      Log.d("##log","Return value on creating file1   "+ret);
                      fileSettings.fileSize=100;
                      ret=desfire.createStdDatafile(2,fileSettings);
                      Log.d("##log","Return value on creating file2   "+ret);
                      fileSettings.fileSize=3000;
                      ret=desfire.createStdDatafile(7,fileSettings);
                      Log.d("##log","Return value on creating file7   "+ret);
                      fileSettings.fileSize=200;
                      ret=desfire.createStdDatafile(10,fileSettings);
                      Log.d("##log","Return value on creating file10   "+ret);
                      ret=desfire.writeData(0,0,0,personaldetails.getBytes());
                      Log.d("##log","Return value on writing the data to the file0   "+ret);
                      ret=desfire.writeData(2,0,0,cardpin.getBytes());
                      Log.d("##log","Return value on writing the data to the file   "+ret);
                      info.setResultFalg(true);
                      stopPICC();
                      return  info;
                  }

           } catch (SDKException e) {

               e.printStackTrace();
           }
       }
       else {
           Log.d("##desfirelog","Entering a zone of contactless emv or no known card type detected");
           EmvContactlessCard emvContactlessCard = null;
       }
       stopPICC();
       return info ;
   }
    public static String decompress(byte[] input) {
        try {

            Inflater decompresser = new Inflater();
            decompresser.setInput(input, 0, input.length);
            Log.d("##reach", "1");
            byte[] result = new byte[input.length * 10];
            boolean ret = decompresser.needsInput();
            Log.d("##ret", " needs input " + ret);
            boolean ret1 = decompresser.needsDictionary();
            Log.d("##ret", " needs dict" + ret1);
            //decompresser.setDictionary();
            int resultLength = decompresser.inflate(result);
            Log.d("##reach", "2");
            //decompresser.end();
            Log.d("##reach", "3");
            String outputString = new String(result, 0, resultLength, "UTF-8");
            Log.d("##reach", "4");
            return outputString;

        } catch (java.io.UnsupportedEncodingException ex) {
            Log.d("##errrd", ex.toString());
            return null;
        } catch (java.util.zip.DataFormatException ex) {
            Log.d("##errrd", ex.toString());
            return null;
        }
    }
    public static byte[] addAll(final byte[] array1, byte[] array2) {
        byte[] joinedArray = Arrays.copyOf(array1, (array1==null)? 0 :array1.length + ((array2==null)? 0 :array2.length));
        System.arraycopy(array2, 0, joinedArray, (array1==null)? 0 :array1.length, (array2==null)? 0 :array2.length);
        return joinedArray;
    }
    public static  String  read_personaldetails_file(MifareDesfire desfire,int fileNo){
        byte[] ret=null;
        Log.d("##data", "Before reading the card");
        byte[] outData0 = desfire.readData(fileNo,0,0,300);
          if(outData0==null)
              return  null;
        byte[] outData1 = desfire.readData(fileNo,0,300,300);
        if(outData1==null)
             return  new String(outData0);
        byte[] outData2= desfire.readData(fileNo,0,600,900);
         if(outData2==null){
             return new String(addAll(outData0,outData1));
         }
        byte[] dest1=addAll(outData0,outData1);
        byte[] dest2=addAll(dest1,outData2);
        byte[] outData3= desfire.readData(fileNo,0,900,100);
         if(outData3==null){
             return  new String(dest2);
         }
        byte[] dest3=addAll(dest2,outData3);
        return  new String(dest3);

    } public static  String  read_carddata_file(MifareDesfire desfire,int fileNo) {

        Log.d("##data", "Before reading the card");
        byte[] outData0 = desfire.readData(fileNo,0,0,300);
        if(outData0==null)
            return  null;
        byte[] outData1 = desfire.readData(fileNo,0,300,300);
        if(outData1==null)
            return  new String(outData0);
        byte[] outData2= desfire.readData(fileNo,0,600,300);
        if(outData2==null){
            return new String(addAll(outData0,outData1));
        }
        byte[] dest1=addAll(outData0,outData1);
        byte[] dest2=addAll(dest1,outData2);
        byte[] outData3= desfire.readData(fileNo,0,900,300);
        if(outData3==null){
            return  new String(dest2);
        }
        byte[] dest3=addAll(dest2,outData3);
        byte[] outData4= desfire.readData(fileNo,0,1200,300);
        if(outData4==null){
            return  new String(dest3);
        }
        byte[] dest4=addAll(dest3,outData4);
        byte[] outData5= desfire.readData(fileNo,0,1500,300);
        if(outData5==null){
            return  new String(dest4);
        }
        byte[] dest5=addAll(dest4,outData5);
        byte[] outData6= desfire.readData(fileNo,0,1800,300);
        if(outData6==null){
            return  new String(dest5);
        }
        byte[] dest6=addAll(dest5,outData6);
        byte[] outData7= desfire.readData(fileNo,0,2100,300);
        if(outData7==null){
            return  new String(dest6);
        }
        byte[] dest7=addAll(dest6,outData7);
        byte[] outData8= desfire.readData(fileNo,0,2400,300);
        if(outData8==null){
            return  new String(dest7);
        }
        byte[] dest8=addAll(dest7,outData8);
        byte[] outData9= desfire.readData(fileNo,0,2700,300);
        if(outData9==null){
            return  new String(dest8);
        }
        byte[] dest9=addAll(dest8,outData9);
        return  new String(dest9);

    }
    public static  String read_cardpin_file(MifareDesfire desfire,int fileNo){
        byte[] ret=null;
        Log.d("##data", "Before reading the card");
        byte[] outData0 = desfire.readData(fileNo,0,0,100);
        if(outData0==null)
            return  null;
        else{
            return  new String(outData0);
        }

    }
    public static  String read_txnhistory_file(MifareDesfire desfire,int fileNo){
        String ret=null;
        byte[] outData0 = desfire.readData(fileNo,0,0,200);
        if(outData0==null)
            return  null;
        else{
            return  new String(outData0);
        }
    }
    public static  String read_second_alternate0_details(MifareDesfire desfire,int fileNo){
        String ret=null;
        byte[] outData = desfire.readData(fileNo, 0, 0, 300);
        byte[] outData1 = desfire.readData(fileNo, 0, 300, 300);
        byte[] outData2 = desfire.readData(fileNo, 0, 600, 100);
        byte[] dest1 = addAll(outData, outData1);
        byte[] dest2 = addAll(dest1, outData2);
        return decompress(dest2);
    }
    public static  String read_second_alternate1_details(MifareDesfire desfire,int fileNo){
        String ret=null;
        byte[] outData = desfire.readData(fileNo, 0, 0, 300);
        byte[] outData1 = desfire.readData(fileNo, 0, 300, 300);
        byte[] outData2 = desfire.readData(fileNo, 0, 600, 100);
        byte[] dest1 = addAll(outData, outData1);
        byte[] dest2 = addAll(dest1, outData2);
        return decompress(dest2);

    }
    public static  String  read_beneficiary_account_details(MifareDesfire desfire,int fileNo){
        byte[] outData = desfire.readData(fileNo, 0, 0, 300);
        byte[] outData1 = desfire.readData(fileNo, 0, 300, 200);
        byte[] dest1 = addAll(outData, outData1);
        return decompress(dest1);
    }
    public static  String  read_beneficiary_topup_details(MifareDesfire desfire,int fileNo){
        String  ret=null;
        byte[] outData = desfire.readData(fileNo, 0, 0, 300);
        byte[] outData1 = desfire.readData(fileNo, 0, 300, 300);
        byte[] outData2 = desfire.readData(fileNo, 0, 600, 300);
        byte[] outData3 = desfire.readData(fileNo, 0, 900, 100);
        byte[] dest1 = addAll(outData, outData1);
        byte[] dest2 = addAll(dest1, outData2);
        byte[] dest3 = addAll(dest2, outData3);
        return decompress(dest2);
    }public static  int  update_tbc_cardbalance(MifareDesfire desfire,int fileNo, byte[] data){

        int ret=desfire.writeData(fileNo, 0, 0, data);
        return  ret;

    }

    private class  Card_Structure {

        public  int personalDetails=0;
        public  int cardDetails=1;
        public  int cpin=2;
        public  int fingerprint1=3;
        public  int fingerprint2=4;
        public  int fingerprint3=5;
        public  int fingerprint4=6;
        public  int carddata=7;
        public  int transactionAuthKey=8;
        public  int transactionIden=9;
        public  int transactionHistory=10;
    }

    private boolean mifaredesfire_card_update(){
        boolean result = false;
        try {
            MifareDesfire desfire = MifareDesfire.connect();
            Card_Structure card_structure = new Card_Structure();
            int keyNo = 0;
            int ret;
            byte[] key = new byte[16];
            boolean card_read_status = true;
            boolean authRet = desfire.authenticate(keyNo, key);
            byte[] appId = new byte[]{0x00, 0x00, 0x1};
            ret = desfire.selectApplication(appId);
            if (!(ret == 0)) {

                description = "Card not honoured";
                this.card_status = card_read_status;
                return  false;
            }
             ret= desfire.writeData(new Card_Structure().personalDetails,0,0,compress(account_details));
             if(ret==0){
                 Log.d("##update","card updated successfully");
                 return true;
             }
            Log.d("##update","card not updated successfully");
               return false;

        }catch (Exception e){

        }
         return result;
    }
    public byte[] compress(String inputString) {
        try {
            byte[] input = inputString.getBytes("UTF-8");
            byte[] output = new byte[input.length];
            Deflater compresser = new Deflater();
            compresser.setInput(input);
            compresser.finish();
            compresser.deflate(output);
            compresser.end();
            return output;
        } catch (java.io.UnsupportedEncodingException ex) {
            Log.d("##errc", ex.toString());
            return null;
        }
    }
    public String byteToString(byte[] uid){
        String uidhex = new String();
        for (int i = 0; i < uid.length; i++) {

            String x = Integer.toHexString(((int) uid[i] & 0xff));
            if (x.length() == 1) {
                x = '0' + x;
            }
            uidhex += x;
        }
         return  uidhex;
    }


}
