package com.newpos.libpay.device.printer;

/**
 * Created by Chirchir on 4/12/2019.
 */

public class Quantitydetails {
    String name;
    String quantity;
    String value;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

}

