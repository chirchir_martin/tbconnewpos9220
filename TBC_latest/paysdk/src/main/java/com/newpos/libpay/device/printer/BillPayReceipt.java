package com.newpos.libpay.device.printer;

/**
 * Created by HARLEX on 6/7/2018.
 */

public class BillPayReceipt  extends Receipt{
    String accountNo;
    String deviceid;
    String agentId;
    String operationType;
    String transactionId;
    Double transcharge;
    String accountname;
    String amount;
    String currencyId;
    String date;
    String custName;
    String narration;
    String trfNo;
    String agentname;
    String currencyname;

    public BillPayReceipt() {
    }

    public BillPayReceipt(String accountNo, String deviceid, String agentId, String operationType, String transactionId, Double transcharge, String accountname, String amount, String currencyId, String date, String custName, String narration, String trfNo, String agentname, String currencyname) {
        this.accountNo = accountNo;
        this.deviceid = deviceid;
        this.agentId = agentId;
        this.operationType = operationType;
        this.transactionId = transactionId;
        this.transcharge = transcharge;
        this.accountname = accountname;
        this.amount = amount;
        this.currencyId = currencyId;
        this.date = date;
        this.custName = custName;
        this.narration = narration;
        this.trfNo = trfNo;
        this.agentname = agentname;
        this.currencyname = currencyname;
    }

    public String getAccountname() {
        return accountname;
    }

    public void setAccountname(String accountname) {
        this.accountname = accountname;
    }

    public String getAccountNo() {
        return accountNo;
    }

    public void setAccountNo(String accountNo) {
        this.accountNo = accountNo;
    }

    public String getDeviceid() {
        return deviceid;
    }

    public void setDeviceid(String deviceid) {
        this.deviceid = deviceid;
    }

    public String getAgentId() {
        return agentId;
    }

    public void setAgentId(String agentId) {
        this.agentId = agentId;
    }

    public String getOperationType() {
        return operationType;
    }

    public void setOperationType(String operationType) {
        this.operationType = operationType;
    }

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public Double getTranscharge() {
        return transcharge;
    }

    public void setTranscharge(Double transcharge) {
        this.transcharge = transcharge;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getCurrencyId() {
        return currencyId;
    }

    public void setCurrencyId(String currencyId) {
        this.currencyId = currencyId;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getCustName() {
        return custName;
    }

    public void setCustName(String custName) {
        this.custName = custName;
    }

    public String getNarration() {
        return narration;
    }

    public void setNarration(String narration) {
        this.narration = narration;
    }

    public String getTrfNo() {
        return trfNo;
    }

    public void setTrfNo(String trfNo) {
        this.trfNo = trfNo;
    }

    public String getAgentname() {
        return agentname;
    }

    public void setAgentname(String agentname) {
        this.agentname = agentname;
    }

    public String getCurrencyname() {
        return currencyname;
    }

    public void setCurrencyname(String currencyname) {
        this.currencyname = currencyname;
    }
}
