package com.newpos.libpay.trans.finace;

/**
 * Created by zhouqiang on 2017/12/11.
 * @author zhouqiang
 * service enter mode of field 22
 */
public enum ServiceEntryMode {
    /**
     * Manual enter card number
     */
    HAND(1),

    /**
     * mag-stripe mode
     */
    MAG(2),

    /**
     * insert card mode
     */
    ICC(5),

    /**
     * contactless mode
     */
    NFC(7),

    /**
     * scan QR code mode
     */
    QRC(9);

    private int val ;

    public int getVal(){
        return this.val ;
    }

    private ServiceEntryMode(int val){
        this.val = val ;
    }
}
