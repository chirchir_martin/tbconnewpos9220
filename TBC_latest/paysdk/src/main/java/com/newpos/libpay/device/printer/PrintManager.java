package com.newpos.libpay.device.printer;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.os.SystemClock;
import android.util.Log;

import com.android.newpos.libemv.PBOCUtil;
import com.newpos.libpay.Logger;
import com.newpos.libpay.R;
import com.newpos.libpay.device.pinpad.WorkKeyinfo;
import com.newpos.libpay.global.TMConfig;
import com.newpos.libpay.presenter.TransInterface;
import com.newpos.libpay.trans.Tcode;
import com.newpos.libpay.trans.Type;
import com.newpos.libpay.trans.finace.ServiceEntryMode;
import com.newpos.libpay.trans.translog.TransLog;
import com.newpos.libpay.trans.translog.TransLogData;
import com.newpos.libpay.utils.ISOUtil;
import com.newpos.libpay.utils.PAYUtils;
import com.pos.device.printer.PrintCanvas;
import com.pos.device.printer.PrintTask;
import com.pos.device.printer.Printer;
import com.pos.device.printer.PrinterCallback;

import org.json.JSONArray;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.CountDownLatch;

import static java.lang.String.format;

/**
 * Created by zhouqiang on 2017/3/14.
 * @author zhouqiang
 * printer manager
 */
public class PrintManager {
	DecimalFormat formatter = new DecimalFormat("#,###.##");
	private static PrintManager mInstance ;
	private static TMConfig cfg ;

	private PrintManager(){}

	private static Context mContext ;
	private static TransInterface transUI ;

	public static PrintManager getmInstance(Context c , TransInterface tui){
		mContext = c ;
		transUI = tui ;
		if(null == mInstance){
			mInstance = new PrintManager();
		}
		cfg = TMConfig.getInstance();
		return mInstance ;
	}

	private Printer printer = null ;
	private PrintTask printTask = null ;

	/**
	 * print receipt
	 * @param data transaction log
	 * @param isRePrint
	 * @return
	 */
	public int print(final TransLogData data, final boolean isRePrint){
		this.printTask = new PrintTask();
		this.printTask.setGray(130);
		data.setMode(ServiceEntryMode.HAND);
		boolean isICC = data.getMode() == ServiceEntryMode.ICC;
		boolean isNFC = data.getMode() == ServiceEntryMode.NFC;
		boolean isScan = data.getMode() == ServiceEntryMode.QRC ;
		int ret = -1;
		if (TransLog.getInstance().getSize() == 0) {
			return Tcode.PRINT_FAIL;
		}
		printer = Printer.getInstance() ;
		if (printer == null) {
			return Tcode.PRINT_FAIL ;
		}
		int num = cfg.getPrinterTickNumber() ;
		Logger.debug("PrintManager>>start>>PrinterTickNumber="+num);
		Bitmap image = PAYUtils.getLogoByBankId(mContext, cfg.getBankid());
		for (int i = 0; i < num; i++) {
			Logger.debug("PrintManager>>start>>for>>i="+i);
			PrintCanvas canvas = new PrintCanvas() ;
			Paint paint = new Paint() ;
			setFontStyle(paint , 2 , false);
			canvas.drawText(PrintRes.CH.WANNING , paint);
			printLine(paint , canvas);
			setFontStyle(paint , 1 , true);
			canvas.drawBitmap(image , paint);
			printLine(paint , canvas);
			setFontStyle(paint , 2 , false);
			if (i == 0) {
				canvas.drawText(PrintRes.CH.MERCHANT_COPY, paint);
			}else if (i == 1){
				canvas.drawText(PrintRes.CH.CARDHOLDER_COPY , paint);
			}else{
				canvas.drawText(PrintRes.CH.BANK_COPY , paint);
			}
			printLine(paint , canvas);
			setFontStyle(paint , 2 , false);
			canvas.drawText(PrintRes.CH.MERCHANT_NAME+"\n"+cfg.getMerchName() , paint);
			canvas.drawText(PrintRes.CH.MERCHANT_ID+"\n"+cfg.getMerchID() , paint);
			canvas.drawText(PrintRes.CH.TERNIMAL_ID+"\n"+cfg.getTermID() , paint);
			String operNo = data.getOprNo() < 10 ? "0" + data.getOprNo() : data.getOprNo()+"";
			canvas.drawText(PrintRes.CH.OPERATOR_NO+"    "+operNo, paint);
			printLine(paint , canvas);
			setFontStyle(paint , 2 , false);
			canvas.drawText(PrintRes.CH.ISSUER, paint);
			canvas.drawText(PrintRes.CH.ACQUIRER, paint);
			if(isScan){
				canvas.drawText(PrintRes.CH.SCANCODE, paint);
			}else {
				canvas.drawText(PrintRes.CH.CARD_NO, paint);
			}
			setFontStyle(paint , 3 , true);
			if (isICC){
				canvas.drawText("     "+ PAYUtils.getSecurityNum(data.getPan(), 6, 4) + " I" , paint);
			}else if(isNFC){
				canvas.drawText("     "+ PAYUtils.getSecurityNum(data.getPan(), 6, 4) + " C" , paint);
			}else if(isScan){
				canvas.drawText("     "+ data.getPan() , paint);
			}else{
				canvas.drawText("     "+ PAYUtils.getSecurityNum(data.getPan(), 6, 4) + " S" , paint);
			}
			setFontStyle(paint , 2 , false);
			canvas.drawText(PrintRes.CH.TRANS_TYPE , paint);
			setFontStyle(paint , 3 , true);
			canvas.drawText(formatTranstype(data.getEName()) , paint);
			setFontStyle(paint , 2 , false);
			if (!PAYUtils.isNullWithTrim(data.getExpDate())){
				canvas.drawText(PrintRes.CH.CARD_EXPDATE+"       " + data.getExpDate() , paint);
			}
			printLine(paint , canvas);
			setFontStyle(paint , 2 , false);
			if(!PAYUtils.isNullWithTrim(data.getBatchNo())){
				canvas.drawText(PrintRes.CH.BATCH_NO + data.getBatchNo(), paint);
			}
			if(!PAYUtils.isNullWithTrim(data.getTraceNo())){
				canvas.drawText(PrintRes.CH.VOUCHER_NO+data.getTraceNo(), paint);
			}
			if(!PAYUtils.isNullWithTrim(data.getAuthCode())){
				canvas.drawText(PrintRes.CH.AUTH_NO+data.getAuthCode() , paint);
			}
			setFontStyle(paint , 2 , false);
			if(!PAYUtils.isNullWithTrim(data.getLocalDate()) && !PAYUtils.isNullWithTrim(data.getLocalTime())){
				String timeStr = PAYUtils.StringPattern(data.getLocalDate() + data.getLocalTime(), "yyyyMMddHHmmss", "yyyy/MM/dd  HH:mm:ss");
				canvas.drawText(PrintRes.CH.DATE_TIME+"\n          " + timeStr, paint);
			}
			if(!PAYUtils.isNullWithTrim(data.getRRN())){
				canvas.drawText(PrintRes.CH.REF_NO+ data.getRRN(), paint);
			}
			canvas.drawText(PrintRes.CH.AMOUNT, paint);
			setFontStyle(paint , 3 , true);
			canvas.drawText("           "+PrintRes.CH.RMB+"     "+ PAYUtils.getStrAmount(data.getAmount()) + "" , paint);
			printLine(paint , canvas);
			setFontStyle(paint , 1 , false);
			if(!PAYUtils.isNullWithTrim(data.getRefence())){
				canvas.drawText(PrintRes.CH.REFERENCE +"\n" + data.getRefence() , paint);
			}
			if(data.getICCData() != null){
				printAppendICCData(data.getICCData() , canvas , paint);
			}
			if (isRePrint) {
				setFontStyle(paint , 3 , true);
				canvas.drawText(PrintRes.CH.REPRINT , paint);
			}
			if (i != 1 && !isScan) {
				setFontStyle(paint , 3 , true);
				canvas.drawText("         "+PrintRes.CH.CARDHOLDER_SIGN+"\n\n\n" , paint);
				printLine(paint , canvas);
				setFontStyle(paint , 1 , false);
				canvas.drawText(PrintRes.CH.AGREE_TRANS+"\n" , paint);
			}
			ret = printData(canvas);
		}
		if (printer != null) {
			printer = null;
		}
		if (image != null){
			image.recycle();
		}
		return ret;
	}

	/**
	 * print settlement receipt
	 * @param data
	 * @return
	 */
	public int printSettle(final TransLogData data){
		this.printTask = new PrintTask();
		this.printTask.setGray(130);
		int ret ;

		printer = Printer.getInstance() ;
		if(printer == null){
			return Tcode.PRINT_FAIL ;
		}

		PrintCanvas canvas = new PrintCanvas() ;
		Paint paint = new Paint() ;

		setFontStyle(paint , 2 , false);
		canvas.drawText(PrintRes.CH.WANNING , paint);
		printLine(paint , canvas);
		setFontStyle(paint , 1 , true);
		Bitmap image = PAYUtils.getLogoByBankId(mContext,cfg.getBankid());
		canvas.drawBitmap(image , paint);
		printLine(paint , canvas);
		setFontStyle(paint , 2 , false);
		canvas.drawText(PrintRes.CH.SETTLE_SUMMARY, paint);
		printLine(paint , canvas);
		setFontStyle(paint , 2 , false);
		canvas.drawText(PrintRes.CH.MERCHANT_NAME+"\n"+cfg.getMerchName() , paint);
		canvas.drawText(PrintRes.CH.MERCHANT_ID+"\n"+cfg.getMerchID() , paint);
		canvas.drawText(PrintRes.CH.TERNIMAL_ID+"\n"+cfg.getTermID() , paint);
		String operNo = data.getOprNo() < 10 ? "0" + data.getOprNo() : data.getOprNo()+"";
		canvas.drawText(PrintRes.CH.OPERATOR_NO+"    "+operNo, paint);
		if(!PAYUtils.isNullWithTrim(data.getBatchNo())){
			canvas.drawText(PrintRes.CH.BATCH_NO + data.getBatchNo(), paint);
		}
		if(!PAYUtils.isNullWithTrim(data.getLocalDate()) && !PAYUtils.isNullWithTrim(data.getLocalTime())){
			String timeStr = PAYUtils.StringPattern(data.getLocalDate() + data.getLocalTime(), "yyyyMMddHHmmss", "yyyy/MM/dd  HH:mm:ss");
			canvas.drawText(PrintRes.CH.DATE_TIME+"\n          " + timeStr, paint);
		}
		printLine(paint , canvas);
		canvas.drawText(PrintRes.CH.SETTLE_LIST , paint);
		printLine(paint , canvas);
		canvas.drawText(PrintRes.CH.SETTLE_INNER_CARD , paint);
		List<TransLogData> list = TransLog.getInstance().getData();
		int saleAmount = 0 ;
		int saleSum = 0 ;
		int quickAmount = 0 ;
		int quickSum = 0 ;
		int voidAmount = 0 ;
		int voidSum = 0 ;
		for (int i = 0 ; i < list.size() ; i++ ){
			TransLogData tld = list.get(i) ;
			if(tld.getEName().equals(Type.SALE)){
				saleAmount += tld.getAmount() ;
				saleSum ++ ;
			}if(tld.getEName().equals(Type.QUICKPASS)){
				if(tld.isOnline()){
					saleAmount += tld.getAmount() ;
					saleSum ++ ;
				}else{
					quickAmount += tld.getAmount() ;
					quickSum ++ ;
				}
			}if(tld.getEName().equals(Type.VOID)){
				voidAmount += tld.getAmount() ;
				voidSum ++ ;
			}
		}

		if(saleSum != 0){
			canvas.drawText(formatTranstype(Type.SALE)+"           "+saleSum+"               "+PAYUtils.getStrAmount(saleAmount) , paint);
		}if(quickSum != 0){
			canvas.drawText("EC SALE/SALE"+"           "+quickSum+"               "+PAYUtils.getStrAmount(quickAmount) , paint);
		}if(voidSum != 0){
			canvas.drawText(formatTranstype(Type.VOID)+"           "+voidSum+"               "+PAYUtils.getStrAmount(voidAmount) , paint);
		}

		printLine(paint , canvas);
		canvas.drawText(PrintRes.CH.SETTLE_OUTER_CARD , paint);

		canvas.drawText("\n\n\n\n\n" , paint);

		//detail
		canvas.drawText(PrintRes.CH.WANNING , paint);
		printLine(paint , canvas);
		setFontStyle(paint , 1 , true);
		canvas.drawBitmap(image , paint);
		setFontStyle(paint , 2 , false);
		printLine(paint , canvas);
		canvas.drawText(PrintRes.CH.SETTLE_DETAILS, paint);
		printLine(paint , canvas);
		setFontStyle(paint , 2 , false);
		canvas.drawText(PrintRes.CH.SETTLE_DETAILS_LIST_CH , paint);
		setFontStyle(paint , 1 , false);
		canvas.drawText(PrintRes.CH.SETTLE_DETAILS_LIST_EN , paint);
		setFontStyle(paint , 2 , false);
		printLine(paint , canvas);

		//transaction detail
		List<TransLogData> list1 = TransLog.getInstance().getData();
		for (int i = 0 ; i < list1.size() ; i++ ){
			TransLogData tld = list1.get(i) ;
			if(tld.getEName().equals(Type.SALE) || tld.getEName().equals(Type.QUICKPASS) || tld.getEName().equals(Type.VOID)){
				canvas.drawText(tld.getTraceNo()+"     "+
						formatDetailsType(tld)+"    "+
						formatDetailsAuth(tld)+"    "+
						PAYUtils.getStrAmount(tld.getAmount())+"   "+
						tld.getPan() , paint);
			}
		}
		ret = printData(canvas);
		return ret ;
	}

	/**
	 * print transaction detail
	 * @return
	 */
	public int printDetails(){
		this.printTask = new PrintTask();
		this.printTask.setGray(130);
		int ret = -1;
		if (TransLog.getInstance().getSize() == 0) {
			return Tcode.PRINT_FAIL;
		}
		printer = Printer.getInstance() ;
		if (printer == null) {
			return Tcode.PRINT_FAIL ;
		}
		PrintCanvas canvas = new PrintCanvas() ;
		Paint paint = new Paint() ;
		setFontStyle(paint , 2 , true);
		canvas.drawText(PrintRes.CH.WANNING , paint);
		printLine(paint , canvas);
		setFontStyle(paint , 3 , true);
		canvas.drawText("                     "+PrintRes.CH.DETAILS , paint);
		setFontStyle(paint , 2 , true);
		canvas.drawText(PrintRes.CH.MERCHANT_NAME+"\n"+cfg.getMerchName() , paint);
		canvas.drawText(PrintRes.CH.MERCHANT_ID+"\n"+cfg.getMerchID() , paint);
		canvas.drawText(PrintRes.CH.TERNIMAL_ID+"\n"+cfg.getTermID() , paint);
		canvas.drawText(PrintRes.CH.BATCH_NO+"\n"+cfg.getBatchNo() , paint);
		canvas.drawText(PrintRes.CH.DATE_TIME+"\n"+PAYUtils.getSysTime(), paint);
		printLine(paint , canvas);
		int num = TransLog.getInstance().getSize() ;
		for (int i = 0 ; i < num ; i++){
			setFontStyle(paint , 1 , true);
			TransLogData data = TransLog.getInstance().get(i);
			if(data.getMode() == ServiceEntryMode.QRC){
				canvas.drawText(PrintRes.CH.SCANCODE+PAYUtils.getSecurityNum(data.getPan(), 6, 4), paint);
			}else {
				if(data.getMode() == ServiceEntryMode.ICC){
					canvas.drawText(PrintRes.CH.CARD_NO+PAYUtils.getSecurityNum(data.getPan(), 6, 4) + " I", paint);
				}else if(data.getMode() == ServiceEntryMode.NFC){
					canvas.drawText(PrintRes.CH.CARD_NO+PAYUtils.getSecurityNum(data.getPan(), 6, 4) + " C", paint);
				}else {
					canvas.drawText(PrintRes.CH.CARD_NO+PAYUtils.getSecurityNum(data.getPan(), 6, 4) + " S", paint);
				}
			}
			canvas.drawText(PrintRes.CH.TRANS_TYPE+formatTranstype(data.getEName()) , paint);
			canvas.drawText(PrintRes.CH.AMOUNT+PrintRes.CH.RMB+PAYUtils.getStrAmount(data.getAmount()), paint);
			canvas.drawText(PrintRes.CH.VOUCHER_NO+data.getTraceNo(), paint);
			printLine(paint , canvas);
		}
		ret = printData(canvas);
		if (printer != null) {
			printer = null;
		}
		return ret;
	}

	/**
	 * print data on receipt
	 * @param pCanvas
	 * @return
	 */
	private int printData(PrintCanvas pCanvas) {
		final CountDownLatch latch = new CountDownLatch(1);
		int ret = printer.getStatus();
		Logger.debug("printer status："+ret);
		if(Printer.PRINTER_STATUS_PAPER_LACK == ret){
			Logger.debug("The printer is short of paper, please pack the paper");
			transUI.handling(Tcode.PRINTER_LACK_PAPER);
			long start = SystemClock.uptimeMillis() ;
			while (true){
				if(SystemClock.uptimeMillis() - start > 60 * 1000){
					ret = Printer.PRINTER_STATUS_PAPER_LACK ;
					break;
				}
				if(printer.getStatus() == Printer.PRINTER_OK){
					ret = Printer.PRINTER_OK ;
					break;
				}else {
					try {
						Thread.sleep(100);
					} catch (InterruptedException e) {
						Logger.debug("printer task interrupted");
					}
				}
			}
		}
		Logger.debug("printing...");
		Log.d("##printer","Started doing print");
		if(ret == Printer.PRINTER_OK){
			transUI.handling(Tcode.PRINTING_RECEPT);
			printTask.setPrintCanvas(pCanvas);
			Logger.debug("started printing work");
			Log.d("##printer","Set the printer canvas");
			printer.startPrint( printTask , new PrinterCallback() {
				@Override
				public void onResult(int i, PrintTask printTask) {
					Logger.debug("on count down latch  printing work");
					latch.countDown();

				}
			});
			Log.d("##printer","Done doing the printing");
			try {
				latch.await();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		Logger.debug("done printing work");
		return ret ;
	}

	/** =======================private function=====================**/

	private String formatTranstype(String type){
		int index = 0 ;
		for (int i = 0 ; i < PrintRes.TRANSEN.length ; i++){
			if(PrintRes.TRANSEN[i].equals(type)){
				index = i ;
			}
		}
		if(Locale.getDefault().getLanguage().equals("zh")){
			return PrintRes.TRANSCH[index]+"("+type+")";
		}else {
			return type ;
		}
	}

	private String formatDetailsType(TransLogData data){
		if(data.getMode() == ServiceEntryMode.ICC){
			return "I" ;
		}else if(data.getMode() == ServiceEntryMode.NFC){
			return "C" ;
		}else {
			return "S" ;
		}
	}

	private String formatDetailsAuth(TransLogData data){
		if(data.getAuthCode() == null){
			return "000000" ;
		}else {
			return data.getAuthCode() ;
		}
	}

	/**
	 * set printer font and style
	 * @param paint
	 * @param size font size 1---small , 2---middle , 3---large
	 * @param isBold bold
	 * @author zq
	 */
	private void setFontStyle(Paint paint , int size , boolean isBold){
		if(isBold) {
			paint.setTypeface(Typeface.DEFAULT_BOLD);
		}else {
			paint.setTypeface(Typeface.DEFAULT);
		}
		switch (size) {
			case 0 : break;
			case 1 : paint.setTextSize(16F) ;break;
			case 2 : paint.setTextSize(22F) ;break;
			case 3 : paint.setTextSize(30F) ;break;
			default:break;
		}
	}

	/**
	 * print a line on receipt
	 * @param paint
	 * @param canvas
	 */
	private void printLine(Paint paint , PrintCanvas canvas){
		String line = "----------------------------------------------------------------";
		setFontStyle(paint , 1 , true);
		canvas.drawText(line , paint);
	}

	private void printAppendICCData(byte[] ICCData , PrintCanvas canvas , Paint paint){
		byte[] temp = new byte[256];
		int len = PBOCUtil.get_tlv_data(ICCData, ICCData.length, 0x4F, temp, false);
		if (!PAYUtils.isNullWithTrim(ISOUtil.trimf(ISOUtil.byte2hex(temp, 0, len)))){
			canvas.drawText("AID: "+ ISOUtil.byte2hex(temp, 0, len) , paint);
		}
		len = PBOCUtil.get_tlv_data(ICCData, ICCData.length, 0x50, temp, false);
		if (!PAYUtils.isNullWithTrim(ISOUtil.trimf(ISOUtil.byte2hex(temp, 0, len)))){
			canvas.drawText("LABLE: "+ ISOUtil.hex2AsciiStr(ISOUtil.byte2hex(temp, 0, len)) , paint);
		}
		len = PBOCUtil.get_tlv_data(ICCData, ICCData.length, 0x9F26, temp, false);
		if (!PAYUtils.isNullWithTrim(ISOUtil.trimf(ISOUtil.byte2hex(temp, 0, len)))){
			canvas.drawText("TC: "+ ISOUtil.byte2hex(temp, 0, len) , paint);
		}
		len = PBOCUtil.get_tlv_data(ICCData, ICCData.length, 0x5F34,temp, false);
		if (!PAYUtils.isNullWithTrim(ISOUtil.trimf(ISOUtil.byte2hex(temp, 0, len)))){
			canvas.drawText("PanSN: "+ ISOUtil.byte2hex(temp, 0, len) , paint);
		}
		len = PBOCUtil.get_tlv_data(ICCData, ICCData.length, 0x95, temp, false);
		if (!PAYUtils.isNullWithTrim(ISOUtil.trimf(ISOUtil.byte2hex(temp, 0, len)))){
			canvas.drawText("TVR: "+ ISOUtil.byte2hex(temp, 0, len) , paint);
		}
		len = PBOCUtil.get_tlv_data(ICCData, ICCData.length, 0x9B, temp, false);
		if (!PAYUtils.isNullWithTrim(ISOUtil.trimf(ISOUtil.byte2hex(temp, 0, len)))){
			canvas.drawText("TSI: "+ ISOUtil.byte2hex(temp, 0, len) , paint);
		}
		len = PBOCUtil.get_tlv_data(ICCData, ICCData.length, 0x9F36, temp, false);
		if (!PAYUtils.isNullWithTrim(ISOUtil.trimf(ISOUtil.byte2hex(temp, 0, len)))){
			canvas.drawText("ATC: "+ ISOUtil.byte2hex(temp, 0, len) + "" , paint);
		}
		len = PBOCUtil.get_tlv_data(ICCData, ICCData.length, 0x9F33, temp, false);
		if (!PAYUtils.isNullWithTrim(ISOUtil.trimf(ISOUtil.byte2hex(temp, 0, len)))){
			canvas.drawText("TermCap: "+ ISOUtil.byte2hex(temp, 0, len) + "" , paint);
		}
		len = PBOCUtil.get_tlv_data(ICCData, ICCData.length, 0x9F09, temp, false);
		if (!PAYUtils.isNullWithTrim(ISOUtil.trimf(ISOUtil.byte2hex(temp, 0, len)))){
			canvas.drawText("AppVer: "+ ISOUtil.byte2hex(temp, 0, len) + "" , paint);
		}
		len = PBOCUtil.get_tlv_data(ICCData, ICCData.length, 0x9F34, temp, false);
		if (!PAYUtils.isNullWithTrim(ISOUtil.trimf(ISOUtil.byte2hex(temp, 0, len)))){
			canvas.drawText("CVM: "+ ISOUtil.byte2hex(temp, 0, len) + "" , paint);
		}
		len = PBOCUtil.get_tlv_data(ICCData, ICCData.length, 0x9F10, temp, false);
		if (!PAYUtils.isNullWithTrim(ISOUtil.trimf(ISOUtil.byte2hex(temp, 0, len)))){
			canvas.drawText("IAD: "+ ISOUtil.byte2hex(temp, 0, len) + "" , paint);
		}
		len = PBOCUtil.get_tlv_data(ICCData, ICCData.length, 0x82, temp, false);
		if (!PAYUtils.isNullWithTrim(ISOUtil.trimf(ISOUtil.byte2hex(temp, 0, len)))){
			canvas.drawText("AIP: "+ ISOUtil.byte2hex(temp, 0, len) + "" , paint);
		}
		len = PBOCUtil.get_tlv_data(ICCData, ICCData.length, 0x9F1E, temp, false);
		if (!PAYUtils.isNullWithTrim(ISOUtil.trimf(ISOUtil.byte2hex(temp, 0, len)))){
			canvas.drawText("IFD: "+ ISOUtil.byte2hex(temp, 0, len) + "" , paint);
		}
	}
	public int printAgentBillReceipt(Context mContext, BillPayReceipt receipt){
		this.printTask = new PrintTask();
		this.printTask.setGray(130);
		int ret = -1;
		printer = Printer.getInstance() ;
		if (printer == null) {
			return Tcode.PRINT_FAIL ;
		}
		PrintCanvas canvas = new PrintCanvas() ;
		Paint paint = new Paint() ;
		BitmapDrawable drawable = (BitmapDrawable) mContext.getResources().getDrawable(R.drawable.download23);
		Bitmap bitmap = drawable.getBitmap();
		BitmapDrawable drawable1 = (BitmapDrawable) mContext.getResources().getDrawable(R.drawable.ecompass);
		Bitmap bitmap1 = drawable1.getBitmap();
		setFontStyle(paint , 2 , true);
		canvas.drawText("**********Agent Copy**********",paint);
		setFontStyle(paint , 2 , true);
		canvas.drawBitmap(bitmap,paint);
		canvas.drawText("==========================" , paint);
		setFontStyle(paint , 3 , true);
		canvas.drawText("      Bill Payment" , paint);
		setFontStyle(paint , 2 , true);
		canvas.drawText("==========================" , paint);

		setFontStyle(paint , 2 , true);
		canvas.drawText("Agent Name       :"+receipt.getAgentname() , paint);
		canvas.drawText("Device           :"+receipt.getDeviceid() , paint);
		//canvas.drawText("Batch No      :    45338" , paint);
		canvas.drawText("Date and Time    :"+receipt.getDate(), paint);
		canvas.drawText("                     " , paint);
		canvas.drawText("===========================" , paint);
		setFontStyle(paint , 2 , true);
		canvas.drawText("Transaction Details",paint);
		setFontStyle(paint , 2 , true);
		canvas.drawText("===========================" , paint);

		canvas.drawText("Account No        :"+receipt.getAccountNo(),paint);
		canvas.drawText("Account Name      :"+receipt.getAccountname(),paint);
		canvas.drawText("TRef No.          :"+receipt.getTrfNo(),paint);
		canvas.drawText("===========================" , paint);
		setFontStyle(paint , 3 , true);
		canvas.drawText("Amount        :"+receipt.getCurrencyname()+":"+formatter.format(Double.parseDouble(receipt.getAmount())),paint);
		canvas.drawText("Trans charge  :  "+receipt.getCurrencyname()+":"+formatter.format(receipt.getTranscharge()),paint);
		setFontStyle(paint , 2 , true);
		canvas.drawText("===========================" , paint);

		canvas.drawText("===========================" , paint);
		canvas.drawBitmap(bitmap1,paint);
		setFontStyle(paint , 2 , true);
		canvas.drawText(" powered by www.compulynx.com",paint);
		Log.d("##reached","before print");
		ret = printData(canvas);
		Log.d("##reached","after print");
		if (printer != null) {
			printer = null;
		}
		return  ret;
	}
	public int printCustomerBillReceipt(Context mContext, UtilitiesReceipt receipt,String owner,String receiptName){
		this.printTask = new PrintTask();
		this.printTask.setGray(130);
		int ret = -1;
		printer = Printer.getInstance() ;
		if (printer == null) {
			return Tcode.PRINT_FAIL ;
		}
		Log.d("##log","123");
		PrintCanvas canvas = new PrintCanvas() ;
		Paint paint = new Paint() ;
		BitmapDrawable drawable = (BitmapDrawable) mContext.getResources().getDrawable(R.drawable.postbanklogo);
		Bitmap bitmap = drawable.getBitmap();
		setFontStyle(paint , 2 , false);
		canvas.drawText("  **********"+owner+ "**********",paint);
		setFontStyle(paint , 2 , false);
		canvas.setX(80);
		canvas.drawBitmap(bitmap,paint);
		canvas.setX(0);
		setFontStyle(paint , 2 , false);
		canvas.drawText("-----------------------------------------------" , paint);
		setFontStyle(paint , 2 , false);
		canvas.drawText("Receipt No :    "+receipt.getTrfNo() , paint);
		canvas.drawText("    " , paint);
		canvas.drawText("Agent Id :    "+receipt.getAgentId() , paint);
		canvas.drawText("    " , paint);
		canvas.drawText("Device     :    "+receipt.getDeviceid() , paint);
		canvas.drawText("    " , paint);
		canvas.drawText("Branch Name  :   "+receipt.getBranch_name() , paint);
		canvas.drawText("    " , paint);
		//canvas.drawText("Batch No      :    45338" , paint);

		canvas.drawText("Date/Time : "+receipt.getDate(), paint);
		canvas.drawText("-----------------------------------------------" , paint);
		setFontStyle(paint , 3 , false);
		canvas.drawText(""+receiptName+"  " , paint);
		canvas.drawText("    " , paint);
		setFontStyle(paint , 2 , false);
		/*if(!receipt.getAccountNo().equalsIgnoreCase("cash")) {
			Log.d("##log","128");
			canvas.drawText("Received From    :  " + maskAccount(receipt.getAccountNo(),4),paint);
			canvas.drawText("    ", paint);

		}else{

			Log.d("##log","129");
			canvas.drawText("Paid with Cash ", paint);
			canvas.drawText("    ", paint);

		}*/
		Log.d("##log","126");
		 if(receipt.getBillname().equalsIgnoreCase("UMEME")){

             canvas.drawText("Metre No   :  "+""+receipt.getMetreNo(),paint);
             canvas.drawText("    " , paint);
			 canvas.drawText("Energy Token   :  "+""+receipt.getEnergyToken(),paint);
			 canvas.drawText("    " , paint);
			 canvas.drawText("Token Value   :  "+""+receipt.getTokenValue(),paint);
			 canvas.drawText("    " , paint);

         }
		if(receipt.isSchool()){

			canvas.drawText("Student Name  :  "+""+receipt.getStudentname(),paint);
			canvas.drawText("    " , paint);
			canvas.drawText("Student Number   :  "+""+receipt.getStudentnumber(),paint);
			canvas.drawText("    " , paint);
			canvas.drawText("School Name   :  "+""+receipt.getSchoolname(),paint);
			canvas.drawText("    " , paint);
			canvas.drawText("School Account   :  "+""+receipt.getSchoolaccount(),paint);
			canvas.drawText("    " , paint);

		}
		Log.d("##log","127");
		//canvas.drawText("Bill Name   :  "+""+receipt.getBillname(),paint);
		//canvas.drawText("    " , paint);
		canvas.drawText("Cust Ref   :  "+""+receipt.getCustRef(),paint);
		canvas.drawText("    " , paint);
		canvas.drawText("Cust Name   :  "+""+receipt.getCustName(),paint);
		canvas.drawText("    " , paint);
		canvas.drawText("Txn Amount    :  "+"UGX "+formatter.format(Double.parseDouble(receipt.getAmount())),paint);
		canvas.drawText("    " , paint);
		canvas.drawText("Txn Fee    :  "+"UGX "+formatter.format(Double.parseDouble(""+receipt.getTranscharge())),paint);
		canvas.drawText("    " , paint);
		canvas.drawText("Excise Duty :  "+"UGX "+formatter.format(Double.parseDouble(""+receipt.getExerciseDuty())),paint);
		canvas.drawText("   ",paint);
		double total=Double.parseDouble(receipt.getAmount())+Double.parseDouble(""+receipt.getTranscharge())+Double.parseDouble(""+receipt.getExerciseDuty());
		canvas.drawText("Total Amount  :"+"UGX "+formatter.format(total),paint);
		canvas.drawText("   ",paint);
		if(!receipt.getAccountNo().equalsIgnoreCase("cash")) {
			Log.d("##log","128");
			canvas.drawText("Received From    :  " + maskAccount(receipt.getAccountNo(),4),paint);
			canvas.drawText("    ", paint);

		}else{

			Log.d("##log","129");
			canvas.drawText("Paid with Cash ", paint);
			canvas.drawText("    ", paint);

		}
		canvas.drawText("Trans Id    :   "+receipt.getTransactionId(),paint);
		canvas.drawText("-----------------------------------------------" , paint);
		canvas.drawText("You were served by "+receipt.getAgentname() , paint);
		if(owner.contains("Agent")) {
			canvas.drawText("Cust sign :_ _ _ _ _ _ _ _ _ _ _ _ _", paint);
		}
		canvas.drawText("      Thanks for banking with  us" , paint);
		ret = printData(canvas);
		if (printer != null) {
			printer = null;
		}
		return  ret;
	}
	public int printCustomerBalanceReceipt(Context mContext, InquiryReceipt receipt,String receiptName){
		this.printTask = new PrintTask();
		this.printTask.setGray(130);
		int ret = -1;
		printer = Printer.getInstance() ;
		if (printer == null) {
			return Tcode.PRINT_FAIL ;
		}
		PrintCanvas canvas = new PrintCanvas() ;
		Paint paint = new Paint() ;
		BitmapDrawable drawable = (BitmapDrawable) mContext.getResources().getDrawable(R.drawable.postbanklogo);
		Bitmap bitmap = drawable.getBitmap();
		setFontStyle(paint , 2 , true);
		canvas.setX(80);
		canvas.drawBitmap(bitmap,paint);
		canvas.setX(0);
		setFontStyle(paint , 2 , true);
		canvas.drawText("-----------------------------------------------" , paint);
		setFontStyle(paint , 2 , true);
		canvas.drawText("Receipt No :    "+receipt.getTrfNo() , paint);
		canvas.drawText("    " , paint);
		canvas.drawText("Agent Id :    "+receipt.getAgentId() , paint);
		canvas.drawText("    " , paint);
		canvas.drawText("Device     :    "+receipt.getDeviceid() , paint);
		canvas.drawText("    " , paint);
		canvas.drawText("Branch Name  :   "+receipt.getBranch_name() , paint);
		canvas.drawText("    " , paint);
		//canvas.drawText("Batch No      :    45338" , paint);
		canvas.drawText("Date/Time  :  "+receipt.getDate(), paint);
		canvas.drawText("-----------------------------------------------" , paint);
		setFontStyle(paint , 3 , true);
		canvas.drawText(""+receiptName+"  " , paint);
		canvas.drawText("    " , paint);
		setFontStyle(paint , 2 , true);
		canvas.drawText("Cust Account    :  "+maskAccount(receipt.getAccountNo(),4),paint);
		canvas.drawText("    " , paint);
		canvas.drawText("Account Balance   :  "+"UGX "+receipt.getAccountbalance(),paint);
		canvas.drawText("    " , paint);
		canvas.drawText("Available Balance   :  "+"UGX "+receipt.getAvailable_balance(),paint);
		canvas.drawText("    " , paint);
		canvas.drawText("Auth By  :  "+""+receipt.getAuthentication(),paint);
		canvas.drawText("    " , paint);
		canvas.drawText("Trans Id    :   "+receipt.getTransactionId(),paint);
		canvas.drawText("    " , paint);
		//canvas.drawText("Txn Charges",paint);
		//canvas.drawText("   ",paint);
		//canvas.setX(40);
		canvas.drawText("Exercise duty :  "+"UGX "+formatter.format(Double.parseDouble(""+receipt.getExerciseDuty())),paint);
		//canvas.setX(40);
		canvas.drawText("   ",paint);
		//canvas.drawText("Agent Comm  :  "+"UGX "+formatter.format(Double.parseDouble(""+receipt.getAgentComission())),paint);
		//canvas.setX(40);
		//canvas.drawText("Bank Income :  "+"UGX "+formatter.format(Double.parseDouble(""+receipt.getBankIncome())),paint);
		//canvas.setX(40);
		canvas.drawText("Txn Charge     :  "+"UGX "+formatter.format(Double.parseDouble(""+receipt.getTranscharge())),paint);
		//canvas.setX(0);
		//canvas.drawText("    " , paint);
		canvas.drawText("-----------------------------------------------" , paint);
		canvas.drawText("    " , paint);
		canvas.drawText("      Thanks for banking with  us" , paint);
		canvas.drawText("    " , paint);
		if (printer != null) {
			printer = null;
		}
		return  ret;
	}
	public int  printCustomerCashWithdrawalReceipt(Context mContext, WithdrawalReceipt receipt,String owner,String receiptName){
		this.printTask = new PrintTask();
		this.printTask.setGray(130);
		int ret = -1;
		printer = Printer.getInstance() ;
		if (printer == null) {
			return Tcode.PRINT_FAIL ;
		}
		PrintCanvas canvas = new PrintCanvas() ;
		Paint paint = new Paint() ;
		BitmapDrawable drawable = (BitmapDrawable) mContext.getResources().getDrawable(R.drawable.postbanklogo);
		Bitmap bitmap = drawable.getBitmap();
		setFontStyle(paint , 2 , false);
		canvas.drawText("   **********"+owner+ "**********",paint);
		setFontStyle(paint , 2 , false);
		canvas.setX(80);
		canvas.drawBitmap(bitmap,paint);
		canvas.setX(0);
		setFontStyle(paint , 2 , false);
		canvas.drawText("-----------------------------------------------" , paint);
		setFontStyle(paint , 2 , false);
		canvas.drawText("Receipt No :    "+receipt.getTrfNo() , paint);
		canvas.drawText("    " , paint);
		canvas.drawText("Agent Id :    "+receipt.getAgentId() , paint);
		canvas.drawText("    " , paint);
		canvas.drawText("Device     :    "+receipt.getDeviceid() , paint);
		canvas.drawText("    " , paint);
		canvas.drawText("Branch Name  :   "+receipt.getBranch_name() , paint);
		canvas.drawText("    " , paint);
		//canvas.drawText("Batch No      :    45338" , paint);
		canvas.drawText("Date/Time  :  "+receipt.getDate(), paint);
		canvas.drawText("-----------------------------------------------" , paint);
		setFontStyle(paint , 3 , false);
		canvas.drawText(""+"Cash Withdrawal"+"  " , paint);
		canvas.drawText("    " , paint);
		setFontStyle(paint , 2 , false);
		canvas.drawText("Cust Name    :  "+receipt.getCustName(),paint);
		canvas.drawText("    " , paint);
		canvas.drawText("Amount    :  "+"UGX "+formatter.format(Double.parseDouble(receipt.getAmount())),paint);
		canvas.drawText("    " , paint);
		canvas.drawText("Cust Account  :  "+""+maskAccount(receipt.getAccountNo(),4),paint);
		canvas.drawText("    " , paint);
		canvas.drawText("Auth By  :  "+""+receipt.getAuthname(),paint);
		canvas.drawText("    " , paint);
		canvas.drawText("Txn Amount    :  "+"UGX "+formatter.format(Double.parseDouble(""+receipt.getAmount())),paint);
		canvas.drawText("    " , paint);
		canvas.drawText("Txn Fee     :  "+"UGX "+formatter.format(Double.parseDouble(""+receipt.getTranscharge())),paint);
		canvas.drawText("   ",paint);
		canvas.drawText("Excise Duty :  "+"UGX "+formatter.format(Double.parseDouble(""+receipt.getExerciseDuty())),paint);
		canvas.drawText("   ",paint);
		double total=Double.parseDouble(""+receipt.getAmount())+Double.parseDouble(""+receipt.getTranscharge())+Double.parseDouble(""+receipt.getExerciseDuty());
		canvas.drawText("Total  Amount  :"+"UGX "+formatter.format(total),paint);
		canvas.drawText("   ",paint);
		canvas.drawText("Trans Id    :   "+receipt.getTransactionId(),paint);
		canvas.drawText("-----------------------------------------------" , paint);
		canvas.drawText("You were served by "+receipt.getAgentname() , paint);
		if(owner.contains("Agent")) {
			canvas.drawText("Cust sign :_ _ _ _ _ _ _ _ _ _ _ _ _", paint);
		}
		canvas.drawText("      Thanks for banking with  us" , paint);
		//canvas.drawText("    " , paint);
		ret = printData(canvas);
		Log.d("##reached","after print");
		if (printer != null) {
			printer = null;
		}
		return  ret;
	}
	public int printCustomerCashDepositReceipt1(Context mContext, DepositReceipt receipt,String  owner,String receiptName){
		this.printTask = new PrintTask();
		this.printTask.setGray(130);
		int ret = -1;
		printer = Printer.getInstance() ;
		String  transdetails="";
		if (printer == null) {
			return Tcode.PRINT_FAIL ;
		}
		PrintCanvas canvas = new PrintCanvas() ;
		Paint paint = new Paint() ;
		BitmapDrawable drawable = (BitmapDrawable) mContext.getResources().getDrawable(R.drawable.postbanklogo);
		Bitmap bitmap = drawable.getBitmap();
		setFontStyle(paint , 2 , false);
		canvas.drawText("   **********"+owner+ "**********",paint);
		setFontStyle(paint , 2 , false);
		canvas.setX(80);
		canvas.drawBitmap(bitmap,paint);
		canvas.setX(0);
		setFontStyle(paint , 2 , false);
		canvas.drawText("-----------------------------------------------" , paint);
		setFontStyle(paint , 2 , false);
		canvas=printText("Receipt No",receipt.getTrfNo(),canvas,paint);
		canvas.drawText("    " , paint);
		canvas=printText("Agent Id",receipt.getAgentId(),canvas,paint);
		canvas.drawText("    " , paint);
		canvas=printText("Device",receipt.getDeviceid(),canvas,paint);
		canvas.drawText("    " , paint);
		canvas=printText("Branch Name",receipt.getBranch_name(),canvas,paint);
		canvas.drawText("    " , paint);
		canvas=printText("Date/Time",receipt.getDate(),canvas,paint);
		canvas.drawText("-----------------------------------------------" , paint);
		setFontStyle(paint , 3 , false);
		canvas=printText("Cash deposit"," ",canvas,paint);
		canvas.drawText("    " , paint);
		setFontStyle(paint , 2 , false);
		canvas=printText("Cust Name",receipt.getCustName(),canvas,paint);
		canvas.drawText("    " , paint);
		canvas=printText("Cust Account",maskAccount(receipt.getAccountNo(),4),canvas,paint);
		double total=Double.parseDouble(""+receipt.getAmount())+Double.parseDouble(""+receipt.getTranscharge())+Double.parseDouble(""+receipt.getExerciseDuty());
		int len =("UGX "+total).length();
		canvas.drawText("    " , paint);
		canvas=printAmount("Txn Amount","UGX "+formatter.format(Double.parseDouble(""+receipt.getAmount())),canvas,paint,len);
		canvas.drawText("    " , paint);
		canvas=printAmount("Txn Fee","UGX "+formatter.format(Double.parseDouble(""+receipt.getTranscharge())),canvas,paint,len);
		canvas.drawText("   ",paint);
		canvas=printAmount("Excise Duty","UGX "+formatter.format(Double.parseDouble(""+receipt.getExerciseDuty())),canvas,paint,len);
		canvas.drawText("   ",paint);
		canvas=printAmount("Total  Amount","UGX "+formatter.format(total),canvas,paint,len);
		canvas.drawText("   ",paint);
		canvas=printText("Received From",receipt.getNarration(),canvas,paint);
		canvas.drawText("    " , paint);
		printText("Trans Id",receipt.getTransactionId(),canvas,paint);
		canvas.drawText("-----------------------------------------------" , paint);
		canvas.drawText("You were served by "+receipt.getAgentname() , paint);
		if(owner.contains("Agent")) {
			canvas.drawText("Cust sign :_ _ _ _ _ _ _ _ _ _ _ _ _", paint);
		}
		canvas.drawText("      Thanks for banking with  us" , paint);
		Log.d("##receipt", transdetails);
		ret = printData(canvas);
		if (printer != null) {
			printer = null;
		}
		return  ret;
	}
	public int printCustomerCashDepositReceipt(Context mContext, DepositReceipt receipt,String  owner,String receiptName){
		this.printTask = new PrintTask();
		this.printTask.setGray(130);
		int ret = -1;
		printer = Printer.getInstance() ;
        String  transdetails="";
		if (printer == null) {
			return Tcode.PRINT_FAIL ;
		}
		PrintCanvas canvas = new PrintCanvas() ;
		Paint paint = new Paint() ;
		BitmapDrawable drawable = (BitmapDrawable) mContext.getResources().getDrawable(R.drawable.postbanklogo);
		Bitmap bitmap = drawable.getBitmap();
		setFontStyle(paint , 2 , false);
		canvas.drawText("   **********"+owner+ "**********",paint);
		setFontStyle(paint , 2 , false);
		canvas.setX(80);
		canvas.drawBitmap(bitmap,paint);
		canvas.setX(0);
		setFontStyle(paint , 2 , false);
		canvas.drawText("-----------------------------------------------" , paint);
		setFontStyle(paint , 2 , false);
		canvas.drawText("Receipt No :    "+receipt.getTrfNo() , paint);
		canvas.drawText("    " , paint);
		canvas.drawText("Agent Id :    "+receipt.getAgentId() , paint);
		canvas.drawText("    " , paint);
		canvas.drawText("Device     :    "+receipt.getDeviceid() , paint);
		canvas.drawText("    " , paint);
		canvas.drawText("Branch Name  :   "+receipt.getBranch_name() , paint);
		canvas.drawText("    " , paint);
		canvas.drawText("Date/Time  :  "+receipt.getDate(), paint);
		canvas.drawText("-----------------------------------------------" , paint);
		setFontStyle(paint , 3 , false);
		canvas.drawText(""+"Cash deposit"+"  " , paint);
		canvas.drawText("    " , paint);
		setFontStyle(paint , 2 , false);
		canvas.drawText("Cust Name    :  "+receipt.getCustName(),paint);
		canvas.drawText("    " , paint);
		canvas.drawText("Cust Account   :  "+""+maskAccount(receipt.getAccountNo(),4),paint);
		canvas.drawText("    " , paint);
		canvas.drawText("Txn Amount    :  "+"UGX "+formatter.format(Double.parseDouble(""+receipt.getAmount())),paint);
		canvas.drawText("    " , paint);
		canvas.drawText("Txn Fee     :  "+"UGX "+formatter.format(Double.parseDouble(""+receipt.getTranscharge())),paint);
		canvas.drawText("   ",paint);
		canvas.drawText("Excise Duty :  "+"UGX "+formatter.format(Double.parseDouble(""+receipt.getExerciseDuty())),paint);
		canvas.drawText("   ",paint);
		double total=Double.parseDouble(""+receipt.getAmount())+Double.parseDouble(""+receipt.getTranscharge())+Double.parseDouble(""+receipt.getExerciseDuty());
		canvas.drawText("Total  Amount  :"+"UGX "+formatter.format(total),paint);
		canvas.drawText("   ",paint);
		canvas.drawText("Received From :  "+""+receipt.getNarration(),paint);
		canvas.drawText("    " , paint);
		canvas.drawText("Trans Id    :   "+receipt.getTransactionId(),paint);
		canvas.drawText("-----------------------------------------------" , paint);
		canvas.drawText("You were served by "+receipt.getAgentname() , paint);
		if(owner.contains("Agent")) {
			canvas.drawText("Cust sign :_ _ _ _ _ _ _ _ _ _ _ _ _", paint);
		}
		 canvas.drawText("      Thanks for banking with  us" , paint);
         Log.d("##receipt", transdetails);
		ret = printData(canvas);
		if (printer != null) {
			printer = null;
		}
		return  ret;
	}

	public int printCashInterTransferReceipt(Context mContext, InterBankTransferReceipt receipt,String owner,String receiptName){
		this.printTask = new PrintTask();
		this.printTask.setGray(130);
		int ret = -1;
		printer = Printer.getInstance() ;
		if (printer == null) {
			return Tcode.PRINT_FAIL ;
		}
		PrintCanvas canvas = new PrintCanvas() ;
		Paint paint = new Paint() ;
		BitmapDrawable drawable = (BitmapDrawable) mContext.getResources().getDrawable(R.drawable.postbanklogo);
		Bitmap bitmap = drawable.getBitmap();
		setFontStyle(paint , 2 , true);
		canvas.drawText("   **********"+owner+ "**********",paint);
		setFontStyle(paint , 2 , true);
		canvas.setX(80);
		canvas.drawBitmap(bitmap,paint);
		canvas.setX(0);
		setFontStyle(paint , 2 , true);
		canvas.drawText("-----------------------------------------------" , paint);
		setFontStyle(paint , 2 , true);
		canvas.drawText("Receipt No :    "+receipt.getTrfNo() , paint);
		canvas.drawText("    " , paint);
		canvas.drawText("Agent Id :    "+receipt.getAgentId() , paint);
		canvas.drawText("    " , paint);
		canvas.drawText("Device     :    "+receipt.getDeviceid() , paint);
		canvas.drawText("    " , paint);
		canvas.drawText("Branch Name  :   "+receipt, paint);
		canvas.drawText("    " , paint);
		//canvas.drawText("Batch No      :    45338" , paint);
		canvas.drawText("Date/Time  :  "+receipt.getDate(), paint);
		canvas.drawText("-----------------------------------------------" , paint);
		setFontStyle(paint , 3 , true);
		canvas.drawText(""+receiptName+"  " , paint);
		canvas.drawText("    " , paint);
		setFontStyle(paint , 2 , true);
		canvas.drawText("Cust Name   :  "+""+receipt.getCustName(),paint);
		canvas.drawText("    " , paint);
		canvas.drawText("Cust Account    :  "+maskAccount(receipt.getAccountTo(),4),paint);
		canvas.drawText("    " , paint);
		canvas.drawText("Amount    :  "+receipt.getAmount(),paint);
		canvas.drawText("    " , paint);
		canvas.drawText("Received From :  "+""+receipt.getNarration(),paint);
		canvas.drawText("    " , paint);
		canvas.drawText("Trans Id    :   "+receipt.getTransactionId(),paint);
		canvas.drawText("    " , paint);
		canvas.drawText("Txn Charges",paint);
		canvas.drawText("    " , paint);
		//canvas.drawText("Txn Charges",paint);
		//canvas.drawText("   ",paint);
		//canvas.setX(40);
		canvas.drawText("Exercise duty :  "+"UGX "+formatter.format(Double.parseDouble(""+receipt.getExerciseDuty())),paint);
		//canvas.setX(40);
		canvas.drawText("   ",paint);
		//canvas.drawText("Agent Comm  :  "+"UGX "+formatter.format(Double.parseDouble(""+receipt.getAgentComission())),paint);
		//canvas.setX(40);
		//canvas.drawText("Bank Income :  "+"UGX "+formatter.format(Double.parseDouble(""+receipt.getBankIncome())),paint);
		//canvas.setX(40);
		canvas.drawText("Txn Charge     :  "+"UGX "+formatter.format(Double.parseDouble(""+receipt.getTranscharge())),paint);
		//canvas.setX(0);
		canvas.drawText("    " , paint);
		canvas.drawText("-----------------------------------------------" , paint);
		canvas.drawText("    " , paint);
		canvas.drawText("      Thanks for banking with  us" , paint);
		canvas.drawText("    " , paint);
		ret = printData(canvas);
		if (printer != null) {
			printer = null;
		}
		return  ret;
	}
	public int printCustomerCashIntraTransferReceipt(Context mContext, IntraBankTransferReceipt receipt){
		this.printTask = new PrintTask();
		this.printTask.setGray(130);
		int ret = -1;
		printer = Printer.getInstance() ;
		if (printer == null) {
			return Tcode.PRINT_FAIL ;
		}
		PrintCanvas canvas = new PrintCanvas() ;
		Paint paint = new Paint() ;
		BitmapDrawable drawable = (BitmapDrawable) mContext.getResources().getDrawable(R.drawable.download23);
		Bitmap bitmap = drawable.getBitmap();
		BitmapDrawable drawable1 = (BitmapDrawable) mContext.getResources().getDrawable(R.drawable.ecompass);
		Bitmap bitmap1 = drawable1.getBitmap();
		setFontStyle(paint , 2 , true);
		canvas.drawText("    *****Customer Copy*****",paint);
		canvas.drawBitmap(bitmap,paint);
		canvas.drawText("==========================" , paint);
		setFontStyle(paint , 3 , true);
		canvas.drawText("       Intra Bank Transfer  " , paint);
		setFontStyle(paint , 2 , true);
		canvas.drawText("==========================" , paint);

		setFontStyle(paint , 2 , true);
		canvas.drawText("Agent Name :    "+receipt.getAgentname() , paint);
		canvas.drawText("Device   :    "+receipt.getDeviceid() , paint);
		canvas.drawText("Date and Time :  "+receipt.getDate(), paint);
		canvas.drawText("                     " , paint);
		canvas.drawText("===========================" , paint);
		setFontStyle(paint , 2 , true);
		canvas.drawText("Transaction Details",paint);
		setFontStyle(paint , 2 , true);
		canvas.drawText("===========================" , paint);

		canvas.drawText("From Account    :  "+receipt.getAccountFrom(),paint);
		canvas.drawText("To Account    :   "+receipt.getAccountTo(),paint);
		canvas.drawText("TRef No.      :   "+receipt.getTrfNo(),paint);
		canvas.drawText("===========================" , paint);
		setFontStyle(paint , 3 , true);
		canvas.drawText("Amount        :"+receipt.getCurrencyname()+":"+formatter.format(Double.parseDouble(receipt.getAmount())),paint);
		canvas.drawText("Trans charge  :  "+receipt.getCurrencyname()+":"+formatter.format(receipt.getTranscharge()),paint);
		setFontStyle(paint , 2 , true);
		canvas.drawText("===========================" , paint);
		canvas.drawText("===========================" , paint);
		canvas.drawBitmap(bitmap1,paint);
		setFontStyle(paint , 2 , true);
		canvas.drawText(" powered by www.compulynx.com ",paint);
		ret = printData(canvas);
		if (printer != null) {
			printer = null;
		}
		return  ret;
	}
	public int printAgentCashIntraTransferReceipt(Context mContext, IntraBankTransferReceipt receipt){
		this.printTask = new PrintTask();
		this.printTask.setGray(130);
		int ret = -1;
		printer = Printer.getInstance() ;
		if (printer == null) {
			return Tcode.PRINT_FAIL ;
		}
		PrintCanvas canvas = new PrintCanvas() ;
		Paint paint = new Paint() ;
		BitmapDrawable drawable = (BitmapDrawable) mContext.getResources().getDrawable(R.drawable.download23);
		Bitmap bitmap = drawable.getBitmap();
		BitmapDrawable drawable1 = (BitmapDrawable) mContext.getResources().getDrawable(R.drawable.ecompass);
		Bitmap bitmap1 = drawable1.getBitmap();
		setFontStyle(paint , 2 , true);
		canvas.drawText("**********Agent Copy******",paint);
		canvas.drawBitmap(bitmap,paint);
		canvas.drawText("==========================" , paint);
		setFontStyle(paint , 3 , true);
		canvas.drawText("       Intra Bank Transfer  " , paint);
		setFontStyle(paint , 2 , true);
		canvas.drawText("==========================" , paint);

		setFontStyle(paint , 2 , true);
		canvas.drawText("Agent Name :    "+receipt.getAgentname() , paint);
		canvas.drawText("Device   :    "+receipt.getDeviceid() , paint);
		canvas.drawText("Date and Time :  "+receipt.getDate(), paint);
		canvas.drawText("                     " , paint);
		canvas.drawText("===========================" , paint);
		setFontStyle(paint , 2 , true);
		canvas.drawText("Transaction Details",paint);
		setFontStyle(paint , 2 , true);
		canvas.drawText("===========================" , paint);

		canvas.drawText("From Account    :  "+receipt.getAccountFrom(),paint);
		canvas.drawText("To Account    :   "+receipt.getAccountTo(),paint);
		canvas.drawText("TRef No.      :   "+receipt.getTrfNo(),paint);
		canvas.drawText("===========================" , paint);
		setFontStyle(paint , 3 , true);
		canvas.drawText("Amount        :"+receipt.getCurrencyname()+":"+formatter.format(Double.parseDouble(receipt.getAmount())),paint);
		canvas.drawText("Trans charge  :  "+receipt.getCurrencyname()+":"+formatter.format(receipt.getTranscharge()),paint);
		setFontStyle(paint , 2 , true);
		canvas.drawText("===========================" , paint);
		canvas.drawText("===========================" , paint);
		canvas.drawBitmap(bitmap1,paint);
		setFontStyle(paint , 2 , true);
		canvas.drawText(" powered by www.compulynx.com ",paint);
		ret = printData(canvas);
		if (printer != null) {
			printer = null;
		}
		return  ret;
	}
	public int printTransferReceipt(Context mContext, TransferReceipt receipt,String  owner,String receiptName){
		this.printTask = new PrintTask();
		this.printTask.setGray(130);
		int ret = -1;
		printer = Printer.getInstance() ;
		if (printer == null) {
			return Tcode.PRINT_FAIL ;
		}
		PrintCanvas canvas = new PrintCanvas() ;
		Paint paint = new Paint() ;
		BitmapDrawable drawable = (BitmapDrawable) mContext.getResources().getDrawable(R.drawable.postbanklogo);
		Bitmap bitmap = drawable.getBitmap();
		setFontStyle(paint , 2 , false);
		canvas.drawText("   **********"+owner+ "**********",paint);
		setFontStyle(paint , 2 , false);
		canvas.setX(80);
		canvas.drawBitmap(bitmap,paint);
		canvas.setX(0);
		setFontStyle(paint , 2 , false);
		canvas.drawText("-----------------------------------------------" , paint);
		setFontStyle(paint , 2 , false);
		canvas.drawText("Receipt No :    "+receipt.getTrfNo(), paint);
		canvas.drawText("    " , paint);
		canvas.drawText("Agent Id :    "+receipt.getAgentId(), paint);
		canvas.drawText("    " , paint);
		canvas.drawText("Device     :    "+receipt.getDeviceid() , paint);
		canvas.drawText("    " , paint);
		canvas.drawText("Branch Name  :   "+receipt.getBranch_name(), paint);
		canvas.drawText("    " , paint);
		//canvas.drawText("Batch No      :    45338" , paint);
		canvas.drawText("Date/Time  :  "+receipt.getDate(), paint);
		canvas.drawText("-----------------------------------------------" , paint);
		setFontStyle(paint , 3 , false);
		canvas.drawText(""+receiptName+"  " , paint);
		//canvas.drawText(""+"Wallet Transfer"+"  " , paint);
		canvas.drawText("    " , paint);
		setFontStyle(paint , 2 , false);
		canvas.drawText("Transfered By   :  "+receipt.getCustName(),paint);
		canvas.drawText("    " , paint);
		canvas.drawText("Account From   :  "+maskAccount(receipt.getAccountfrom(),4),paint);
		canvas.drawText("    " , paint);
		canvas.drawText("Acount To  :  "+maskAccount(receipt.getAccountNo(),4),paint);
		canvas.drawText("    " , paint);
		if(true)
		 canvas.drawText("Phone Number  :  "+""+"256770124356",paint);
		canvas.drawText("    " , paint);
		canvas.drawText("Auth By   :  "+receipt.getAuthname(),paint);
		canvas.drawText("    " , paint);
		canvas.drawText("Trans Id    :   "+receipt.getTransactionId(),paint);
		canvas.drawText("    " , paint);
		canvas.drawText("Txn Amount    :  "+"UGX "+formatter.format(Double.parseDouble(""+receipt.getAmount())),paint);
		canvas.drawText("    " , paint);
		canvas.drawText("Txn Fee     :  "+"UGX "+formatter.format(Double.parseDouble(""+receipt.getTranscharge())),paint);
		canvas.drawText("   ",paint);
		canvas.drawText("Excise Duty :  "+"UGX "+formatter.format(Double.parseDouble(""+receipt.getExerciseDuty())),paint);
		canvas.drawText("   ",paint);
		double total=Double.parseDouble(""+receipt.getAmount())+Double.parseDouble(""+receipt.getTranscharge())+Double.parseDouble(""+receipt.getExerciseDuty());
		canvas.drawText("Total  Amount  :"+"UGX "+formatter.format(total),paint);
		canvas.drawText("   ",paint);
		canvas.drawText("Trans Id    :   "+receipt.getTransactionId(),paint);
		canvas.drawText("-----------------------------------------------" , paint);
		canvas.drawText("You were served by "+receipt.getAgentname() , paint);
		if(owner.contains("Agent")) {
			canvas.drawText("Cust sign :_ _ _ _ _ _ _ _ _ _ _ _ _", paint);
		}
		canvas.drawText("      Thanks for banking with  us" , paint);
		ret = printData(canvas);
		if (printer != null) {
			printer = null;
		}
		return  ret;
	}
	public int printInquriesReceipt(Context mContext, InquiryReceipt receipt,String  owner,String receiptName){
		this.printTask = new PrintTask();
		this.printTask.setGray(130);
		int ret = -1;
		printer = Printer.getInstance() ;
		if (printer == null) {
			return Tcode.PRINT_FAIL ;
		}
		PrintCanvas canvas = new PrintCanvas() ;
		Paint paint = new Paint() ;
		BitmapDrawable drawable = (BitmapDrawable) mContext.getResources().getDrawable(R.drawable.postbanklogo);
		Bitmap bitmap = drawable.getBitmap();
		setFontStyle(paint , 2 , false);
		canvas.drawText("   **********"+owner+ "**********",paint);
		setFontStyle(paint , 2 , false);
		canvas.setX(80);
		canvas.drawBitmap(bitmap,paint);
		canvas.setX(0);
		setFontStyle(paint , 2 , false);
		canvas.drawText("-----------------------------------------------" , paint);
		setFontStyle(paint , 2 , false);
		canvas.drawText("Receipt No :    "+receipt.getTrfNo() , paint);
		canvas.drawText("    " , paint);
		canvas.drawText("Agent Id :    "+receipt.getAgentId() , paint);
		canvas.drawText("    " , paint);
		canvas.drawText("Device     :    "+receipt.getDeviceid() , paint);
		canvas.drawText("    " , paint);
		canvas.drawText("Branch Name  :   "+receipt.getBranch_name() , paint);
		canvas.drawText("    " , paint);
		//canvas.drawText("Batch No      :    45338" , paint);
		canvas.drawText("Date/Time  :  "+receipt.getDate(), paint);
		canvas.drawText("-----------------------------------------------" , paint);
		if(receipt.balance) {
			setFontStyle(paint, 2, false);
			canvas.drawText(receiptName, paint);
			canvas.drawText(" ", paint);
			setFontStyle(paint, 2, false);
			canvas.drawText("Account No  :  " + maskAccount(receipt.getAccountNo(),4),paint);
			canvas.drawText("    ", paint);
			canvas.drawText("Actual Balance  :  " +receipt.getAccountbalance(),paint);
			canvas.drawText(" ", paint);
			canvas.drawText("Available Balance  :  " +""+receipt.getAccountbalance(),paint);
			canvas.drawText("  ", paint);

		}
		if(!receipt.balance) {
			setFontStyle(paint, 2, false);
			canvas.drawText(receiptName, paint);
			canvas.drawText("  ", paint);
			setFontStyle(paint, 2, false);
			canvas.drawText("Account No  :  " + maskAccount(receipt.getAccountNo(),4),paint);
			canvas.drawText("    ", paint);
			canvas.drawText("Transactions   (  Last 10 transactions)", paint);
			setFontStyle(paint , 1 , false);
            canvas.drawText("-----------------------------------------------" , paint);
			try {

				JSONArray transArray = new JSONArray(receipt.getTransactions());
				for (int i=0; i< transArray.length();i++) {

					/*canvas.drawText("   "+(i+1)+" . "+formatStatementRow(transArray.getString(i)), paint);*/
					canvas.drawText("   "+(i+1)+" . "+transArray.getString(i), paint);
					canvas.drawText("    ", paint);
				}
			}catch (Exception e){

			}
		}
		setFontStyle(paint , 2 , false);
		canvas.drawText("Auth By   :  "+""+receipt.getAuthname(),paint);
		canvas.drawText("    " , paint);
		canvas.drawText("Txn Fee     :  "+"UGX "+formatter.format(Double.parseDouble(""+receipt.getTranscharge())),paint);
		canvas.drawText("   ",paint);
		canvas.drawText("Excise Duty :  "+"UGX "+formatter.format(Double.parseDouble(""+receipt.getExerciseDuty())),paint);
		canvas.drawText("   ",paint);
		double total=Double.parseDouble(""+receipt.getTranscharge())+Double.parseDouble(""+receipt.getExerciseDuty());
		canvas.drawText("Total charge  :"+"UGX "+formatter.format(total),paint);
		canvas.drawText("   ",paint);
		canvas.drawText("Trans Id    :   "+receipt.getTransactionId(),paint);
		canvas.drawText("-----------------------------------------------" , paint);
		canvas.drawText("You were served by "+receipt.getAgentname() , paint);
		if(owner.contains("Agent")) {
			canvas.drawText("Cust sign :_ _ _ _ _ _ _ _ _ _ _ _ _", paint);
		}
		canvas.drawText("      Thanks for banking with  us" , paint);
		ret = printData(canvas);
		if (printer != null) {
			printer = null;
		}
		return  ret;
	}public int printRequestReceipt(Context mContext, RequestReceipt receipt,String  owner,String receiptName){
		this.printTask = new PrintTask();
		this.printTask.setGray(130);
		int ret = -1;
		printer = Printer.getInstance() ;
		if (printer == null) {
			return Tcode.PRINT_FAIL ;
		}
		PrintCanvas canvas = new PrintCanvas() ;
		Paint paint = new Paint() ;
		BitmapDrawable drawable = (BitmapDrawable) mContext.getResources().getDrawable(R.drawable.postbanklogo);
		Bitmap bitmap = drawable.getBitmap();
		setFontStyle(paint , 2 , false);
		canvas.drawText("   **********"+owner+ "**********",paint);
		setFontStyle(paint , 2 , false);
		canvas.setX(80);
		canvas.drawBitmap(bitmap,paint);
		canvas.setX(0);
		setFontStyle(paint , 2 , false);
		canvas.drawText("-----------------------------------------------" , paint);
		setFontStyle(paint , 2 , false);
		canvas.drawText("Receipt No :    "+receipt.getTrfNo() , paint);
		canvas.drawText("    " , paint);
		canvas.drawText("Agent Id :    "+receipt.getAgentId() , paint);
		canvas.drawText("    " , paint);
		canvas.drawText("Device     :    "+receipt.getDeviceid() , paint);
		canvas.drawText("    " , paint);
		canvas.drawText("Branch Name  :   "+receipt.getBranchName() , paint);
		canvas.drawText("    " , paint);
		//canvas.drawText("Batch No      :    45338" , paint);
		canvas.drawText("Date/Time  :  "+receipt.getDate(), paint);
		canvas.drawText("-----------------------------------------------" , paint);
		setFontStyle(paint, 2, true);
		canvas.drawText(receipt.getReceiptname(), paint);
		canvas.drawText(" ", paint);
		setFontStyle(paint, 2, false);
		canvas.drawText("Account Name  :  " + receipt.getAccountname(),paint);
		canvas.drawText("    ", paint);
		canvas.drawText("Account No  :  " + maskAccount(receipt.getAccountNo(),4),paint);
		canvas.drawText("    ", paint);
		if(!(receipt.getReceiptname().contains("Loan"))) {
			canvas.drawText("Date From  :  " + receipt.getDatefrom(), paint);
			canvas.drawText("    ", paint);
			canvas.drawText("Date To  :  " + receipt.getDateto(), paint);
			canvas.drawText("    ", paint);
			canvas.drawText("Request sent to  :  " + receipt.getCustemail(), paint);
			canvas.drawText("    ", paint);
		}else{
			canvas.drawText("Loan Type  :  " + receipt.getLoantype(), paint);
			canvas.drawText("    ", paint);
			canvas.drawText("Loan Amount   :  "+"UGX "+formatter.format(Double.parseDouble(""+receipt.getAmount())),paint);
			canvas.drawText("    ", paint);
			if(receipt.getLoansec().length()>1) {
				canvas.drawText("Loan Security   :  " + receipt.getAmount(), paint);
				canvas.drawText("    ", paint);
			}
		}

		setFontStyle(paint , 2 , false);
		canvas.drawText("Auth By   :  "+""+receipt.getAuthname(),paint);
		canvas.drawText("    " , paint);
		canvas.drawText("Txn Fee     :  "+"UGX "+formatter.format(Double.parseDouble(""+receipt.getTranscharge())),paint);
		canvas.drawText("   ",paint);
		canvas.drawText("Excise Duty :  "+"UGX "+formatter.format(Double.parseDouble(""+receipt.getExerciseDuty())),paint);
		canvas.drawText("   ",paint);
		double total=Double.parseDouble(""+receipt.getTranscharge())+Double.parseDouble(""+receipt.getExerciseDuty());
		canvas.drawText("Total charge  :"+"UGX "+formatter.format(total),paint);
		canvas.drawText("   ",paint);
		canvas.drawText("Trans Id    :   "+receipt.getTransactionId(),paint);
		canvas.drawText("-----------------------------------------------" , paint);
		canvas.drawText("You were served by "+receipt.getAgentname() , paint);
		if(owner.contains("Agent")) {
			canvas.drawText("Cust sign :_ _ _ _ _ _ _ _ _ _ _ _ _", paint);
		}
		canvas.drawText("      Thanks for banking with  us" , paint);
		ret = printData(canvas);
		if (printer != null) {
			printer = null;
		}
		return  ret;
	}

	public static String formatAccount(String amount){
		format("%,.2f",amount);
		return amount;
	}
	private String maskAcount(String account){
		String  end_str =account.substring(9,account.length());
		String  lead_str=account.substring(0,4);
		return account.replace(lead_str,"xxxx").replace(end_str,"xxxxx");
	}
	private String formatStatementRow(String row){
		String ret_str=null;
		int lastfsIndex=row.lastIndexOf("/");
		int lastspIndex=row.lastIndexOf(" ");
		String substr=row.substring(lastfsIndex,lastspIndex+1);
		if(substr.contains("C")||substr.contains("c")){
			return row.replace(substr,"  ");
		}if(substr.contains("D")||substr.contains("d")){
			return  row.replace(substr,"  -");
		}

		 return  ret_str;
	}
	public void addExtaUtilParams(PrintCanvas canvas ,Paint paint,String ... params){

		for(String  param: params){
			canvas.drawText(param,paint);
			canvas.drawText("    " , paint);
		}


	}
	private String maskAccount(String account,int last){
		String substr=account.substring(account.length()-5);
		String acc=account;
		String sub=acc.substring(acc.length()-last);
		String asteric="";
		for(int i=0; i< acc.length()-last;i++){
			asteric=asteric+"X";
		}
		  return  asteric+sub;

	}
	public int testPrinter(){

		this.printTask = new PrintTask();
		this.printTask.setGray(130);
		int ret = -1;
		printer = Printer.getInstance() ;
		if (printer == null) {
			Log.d("##printer","Did not find  a printer instance");
			return Tcode.PRINT_FAIL ;
		}else
			Log.d("##printer","Found   a  printer instance");
		PrintCanvas canvas = new PrintCanvas() ;
		Paint paint = new Paint() ;
		BitmapDrawable drawable = (BitmapDrawable) mContext.getResources().getDrawable(R.drawable.postbanklogo);
		Bitmap bitmap = drawable.getBitmap();
		setFontStyle(paint , 1, true);
		printText("Arabian cuisine","",canvas,paint);
		setFontStyle(paint  , 1 , true);
		canvas.setX(0);
		setFontStyle(paint , 1 , true);
		printLine(paint, canvas);
		setFontStyle(paint , 1 , true);
		canvas.setX(20);
		canvas.drawText("Receipt No:    "+"4487484839" , paint);
		canvas.setX(20);
		canvas.drawText("ETR No:    "+"46447382993" , paint);
		canvas.setX(20);
		printText("Date/Time ","7/11/2018",canvas,paint);
		canvas.setX(0);
		printLine(paint, canvas);
		setFontStyle(paint , 2, true);
		canvas.setX(20);
		printText("Items","  ",canvas ,paint);
		canvas.drawText("    " , paint);
		setFontStyle(paint , 1, true);
		canvas.setX(20);
		canvas.drawText("Masala chips - 250/=",paint);
		canvas.drawText("    " , paint);
		canvas.setX(20);
		canvas.drawText("Cock tail - 150/=",paint);
		canvas.drawText("    " , paint);
		canvas.setX(20);
		canvas.drawText("Total amount - 400/=",paint);
		canvas.setX(10);
		canvas.drawText("-----------------------------------------------" , paint);
		canvas.drawText("      Thanks for dinning  with  us" , paint);
		setFontStyle(paint , 1, true);
		canvas.drawText("    " , paint);
		setFontStyle(paint , 1, true);
		canvas.drawText("Arabian cuisine",paint);
		setFontStyle(paint  , 1 , true);
		canvas.setX(0);
		setFontStyle(paint , 1 , true);
		canvas.drawText("-----------------------------------------------" , paint);
		setFontStyle(paint , 1 , true);
		canvas.setX(20);
		canvas.drawText("Receipt No:    "+"4487484839" , paint);
		canvas.setX(20);
		canvas.drawText("ETR No:    "+"46447382993" , paint);
		canvas.setX(20);
		canvas.drawText("Date/Time  :  "+"5/11/2018", paint);
		canvas.setX(0);
		canvas.drawText("-----------------------------------------------" , paint);
		setFontStyle(paint , 2, true);
		canvas.setX(20);
		canvas.drawText(""+"Items"+"  " , paint);
		canvas.drawText("    " , paint);
		setFontStyle(paint , 1, true);
		canvas.setX(20);
		canvas.drawText("Mutton curry - 400/=",paint);
		canvas.drawText("    " , paint);
		canvas.setX(20);
		canvas.drawText("Passion juice - 150/=",paint);
		canvas.drawText("    " , paint);
		canvas.setX(20);
		canvas.drawText("Total amount - 550/=",paint);
		canvas.setX(10);
		canvas.drawText("-----------------------------------------------" , paint);
		canvas.drawText("      Thanks for dinning  with  us" , paint);
		setFontStyle(paint , 1, true);
		canvas.drawText("    " , paint);
		setFontStyle(paint , 1, true);
		canvas.drawText("Arabian cuisine",paint);
		setFontStyle(paint  , 1 , true);
		canvas.setX(0);
		setFontStyle(paint , 1 , true);
		canvas.drawText("-----------------------------------------------" , paint);
		setFontStyle(paint , 1 , true);
		canvas.setX(20);
		canvas.drawText("Receipt No:    "+"4487484839" , paint);
		canvas.setX(20);
		canvas.drawText("ETR No:    "+"46447382993" , paint);
		canvas.setX(20);
		canvas.drawText("Date/Time  :  "+"6/11/2018", paint);
		canvas.setX(0);
		canvas.drawText("-----------------------------------------------" , paint);
		setFontStyle(paint , 2, true);
		canvas.setX(20);
		canvas.drawText(""+"Items"+"  " , paint);
		canvas.drawText("    " , paint);
		setFontStyle(paint , 1, true);
		canvas.setX(20);
		canvas.drawText("Chicken curry - 400/=",paint);
		canvas.drawText("    " , paint);
		canvas.setX(20);
		canvas.drawText("Passion juice - 150/=",paint);
		canvas.drawText("    " , paint);
		canvas.setX(20);
		canvas.drawText("Total amount - 550/=",paint);
		canvas.setX(10);
		canvas.drawText("-----------------------------------------------" , paint);
		canvas.drawText("      Thanks for dinning  with  us" , paint);
		setFontStyle(paint , 1, true);
		canvas.drawText("    " , paint);
		ret = printData(canvas);
		if (printer != null) {
			printer = null;
		}
		return  ret;

	}
	public  int  printTBCReceipt(Context mContext,TBCTransactionReceipt receipt){
		this.printTask = new PrintTask();
		this.printTask.setGray(130);
		int ret = -1;
		printer = Printer.getInstance() ;
		if (printer == null) {
			return Tcode.PRINT_FAIL ;
		}
		PrintCanvas canvas = new PrintCanvas() ;
		Paint paint = new Paint() ;
		BitmapDrawable drawable = (BitmapDrawable) mContext.getResources().getDrawable(R.drawable.tbc);
		Bitmap bitmap = drawable.getBitmap();
		setFontStyle(paint , 2 , false);
		canvas.drawText("   **********"+receipt.getOwner()+ "**********",paint);
		setFontStyle(paint , 2 , false);
		canvas.setX(80);
		canvas.drawBitmap(bitmap,paint);
		canvas.setX(0);
		setFontStyle(paint , 2 , false);
		canvas.drawText("-----------------------------------------------" , paint);
		setFontStyle(paint , 2 , false);
		canvas.drawText("Receipt No :    "+receipt.getReceiptNo() , paint);
		canvas.drawText("    " , paint);
		canvas.drawText("Device MAC     :    "+receipt.getMacid() , paint);
		canvas.drawText("    " , paint);
		canvas.drawText("User Name :  "+receipt.getUsername(), paint);
		canvas.drawText("    " , paint);
		canvas.drawText("Date :  "+receipt.getDate(), paint);
		canvas.drawText("    " , paint);
		canvas.drawText("Time  :  "+receipt.getRationNo(),paint);
		canvas.drawText("Ration No    :  "+receipt.getRationNo(),paint);
		canvas.drawText("    " , paint);
		canvas.drawText("Voucher Id  :  "+" "+receipt.getVoucherid(),paint);
		canvas.drawText("    " , paint);
		canvas.drawText("-----------------------------------------------" , paint);
		canvas.drawText("Commodity Details"+"",paint);
		canvas.drawText("Name            Qty               Val"+"",paint);
		canvas.drawText("    " , paint);
		for(Quantitydetails detail: receipt.getDetails()){
			canvas.drawText(detail.getName()+"           "+detail.getQuantity()+"           "+detail.getValue() , paint);
		}
		canvas.drawText("-----------------------------------------------" , paint);
		canvas.drawText("Opening Balance   :  "+"TBH "+formatter.format(Double.parseDouble(""+receipt.getOpenbal())),paint);
		canvas.drawText("    " , paint);
		canvas.drawText("Txn   Value  :  "+"TBH "+formatter.format(Double.parseDouble(""+receipt.getTransval())),paint);
		canvas.drawText("   ",paint);
		canvas.drawText("Closing Balance  :  "+"TBH "+formatter.format(Double.parseDouble(""+receipt.getCloseval())),paint);
		canvas.drawText("   ",paint);
		canvas.drawText("-----------------------------------------------" , paint);
		if(receipt.getOwner().contains("Merchant")) {
			canvas.drawText("Cust sign :_ _ _ _ _ _ _ _ _ _ _ _ _", paint);
		}
		canvas.drawText("      Thanks for shopping with  us" , paint);
		ret = printData(canvas);
		if (printer != null) {
			printer = null;
		}
		return  ret;
	}
	public  int  printXreport(Context mContext,Xreport receipt){
		this.printTask = new PrintTask();
		this.printTask.setGray(130);
		int ret = -1;
		printer = Printer.getInstance() ;
		if (printer == null) {
			return Tcode.PRINT_FAIL ;
		}
		PrintCanvas canvas = new PrintCanvas() ;
		Paint paint = new Paint() ;
		BitmapDrawable drawable = (BitmapDrawable) mContext.getResources().getDrawable(R.drawable.tbc);
		Bitmap bitmap = drawable.getBitmap();
		setFontStyle(paint , 2 , false);
		//canvas.drawText("   **********"+receipt.getOwner()+ "**********",paint);
		setFontStyle(paint , 2 , false);
		canvas.setX(80);
		canvas.drawBitmap(bitmap,paint);
		canvas.setX(0);
		setFontStyle(paint , 2 , false);
		canvas.drawText("-----------------------------------------------" , paint);
		setFontStyle(paint , 2 , false);
		//canvas.drawText("Receipt No :    "+receipt.getReceiptNo() , paint);
		//canvas.drawText("    " , paint);
		canvas.drawText("Device MAC     :    "+receipt.getDevicemac() , paint);
		canvas.drawText("    " , paint);
		canvas.drawText("User Name :  "+receipt.getUsername(), paint);
		canvas.drawText("    " , paint);
		canvas.drawText("Date :  "+receipt.getDate(), paint);
		canvas.drawText("    " , paint);
		canvas.drawText("Time  :  "+receipt.getTime(),paint);
		canvas.drawText("    " , paint);
		canvas.drawText("-----------------------------------------------" , paint);
		canvas.drawText("Transaction Count   :  "+" "+receipt.getTxncount(),paint);
		canvas.drawText("    " , paint);
		canvas.drawText("Voided Count   :  "+" "+receipt.getVoidcount(),paint);
		canvas.drawText("    " , paint);
		canvas.drawText("Sales Amount   :  "+" "+receipt.getSales(),paint);
		canvas.drawText("    " , paint);
		canvas.drawText("Voided Amount   :  "+" "+receipt.getVoidamount(),paint);
		canvas.drawText("    " , paint);
		canvas.drawText("Net Sales   :  "+" "+receipt.getNetsales(),paint);
		canvas.drawText("    " , paint);
		canvas.drawText("-----------------------------------------------" , paint);
		ret = printData(canvas);
		if (printer != null) {
			printer = null;
		}
		return  ret;
	}
	private String appendSpaces(int len){
		String ret="";
		for(int x=0; x<len; x++){
			ret=ret+"-";
		}
		 return  ret;
	}
	private PrintCanvas printText(String part1,String part2,PrintCanvas canvas,Paint  paint){
		int spaces=20-part1.length();
		if(part2.length()>20){

			String overflow=part2.substring(19,part2.length());
			String text=part2.substring(0,19)+"-";
			canvas.drawText(part1+spaces+"  :  "+text,paint);
			printTextCentre(overflow,canvas,paint);

		}else{

			canvas.drawText(part1+spaces+"  :  "+part2,paint);

		}
		 return  canvas;

	}
	private PrintCanvas printAmount(String part1,String part2,PrintCanvas canvas,Paint paint,int len){
		part1=part1+appendSpaces(20-part1.length());
		part2=appendSpaces(len-part2.length())+part2;
		canvas.drawText(part1+"  :  "+"UGX "+part2,paint);
		return  canvas;
	}
	private PrintCanvas printTextCentre(String text,PrintCanvas canvas,Paint paint){
		canvas.drawText(appendSpaces(30)+text,paint);
		return canvas;
	}

}
