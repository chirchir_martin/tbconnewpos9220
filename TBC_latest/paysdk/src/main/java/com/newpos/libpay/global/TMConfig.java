package com.newpos.libpay.global;

import android.content.Context;

import com.newpos.libpay.PaySdk;
import com.newpos.libpay.PaySdkException;
import com.newpos.libpay.utils.ISOUtil;
import com.newpos.libpay.utils.PAYUtils;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.Serializable;
import java.util.Enumeration;
import java.util.Properties;

/**
 * Created by zhouqiang on 2017/4/29.
 * @author zhouqiang
 * terminal configure
 */

public class TMConfig implements Serializable {

	private static final long serialVersionUID = 1L;
	private static String ConfigPath = "config.dat";
	private static TMConfig mInstance = null ;

	/**
	 * Save path for all files in this program
	 */
	private static String ROOT_FILE_PATH ;
	public static String getRootFilePath() {
		return ROOT_FILE_PATH;
	}
	public static void setRootFilePath(String rootFilePath) {
		ROOT_FILE_PATH = rootFilePath;
	}

	/**
	 * Whether to open SDK debug information
	 */
	private boolean isDebug ;
	public boolean isDebug() {
		return isDebug;
	}

	public TMConfig setDebug(boolean debug) {
		isDebug = debug;
		return mInstance ;
	}

	/**
	 * Whether to open online function
	 */
	private boolean isOnline ;
	public boolean isOnline() {
		return isOnline;
	}

	public TMConfig setOnline(boolean online) {
		isOnline = online;
		return mInstance ;
	}

	/**
	 * Whether to open the voice play in the transaction process
	 */
	private boolean isVocie ;
	public boolean isVocie() {
		return isVocie;
	}

	public TMConfig setVocie(boolean vocie) {
		isVocie = vocie;
		return mInstance ;
	}

	/**
	 * Bank Logo list index
	 */
	private int bankid ;
	public int getBankid() {
		return bankid;
	}

	public TMConfig setBankid(int bankid) {
		if(bankid < TMConstants.BANKID.ASSETS.length && bankid >= 0){
			this.bankid = bankid ;
		}else {
			this.bankid = 0 ;
		}
		return mInstance ;
	}

	/**
	 * The payment specification that marks the current SDK environment support is different in related standards and transaction algorithms
	 * 1 ==== CUP
	 * 2 ==== CITIC Bank
	 */
	private int standard ;
	public int getStandard() {
		return standard;
	}

	public TMConfig setStandard(int standard) {
		if(standard == 1 || standard == 2){
			this.standard = standard ;
		}else {
			this.standard = 1;
		}
		return mInstance ;
	}

	/**
	 * ip
	 */
	private String ip ;
	public String getIp() {
		return ip;
	}

	public TMConfig setIp(String ip) {
		this.ip = ip;
		return mInstance ;
	}

	/**
	 * ip2
	 */
	private String ip2 ;
	public String getIP2(){
		return ip2 ;
	}

	public TMConfig setIp2(String s){
		this.ip2 = s ;
		return mInstance ;
	}

	/**
	 * whether force PBOC process
	 */
	private boolean forcePboc ;
	public boolean isForcePboc() {
		return forcePboc;
	}

	public TMConfig setForcePboc(boolean forcePboc) {
		this.forcePboc = forcePboc;
		return mInstance ;
	}

	/**
	 * port
	 */
	private String port ;
	public String getPort() {
		return port;
	}

	public TMConfig setPort(String port) {
		this.port = port;
		return mInstance ;
	}

	/**
	 * port2
	 */
	private String port2 ;
	public String getPort2(){
		return port2 ;
	}

	public TMConfig setPort2(String s){
		this.port2 = s ;
		return mInstance ;
	}

	/**
	 * online timeout
	 */
	private int timeout ;
	public int getTimeout() {
		return timeout;
	}

	public TMConfig setTimeout(int timeout) {
		this.timeout = timeout;
		return mInstance ;
	}

	/**
	 * public network
	 */
	private boolean isPubCommun ;
	public TMConfig setPubCommun(boolean is){
		this.isPubCommun = is ;
		return mInstance ;
	}

	public boolean getPubCommun(){
		return isPubCommun ;
	}

	/**
	 * User operation timeout(s)
	 */
	private int waitUserTime ;
	public int getWaitUserTime() {
		return waitUserTime;
	}

	public TMConfig setWaitUserTime(int waitUserTime) {
		this.waitUserTime = waitUserTime;
		return mInstance ;
	}

	/**
	 * Open the flash when the code is scavenged
	 */
	private boolean scanTorchOn ;
	public boolean isScanTorchOn() {
		return scanTorchOn;
	}

	public TMConfig setScanTorchOn(boolean scanTorchOn) {
		this.scanTorchOn = scanTorchOn;
		return mInstance ;
	}

	/**
	 * Open the beep when the code is scavenged
	 */
	private boolean scanBeeper ;
	public boolean isScanBeeper() {
		return scanBeeper;
	}

	public TMConfig setScanBeeper(boolean scanBeeper) {
		this.scanBeeper = scanBeeper;
		return mInstance ;
	}

	/**
	 * Rear scavenger
	 */
	private boolean scanBack ;
	public boolean isScanBack() {
		return scanBack;
	}

	public TMConfig setScanBack(boolean scanFront) {
		this.scanBack = scanFront;
		return mInstance ;
	}

	/**
	 * whether need password for void transaction
	 */
	private boolean revocationPassWSwitch ;
	public boolean getRevocationPassSwitch(){
		return revocationPassWSwitch ;
	}

	public TMConfig setRevocationPassWSwitch(boolean is){
		this.revocationPassWSwitch = is ;
		return mInstance ;
	}

	/**
	 * whether need card for void transaction
	 */
	private boolean revocationCardSwitch ;
	public TMConfig setRevocationCardSwitch(boolean is){
		this.revocationCardSwitch = is ;
		return mInstance ;
	}

	public boolean getRevocationCardSwitch(){
		return revocationCardSwitch ;
	}

	/**
	 * whether need password for void pre-auth transaction
	 */
	private boolean preauthVoidPassSwitch ;
	public boolean isPreauthVoidPassSwitch() {
		return preauthVoidPassSwitch;
	}

	public TMConfig setPreauthVoidPassSwitch(boolean preauthVoidPassSwitch) {
		this.preauthVoidPassSwitch = preauthVoidPassSwitch;
		return mInstance ;
	}

	/**
	 * whether need password for complete pre-auth transaction
	 */
	private boolean preauthCompletePassSwitch ;
	public boolean isPreauthCompletePassSwitch() {
		return preauthCompletePassSwitch;
	}

	public TMConfig setPreauthCompletePassSwitch(boolean preauthCompletePassSwitch) {
		this.preauthCompletePassSwitch = preauthCompletePassSwitch;
		return mInstance ;
	}

	/**
	 * whether need password for void complete pre-auth transaction
	 */
	private boolean preauthCompleteVoidCardSwitch ;
	public boolean isPreauthCompleteVoidCardSwitch() {
		return preauthCompleteVoidCardSwitch;
	}

	public TMConfig setPreauthCompleteVoidCardSwitch(boolean preauthCompleteVoidCardSwitch) {
		this.preauthCompleteVoidCardSwitch = preauthCompleteVoidCardSwitch;
		return mInstance ;
	}

	/**
	 * master passwords
	 */
	private String masterPass ;
	public String getMasterPass(){
		return masterPass ;
	}

	public TMConfig setMasterPass(String pass){
		this.masterPass = pass ;
		return mInstance ;
	}

	/**
	 * maintain password
	 */
	private String maintainPass ;
	public String getMaintainPass(){
		return maintainPass ;
	}

	public TMConfig setMaintainPass(String pass){
		this.maintainPass = pass ;
		return mInstance ;
	}

	/**
	 * master key index
	 */
	private int masterKeyIndex ;
	public int getMasterKeyIndex() {
		return masterKeyIndex;
	}

	public TMConfig setMasterKeyIndex(int masterKeyIndex) {
		this.masterKeyIndex = masterKeyIndex;
		return mInstance ;
	}

	/**
	 * whether check ICC when swipe card
	 */
	private boolean isCheckICC ;
	public boolean isCheckICC(){
		return isCheckICC ;
	}

	public TMConfig setCheckICC(boolean is){
		this.isCheckICC = is ;
		return mInstance ;
	}

	/**
	 * TPDU
	 */
	private String tpdu ;
	public String getTpdu() {
		return tpdu;
	}

	public TMConfig setTpdu(String tpdu) {
		this.tpdu = tpdu;
		return mInstance ;
	}

	/**
	 * header
	 */
	private String header ;
	public String getHeader() {
		return header;
	}

	public TMConfig setHeader(String header) {
		this.header = header;
		return mInstance ;
	}

	/**
	 * terminal ID
	 */
	private String TermID ;
	public String getTermID() {
		return TermID;
	}

	public TMConfig setTermID(String termID) {
		TermID = termID;
		return mInstance ;
	}

	/**
	 * merchant ID
	 */
	private String MerchID ;
	public String getMerchID() {
		return MerchID;
	}

	public TMConfig setMerchID(String merchID) {
		MerchID = merchID;
		return mInstance ;
	}

	/**
	 * batch NO.
	 */
	private int BatchNo ;
	public String getBatchNo() {
		return ISOUtil.padleft(BatchNo + "", 6, '0');
	}

	public TMConfig setBatchNo(int batchNo) {
		BatchNo = batchNo;
		return mInstance ;
	}

	/**
	 * voucher NO.
	 */
	private int TraceNo ;
	public String getTraceNo() {
		return ISOUtil.padleft(TraceNo + "", 6, '0');
	}

	public TMConfig setTraceNo(int traceNo) {
		TraceNo = traceNo;
		return mInstance ;
	}

	/**
	 * operator NO.
	 */
	private int oprNo ;
	public int getOprNo() {
		return oprNo;
	}

	public TMConfig setOprNo(int oprNo) {
		this.oprNo = oprNo;
		return mInstance ;
	}

	/**
	 * print receipt numbers.
	 * the value is 1-3.
	 * 1 -> merchant receipt
	 * 2 -> cardholder receipt
	 * 3 -> bank receipt
	 */
	private int PrinterTickNumber ;
	public int getPrinterTickNumber() {
		return PrinterTickNumber;
	}

	public TMConfig setPrinterTickNumber(int n) {
		this.PrinterTickNumber = n ;
		return mInstance ;
	}

	/**
	 * merchant name
	 */
	private String MerchName ;
	public String getMerchName() {
		return MerchName;
	}

	public TMConfig setMerchName(String merchName) {
		MerchName = merchName;
		return mInstance ;
	}

	/**
	 * whether print english on receipt
	 * 1 -> Chinese
	 * 2 -> English
	 * 3 -> Chinese and English
	 */
	private int printEn ;
	public int getPrintEn() {
		return printEn;
	}

	public TMConfig setPrintEn(int lang) {
		this.printEn = lang;
		return mInstance ;
	}

	/**
	 * whether encrypt track data
	 */
	private boolean isTrackEncrypt ;
	public boolean isTrackEncrypt() {
		return isTrackEncrypt;
	}

	public TMConfig setTrackEncrypt(boolean is){
		this.isTrackEncrypt = is ;
		return mInstance ;
	}

	/**
	 * whether is single long key
	 */
	private boolean isSingleKey ;
	public boolean isSingleKey() {
		return isSingleKey;
	}

	public TMConfig setSingleKey(boolean is){
		this.isSingleKey = is ;
		return mInstance ;
	}

	/**
	 * currency code
	 */
	private String CurrencyCode ;
	public String getCurrencyCode() {
		return CurrencyCode;
	}

	public TMConfig setCurrencyCode(String cur){
		this.CurrencyCode = cur ;
		return mInstance ;
	}

	/**
	 * firm code
	 */
	private String firmCode ;
	public String getFirmCode() {
		return firmCode;
	}

	public TMConfig setFirmCode(String firmCode) {
		this.firmCode = firmCode;
		return mInstance ;
	}

	/**
	 * reversal resend times
	 */
	private int reversalCount ;
	public int getReversalCount() {
		return reversalCount;
	}

	public TMConfig setReversalCount(int reversalCount) {
		this.reversalCount = reversalCount;
		return mInstance ;
	}

	private TMConfig() {
		try {
			loadFile(PaySdk.getInstance().getContext() ,
					PaySdk.getInstance().getParaFilepath());
		}catch (PaySdkException pse){
			System.err.println("TMConfig->"+pse.toString());
		}
	}

	public static TMConfig getInstance() {
		String fullPath = getRootFilePath() + ConfigPath;
		if (mInstance == null) {
			try {
				mInstance = (TMConfig) PAYUtils.file2Object(fullPath);
			} catch (FileNotFoundException e) {
				System.err.println("getInstance->"+e.toString());
			} catch (IOException e) {
				System.err.println("getInstance->"+e.toString());
			} catch (ClassNotFoundException e) {
				System.err.println("getInstance->"+e.toString());
			}
			if (mInstance == null) {
				mInstance = new TMConfig();
			}
		}
		return mInstance ;
	}

	private void loadFile(Context context , String path) {
		System.out.println("loadFile->path:"+path);
		String T = "1" ;
		Properties properties = PAYUtils.lodeConfig(context , TMConstants.DEFAULTCONFIG);
		if(properties!=null){
			Enumeration<?> enumeration = properties.propertyNames() ;
			while (enumeration.hasMoreElements()){
				String name = (String) enumeration.nextElement() ;
				System.out.println("loadFile->name:"+name);
				if(!PAYUtils.isNullWithTrim(name)){
					int index = Integer.parseInt(name.substring(name.length()-2 , name.length()));
					String prop = properties.getProperty(name);
					try {
						switch (index-1){
							case 0 :setIp(prop);break;
							case 1 :setPort(prop);break;
							case 2 :setIp2(prop);break;
							case 3 :setPort2(prop);break;
							case 4 :setTimeout(Integer.parseInt(prop) * 1000);break;
							case 5 :setPubCommun(prop.equals(T)?true:false);break;
							case 6 :setWaitUserTime(Integer.parseInt(prop));break;
							case 7 :setRevocationPassWSwitch(prop.equals(T)?true:false);break;
							case 8 :setRevocationCardSwitch(prop.equals(T)?true:false);break;
							case 9 :setPreauthVoidPassSwitch(prop.equals(T)?true:false);break;
							case 10 :setPreauthCompletePassSwitch(prop.equals(T)?true:false);break;
							case 11 :setPreauthCompleteVoidCardSwitch(prop.equals(T)?true:false);break;
							case 12 :setMasterPass(prop);break;
							case 13 :setMasterKeyIndex(Integer.parseInt(prop));break;
							case 14 :setCheckICC(prop.equals(T)?true:false);break;
							case 15 :setTpdu(prop);break;
							case 16 :setHeader(prop);break;
							case 17 :setTermID(prop);break;
							case 18 :setMerchID(prop);break;
							case 19 :setBatchNo(Integer.parseInt(prop));break;
							case 20 :setTraceNo(Integer.parseInt(prop));break;
							case 21 :setOprNo(Integer.parseInt(prop));break;
							case 22 :setPrinterTickNumber(Integer.parseInt(prop));break;
							case 23 :setMerchName(prop);break;
							case 24 :setPrintEn(Integer.parseInt(prop));break;
							case 25 :setTrackEncrypt(prop.equals(T)?true:false);break;
							case 26 :setSingleKey(prop.equals(T)?true:false);break;
							case 27 :setCurrencyCode(prop);break;
							case 28 :setFirmCode(prop);break;
							case 29 :setReversalCount(Integer.parseInt(prop));break;
							case 30 :setMaintainPass(prop);break;
							case 31 :setScanTorchOn(prop.equals(T)?true:false);break;
							case 32 :setScanBeeper(prop.equals(T)?true:false);break;
							case 33 :setScanBack(prop.equals(T)?true:false);break;
							case 34 :setDebug(prop.equals(T)?true:false);break;
							case 35 :setOnline(prop.equals(T)?true:false);break;
							case 36 :setBankid(Integer.parseInt(prop));break;
							case 37 :setStandard(Integer.parseInt(prop));break;
							case 38 :setForcePboc(prop.equals(T)?true:false);break;
							case 39 :setVocie(prop.endsWith(T)?true:false);break;
						}
					}catch (Exception e){
						System.err.println("loadFile->"+e.toString());
					}
				}
			}
		}
		save();
	}

	/**
	 * increase voucher NO.
	 * @return
     */
	public TMConfig incTraceNo() {
		if (this.TraceNo == 999999) {
			this.TraceNo = 0;
		}
		this.TraceNo += 1;
		this.save();
		return mInstance ;
	}

	/**
	 * save terminal config
	 */
	public void save(){
		String FullName = getRootFilePath() + ConfigPath;
		try {
			File file = new File(FullName);
			if (file.exists()) {
				file.delete();
			}
			PAYUtils.object2File(mInstance, FullName);
		} catch (FileNotFoundException e) {
			System.err.println("save->"+e.toString());
		} catch (IOException e) {
			System.err.println("save->"+e.toString());
		}
	}
}