package com.newpos.libpay.device.pinpad;

/**
 * Created by zhouqiang on 2017/12/12.
 * @author zhouqiang
 * enter PIN result
 */
public enum PinResult {
    /**
     * enter PIN succeed
     */
    SUCCESS(0),

    /**
     * enter PIN failed,such as timeout,cancel or others.
     */
    FAIL(1),

    /**
     * Do not do anything,realize it by Presenter
     */
    NO_OPERATION(2);

    private int val ;

    public int getVal(){
        return val ;
    }

    private PinResult(int val){
        this.val = val ;
    }
}
