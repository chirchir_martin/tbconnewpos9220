package com.newpos.libpay.device.printer;

import java.util.List;

/**
 * Created by Chirchir on 4/12/2019.
 */

public class TBCTransactionReceipt extends Receipt {
    String owner;
    String  username;
    String  macid;
    String  voucherid;
    String  time;
    String  date;
    String receiptNo;
    String rationNo;
    String openbal;
    String transval;
    String closeval;
    List<Quantitydetails> details;

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getMacid() {
        return macid;
    }

    public void setMacid(String macid) {
        this.macid = macid;
    }

    public String getVoucherid() {
        return voucherid;
    }

    public void setVoucherid(String voucherid) {
        this.voucherid = voucherid;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getReceiptNo() {
        return receiptNo;
    }

    public void setReceiptNo(String receiptNo) {
        this.receiptNo = receiptNo;
    }

    public String getRationNo() {
        return rationNo;
    }

    public void setRationNo(String rationNo) {
        this.rationNo = rationNo;
    }

    public String getOpenbal() {
        return openbal;
    }

    public void setOpenbal(String openbal) {
        this.openbal = openbal;
    }

    public String getTransval() {
        return transval;
    }

    public void setTransval(String transval) {
        this.transval = transval;
    }

    public String getCloseval() {
        return closeval;
    }

    public void setCloseval(String closeval) {
        this.closeval = closeval;
    }

    public List<Quantitydetails> getDetails() {
        return details;
    }

    public void setDetails(List<Quantitydetails> details) {
        this.details = details;
    }

}
