package com.newpos.libpay.trans;

/**
 * Created by zhouqiang on 2017/3/26.
 * @author zhouqiang
 * Transaction return code
 */

public class Tcode {
    public static final int START = 300 ;

    /**
     * socket failed,please check whether the network is available
     */
    public static final int SOCKET_FAIL = START + 1;

    /**
     * send data failed,please check send data or network
     */
    public static final int SEND_DATA_FAIL = START + 2;

    /**
     * receive data failed,please check whether the network is available
     */
    public static final int RECEIVE_DATA_FAIL = START + 3;

    /**
     * user cancel
     */
    public static final int USER_CANCEL = START + 4;

    /**
     * search card failed
     */
    public static final int SEARCH_CARD_FAIL = START + 5;

    /**
     * scan QR code failed
     */
    public static final int SCAN_CODE_FAIL = START + 6;

    /**
     * print receipt failed
     */
    public static final int PRINT_FAIL = START + 7;

    /**
     * can't find transactions
     */
    public static final int CANNOT_FIND_TRANS = START + 8;

    /**
     * check MAC failed
     */
    public static final int RECEIVE_MAC_ERROR = START + 9;

    /**
     * illegal package,please retry
     */
    public static final int ILLEGAL_PACKAGE = START + 10;

    /**
     * read EC amount failed
     */
    public static final int READ_ECAMOUNT_FAIL = START + 11;

    /**
     * no transactions need settlement
     */
    public static final int BATCH_NO_TRANS = START + 12;

    /**
     * IC card is not allowed fallback
     */
    public static final int IC_NOT_FALLBACK = START + 13;

    /**
     * Wrong password for the supervisor
     */
    public static final int MASSTER_PASS_ERROR = START + 14;

    /**
     * Failure of settlement
     */
    public static final int SETTLE_UPSEND_FAIL = START + 15;

    /**
     * reversal failed
     */
    public static final int REVERSAL_FAIL = START + 16;

    /**
     * refund amount exceed the limit
     */
    public static final int REFUND_AMOUNT_BEYOND = START + 17;

    /**
     * downloading CAPK
     */
    public static final int EMV_CAPK_DOWNLOAING = START + 18;

    /**
     * downloading AID
     */
    public static final int EMV_AID_DOWNLOADING = START + 19;

    /**
     * download succeed
     */
    public static final int EMV_DOWNLOADING_SUCC = START + 20;

    /**
     * Terminal is signing in
     */
    public static final int TERMINAL_LOGON = START + 21;

    /**
     * The terminal is sign out
     */
    public static final int TERMINAL_LOGOUT = START + 22;

    /**
     * connecting
     */
    public static final int CONNECTING_CENTER = START + 23;

    /**
     * printing receipt
     */
    public static final int PRINTING_RECEPT = START + 24;

    /**
     * printing settlement detail receipt
     */
    public static final int PRINTING_DETAILS = START + 25;

    /**
     * sending reversal data
     */
    public static final int SEND_REVERSAL = START + 26;

    /**
     * The printer is short of paper, please pack the paper
     */
    public static final int PRINTER_LACK_PAPER = START + 27;

    /**
     * sale succeed
     */
    public static final int SALE_SUCCESS = START + 28;

    /**
     * enquiry balance succeed
     */
    public static final int ENQUIRY_SUCCESS = START + 29;

    /**
     * void succeed
     */
    public static final int VOID_SUCCESS = START + 30;

    /**
     * enquiry EC balance succeed
     */
    public static final int EC_ENQUIRY_SUCCESS = START + 31;

    /**
     * Quick pass succeed
     */
    public static final int QUICKPASS_SUCCESS = START + 32;

    /**
     * sign in succeed
     */
    public static final int LOGON_SUCCESS = START + 33;

    /**
     * sign out succeed
     */
    public static final int LOGOUT_SUCCESS = START + 34;

    /**
     * transaction is processing
     */
    public static final int PROCESSING = START + 35;

    /**
     * send settlement notice
     */
    public static final int SEND_SETTLE_NOTICE = START + 36;

    /**
     * send settlement transaction details
     */
    public static final int SEND_SETTLE_TRANS_DETAILS = START + 37;

    /**
     * send settlement finished notice
     */
    public static final int SEND_SETTLE_FINISH_NOTICE = START + 38;

    /**
     * settlement succeed
     */
    public static final int SETTLE_SUCCESS = START + 39;

    /**
     * sending script
     */
    public static final int SEND_SETTLE_SCRIPT = START + 40;

    /**
     * terminal sign in and download parameters succeed
     */
    public static final int LOGON_DOWN_SUCCESS = START + 41;

    /**
     * receiving data from network center
     */
    public static final int RECEIVE_CENtER_DATA = START + 42;

    /**
     * pre-auth succeed
     */
    public static final int PREAUTH_SUCCESS = START + 43;

    /**
     * complete pre-auth succeed
     */
    public static final int PREAUTH_COMPLETE_SUCCESS = START + 44;

    /**
     * void complete pre-auth succeed
     */
    public static final int COMPLETE_VOID_SUCCESS = START + 45;

    /**
     * void pre-auth succeed
     */
    public static final int PREAUTH_VOID_SUCCESS = START + 46;

    /**
     * refund succeed
     */
    public static final int REFUND_SUCCESS = START + 47;

    /**
     * scan pay succeed
     */
    public static final int SCAN_PAY_SUCCESS = START + 48;

    /**
     * sending data to network center
     */
    public static final int SEND_DATA_CENTER = START + 49;

    /**
     * void scan pay succeed
     */
    public static final int SCAN_VOID_SUCCESS = START + 50;

    /**
     * refund scan pay succeed
     */
    public static final int SCAN_REFUND_SUCCESS = START + 51;

    /**
     * unknown error
     */
    public static final int UNKNOWN_ERROR = 999 ;
}
