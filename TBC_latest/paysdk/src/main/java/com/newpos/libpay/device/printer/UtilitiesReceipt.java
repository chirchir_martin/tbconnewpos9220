package com.newpos.libpay.device.printer;

/**
 * Created by Chirchir on 8/2/2018.
 */

public class UtilitiesReceipt extends Receipt {
    String accountNo;
    String deviceid;
    String agentId;
    String operationType;
    String transactionId;
    Double transcharge;
    String amount;
    String currencyId;
    String date;
    String custName;
    String narration;
    String trfNo;
    String agentname;
    String currencyname;
    String authname;
    String metreNo;
    String schoolaccount;
    String schoolname;

    public boolean isairtime() {
        return isairtime;
    }

    public void setIsairtime(boolean isairtime) {
        this.isairtime = isairtime;
    }

    boolean isairtime;
    public boolean isSchool() {
        return isSchool;
    }

    public void setSchool(boolean school) {
        isSchool = school;
    }

    boolean isSchool;
    public String getSchoolaccount() {
        return schoolaccount;
    }

    public void setSchoolaccount(String schoolaccount) {
        this.schoolaccount = schoolaccount;
    }

    public String getSchoolname() {
        return schoolname;
    }

    public void setSchoolname(String schoolname) {
        this.schoolname = schoolname;
    }

    public String getStudentname() {
        return studentname;
    }

    public void setStudentname(String studentname) {
        this.studentname = studentname;
    }

    public String getStudentnumber() {
        return studentnumber;
    }

    public void setStudentnumber(String studentnumber) {
        this.studentnumber = studentnumber;
    }

    String studentname;
    String studentnumber;
    public String getCustRef() {
        return custRef;
    }

    public void setCustRef(String custRef) {
        this.custRef = custRef;
    }

    String custRef;
    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    String area;

    public String getMetreNo() {
        return metreNo;
    }

    public void setMetreNo(String metreNo) {
        this.metreNo = metreNo;
    }

    public String getEnergyToken() {
        return energyToken;
    }

    public void setEnergyToken(String energyToken) {
        this.energyToken = energyToken;
    }

    public String getTokenValue() {
        return tokenValue;
    }

    public void setTokenValue(String tokenValue) {
        this.tokenValue = tokenValue;
    }

    String energyToken;
    String tokenValue;
    public String getBillno() {
        return billno;
    }

    public void setBillno(String billno) {
        this.billno = billno;
    }

    String billno;

    public String getBillname() {
        return billname;
    }

    public void setBillname(String billname) {
        this.billname = billname;
    }

    String billname;
    String branch_name;
    public String getAuthname() {
        return authname;
    }
    public void setAuthname(String authname) {
        this.authname = authname;
    }
    public String getAccountNo() {
        return accountNo;
    }

    public void setAccountNo(String accountNo) {
        this.accountNo = accountNo;
    }

    public String getDeviceid() {
        return deviceid;
    }

    public void setDeviceid(String deviceid) {
        this.deviceid = deviceid;
    }

    public String getAgentId() {
        return agentId;
    }

    public void setAgentId(String agentId) {
        this.agentId = agentId;
    }

    public String getOperationType() {
        return operationType;
    }

    public void setOperationType(String operationType) {
        this.operationType = operationType;
    }

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public Double getTranscharge() {
        return transcharge;
    }

    public void setTranscharge(Double transcharge) {
        this.transcharge = transcharge;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getCurrencyId() {
        return currencyId;
    }

    public void setCurrencyId(String currencyId) {
        this.currencyId = currencyId;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getCustName() {
        return custName;
    }

    public void setCustName(String custName) {
        this.custName = custName;
    }

    public String getNarration() {
        return narration;
    }

    public void setNarration(String narration) {
        this.narration = narration;
    }

    public String getTrfNo() {
        return trfNo;
    }

    public void setTrfNo(String trfNo) {
        this.trfNo = trfNo;
    }

    public String getAgentname() {
        return agentname;
    }

    public void setAgentname(String agentname) {
        this.agentname = agentname;
    }

    public String getCurrencyname() {
        return currencyname;
    }

    public void setCurrencyname(String currencyname) {
        this.currencyname = currencyname;
    }

    public String getBranch_name() {
        return branch_name;
    }

    public void setBranch_name(String branch_name) {
        this.branch_name = branch_name;
    }


}
