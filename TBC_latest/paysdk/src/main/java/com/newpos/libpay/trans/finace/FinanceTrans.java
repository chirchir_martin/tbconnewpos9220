package com.newpos.libpay.trans.finace;

import android.content.Context;

import com.android.newpos.libemv.EMVISRCode;
import com.android.newpos.libemv.PBOCCardInfo;
import com.android.newpos.libemv.PBOCException;
import com.android.newpos.libemv.PBOCListener;
import com.android.newpos.libemv.PBOCOnlineResult;
import com.android.newpos.libemv.PBOCPin;
import com.android.newpos.libemv.PBOCPinRet;
import com.android.newpos.libemv.PBOCPinType;
import com.android.newpos.libemv.PBOCTransProperty;
import com.android.newpos.libemv.PBOCUtil;
import com.android.newpos.libemv.PBOCode;
import com.newpos.libpay.Logger;
import com.newpos.libpay.device.pinpad.PinInfo;
import com.newpos.libpay.device.pinpad.PinResult;
import com.newpos.libpay.device.pinpad.PinType;
import com.newpos.libpay.device.pinpad.PinpadManager;
import com.newpos.libpay.device.printer.PrintManager;
import com.newpos.libpay.presenter.TransInterface;
import com.newpos.libpay.trans.Tcode;
import com.newpos.libpay.trans.Trans;
import com.newpos.libpay.trans.Type;
import com.newpos.libpay.trans.manager.reversal.RevesalTrans;
import com.newpos.libpay.trans.manager.script.ScriptTrans;
import com.newpos.libpay.trans.translog.TransLog;
import com.newpos.libpay.trans.translog.TransLogData;
import com.newpos.libpay.utils.ISOUtil;
import com.newpos.libpay.utils.PAYUtils;
import com.pos.device.printer.Printer;

/**
 * Created by zhouqiang on 2017/12/11.
 * @author zhouqiang
 * Finance Trans
 */

public class FinanceTrans extends Trans {

	/**
	 * service entry mode
	 * @ {@link ServiceEntryMode}
	 */

	protected ServiceEntryMode inputMode ;

	/**
	 * transaction whether need save
	 */
	protected boolean isSaveLog;

	/**
	 * transaction whether need PIN or whether exist PIN
	 */
	protected boolean isPinExist;

	/**
	 * transaction whether need reversal
	 */
	protected boolean isReversal;

	/**
	 * whether need preprocessing,such as reversal,send up script and so on
	 */
	protected boolean isProcPreTrans;

	/**
	 * whether need post-processing after finished transaction
	 */
	protected boolean isProcSuffix;

	/**
	 * whether allow fallback
	 */
	protected boolean isFallBack;

	/**
	 * whether need GAC2
	 */
	protected boolean isNeedGAC2;

	/**
	 * whether debit card transaction
	 */
	protected boolean isDebit;

	/**
	 * whether need print receipt
	 */
	protected boolean isNeedPrint;

	/**
	 * transaction log
	 */
	protected TransLog transLog;

	/**
	 * transaction log data
	 */
	protected TransLogData data;

	/**
	 * @param ctx context @{@link Context}
	 * @param transEname transaction name @{@link com.newpos.libpay.trans.Type}
     */
	public FinanceTrans(Context ctx, String transEname , TransInterface tt) {

		super(ctx, transEname , tt);
		transLog = TransLog.getInstance();
		iso8583.setHasMac(true);
		setTraceNoInc(true);

	}

	/**
	 * process some special data before online
     */
	protected void setSpecialDatas() {
		EntryMode = ISOUtil.padleft(String.valueOf(inputMode.getVal()), 2, '0');
		if (isPinExist) {

			CaptureCode = "12";
			EntryMode += "10";

		}else {

			EntryMode += "20";

		}
		if (isPinExist || Track2 != null || Track3 != null) {
			if(isPinExist) {
				SecurityInfo = "2";
			} else {
				SecurityInfo = "0";
			}
			if (cfg.isSingleKey()) {

				SecurityInfo += "0";

			} else {

				SecurityInfo += "6";

			}
			if (cfg.isTrackEncrypt()) {
				SecurityInfo += "10000000000000";
			} else {
				SecurityInfo += "00000000000000";
			}
		}
		appendField60("048");
	}

	/**
	 * set some IC card data
	 */
	protected void setICCData(){
		Logger.debug("==FinanceTrans->setICCData==");
		PBOCCardInfo info = PBOCUtil.getPBOCCardInfo() ;
		Pan = info.getCardNO();
		ExpDate = info.getExpDate();
		Track2 = info.getCardTrack2();
		Track1 = info.getCardTrack1();
		Track3 = info.getCardTrack3();
		PanSeqNo = info.getCardSeqNo();
		ICCData = PBOCUtil.getF55Data(PBOCUtil.wOnlineTags);
	}

	/**
	 * set transaction data in iso8583
	 */
	protected void setFields() {
        Logger.debug("==FinanceTrans->setFields==");

		if (MsgID != null) {
			iso8583.setField(0, MsgID);
		}
		if (Pan != null) {
			iso8583.setField(2, Pan);
		}
		if (ProcCode != null) {
			iso8583.setField(3, ProcCode);
		}
		if (Amount > 0) {
			String AmoutData = ISOUtil.padleft(Amount + "", 12, '0');
			iso8583.setField(4, AmoutData);
		}
		if (TraceNo != null) {
			iso8583.setField(11, TraceNo);
		}
		if (LocalTime != null) {
			iso8583.setField(12, LocalTime);
		}
		if (LocalDate != null) {
			iso8583.setField(13, LocalDate);
		}
		if (ExpDate != null) {
			iso8583.setField(14, ExpDate);
		}
		if (SettleDate != null) {
			iso8583.setField(15, SettleDate);
		}
		if (EntryMode != null) {
			iso8583.setField(22, EntryMode);
		}
		if (PanSeqNo != null) {
			iso8583.setField(23, PanSeqNo);
		}
		if (SvrCode != null) {
			iso8583.setField(25, SvrCode);
		}
		if (CaptureCode != null) {
			iso8583.setField(26, CaptureCode);
		}
		if (AcquirerID != null) {
			iso8583.setField(32, AcquirerID);
		}
		Logger.debug("Track2:"+Track2);
		Track2 = "6212163510102451610=4912120503" ;
		if (Track2 != null && cfg.isTrackEncrypt()) {
            Track2 = PinpadManager.getInstance().getEac(
					cfg.getMasterKeyIndex() , Track2);
			Logger.debug("Track2:"+Track2);
		}
		iso8583.setField(35, Track2);
		if (Track3 != null && cfg.isTrackEncrypt()) {
            Track3 = PinpadManager.getInstance().getEac(
					cfg.getMasterKeyIndex() , Track3);
		}
		iso8583.setField(36, Track3);
		if (RRN != null) {
			iso8583.setField(37, RRN);
		}
		if (AuthCode != null) {
			iso8583.setField(38, AuthCode);
		}
		if (RspCode != null) {
			iso8583.setField(39, RspCode);
		}
		if (TermID != null) {
			iso8583.setField(41, TermID);
		}
		if (MerchID != null) {
			iso8583.setField(42, MerchID);
		}
		if (Field44 != null) {
			iso8583.setField(44, Field44);
		}
		if (Field48 != null) {
			iso8583.setField(48, Field48);
		}
		if (CurrencyCode != null) {
			iso8583.setField(49, CurrencyCode);
		}
		if (PIN != null) {
			iso8583.setField(52, PIN);
		}
		if (SecurityInfo != null) {
			iso8583.setField(53, SecurityInfo);
		}
		if (ExtAmount != null) {
			iso8583.setField(54, ExtAmount);
		}
		if (ICCData != null) {
			iso8583.setField(55, ISOUtil.byte2hex(ICCData));
		}
		if (Field60 != null) {
			iso8583.setField(60, Field60);
		}
		if (Field61 != null) {
			iso8583.setField(61, Field61);
		}
		if (Field62 != null) {
			iso8583.setField(62, Field62);
		}
		if (Field63 != null) {
			iso8583.setField(63, Field63);
		}
	}

	/**
	 * start PBOC transaction
	 * @param property transaction property
	 * @return
     */
	protected int startPBOC(PBOCTransProperty property){
		transInterface.handling(Tcode.PROCESSING);
		int pboccode ;
		try {
			pboccode = pbocManager.startPBOC(property , listener);
		} catch (PBOCException e) {
			pboccode = PBOCode.PBOC_UNKNOWN_ERROR ;
		}
		return pboccode ;
	}

	/**
	 * handle enter PIN of contactless card
	 * @return
     */
	protected int handleNFCPin(){
		if(!pbocManager.isCardSupportedOnlinePIN()){
			return 0 ;
		}
		PinType type = new PinType();
		type.setOnline(true);
		type.setCardNO(PBOCUtil.getPBOCCardInfo().getCardNO());
		type.setAmount(String.valueOf(Amount));
		PinInfo info = transInterface.getPinpadPin(type);
		PinResult result = info.getResult() ;
		if(result != PinResult.SUCCESS){
			return info.getErrno() ;
		}
		byte[] pin = info.getPinblock() ;
		if(pin == null){
			isPinExist = false ;
		}else {
			isPinExist = true ;
			PIN = ISOUtil.hexString(pin);
		}
		return 0 ;
	}

	/**
	 * handle mag-stripe card data
	 * @return
     */
	protected int handleMAGData(String[] tracks){
		String data1 = null;
		String data2 = null;
		String data3 = null;
		int msgLen = 0;
		if (tracks[0].length() > 0 && tracks[0].length() <= 80) {
			data1 = new String(tracks[0]);
		}
		if (tracks[1].length() >= 13 && tracks[1].length() <= 37) {
			data2 = new String(tracks[1]);
			if(!data2.contains("=")){
				return Tcode.SEARCH_CARD_FAIL ;
			}
			String judge = data2.substring(0, data2.indexOf('='));
			if(judge.length() < 13 || judge.length() > 19){
				return Tcode.SEND_DATA_FAIL ;
			}
			if (data2.indexOf('=') != -1) {
				msgLen++;
			}
		}
		if (tracks[2].length() >= 15 && tracks[2].length() <= 107) {
			data3 = new String(tracks[2]);
		}
		if (msgLen == 0) {
			return Tcode.SEARCH_CARD_FAIL ;
		}
		if (!isFallBack) {
			int splitIndex = data2.indexOf("=");
			if (data2.length() - splitIndex < 5) {
				return Tcode.SEARCH_CARD_FAIL ;
			}
			char iccChar = data2.charAt(splitIndex + 5);
			if (iccChar == '2' || iccChar == '6') {
				return Tcode.IC_NOT_FALLBACK ;
			}
		}
		Pan = data2.substring(0, data2.indexOf('='));
		Track2 = data2;
		Track3 = data3;
		return 0 ;
	}

	/**
	 * handle enter PIN of mag-stripe card
	 * @return
     */
	protected int handleMAGPin(){

		PinType type = new PinType();
		type.setOnline(true);
		type.setCardNO(Pan);
		type.setAmount(String.valueOf(Amount));
		PinInfo info = transInterface.getPinpadPin(type);
		PinResult result = info.getResult() ;
		if(result != PinResult.SUCCESS){
			return info.getErrno();
		}
		byte[] pin = info.getPinblock() ;
		if(pin == null){
			isPinExist = false ;
		}else {
			isPinExist = true ;
			PIN = ISOUtil.hexString(pin);
		}
		return 0 ;
	}

	/**
	 * prepare online
	 */
	protected void prepareOnline(){
		transInterface.handling(Tcode.CONNECTING_CENTER);
		setSpecialDatas();
		int retVal ;
		if(cfg.isOnline()){

			retVal = OnlineTrans();

		}else {

			retVal = LocalPresentations();

		}
		clearPan();
		if(retVal!=0){
			transInterface.showError(retVal);
			return;
		}
		int code = 0 ;
		String additionalInfo = null ;
		switch (TransEName){
			case Type.SALE :
				code = Tcode.SALE_SUCCESS ;
				additionalInfo = PAYUtils.getStrAmount(Amount) ;
				break;
			case Type.VOID :
				code = Tcode.VOID_SUCCESS ;
				additionalInfo = PAYUtils.getStrAmount(Amount) ;
				data.setVoided(true);
				TransLog.getInstance().updateTransLog(
						TransLog.getInstance().getCurrentIndex(data),data);
				break;
			case Type.ENQUIRY :
				code = Tcode.ENQUIRY_SUCCESS ;
				additionalInfo = PAYUtils.getStrAmount(Amount) ;
				break;
			case Type.QUICKPASS :
				code = Tcode.QUICKPASS_SUCCESS ;
				additionalInfo = PAYUtils.getStrAmount(Amount) ;
				break;
			case Type.REFUND :
				code = Tcode.REFUND_SUCCESS ;
				additionalInfo = PAYUtils.getStrAmount(Amount) ;
				data.setVoided(true);
				TransLog.getInstance().updateTransLog(
						TransLog.getInstance().getCurrentIndex(data),data);
				break;
			case Type.PREAUTH :
				code = Tcode.PREAUTH_SUCCESS ;
				additionalInfo = PAYUtils.getStrAmount(Amount) ;
				break;
			case Type.PREAUTHVOID :
				code = Tcode.PREAUTH_VOID_SUCCESS ;
				additionalInfo = PAYUtils.getStrAmount(Amount) ;
				data.setVoided(true);
				TransLog.getInstance().updateTransLog(
						TransLog.getInstance().getCurrentIndex(data),data);
				break;
			case Type.PREAUTHCOMPLETE :
				data.setPreComp(true);
				TransLog.getInstance().updateTransLog(
						TransLog.getInstance().getCurrentIndex(data),data);
				code = Tcode.PREAUTH_COMPLETE_SUCCESS ;
				additionalInfo = PAYUtils.getStrAmount(Amount) ;
				break;
			case Type.PREAUTHCOMPLETEVOID :
				data.setVoided(true);
				TransLog.getInstance().updateTransLog(
						TransLog.getInstance().getCurrentIndex(data),data);
				code = Tcode.COMPLETE_VOID_SUCCESS ;
				additionalInfo = PAYUtils.getStrAmount(Amount) ;
				break;
		}
		transInterface.trannSuccess(code , additionalInfo);
	}

	/**
	 * PBOC listener
	 */
	private final PBOCListener listener = new PBOCListener() {
		@Override
		public int dispMsg(int i, String s, int i1) {
			return 0;
		}

		@Override
		public int callbackSelApp(String[] strings) {
			return transInterface.choseAppList(strings);
		}

		@Override
		public int callbackCardNo(String s) {
			Pan = s ;
			return transInterface.confirmCardNO(s);
		}

		@Override
		public PBOCPin callbackEnterPIN(PBOCPinType pbocPinType) {
			PBOCPin pin = new PBOCPin();
			PinType type = new PinType();
			type.setAmount(String.valueOf(Amount));
			type.setCardNO(Pan);
			type.setOnline(pbocPinType.isOnlinePin());
			type.setCounts(pbocPinType.getOfflinePinCounts());
			type.setPinKey(pbocPinType.getPinKey());
			type.setType(pbocPinType.isPlainPin()?0:1);
			PinInfo info = transInterface.getPinpadPin(type);
			PinResult result = info.getResult() ;
			if(result == PinResult.SUCCESS){
				pin.setPbocPinRet(PBOCPinRet.SUCCESS);
			}if(result == PinResult.FAIL){
				pin.setPbocPinRet(PBOCPinRet.FAIL);
			}if(result == PinResult.NO_OPERATION){
				pin.setPbocPinRet(PBOCPinRet.NO_OPERATION);
				pin.setPinKeyIndex(cfg.getMasterKeyIndex());
			}
			pin.setPinBlock(info.getPinblock());
			pin.setErrno(info.getErrno());
			return pin;
		}

		@Override
		public int callbackVerifyCert(int i, String s) {
			return transInterface.confirmCardVerifyCert(s);
		}

		@Override
		public void pbocBeforeGPO() {
			transInterface.beforeGPO();
		}
	};

	/**
	 * Online transaction
     * @return
     */
	protected int OnlineTrans() {
		int retVal = 0 ;
		setFields();
		if(isProcPreTrans){
			retVal = preTrans();
			if(retVal != 0){
				return retVal ;
			}
		}

		transInterface.handling(Tcode.CONNECTING_CENTER);
        if (connect() != 0){
			return Tcode.SOCKET_FAIL ;
		}

		if (isReversal) {
			Logger.debug("FinanceTrans->OnlineTrans->save Reversal");
			TransLogData Reveral = setReveralData();
			TransLog.saveReversal(Reveral);
		}

		transInterface.handling(Tcode.SEND_DATA_CENTER);
		if (send() != 0){
			netWork.close();
			return Tcode.SEND_DATA_FAIL ;
		}

		//increase trace no
		if (isTraceNoInc) {
			cfg.incTraceNo();
		}

		transInterface.handling(Tcode.RECEIVE_CENtER_DATA);
		byte[] respData = receive();
		netWork.close();
		if (respData == null){
			return Tcode.RECEIVE_DATA_FAIL ;
		}

		retVal = iso8583.unPacketISO8583(respData);
		if(retVal!=0){

			if(retVal == Tcode.RECEIVE_MAC_ERROR && isReversal){
				TransLogData newR = TransLog.getReversal() ;
				newR.setRspCode("A0");
				TransLog.clearReveral();
				TransLog.saveReversal(newR);
			}
			return retVal ;

		}

		RspCode = iso8583.getfield(39);
		AuthCode = iso8583.getfield(38);
		String strICC = iso8583.getfield(55);
		if (!PAYUtils.isNullWithTrim(strICC)){
			ICCData = ISOUtil.str2bcd(strICC, false);
		}else{
			ICCData = null ;
		}
		if(!RSP_00_SUCCESS.equals(RspCode)){
			TransLog.clearReveral();
			return formatRsp(RspCode);
		}

		if(isNeedGAC2){
			retVal = genAC2Trans();
			if(retVal != PBOCode.PBOC_TRANS_SUCCESS){
				return retVal ;
			}
		}

		//send up issuer script
		TransLogData data = TransLog.getScriptResult();
		if (data != null) {
			ScriptTrans script = new ScriptTrans(context, Type.SENDSCRIPT);
			int ret = script.sendScriptResult(data);
			if (ret == 0) {
				TransLog.clearScriptResult();
			}
		}

		// save transaction log
		if (isSaveLog) {
			TransLogData logData = setLogData();
			transLog.saveLog(logData);
		}
		//clear reversal log after transaction succeed
		TransLog.clearReveral();

		if(TransEName.equals(Type.ENQUIRY)){
			String f54 = iso8583.getfield(54) ;
			if(!PAYUtils.isNullWithTrim(f54)){
				Amount = Long.parseLong(f54.substring(f54.indexOf('C')+1 , f54.length())) ;
			}else {
				return Tcode.RECEIVE_DATA_FAIL ;
			}
		}

		if(isNeedPrint){
			transInterface.handling(Tcode.PRINTING_RECEPT);
			PrintManager printManager = PrintManager.getmInstance(context , transInterface);
			do{
				retVal = printManager.print(transLog.getLastTransLog(), false);
			}while (retVal == Printer.PRINTER_STATUS_PAPER_LACK);
			if (retVal == Printer.PRINTER_OK) {
				retVal = 0 ;
			} else {
				retVal = Tcode.PRINT_FAIL;
			}
		}

		return retVal ;
	}

	/**
	 * deal with reversal
	 * @return
     */
	private int preTrans(){
		int retVal = 0 ;
		TransLogData revesalData = TransLog.getReversal();
		if (revesalData != null) {
			transInterface.handling(Tcode.SEND_REVERSAL);
			RevesalTrans revesal = new RevesalTrans(context, Type.REVERSAL);
			for (int i = 0; i < cfg.getReversalCount() ; i++) {
				retVal = revesal.sendRevesal();
				if(retVal == 0){
					//clear reversal log after reversal succeed
					TransLog.clearReveral();
					break;
				}
			}
			//reversal end
			Logger.debug("preTrans->sendRevesal:"+retVal);
			if(retVal == Tcode.SOCKET_FAIL || retVal == Tcode.SEND_DATA_FAIL){
				//error
				return retVal ;
			}else {
				if(retVal != 0){
					//reversal failed
					TransLog.clearReveral();
					return Tcode.REVERSAL_FAIL ;
				}
			}
		}
		return retVal ;
	}

	/**
	 * deal with GAC2
	 * @return
     */
	private int genAC2Trans(){
		PBOCOnlineResult result = new PBOCOnlineResult();
		result.setField39(RspCode.getBytes());
		result.setFiled38(AuthCode.getBytes());
		result.setField55(ICCData);
		result.setResultCode(PBOCOnlineResult.ONLINECODE.SUCCESS);
		int retVal = pbocManager.afterOnlineProc(result);
		Logger.debug("genAC2Trans->afterOnlineProc:"+retVal);

		//Issue script deal result
		int isResult = pbocManager.getISResult();
		if(isResult != EMVISRCode.NO_ISR){
			// save issue script result
			byte[] temp = new byte[256];
			int len = PAYUtils.pack_tags(PAYUtils.wISR_tags, temp);
			if (len > 0) {
				ICCData = new byte[len];
				System.arraycopy(temp, 0, ICCData, 0, len);
			} else{
				ICCData = null;
			}
			TransLogData scriptResult = setScriptData();
			TransLog.saveScriptResult(scriptResult);
		}

		if(retVal != PBOCode.PBOC_TRANS_SUCCESS){
			//IC card transaction failed, if return "00" in field 39,
			//update the field 39 as "06" in reversal data
			TransLogData revesalData = TransLog.getReversal();
			if(revesalData!=null){
				revesalData.setRspCode("06");
				TransLog.saveReversal(revesalData);
			}
		}

		return retVal ;
	}

	/**
	 * set transaction log data
	 * @return TransLog
	 */
	private TransLogData setLogData() {

		TransLogData LogData = new TransLogData();
		LogData.setPan(PAYUtils.getSecurityNum(Pan, 6, 4));
		LogData.setOprNo(cfg.getOprNo());
		LogData.setBatchNo(BatchNo);
		LogData.setEName(TransEName);
		LogData.setOnline(true);
        LogData.setTraceNo(iso8583.getfield(11));
        LogData.setLocalTime(iso8583.getfield(12));
        LogData.setLocalDate(PAYUtils.getYear() + iso8583.getfield(13));
        LogData.setExpDate(iso8583.getfield(14));
        LogData.setSettleDate(iso8583.getfield(15));
        LogData.setEntryMode(iso8583.getfield(22));
        LogData.setPanSeqNo(iso8583.getfield(23));
        LogData.setAcquirerID(iso8583.getfield(32));
        LogData.setRRN(iso8583.getfield(37));
        LogData.setAuthCode(iso8583.getfield(38));
        LogData.setRspCode(iso8583.getfield(39));
        LogData.setField44(iso8583.getfield(44));
        LogData.setCurrencyCode(iso8583.getfield(49));
		LogData.setICCData(ICCData);
		LogData.setMode(inputMode);
		if(TransEName.equals(Type.ENQUIRY)){

		 }
		else{

			LogData.setAmount(Long.parseLong(iso8583.getfield(4)));
			String field63 = iso8583.getfield(63);
			String IssuerName = field63.substring(0, 3);
			String ref = field63.substring(3, field63.length());
			LogData.setRefence(ref);
			LogData.setIssuerName(IssuerName);

		}
		return LogData;

	}

	/**
	 * set scan pay transaction log data
	 * @param code QR code
	 * @return
     */
	protected TransLogData setScanData(String code){
		TransLogData LogData = new TransLogData();
		LogData.setAmount(Amount);
		LogData.setPan(code);
		LogData.setOprNo(cfg.getOprNo());
		LogData.setBatchNo(BatchNo);
		LogData.setEName(TransEName);
		LogData.setICCData(ICCData);
		LogData.setMode(inputMode);
		LogData.setLocalDate(PAYUtils.getYMD());
		LogData.setTraceNo(TraceNo);
		LogData.setOnline(true);
		LogData.setLocalTime(PAYUtils.getHMS());
		LogData.setSettleDate(PAYUtils.getYMD());
		LogData.setAcquirerID("12345678");
		LogData.setRRN("170907084952");
		LogData.setAuthCode("084952");
		LogData.setRspCode("00");
		LogData.setField44("0425       0461       ");
		LogData.setCurrencyCode("156");
		return LogData;
	}

	/**
	 * offline transaction
	 * @param ec_amount
     * @return
     */
	protected int offlineAccept(String ec_amount){
		if (isSaveLog) {
			TransLogData LogData = new TransLogData();
			if(TransEName.equals(Type.EC_ENQUIRY)){
				LogData.setAmount(Long.parseLong(ec_amount));
			}else {
				LogData.setAmount(Amount);
			}
			LogData.setPan(PAYUtils.getSecurityNum(Pan, 6, 4));
			LogData.setOprNo(cfg.getOprNo());
			LogData.setEName(TransEName);
			LogData.setEntryMode(ISOUtil.padleft(inputMode + "", 2, '0')+"10");
			LogData.setTraceNo(cfg.getTraceNo());
			LogData.setBatchNo(cfg.getBatchNo());
			LogData.setLocalDate(PAYUtils.getYear() + PAYUtils.getLocalDate());
			LogData.setLocalTime(PAYUtils.getLocalTime());
			LogData.setOnline(false);
			LogData.setICCData(ICCData);
			LogData.setMode(inputMode);
			transLog.saveLog(LogData);
			if(isTraceNoInc){
				cfg.incTraceNo();
			}
		}
		if(isNeedPrint){
			transInterface.handling(Tcode.PRINTING_RECEPT);
			PrintManager print = PrintManager.getmInstance(context , transInterface);
			int retVal ;
			do{
				retVal = print.print(transLog.getLastTransLog(), false);
			}while (retVal == Printer.PRINTER_STATUS_PAPER_LACK);
			if (retVal == Printer.PRINTER_OK) {
				return 0 ;
			}else {
				return Tcode.PRINT_FAIL ;
			}
		}
		return 0 ;
	}

	/**
	 * set issue script data
	 * @return
     */
	private TransLogData setScriptData() {
		TransLogData LogData = new TransLogData();
		LogData.setPan(PAYUtils.getSecurityNum(Pan, 6, 4));
		LogData.setICCData(ICCData);
		LogData.setBatchNo(BatchNo);
        LogData.setAmount(Long.parseLong(iso8583.getfield(4)));
        LogData.setTraceNo(iso8583.getfield(11));
        LogData.setLocalTime(iso8583.getfield(12));
        LogData.setLocalDate(iso8583.getfield(13));
        LogData.setEntryMode(iso8583.getfield(22));
        LogData.setPanSeqNo(iso8583.getfield(23));
        LogData.setAcquirerID(iso8583.getfield(32));
        LogData.setRRN(iso8583.getfield(37));
        LogData.setAuthCode(iso8583.getfield(38));
        LogData.setCurrencyCode(iso8583.getfield(49));
		return LogData;
	}

	/**
	 * set reversal data
	 * @return
     */
	private TransLogData setReveralData() {
		TransLogData LogData = new TransLogData();
		LogData.setPan(Pan);
		LogData.setProcCode(ProcCode);
		LogData.setAmount(Amount);
		LogData.setTraceNo(TraceNo);
		LogData.setExpDate(ExpDate);
		LogData.setEntryMode(EntryMode);
		LogData.setPanSeqNo(PanSeqNo);
		LogData.setSvrCode(SvrCode);
		LogData.setAuthCode(AuthCode);
		LogData.setRspCode("98");
		LogData.setCurrencyCode(CurrencyCode);
        byte[] temp = new byte[156];
        if (inputMode == ServiceEntryMode.ICC || inputMode == ServiceEntryMode.NFC) {
            int len = PAYUtils.pack_tags(PAYUtils.reversal_tag, temp);
            if (len > 0) {
                ICCData = new byte[len];
                System.arraycopy(temp, len, ICCData, 0, len);
                LogData.setICCData(ICCData);
            } else {
				ICCData = null;
			}
        }
		LogData.setField60(Field60);
		return LogData;
	}

	/**
	 * this a demo function, only for offline transaction
	 * @return
     */
	private int LocalPresentations(){

		if (isSaveLog) {
			TransLogData LogData = new TransLogData();
			LogData.setAmount(Amount);
			LogData.setPan(PAYUtils.getSecurityNum(Pan, 6, 4));
			LogData.setOprNo(cfg.getOprNo());
			LogData.setEName(TransEName);
			LogData.setEntryMode(ISOUtil.padleft(inputMode + "", 2, '0')+"10");
			LogData.setTraceNo(cfg.getTraceNo());
			LogData.setBatchNo(cfg.getBatchNo());
			LogData.setLocalDate(PAYUtils.getYear() + PAYUtils.getLocalDate());
			LogData.setLocalTime(PAYUtils.getLocalTime());
			LogData.setAuthCode(PAYUtils.getLocalTime());
			LogData.setOnline(false);
			LogData.setICCData(ICCData);
			LogData.setMode(inputMode);
			transLog.saveLog(LogData);
			if(isTraceNoInc){
				cfg.incTraceNo();
			}
		}
		if(isNeedPrint){
			transInterface.handling(Tcode.PRINTING_RECEPT);
			PrintManager print = PrintManager.getmInstance(context , transInterface);
			int retVal ;
			do{
				retVal = print.print(transLog.getLastTransLog(), false);
			}while (retVal == Printer.PRINTER_STATUS_PAPER_LACK);
			if (retVal == Printer.PRINTER_OK) {
				return 0 ;
			}else {
				return Tcode.PRINT_FAIL ;
			}
		}

		return 0 ;
	}

	/**
	 * clear card data,such as card number,track data,IC card data.
	 */
	protected void clearPan() {
		Pan = null;
		Track2 = null;
		Track3 = null;
		ICCData = null ;
		System.gc();
	}
}
