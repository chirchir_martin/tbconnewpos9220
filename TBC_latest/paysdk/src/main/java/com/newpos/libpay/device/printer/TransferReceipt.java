package com.newpos.libpay.device.printer;

import com.newpos.libpay.PaySdk;

/**
 * Created by Chirchir on 7/31/2018.
 */

public class TransferReceipt extends Receipt {
    String accountNo;
    String currencyname;

    public boolean iswallettransfer() {
        return iswallettransfer;
    }

    public void setIswallettransfer(boolean iswallettransfer) {
        this.iswallettransfer = iswallettransfer;
    }

    boolean iswallettransfer;
    public String getAccountto() {
        return accountto;
    }

    public void setAccountto(String accountto) {
        this.accountto = accountto;
    }

    public String getAccounttoname() {
        return accounttoname;
    }

    public void setAccounttoname(String accounttoname) {
        this.accounttoname = accounttoname;
    }

    String accountto;
    String accounttoname;
    String deviceid;
    String agentId;
    String operationType;
    String transactionId;
    Double transcharge;
    String amount;
    String currencyId;
    String date;
    String custName;
    String narration;
    String trfNo;

    public String getAccountfrom() {
        return accountfrom;
    }

    public void setAccountfrom(String accountfrom) {
        this.accountfrom = accountfrom;
    }

    String accountfrom;
    String agentname;
    public String getAuthentication() {
        return authentication;
    }

    public void setAuthentication(String authentication) {
        this.authentication = authentication;
    }

    String authentication;

    public String getAccountNo() {
        return accountNo;
    }

    public void setAccountNo(String accountNo) {
        this.accountNo = accountNo;
    }

    public String getCurrencyname() {
        return currencyname;
    }

    public void setCurrencyname(String currencyname) {
        this.currencyname = currencyname;
    }

    public String getDeviceid() {
        return deviceid;
    }

    public void setDeviceid(String deviceid) {
        this.deviceid = deviceid;
    }

    public String getAgentId() {
        return agentId;
    }

    public void setAgentId(String agentId) {
        this.agentId = agentId;
    }

    public String getOperationType() {
        return operationType;
    }

    public void setOperationType(String operationType) {
        this.operationType = operationType;
    }

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public Double getTranscharge() {
        return transcharge;
    }

    public void setTranscharge(Double transcharge) {
        this.transcharge = transcharge;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getCurrencyId() {
        return currencyId;
    }

    public void setCurrencyId(String currencyId) {
        this.currencyId = currencyId;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getCustName() {
        return custName;
    }

    public void setCustName(String custName) {
        this.custName = custName;
    }

    public String getNarration() {
        return narration;
    }

    public void setNarration(String narration) {
        this.narration = narration;
    }

    public String getTrfNo() {
        return trfNo;
    }

    public void setTrfNo(String trfNo) {
        this.trfNo = trfNo;
    }

    public String getAgentname() {
        return agentname;
    }

    public void setAgentname(String agentname) {
        this.agentname = agentname;
    }

    public String getBranch_name() {
        return branch_name;
    }

    public void setBranch_name(String branch_name) {
        this.branch_name = branch_name;
    }

    String branch_name;

    public String getAuthname() {
        return authname;
    }

    public void setAuthname(String authname) {
        this.authname = authname;
    }

    String authname;
}
