package com.newpos.libpay.trans.translog;

import com.newpos.libpay.trans.finace.ServiceEntryMode;

import java.io.Serializable;

/**
 * transaction log data
 * @author zhouqiang
 */
public class TransLogData implements Serializable {

	/**
	 * Issuer name, from field 63
	 */
	private String IssuerName;
	public String getIssuerName() {
		return IssuerName;
	}

	public void setIssuerName(String issuerName) {
		IssuerName = issuerName;
	}

	/**
	 * online transaction or offline transaction
	 */
	private boolean isOnline ;
	public boolean isOnline() {
		return isOnline;
	}

	public void setOnline(boolean AAC) {
		this.isOnline = AAC;
	}

	/**
	 * service entry mode
	 */
	private ServiceEntryMode mode ;
	public ServiceEntryMode getMode(){
		return mode ;
	}
	public void setMode(ServiceEntryMode mode){
		this.mode = mode ;
	}

	/*
	default value:   0
	send up succeed: 1
	send up failed:  2
	 */
	private int RecState;

	/** tip amount **/
	private long TipAmout = 0;
	/** batch number **/
	private String BatchNo;

	public String getBatchNo() {
		return BatchNo;
	}

	public void setBatchNo(String batchNo) {
		BatchNo = batchNo;
	}

	/**
	 * whether transaction was voided
	 */
	private boolean isVoided;
	public boolean getIsVoided(){
		return isVoided ;
	}

	public void setVoided(boolean isVoided){
		this.isVoided = isVoided ;
	}

	/**
	 * whether complete pre-auth
	 */
	private boolean isPreComp;
	public boolean isPreComp() {
		return isPreComp;
	}

	public void setPreComp(boolean preComp) {
		isPreComp = preComp;
	}

	public int getRecState() {
		return RecState;
	}

	public void setRecState(int recState) {
		RecState = recState;
	}

	/**
	 * transaction english name
	 * refer to @{@link com.newpos.libpay.trans.Type}
	 */
	private String TransEName;
	public String getEName() {
		return TransEName;
	}

	public void setEName(String eName) {
		TransEName = eName;
	}

	/**
	 * field 60 of transaction
	 */
	private String Field60;
	public String getField60() {
		return Field60;
	}

	public void setField60(String field60) {
		Field60 = field60;
	}

	/**
	 * card number, from field 2
	 */
	private String Pan;
	public String getPan() {
		return Pan;
	}

	public void setPan(String pan) {
		Pan = pan;
	}

	/**
	 * transaction amount, field 4
	 */
	private Long Amount;
	public Long getAmount() {
		return Amount;
	}

	public void setAmount(Long amount) {
		Amount = amount;
	}

	/**
	 * IC card data of transaction,from field 55
	 */
	private byte[] ICCData;
	public byte[] getICCData() {
		return ICCData;
	}

	public void setICCData(byte[] iCCData) {
		ICCData = iCCData;
	}

	/**
	 * voucher number, field 11
	 */
	private String TraceNo;
	public String getTraceNo() {
		return TraceNo;
	}

	public void setTraceNo(String traceNo) {
		TraceNo = traceNo;
	}

	/**
	 * transaction time, from field 12
	 */
	private String LocalTime;
	public String getLocalTime() {
		return LocalTime;
	}

	public void setLocalTime(String localTime) {
		LocalTime = localTime;
	}

	/**
	 * transaction date, from field 13
	 */
	private String LocalDate;
	public String getLocalDate() {
		return LocalDate;
	}

	public void setLocalDate(String localDate) {
		LocalDate = localDate;
	}

	/**
	 * card expire date, from field 14
	 */
	private String ExpDate;
	public String getExpDate() {
		return ExpDate;
	}

	public void setExpDate(String expDate) {
		ExpDate = expDate;
	}

	/**
	 * settlement date, from field 15
	 */
	private String SettleDate;
	public String getSettleDate() {
		return SettleDate;
	}

	public void setSettleDate(String settleDate) {
		SettleDate = settleDate;
	}

	/**
	 * service entry mode, from field 22
	 */
	private String EntryMode;
	public String getEntryMode() {
		return EntryMode;
	}

	public void setEntryMode(String entryMode) {
		EntryMode = entryMode;
	}

	/**
	 * Card serial number, from field 23
	 */
	private String PanSeqNo;
	public String getPanSeqNo() {
		return PanSeqNo;
	}

	/**
	 * Acquiring Institution Identification Code,from field 32
	 */
	private String AcquirerID;
	public void setPanSeqNo(String panSeqNo) {
		PanSeqNo = panSeqNo;
	}

	public String getAcquirerID() {
		return AcquirerID;
	}

	public void setAcquirerID(String acquirerID) {
		AcquirerID = acquirerID;
	}

	/**
	 * Retrieval Reference Number, from field 37
	 */
	private String RRN;
	public String getRRN() {
		return RRN;
	}

	public void setRRN(String rRN) {
		RRN = rRN;
	}

	/**
	 * Authorization Identification Response Code,from field 38
	 */
	private String AuthCode;
	public String getAuthCode() {
		return AuthCode;
	}

	public void setAuthCode(String authCode) {
		AuthCode = authCode;
	}

	/**
	 * Response Code, field 39
	 */
	private String RspCode;
	public String getRspCode() {
		return RspCode;
	}

	public void setRspCode(String rspCode) {
		RspCode = rspCode;
	}

	/**
	 * Additional Response Data, from field 44
	 */
	private String Field44;
	public String getField44() {
		return Field44;
	}

	public void setField44(String field44) {
		Field44 = field44;
	}

	/**
	 * Transaction Processing Code, from field 3
	 */
	private String ProcCode;
	public String getProcCode() {
		return ProcCode;
	}

	public void setProcCode(String procCode) {
		ProcCode = procCode;
	}

	/**
	 * Currency Code Of Transaction, from field 49
	 */
	private String CurrencyCode;
	public String getCurrencyCode() {
		return CurrencyCode;
	}

	public void setCurrencyCode(String currencyCode) {
		CurrencyCode = currencyCode;
	}

	/**
	 * data start with third byte of field 63
	 */
	private String Refence;
	public String getRefence() {
		return Refence;
	}

	public void setRefence(String refence) {
		Refence = refence;
	}

	/**
	 * terminal operator NO
	 */
	private int oprNo;
	public int getOprNo() {
		return oprNo;
	}

	public void setOprNo(int oprNo) {
		this.oprNo = oprNo;
	}

	/**
	 * Point Of Service Condition Mode,from field 25
	 */
	private String SvrCode;
	public String getSvrCode() {
		return SvrCode;
	}

	public void setSvrCode(String svrCode) {
		SvrCode = svrCode;
	}
}
