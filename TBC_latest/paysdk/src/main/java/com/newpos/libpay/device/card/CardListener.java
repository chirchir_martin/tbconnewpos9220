package com.newpos.libpay.device.card;

/**
 * Created by zhouqiang on 2017/3/14.
 * @author zhouqiang
 * card listener
 */

public interface CardListener {
    public void callback(CardInfo cardInfo);
}

