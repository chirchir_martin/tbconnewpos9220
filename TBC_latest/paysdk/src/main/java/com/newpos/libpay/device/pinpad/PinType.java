package com.newpos.libpay.device.pinpad;

import com.pos.device.ped.RsaPinKey;

/**
 * Created by zhouqiang on 2017/12/12.
 */

public class PinType {
    private boolean isOnline ;

    /**
     * card number
     */
    private String cardNO ;

    /**
     * transaction amount
     */
    private String amount ;


    /**
     * re-enter counts of offline PIN
     */
    private int counts ;

    /**
     * offline PIN type
     * 0-----clear text
     * 1-----cipher text
     */
    private int type ;

    /**
     * the key of offline cipher pin
     */
    private RsaPinKey pinKey ;

    public boolean isOnline() {
        return isOnline;
    }

    public void setOnline(boolean online) {
        isOnline = online;
    }

    public String getCardNO() {
        return cardNO;
    }

    public void setCardNO(String cardNO) {
        this.cardNO = cardNO;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public int getCounts() {
        return counts;
    }

    public void setCounts(int counts) {
        this.counts = counts;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public RsaPinKey getPinKey() {
        return pinKey;
    }

    public void setPinKey(RsaPinKey pinKey) {
        this.pinKey = pinKey;
    }
}
