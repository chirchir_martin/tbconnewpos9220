package com.newpos.libpay;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.android.newpos.libemv.PBOCManager;
import com.android.newpos.libemv.PBOCPin;
import com.newpos.libpay.device.card.CardManager;
import com.newpos.libpay.device.printer.BillPayReceipt;
import com.newpos.libpay.device.printer.DepositReceipt;
import com.newpos.libpay.device.printer.InquiryReceipt;
import com.newpos.libpay.device.printer.PrintManager;
import com.newpos.libpay.device.printer.Receipt;
import com.newpos.libpay.device.printer.RequestReceipt;
import com.newpos.libpay.device.printer.TBCTransactionReceipt;
import com.newpos.libpay.device.printer.TransferReceipt;
import com.newpos.libpay.device.printer.UtilitiesReceipt;
import com.newpos.libpay.device.printer.WithdrawalReceipt;
import com.newpos.libpay.device.printer.Xreport;
import com.newpos.libpay.global.TMConfig;
import com.newpos.libpay.global.TMConstants;
import com.newpos.libpay.paras.EmvAidInfo;
import com.newpos.libpay.paras.EmvCapkInfo;
import com.newpos.libpay.presenter.TransImplement;
import com.newpos.libpay.presenter.TransInterface;
import com.newpos.libpay.presenter.TransPresenter;
import com.newpos.libpay.presenter.TransView;
import com.newpos.libpay.trans.Type;
import com.newpos.libpay.trans.finace.auth.PreAuth;
import com.newpos.libpay.trans.finace.auth.PreAuthComplete;
import com.newpos.libpay.trans.finace.auth.PreAuthCompleteVoid;
import com.newpos.libpay.trans.finace.auth.PreAuthVoid;
import com.newpos.libpay.trans.finace.ecquery.ECEnquiryTrans;
import com.newpos.libpay.trans.finace.query.EnquiryTrans;
import com.newpos.libpay.trans.finace.quickpass.QuickPassTrans;
import com.newpos.libpay.trans.finace.refund.RefundTrans;
import com.newpos.libpay.trans.finace.revocation.VoidTrans;
import com.newpos.libpay.trans.finace.sale.SaleTrans;
import com.newpos.libpay.trans.finace.scan.ScanRefund;
import com.newpos.libpay.trans.finace.scan.ScanSale;
import com.newpos.libpay.trans.finace.scan.ScanVoid;
import com.newpos.libpay.trans.finace.transfer.TransferTrans;
import com.newpos.libpay.trans.manager.down.DparaTrans;
import com.newpos.libpay.trans.manager.logon.LogonTrans;
import com.newpos.libpay.trans.manager.settle.SettleTrans;
import com.newpos.libpay.utils.PAYUtils;
import com.newpos.libpay.utils.PrintHandler;
import com.pos.device.SDKManager;
import com.pos.device.SDKManagerCallback;

/**
 * Created by zhouqiang on 2017/4/25.
 * @author zhouqiang
 * pay SDK
 */
public class PaySdk {

    /**
     * Singleton design pattern
     */
    private static PaySdk mInstance = null ;

    /**
     * Context
     */
    private Context mContext = null ;

    private Activity mActivity = null ;

    /**
     * MVP mode
     */
    private TransPresenter presenter = null ;

    /**
     * Identificate whether initialized pay sdk
     */
    private static boolean isInit = false ;

    /**
     * the callback function of PaySdk
     */
    private PaySdkListener mListener = null ;

    /**
     * PaySdk's resource file
     * @link @{@link String}
     */
    private String cacheFilePath = null ;

    /**
     * terminal parameters file path
     * @link @{@link String}
     */
    private String paraFilepath = null ;

    public Context getContext() throws PaySdkException {
        if(this.mContext == null){
            throw new PaySdkException(PaySdkException.PARA_NULL);
        }
        return mContext ;
    }

    public PaySdk setActivity(Activity activity){
        this.mActivity = activity ;
        return mInstance ;
    }

    public PaySdk setParaFilePath(String path){
        this.paraFilepath = path ;
        return mInstance ;
    }

    public String getParaFilepath(){
        return this.paraFilepath ;
    }

    public PaySdk setCacheFilePath(String path){
        this.cacheFilePath = path ;
        return mInstance ;
    }

    public String getCacheFilePath(){
        return this.cacheFilePath ;
    }

    private PaySdk(){}

    public PaySdk setListener(PaySdkListener listener){
        this.mListener = listener ;
        return mInstance ;
    }

    public static PaySdk getInstance(){
        if(mInstance == null){
        }
        mInstance = new PaySdk();
        return mInstance ;
    }

    public void init(Context context) throws PaySdkException {
        this.mContext = context ;
        this.init();
    }

    public void init(Context context , PaySdkListener listener) throws PaySdkException {
        this.mContext = context ;
        this.mListener = listener ;
        this.init();
    }

    public void init() throws PaySdkException {
        System.out.println("init->start.....");
        if(this.mContext == null){
            throw new PaySdkException(PaySdkException.PARA_NULL);
        }

        if(this.paraFilepath == null || !this.paraFilepath.endsWith("properties")){
            this.paraFilepath = TMConstants.DEFAULTCONFIG ;
        }

        if(this.cacheFilePath == null){
            this.cacheFilePath = mContext.getFilesDir() + "/" ;
        }else if(!this.cacheFilePath.endsWith("/")){
            this.cacheFilePath += "/" ;
        }

        TMConfig.setRootFilePath(this.cacheFilePath);
        System.out.println("init->paras files path:" + this.paraFilepath);
        System.out.println("init->cache files will be saved in:" + this.cacheFilePath);
        System.out.println("init->pay sdk will run based on:" + (TMConfig.getInstance().getBankid()==1?"UNIONPAY":"CITICPAY") );
        if(!TMConfig.getInstance().isOnline()){
            PAYUtils.copyAssetsToData(this.mContext , EmvAidInfo.FILENAME);
            PAYUtils.copyAssetsToData(this.mContext , EmvCapkInfo.FILENAME);
        }
        SDKManager.init(mContext, new SDKManagerCallback() {
            @Override
            public void onFinish() {
                isInit = true ;
                System.out.println("init->success");
                if(mListener!=null){
                    mListener.success();
                }
            }
        });
    }

    /**
     * release card manager resources
     */
    public void releaseCard(){
        if(isInit){
            CardManager.getInstance(0).releaseAll();
        }
    }
    /**
     * release sdk resources
     */
    public void exit(){
        if(isInit){
            SDKManager.release();
            isInit = false ;
        }
    }

    public void startTrans(String transType , TransView tv) throws PaySdkException {
       Log.d("transactionType",transType);
        if(this.mActivity == null){
            throw new PaySdkException(PaySdkException.PARA_NULL);
        }

        TransInterface impl = new TransImplement(mActivity , tv);
        if(transType.equals(Type.CARDWITHDARWAL)){
           presenter = new SaleTrans(this.mContext , Type.SALE , impl);
        }

        if(transType.equals(Type.SCANSALE)){
            presenter = new ScanSale(this.mContext , Type.SCANSALE , impl);
        }

        if(transType.equals(Type.SCANVOID)){
            presenter = new ScanVoid(this.mContext , Type.SCANVOID , impl);
        }

        if(transType.equals(Type.SCANREFUND)){
            presenter = new ScanRefund(this.mContext , Type.SCANREFUND , impl);
        }

        if(transType.equals(Type.DOWNPARA)){
            presenter = new DparaTrans(this.mContext , Type.DOWNPARA , impl);
        }

        if(transType.equals(Type.LOGON)){
            presenter = new LogonTrans(this.mContext , Type.LOGON , impl);
        }

        if(transType.equals(Type.ENQUIRY)){
            presenter = new EnquiryTrans(this.mContext , Type.ENQUIRY , impl);
        }

        if(transType.equals(Type.VOID)){
            presenter = new VoidTrans(this.mContext , Type.VOID , impl);
        }

        if(transType.equals(Type.EC_ENQUIRY)){
            presenter = new ECEnquiryTrans(this.mContext , Type.EC_ENQUIRY , impl);
        }

        if(transType.equals(Type.QUICKPASS)){
            presenter = new QuickPassTrans(this.mContext , Type.QUICKPASS , impl);
        }

        if(transType.equals(Type.PREAUTH)){
            presenter = new PreAuth(this.mContext , Type.PREAUTH , impl);
        }

        if(transType.equals(Type.PREAUTHCOMPLETE)){
            presenter = new PreAuthComplete(this.mContext , Type.PREAUTHCOMPLETE , impl);
        }

        if(transType.equals(Type.PREAUTHCOMPLETEVOID)){
            presenter = new PreAuthCompleteVoid(this.mContext , Type.PREAUTHCOMPLETEVOID , impl);
        }

        if(transType.equals(Type.PREAUTHVOID)){
            presenter = new PreAuthVoid(this.mContext , Type.PREAUTHVOID , impl);
        }

        if(transType.equals(Type.SETTLE)){
            presenter = new SettleTrans(this.mContext , Type.SETTLE , impl);
        }

        if(transType.equals(Type.REFUND)){
            presenter = new RefundTrans(this.mContext , Type.REFUND , impl);
        }

       /* if(transType.equals(Type.TRANSFER)){
            presenter = new TransferTrans(this.mContext , Type.TRANSFER , impl);
        }*/

        if(isInit){

            new Thread(){
                @Override
                public void run(){
                    presenter.start();
                }
            }.start();

        }else {

            throw new PaySdkException(PaySdkException.NOT_INIT);

        }
    }
     public int printReceipt(Receipt  receipt,  ReceiptType type,String owner,String receiptName){
         int ret=0;
         PrintHandler mHandler= new PrintHandler(mContext);
         PrintManager printManager= PrintManager.getmInstance(mContext,mHandler);
          switch (type){
              case withdrawal:
                  WithdrawalReceipt withdraw=(WithdrawalReceipt) receipt;
                   printManager.printCustomerCashWithdrawalReceipt(mContext,withdraw,owner,receiptName);
                  break;
              case deposit:
                  DepositReceipt deposit=(DepositReceipt) receipt;
                  printManager.printCustomerCashDepositReceipt(mContext,deposit,owner,receiptName);
                  break;
              case  billpayment:
                   UtilitiesReceipt billPayReceipt=(UtilitiesReceipt) receipt;
                    printManager.printCustomerBillReceipt(mContext,billPayReceipt,owner,receiptName);
                  break;
              case  transfer:
                  TransferReceipt transferReceipt=(TransferReceipt) receipt;
                  printManager.printTransferReceipt(mContext,transferReceipt,owner,receiptName);
                  break;
              case inquiries:
                  InquiryReceipt  inquiryReceipt=(InquiryReceipt) receipt;
                   printManager.printInquriesReceipt(mContext,inquiryReceipt,owner,receiptName);
                  break;
              case request:
                  RequestReceipt requestReceipt=(RequestReceipt) receipt;
                  printManager.printRequestReceipt(mContext,requestReceipt,owner,receiptName);
                  break;
              case tbc_txn:
                  TBCTransactionReceipt transReceipt=(TBCTransactionReceipt) receipt;
                  printManager.printTBCReceipt(mContext,transReceipt);
                  break;
              case tbc_xreport:
                  Xreport XReport=(Xreport) receipt;
                  printManager.printXreport(mContext,XReport);
                  break;

          }

           return ret;
     }
     public enum ReceiptType{
         withdrawal,
         deposit,
         billpayment,
         transfer,
         inquiries,
         request,
         tbc_txn,
         tbc_xreport;


     }
}
