package com.newpos.libpay.device.printer;

/**
 * Created by Chirchir on 7/30/2018.
 */

public class Receipt {
    String agentComission;
    String bankIncome;
    String exerciseDuty;
    String transdetails;
    public String getTransdetails() {
        return transdetails;
    }

    public void setTransdetails(String transdetails) {
        this.transdetails = transdetails;
    }

    public String getAgentComission() {
        return agentComission;
    }

    public void setAgentComission(String agentComission) {
        this.agentComission = agentComission;
    }

    public String getBankIncome() {
        return bankIncome;
    }

    public void setBankIncome(String bankIncome) {
        this.bankIncome = bankIncome;
    }

    public String getExerciseDuty() {
        return exerciseDuty;
    }

    public void setExerciseDuty(String exerciseDuty) {
        this.exerciseDuty = exerciseDuty;
    }

}
