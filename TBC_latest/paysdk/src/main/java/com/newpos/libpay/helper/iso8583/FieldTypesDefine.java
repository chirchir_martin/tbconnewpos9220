package com.newpos.libpay.helper.iso8583;

/**
 * field data and length attr
 * @author
 */
public class FieldTypesDefine {
	//data attr
	public static final int FIELDATTR_TYPE_N = 0; 		// N
	public static final int FIELDATTR_TYPE_CN = 1; 		// CN
	public static final int FIELDATTR_TYPE_BIN = 2; 	// Bin
	public static final int FIELDATTR_TYPE_ASCII = 3; 	// ASCII

	//length attr
	public static final int FIELDATTR_LEN_TYPE_NO=0;
	public static final int FIELDATTR_LEN_TYPE_LL=1;
	public static final int FIELDATTR_LEN_TYPE_LLL=2;

}
