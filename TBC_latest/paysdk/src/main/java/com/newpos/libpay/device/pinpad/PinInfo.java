package com.newpos.libpay.device.pinpad;

/**
 * Created by zhouqiang on 2017/3/17.
 * @author zhouqiang
 * enter PIN result Info
 */

public class PinInfo {
    /**
     * enter PIN result
     * @ {@link PinResult}
     */
    private PinResult result ;

    /**
     * error code
     */
    private int errno ;

    /**
     * PIN block
     */
    private byte[] pinblock ;

    public PinResult getResult() {
        return result;
    }

    public void setResult(PinResult result) {
        this.result = result;
    }

    public int getErrno() {
        return errno;
    }

    public void setErrno(int errno) {
        this.errno = errno;
    }

    public byte[] getPinblock() {
        return pinblock;
    }

    public void setPinblock(byte[] pinblock) {
        this.pinblock = pinblock;
    }
}
