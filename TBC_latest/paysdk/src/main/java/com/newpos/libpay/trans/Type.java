package com.newpos.libpay.trans;

/**
 * Created by zhouqiang on 2017/12/11.
 * @author zhouqiang
 * transaction type
 */
public class Type {
    /**
     * LOGON(terminal sign in)
     */
    public static final String LOGON = "LOGON" ;

    /**
     * LOGOUT(terminal sign out)
     */
    public static final String LOGOUT = "LOGOUT" ;

    /**
     * DOWNLOAD PARAMETERS(AIDs,CAPKs,card BINs...)
     */
    public static final String DOWNPARA = "DOWNPARA" ;

    /**
     * QUERY_EMV_CAPK, query CAPK from server
     */
    public static final String QUERY_EMV_CAPK = "QUERY_EMV_CAPK" ;

    /**
     * download CAPK from server
     */
    public static final String DOWNLOAD_EMV_CAPK = "DOWNLOAD_EMV_CAPK" ;

    /**
     * notice server end of download CAPK
     */
    public static final String DOWNLOAD_EMV_CAPK_END = "DOWNLOAD_EMV_CAPK_END" ;

    /**
     * query AID from server
     */
    public static final String QUERY_EMV_PARAM = "QUERY_EMV_PARAM" ;

    /**
     * download AID from server
     */
    public static final String DOWNLOAD_EMV_PARAM = "DOWNLOAD_EMV_PARAM" ;

    /**
     * notice server end of download AID
     */
    public static final String DOWNLOAD_EMV_PARAM_END = "DOWNLOAD_EMV_PARAM_END" ;

    /**
     * SALE
     */
    public static final String SALE = "SALE" ;

    /**
     * query balance
     */
    public static final String ENQUIRY = "ENQUIRY" ;

    /**
     * sale void
     */
    public static final String VOID = "VOID" ;

    /**
     * query EC balance
     */
    public static final String EC_ENQUIRY = "EC_ENQUIRY" ;

    /**
     * Quick Pass sale
     */
    public static final String QUICKPASS = "QUICKPASS" ;

    /**
     * refund
     */
    public static final String REFUND = "REFUND" ;

    /**
     * pre-auth
     */
    public static final String PREAUTH = "PREAUTH" ;

    /**
     * void pre-auth
     */
    public static final String PREAUTHVOID = "PREAUTHVOID" ;

    /**
     * complete pre-auth
     */
    public static final String PREAUTHCOMPLETE = "PREAUTHCOMPLETE" ;

    /**
     * void complete pre-auth
     */
    public static final String PREAUTHCOMPLETEVOID = "PREAUTHCOMPLETEVOID" ;

    /**
     * Transfer accounts
     */
    public static final String INTRA_BANK= "INTRA-BANK";
    public static final String INTER_BANK = "INTER-BANK" ;
    public static final String PREPAID_CARD = "PREPAID-CARD" ;
    public static final String WALLET_TRANSFERS = "WALLET-TRANSFERS" ;
    /**
     * settlement
     */
    public static final String SETTLE = "SETTLE" ;

    /**
     * send up all Transaction details
     */
    public static final String UPSEND = "UPSEND" ;

    /**
     * REVERSAL
     */
    public static final String REVERSAL = "REVERSAL" ;

    /**
     * SEND ISSUER SCRIPT
     */
    public static final String SENDSCRIPT = "SENDSCRIPT" ;

    /**
     * scan pay
     */
    public static final String SCANSALE = "SCANSALE" ;

    /**
     * void scan pay
     */
    public static final String SCANVOID = "SCANVOID" ;

    /**
     * refund scan pay
     */
    public static final String SCANREFUND = "SCANREFUND" ;
    /**
     * CASHDEPOSITS(deposit money into the account)
     */
    public static final String CASHDEPOSITS= "CASHDEPOSIT" ;
    /**
     * LOANREPAYMENT(repayment of loans)
     */
    public static final String LOANREPAYMENT = "LOANREPAYMENT" ;
    /**
     * INSTALLMENTCOLLECTIONS(installments collections)
     */
    public static final String INSTALLMENTCOLLECTIONS= "INSTALLMENTCOLLECTIONS" ;
    /**
     * INTRABANK(Transfer of money between accounts in a single bank)
     */
    public static final String INTRABANK = "INTRABANK" ;
    /**
     * INTERBANK(Transfer of money between accounts in different banks)
     */
    public static final String INTERBANK = "INTERBANK" ;
    /**
     * WALLETTRANSFERS(Transfer of funds beteween wallets)
     */
    public static final String WALLETTRANSFERS = "WALLETTRANSFERS" ;
    /**
     * PREPAIDCARD(Transfer of money between acccount and prepaid cards)
     */
    public static final String PREPAIDCARD = "PREPAIDCARD" ;
    /**
     * MINISTATEMENT(Full statement request)
     */
    public static final String MINISTATEMENT = "MINISTATEMENT" ;

    /**
     * BALANCE( query balance)
     */
    public static final String BALANCE = "BALANCE" ;
    /**
     * CARDWITHDRAWAL(withdrwal of funds with ATM card)
     */
    public static final String CARDWITHDRAWAL_ATM = "ATMCARD" ;
    /**
     * CARDWITHDRAWAL(withdrwal of funds with ATM card)
     */
    public static final String CARDWITHDRAWAL_FP = "FPCARD" ;


    /**
     * CARDLESSWITHDARWAL( withdrawal of funds without card)
     */
    public static final String CARDLESSWITHDARWAL = "CASHWITHDRAWAL" ;
    /**
     * CARDLESSWITHDARWAL( withdrawal of funds without card)
     */
    public static final String CARDWITHDARWAL = "WITHCARD" ;
    /**
     * APPSETTING( appsetting)
     */
    public static final String APPSETTINGS = "APPSETTINGS" ;
    /**
     * OTHERS(to be specified)
     */
    public static final String OTHERS = "OTHERS" ;
    /**
     * OTHERS(to pay hospital bills)
     */
    public static final String HOSPITALBILLS = "HOSPITALBILLS" ;
    /**
     * SCHOOLFEES(to pay school fees)
     */
    public static final String SCHOOLFEES = "XXSCHOOLFEES" ;
    /**
     * UTILLTIES(to pay  utilities)
     */
    public static final String UTILLTIES = "UTILITIES" ;
    /**
     * UTILLTIES(to  request for full statement)
     */
    public static final String FULLSTATEMENT = "FULLSTATEMENT" ;
    /**
     * UTILLTIES(to request for cards)
     */
    public static final String CARDREQUEST = "CARDREQUEST" ;
    /**
     * LOANREQUEST(to request for loans)
     */
    public static final String LOANREQUEST = "LOANREQUEST" ;
    /**
     * REPORTS(to view reports)
     */
    public static final String REPORTS = "REPORTS" ;
    /**
     * ACCOUNT(to initiate account)
     */
    public static final String ACCOUNTINITIATION = "ACCOUNTINITIATION" ;
    /**
     * BILL(NWSC)
     */
    public static final String NWSC = "NWSC" ;
    /**
     * BILL(URA)
     */
    public static final String URA = "URA" ;
    /**
     * BILL(URA)
     */
    public static final String UMEME = "UMEME" ;
}
