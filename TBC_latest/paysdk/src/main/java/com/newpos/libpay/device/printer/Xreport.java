package com.newpos.libpay.device.printer;

/**
 * Created by Chirchir on 4/27/2019.
 */

public class Xreport extends Receipt {
    private String netsalse;
    private String txncount;
    private String username;
    private String devicemac;
    private String time;
    private String date;
    private String sales;
    private String netsales;
    private String voidamount;
    private String voidcount;

    public String getNetsalse() {
        return netsalse;
    }

    public void setNetsalse(String netsalse) {
        this.netsalse = netsalse;
    }

    public String getTxncount() {
        return txncount;
    }

    public void setTxncount(String txncount) {
        this.txncount = txncount;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String unsername) {
        this.username = unsername;
    }

    public String getDevicemac() {
        return devicemac;
    }

    public void setDevicemac(String devicemac) {
        this.devicemac = devicemac;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getSales() {
        return sales;
    }

    public void setSales(String sales) {
        this.sales = sales;
    }

    public String getNetsales() {
        return netsales;
    }

    public void setNetsales(String netsales) {
        this.netsales = netsales;
    }

    public String getVoidamount() {
        return voidamount;
    }

    public void setVoidamount(String voidamount) {
        this.voidamount = voidamount;
    }

    public String getVoidcount() {
        return voidcount;
    }

    public void setVoidcount(String voidcount) {
        this.voidcount = voidcount;
    }

}
