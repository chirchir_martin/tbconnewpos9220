package com.newpos.libpay.presenter;

import com.android.desert.keyboard.InputManager;
import com.newpos.libpay.device.pinpad.PinInfo;
import com.newpos.libpay.device.pinpad.PinType;
import com.newpos.libpay.device.user.OnUserResultListener;
import com.newpos.libpay.trans.translog.TransLogData;

/**
 * Created by zhouqiang on 2017/4/25.
 * User UI
 * @author zhouqiang
 */

public interface TransView {
    /**
     * show search card UI
     * @param timeout : s
     * @param mode search card mode,please refer to:@{@link com.newpos.libpay.device.card.CardManager}
     */
    void showCardView(int timeout, int mode);

    /**
     * show scan QR UI
     * @param timeout : s
     * @param mode scan QR mode, refer to:@{@link com.android.desert.keyboard.InputManager.Style}
     *             if use bank card, please refer to:@{@link TransView}{@link #showCardView(int, int)}
     */
    void showQRCView(int timeout, InputManager.Style mode);

    /**
     * show card number UI
     * @param timeout : s
     * @param pan : card number
     * @param l User confirm or cancel callback function, refer to:@{@link OnUserResultListener}
     */
    void showCardNo(int timeout, String pan, OnUserResultListener l);

    /**
     * show enter parameters UI
     * @param timeout :s
     * @param mode :enter mode @{@link com.android.desert.keyboard.InputManager.Mode}
     * @param l User confirm or cancel callback function @{@link OnUserResultListener}
     */
    void showInputView(int timeout, InputManager.Mode mode, OnUserResultListener l);

    /**
     *  get enter information
     * @param type enter mode @{@link com.android.desert.keyboard.InputManager.Mode}
     * @return enter information
     */
    String getInput(InputManager.Mode type);

    /**
     * show the detail information of transaction
     * @param timeout timeout
     * @param data the detail information of transaction @{@link TransLogData}
     * @param l User confirm or cancel callback function @{@link OnUserResultListener}
     */
    void showTransInfoView(int timeout, TransLogData data, OnUserResultListener l);

    /**
     * show the identity information of card
     * @param timeout  timeout
     * @param info the identity information of card
     * @param l User confirm or cancel callback function @{@link OnUserResultListener}
     */
    void showCardVerifyCertView(int timeout, String info, OnUserResultListener l);

    /**
     * show and select card applications
     * @param timeout timeout
     * @param apps application list
     * @param l User confirm or cancel callback function @{@link OnUserResultListener}
     * @return select index
     */
    int showCardAppListView(int timeout, String[] apps, OnUserResultListener l);

    /**
     * show enter PIN
     * @param timeout timeout
     * @param type the type of PIN
     * @return @{@link PinInfo}
     */
    PinInfo showEnterPinView(int timeout, PinType type, OnUserResultListener listener);

    /**
     *  通知UI显示卡片多语言选择
     * @param timeout 超时时间
     * @param l 需要上层通过此接口给底层回调用户行为 详见@{@link OnUserResultListener}
     */
    void handleBeforceGPO(int timeout);

    /**
     *  show transaction success information
     * @param timeout timeout
     * @param info the detail information of transaction result
     */
    void showSuccess(int timeout, String info);

    /**
     *  show transaction failed information
     * @param timeout timeout
     * @param err the detail information of transaction result
     */
    void showError(int timeout, String err);

    /**
     * show transaction information
     * @param timeout timeout
     * @param status the detail information of transaction result
     */
    void showMsgInfo(int timeout, String status);
}
