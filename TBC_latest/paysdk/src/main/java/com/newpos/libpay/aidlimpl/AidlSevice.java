package com.newpos.libpay.aidlimpl;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.support.annotation.Nullable;

/**
 * Created by zhouqiang on 2017/12/9.
 */

public class AidlSevice extends Service {

    /**
     * AidlBinder object
     */
    private AidlBinder aidlBinder = null ;

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        aidlBinder = new AidlBinder() ;
        return aidlBinder ;
    }

    @Override
    public void onCreate() {
        super.onCreate();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }
}
