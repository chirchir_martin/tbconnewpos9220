package com.newpos.libpay.presenter;

import com.android.desert.keyboard.InputInfo;
import com.android.desert.keyboard.InputManager;
import com.newpos.libpay.device.card.CardInfo;
import com.newpos.libpay.device.pinpad.PinInfo;
import com.newpos.libpay.device.pinpad.PinType;
import com.newpos.libpay.device.pinpad.PinpadListener;
import com.newpos.libpay.device.scanner.QRCInfo;
import com.newpos.libpay.trans.translog.TransLogData;

/**
 * Created by zhouqiang on 2017/3/15.
 * @author zhouqiang
 * define MODEL interface
 */

public interface TransInterface {
    /**
     * get input data from user.
     * such as amount,voucher NO,password and so on.
     * @param type @{@link com.android.desert.keyboard.InputManager.Mode}
     * @return InputInfo @{@link InputInfo}
     */
    InputInfo getInput(InputManager.Mode type);

    /**
     * detect card,get card info
     * @param mode refer to @{@link com.newpos.libpay.device.card.CardType}
     * @return CardInfo @{@link CardInfo}
     * @attention deal with card, refer to @{@link com.newpos.libpay.device.card.CardManager}
     */
    CardInfo getCard(int mode);

    /**
     * get QR code information
     * @param mode @{@link com.android.desert.keyboard.InputManager.Style}
     * @return QRCInfo @{@link QRCInfo}
     * @attention deal with scanner, refer to @{@link com.newpos.libpay.device.scanner.ScannerManager}
     */
    QRCInfo getQRCInfo(InputManager.Style mode);

    /**
     * get PIN from PIN PAD
     * @param type refer to @{@link PinType}
     * @return PinInfo @{@link PinInfo}
     * @attention  deal with PIN pad, refer to@{@link com.newpos.libpay.device.pinpad.PinpadManager}
     * @attention  @{@link com.newpos.libpay.device.pinpad.PinpadManager#getPin(int, PinType, PinpadListener)}
     */
    PinInfo getPinpadPin(PinType type);

    /**
     * notice user confirm card number
     * @param cn card number
     * @return 0:user confirm  others:user cancel
     */
    int confirmCardNO(String cn);

    /**
     * show and select card app
     * @param list card app list
     * @return card app index
     * @attention index start with 0
     */
    int choseAppList(String[] list);

    /**
     * before GPO of EMV process.
     * this callback to the user's purpose is to take into account a lot of special EMV process requirements
     */
    void beforeGPO();

    /**
     * show transaction status
     * @param status Trans Status
     */
    void handling(int status);

    /**
     * 人机交互显示接口(确认原交易信息)notice user confirm transaction information
     * @param logData refer to @{@link TransLogData}
     * @return 0:user confirm  others:user cancel
     */
    int confirmTransInfo(TransLogData logData);

    /**
     * check card identity information
     * @param info card identity information
     * @return 0:user confirm  others:user cancel
     */
    int confirmCardVerifyCert(String info);

    /**
     * show transaction succeed
     * @param code transaction code @{@link com.newpos.libpay.trans.Tcode}
     * @param args addition parameters
     */
    void trannSuccess(int code, String... args);

    /**
     * show error information
     * @param errcode refer to @{@link com.newpos.libpay.trans.Tcode}
     */
    void showError(int errcode);
}
