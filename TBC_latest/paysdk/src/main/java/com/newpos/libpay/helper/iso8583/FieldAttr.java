package com.newpos.libpay.helper.iso8583;

/**
 * field attr
 * @author
 */
public class FieldAttr {
	private int lenType; // field length type: BCD ASCII BIN
	private int lenAttr; // field length attr: no LL LLL
	private int dataType; // field data type: ASCII (N CN)BCD BIN
	private int dataLen; // field data length

	public int getLenType() {
		return lenType;
	}

	public void setLenType(int lenType) {
		this.lenType = lenType;
	}

	public int getLenAttr() {
		return lenAttr;
	}

	public void setLenAttr(int lenAttr) {
		this.lenAttr = lenAttr;
	}

	public int getDataType() {
		return dataType;
	}

	public void setDataType(int dataType) {
		this.dataType = dataType;
	}

	public int getDataLen() {
		return dataLen;
	}

	public void setDataLen(int dataLen) {
		this.dataLen = dataLen;
	}

}
