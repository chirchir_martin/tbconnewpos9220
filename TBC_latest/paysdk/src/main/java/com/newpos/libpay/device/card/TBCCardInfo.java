package com.newpos.libpay.device.card;

/**
 * Created by Chirchir on 4/12/2019.
 */

public class TBCCardInfo extends CardInfo {

    public boolean cardstatus;
    public String cardData;

    public String getCardData() {
        return cardData;
    }

    public void setCardData(String cardData) {
        this.cardData = cardData;
    }

    public boolean isCardstatus() {
        return cardstatus;
    }

    public void setCardstatus(boolean cardstatus) {
        this.cardstatus = cardstatus;
    }

}
