package com.newpos.libpay.device.printer;

import java.util.Locale;

/**
 * Created by zhouqiang on 2017/4/4.
 * 打印常量类
 * @author zhouqiang
 */

public class PrintRes {
    public interface CH{
        boolean zh = Locale.getDefault().getLanguage().equals("zh");
        public static final String WANNING = zh?"警告:该固件是测试版本，不能用于商业用途，在此版本上进行交易可能危害到持卡人的用卡安全。" : "Warning:Debug firmware,use for commercial forbidden,it will be hurt the benefit of cardholder through this version.";
        public static final String MERCHANT_COPY = zh?"商户存根     MERCHANT COPY" :"MERCHANT COPY";
        public static final String CARDHOLDER_COPY = zh?"持卡人存根     CARDHOLDER COPY" :"CARDHOLDER COPY";
        public static final String BANK_COPY = zh?"银行存根     BANK COPY" :"BANK COPY";
        public static final String MERCHANT_NAME = zh?"商户名称(MERCHANT NAME):" :"MERCHANT NAME:";
        public static final String MERCHANT_ID = zh?"商户编号(MERCHANT NO):" :"MERCHANT NO:";
        public static final String TERNIMAL_ID = zh?"终端编号(TERMINAL NO):" :"TERMINAL NO:";
        public static final String OPERATOR_NO = zh?"操作员号(OPERATOR NO):" :"OPERATOR NO";
        public static final String CARD_NO = zh?"卡号(CARD NO):" :"CARD NO:";
        public static final String SCANCODE = zh?"付款码(PayCode):" :"PayCode:" ;
        public static final String ISSUER = zh?"发卡行(ISSUER):  中信银行" :"ISSUER : China Bank";
        public static final String ISSUER2 = zh?"发卡行(ISSUER):" :"ISSUER:";
        public static final String ACQUIRER = zh?"收单行(ACQ):  银联商务" :"ACQ : Unionpay";
        public static final String ACQUIRER2 = zh?"收单行(ACQ):" :"ACQ:";
        public static final String TRANS_AAC = zh?"应用密文(AAC):" :"AAC:";
        public static final String TRANS_AAC_ARQC = zh?"联机交易" :"ARQC";
        public static final String TRANS_AAC_TC = zh?"脱机交易" :"TC";
        public static final String TRANS_TYPE = zh?"交易类型(TXN. TYPE):" :"TXN. TYPE :";
        public static final String CARD_EXPDATE = zh?"卡有效期(EXP. DATE):" :"EXP. DATE:";
        public static final String BATCH_NO = zh?"批次号(BATCH NO):" :"BATCH NO:";
        public static final String VOUCHER_NO = zh?"凭证号(VOUCHER NO):" :"VOUCHER NO:";
        public static final String AUTH_NO = zh?"授权码(AUTH NO):" :"AUTH NO:";
        public static final String DATE_TIME = zh?"日期/时间(DATE/TIME):" :"DATE/TIME:";
        public static final String REF_NO = zh?"交易参考号(REF. NO):" :"REF. NO:";
        public static final String AMOUNT = zh?"金额(AMOUNT):" :"AMOUNT:";
        public static final String EC_AMOUNT = zh?"电子现金余额(AMOUNT):" :"EC AMOUNT:";
        public static final String CARD_AMOUNT = zh?"卡余额(AMOUNT):" :"AMOUNT:";
        public static final String RMB = zh?"RMB:" :"$:";
        public static final String REFERENCE = zh?"备注/REFERENCE" :"REFERENCE";
        public static final String REPRINT = zh?"***** 重打印 *****" :"***** REPRINT *****";
        public static final String CARDHOLDER_SIGN = zh?"持卡人签名" :"CardHolder Signature";
        public static final String AGREE_TRANS = zh?"本人同意以上交易" :"I agree these transaction above";
        public static final String SETTLE_SUMMARY = zh?"结算总计单" :"Settle Sum Receipt";
        public static final String SETTLE_LIST = zh?"类型/TYPE      笔数/SUM      金额/AMOUNT" :"TYPE      SUM      AMOUNT";
        public static final String SETTLE_INNER_CARD = zh?"内卡：对账平" :"Inner card；Reconciliation";
        public static final String SETTLE_OUTER_CARD = zh?"外卡：对账平" :"Outer card:Reconciliation";
        public static final String SETTLE_DETAILS = zh?"结算明细单" :"Settle Detail Receipt";
        public static final String SETTLE_DETAILS_LIST_CH = zh?"凭证号   类型   授权码   金额   卡号" :"VOUCHER     TYPE     AUTHNO     AMOUNT    CARDNO";
        public static final String SETTLE_DETAILS_LIST_EN = zh?"VOUCHER     TYPE     AUTHNO     AMOUNT    CARDNO" :"VOUCHER     TYPE     AUTHNO     AMOUNT    CARDNO";
        public static final String DETAILS = zh?"交易明细" :"Transaction Details";
    }

    public static final String[] TRANSCH = {
        "余额查询",
        "消费",
        "消费撤销",
        "电子现金余额查询",
        "快速消费",
        "结算",
        "预授权",
        "预授权完成",
        "预授权完成撤销",
        "预授权撤销",
        "退货",
        "转账",
        "圈存",
        "圈提",
        "签到",
        "签退",
        "参数公钥下载",
        "扫码消费",
        "扫码撤销",
        "扫码退货"
    };

    public static final String[] TRANSEN = {
            "ENQUIRY",
            "SALE",
            "VOID",
            "EC_ENQUIRY",
            "QUICKPASS",
            "SETTLE",
            "PREAUTH",
            "PREAUTHCOMPLETE",
            "PREAUTHCOMPLETEVOID",
            "PREAUTHVOID",
            "REFUND",
            "TRANSFER",
            "CREFORLOAD",
            "DEBFORLOAD",
            "LOGON",
            "LOGOUT",
            "DOWNPARA",
            "SCANSALE",
            "SCANVOID",
            "SCANREFUND"
    };
}
